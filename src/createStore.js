import { createStore, applyMiddleware, compose } from "redux";
import ReduxThunk from 'redux-thunk';
import rootReducer from './reducers';
import translations from './translations';
import {
  setLocale,
  loadTranslations,
  syncTranslationWithStore,
} from "react-redux-i18n";
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(ReduxThunk)),
);

syncTranslationWithStore(store);
store.dispatch(loadTranslations(translations));
const getLocale = localStorage.getItem('social_network_language') || 'en-US';
store.dispatch(setLocale(getLocale));