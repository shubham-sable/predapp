import React ,{useEffect} from "react";

import { Modal, Button } from "react-bootstrap";

import AnsVotes from "./AnsVotes";
import CommentBox from "../../components/CommentBox";

import moment from "moment";
import { Translate } from "react-redux-i18n";

import './TakePart.css';
import { getCommentReplies } from "actions/TakePartActions";

const QuestionModal = ({ show,
   onHide, 
   quizData, 
   addVote,
   comments,
   commentPostHandle,
   commentLikeUnlikeHandle,
   handleDeleteComment,
   handleLoadReplies,
   handleGetCommentsOfCard,
   handleReRender,
   handleLoadReplyReplies, 
   handleEditComment,
   faCheck }) => {

  // const commentLikeUnlikeHandle = (postId, commentId, isReply, replyCommentId) => {
  //   console.log("CHALO",postId, commentId, isReply, replyCommentId);
  // }
useEffect (() => {

},[show,comments,CommentBox])

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      className={`quiz-modal`}
      data-test="check render"
      
    >
      {quizData &&
        <React.Fragment>
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              {quizData.card_type}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body className="p-0">
            <div className="position-sticky bg-white" style={{ top: 0, zIndex: 1 }} data-test="component render">
              <div className="p-3 banner-que" data-test="check card why">
                <h2>{quizData.card_why}</h2>
              </div>
              <div className="p-0 pl-3 pr-3 dates">
                <div className="start">
                  <strong>{moment(new Date(quizData.createdAt * 1000)).fromNow()}</strong>
                </div>
              </div>
              <div className="p-3 banner-que">
                <p>{quizData.card_description}</p>
              </div>
              <div className="p-0 pl-3 pb-3 pr-3 d-flex align-items-center justify-content-between">
                  <React.Fragment>

                  <AnsVotes votes={quizData.card_answers[0].total_vote} card={quizData} ans={quizData.card_answers[0].answer} isvoted={quizData.isvoted} answer_id={quizData.card_answers[0].id} addVote={addVote} qModel />
                  <AnsVotes votes={quizData.card_answers[1].total_vote} card={quizData} ans={quizData.card_answers[1].answer} isvoted={quizData.isvoted} answer_id={quizData.card_answers[1].id} addVote={addVote} qModel />

                  </React.Fragment>
              </div>
            </div>
            <div className="detailBox">
              <div className="p-0">
                <ul className="commentList">
                  {console.log("this is question modal data",comments,quizData.user,quizData.id)}
                  <CommentBox comments={comments} user={quizData.user} postId={quizData.id} commentPostHandle={commentPostHandle} commentLikeUnlikeHandle={commentLikeUnlikeHandle} handleDeleteComment={handleDeleteComment} handleLoadReplies={handleLoadReplies} handleLoadReplyReplies={handleLoadReplyReplies} handleEditComment={handleEditComment}/>
                </ul>
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button
              className="font-weight-bold border-0 rounded-0"
              type="submit"
              variant="primary"
              size="lg"
              onClick={onHide}
            >
              <Translate value="take_part.follow" />
            </Button>
          </Modal.Footer>
        </React.Fragment>
      }
    </Modal>
  )
};

export default QuestionModal;