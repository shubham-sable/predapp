import React, { Component,useEffect } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../actions/TakePartActions';
import { changeNavTitle } from '../../actions/common';

import {
  faEye,
  faGrinHearts,
  faInbox,
  faCheck,
  faPlus,
} from "@fortawesome/free-solid-svg-icons";
import {
  OverlayTrigger,
  Tooltip,
  Spinner,
} from "react-bootstrap";
import moment from "moment";
import { FloatingButton } from '../../components/Group/Group.style';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import DashboardBadge from "./DashboardBadge";
import QuestionModal from "./QuestionModal";
import AnsVotes from "./AnsVotes";
import CreateTakePartModal from '../../components/TakePart/CreateTakePartModal';

import InfiniteScroll from 'react-infinite-scroller';

import { Translate, I18n } from 'react-redux-i18n';

import './TakePart.css';
import { isIndexed } from 'immutable';
import { faTshirt } from '@fortawesome/fontawesome-free-solid';

class TakePart extends Component {

  constructor(props) {
    super(props);

    this.state = {
      queModalShow: false,
      questionIndex: undefined,
      showTakePartModal: false,
      showTitle:"",
      cardData:{},
      user:{
          id: 1,
          profileImg: "url",
          name: {
            "fname": "Shubham",
            "lname": "Sable"
        },
      }
    }
  }

  componentDidMount() {
    const { changeNavTitle, currentPage } = this.props;

    const titleSettings = {
      title: "sidebar.take_part",
      icon: '/assets/icons/take-part.png'
    }

    changeNavTitle(titleSettings);
    this.loadMoreDataAPI(currentPage);
    this.props.getTotalCommentsOfUser(this.state.user.id);
    this.props.getTotalVotesOfUser(this.state.user.id);
    this.props.loadAllQuizesAction(this.state.user.id, 0, 3);
    this.props.loadMyCards(this.state.user.id,0,3);

  }

  loadMoreDataAPI = (pageNo) => {
    
    const { location: { pathname }, loadAllQuizesAction,loadMyCards,getTotalVotesOfUser,getTotalCommentsOfUser, limit } = this.props;
    const type = pathname === '/take-part/available-cards' ? 'available' : 'my';
    
    
  }

  // componentWillUnmount() {
  //   this.props.setLoadingFlagTakePartAction();
  // }

  toggleQueModal = (index) => {
    console.log("show toggle button ",this.state.queModalShow);
    this.setState((prevState) => ({
      queModalShow: !prevState.queModalShow,
      questionIndex: index,
    }))
    // if(this.state.queModalShow===false){
    //   // console.log("check this out",this.props.availableCards[index]);
    //   // this.props.getCommentsofCard(this.props.availableCards[index].id,1);
    // }
  }

  handleTabChange = (tabName) => {
    if (tabName === 'available') {
      this.props.history.push("/take-part/available-cards");
    } else if (tabName === 'mine') {
      this.props.history.push("/take-part/my-cards");
    }
  }

  addVote = (card_id,answer_id,isVoted) => {
    console.log('vote added',card_id,answer_id,isVoted);
    this.props.addRemoveVoteAction(card_id, answer_id,isVoted,this.state.user.id);
  }

  commentPostHandle = (card_id, comment, isReply, commentId, replyId) =>{
    this.props.postCommentAction(card_id, comment, this.state.user, isReply, commentId, replyId);
  }
  
  commentLikeUnlikeHandle = (card_id, comment_id,isReply,replyCommentId,isliked) =>{
    console.log("comment like  came till takePart",card_id,comment_id,isliked)
    this.props.saveCommentLikeUnlike(card_id, comment_id, isReply, replyCommentId, isliked, this.state.user);
  }

  handleDeleteComment = (card_id,comment_id, isReply, parentId) => {
    console.log("there you go ",card_id,comment_id);
    this.props.deleteCommentAction(card_id, comment_id, isReply, parentId);
  }


  handleLoadReplies = (card_id,comment_id) => {
    this.props.getCommentReplies(card_id, comment_id, this.state.user.id);
  }

  handleGetCommentsOfCard = (index) =>{
    console.log("handle comment of card",index);
    this.props.getCommentsofCard(index,this.state.user.id);

  }

  handleLoadReplyReplies = (card_id,commentId,replyId) =>{
    this.props.postCommentAction(card_id, this.state.user,commentId, replyId);
  }

  handleEditComment = (card_id, commentId, isReply, parentId, comment) =>{
    console.log("look called");
    this.props.editCommentAction(card_id, commentId, isReply, parentId, comment);
  }

  toggleTakePartModal = () => {
    this.setState(prevState => ({
      showTitle:"CREATE CARD",
      cardData:{},
      showTakePartModal: !prevState.showTakePartModal
    }))

  };

  handleClick = (cardData) =>{
    this.setState({
      showTitle:"EDIT CARD",
      cardData:cardData,
      showTakePartModal: !this.state.showTakePartModal
    })

  };

  deleteCardAction = (card_id) => {
    this.props.deleteCard(card_id);
  }

  handleChangePageNumber = (pageNumber) => {
    if (pageNumber !== this.props.currentPage) {
      this.loadMoreDataAPI(pageNumber)
    }
  }

  render() {
    const { loading, currentPage, totalCards, limit, availableCards, myCards, location: { pathname } } = this.props;
    const { queModalShow, questionIndex, showTakePartModal } = this.state;

    const totalPages = Math.ceil(totalCards / limit);

    const activeTab = pathname === '/take-part/available-cards' ? 'available' : 'mine';

    const quizData = questionIndex || questionIndex === 0 ? activeTab  === 'available' ? availableCards[questionIndex] : activeTab  === 'mine' ? myCards[questionIndex] : undefined : undefined;

    return (
      <div style={{ height: 'calc(100% - 98px)' }} data-test='component-render'>
        <div className="container-fluid" style={{ padding: '30px 10px 0 10px' }}>
          <div className="row ml-0 mr-0">
            <DashboardBadge classAdd={`info`} addShadow={activeTab === 'available' ? 'shadow' : ''} icon={faInbox} name={I18n.t('take_part.available_cards')} counts={availableCards.length} onClick={() => this.handleTabChange('available')} />
            <DashboardBadge classAdd={`success`} addShadow={activeTab === 'mine' ? 'shadow' : ''} icon={faEye} name={I18n.t('take_part.my_cards')} counts={myCards.length} onClick={() => this.handleTabChange('mine')} />
            <DashboardBadge classAdd={`danger`} icon={faGrinHearts} name={I18n.t('take_part.received_votes')} counts={this.props.myTotalVotes} />
            <DashboardBadge classAdd={`warning`} icon={faEye} name={I18n.t('take_part.received_comments')} counts={this.props.myTotalComments} />
          </div>
          {loading ?
            <div className="d-flex justify-content-center pt-4" data-test='render when loading spinner' data-test="check when loading is true">
              <Spinner animation="border" role="status">
                <span className="sr-only">Loading...</span>
              </Spinner>
            </div>
            :
            <InfiniteScroll
              pageStart={currentPage}
              initialLoad={false}
              loadMore={this.handleChangePageNumber}
              hasMore={currentPage + 1 <= totalPages}
              // loader={
              //   <div className="d-flex justify-content-center pt-2 pb-2" key={0}>
              //     <Spinner animation="border" role="status">
              //       <span className="sr-only">Loading...</span>
              //     </Spinner>
              //   </div>
              // }
            >
              <div className="row ml-0 mr-0" data-test='render cards'>
                {
                  activeTab === 'available' ?
                    availableCards.map((quiz, index) => {
                      return (
                        <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12 p-1" key={`quiz-${quiz.id}-${index}`}>
                          <div className="tile w-auto available border" 
                          onClick={() =>{ 
                            {
                              this.handleGetCommentsOfCard(quiz.id);
                              this.toggleQueModal(index);
                            }
                            }
                            }>
                            <div className="wrapper">
                              <div className="header">{quiz.card_type}</div>
                              <div className="p-3 banner-que">
                                <h2>{quiz.card_why}</h2>
                              </div>
                              <div className="p-0 pl-3 pr-3 pb-2 dates">
                                <div className="start">
                                  <strong>{moment(new Date(quiz.createdAt)).format("MM.DD.YYYY")}</strong>
                                </div>
                              </div>
                              <div className="take-part-footer">
                                <img src={quiz.user.profileImg} alt={quiz.user.name.fname} /><span>{quiz.user.name.fname}</span><span>{quiz.user.name.lname}</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    })
                    :
                    activeTab === 'mine' &&
                    myCards.map((quiz, index) => {
                      return (
                        <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12" key={`quiz-${quiz.id}-${index}`}>
                          <div className="tile myCards border">
                          <button onClick={()=>this.handleClick(quiz)}>Edit</button><button onClick={()=>this.deleteCardAction(quiz.id)}>Delete</button>
                            <div className="wrapper" >
                              <div className="wrapper-header-content"
                              onClick={() =>{ 
                                {
                                  this.handleGetCommentsOfCard(quiz.id);
                                  this.toggleQueModal(index);
                                }
                                }
                                } >
                                <div className="header">{quiz.card_type}</div>
                                <div className="p-3 banner-que">
                                  <h2>{quiz.card_why}</h2>
                                </div>
                                <div className="p-0 pl-3 pr-3 pb-2 dates">
                                  <div className="start">
                                    <strong>{moment(new Date(quiz.createdAt)).format("MM.DD.YYYY")}</strong>
                                  </div>
                                </div>
                              </div>
                              <div className="take-part-footer d-flex align-items-center justify-content-between">
                                {quiz.isvoted ?
                                  <AnsVotes question={quiz.card_why} votes={quiz.card_answers[0].total_vote} card={quiz} ans={quiz.card_answers[0].answer} isvoted={quiz.isvoted} isAnswered={true}  answer_id={quiz.card_answers[0].id} faCheck={faCheck} addVote={this.addVote} />
                                  :
                                  <React.Fragment>
                                    <AnsVotes question={quiz.card_why} votes={quiz.card_answers[0].total_vote} card={quiz} ans={quiz.card_answers[0].answer} isvoted={quiz.isvoted} isAnswered={false}   answer_id={quiz.card_answers[0].id} addVote={this.addVote} />
                                    <AnsVotes question={quiz.card_why} votes={quiz.card_answers[1].total_vote} card={quiz} ans={quiz.card_answers[1].answer} isvoted={quiz.isvoted} isAnswered={false}   answer_id={quiz.card_answers[1].id} addVote={this.addVote} />
                                  </React.Fragment>
                                }
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    })
                }
              </div>
            </InfiniteScroll>
          }
        </div>
        {/* {console.log("ready to go long tour",quizData)} */}
        <QuestionModal
          show={queModalShow}
          onHide={() => this.toggleQueModal(undefined)}
          quizData={quizData}
          addVote={this.addVote}
          faCheck={faCheck}
          commentPostHandle={this.commentPostHandle}
          commentLikeUnlikeHandle={this.commentLikeUnlikeHandle}
          handleDeleteComment={this.handleDeleteComment}
          handleLoadReplies={this.handleLoadReplies}
          comments={this.props.comment}
          handleGetCommentsOfCard={this.handleGetCommentsOfCard}
          handleLoadReplyReplies={this.handleLoadReplyReplies}
          handleEditComment={this.handleEditComment}
        />
        <CreateTakePartModal show={showTakePartModal} onHide={this.toggleTakePartModal} showTitle={this.state.showTitle} cardData={this.state.cardData}/>
        <OverlayTrigger
          placement={"top"}
          overlay={
            <Tooltip className="font-size-14">
              <Translate value="take_part.create_card" />
            </Tooltip>
          }
        >
          <FloatingButton className="btn-primary" bottom={40} onClick={this.toggleTakePartModal} fontsize={25}>
            <FontAwesomeIcon icon={faPlus} />
          </FloatingButton>
        </OverlayTrigger>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  ...state.takePart,
  availableCards:state.takePart.availableCards
})

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators({ ...Actions, changeNavTitle }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TakePart)
