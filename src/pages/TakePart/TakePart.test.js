import React from 'react';
import Enzyme,{shallow} from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import { testStore , findByTestAtrr } from '../../../Utils';

import TakePart from './TakePart';

Enzyme.configure({ adapter: new EnzymeAdapter() });


const defaultProps={
    availableCards: [],
    myCards: [],
    loading: true,
    currentPage: 1,
    limit: 20,
    totalCards: 45,
    myTotalVotes:0,
    myTotalComments:0,
    comment:[],
    location:"",
}
const setup = (initialState,props={defaultProps,location: {pathname: '/take-part/available-cards'}}) => {
    const store = testStore(initialState);
    const wrapper = shallow(<TakePart store={store} {...props}/>).dive().dive();
    return wrapper;
  }


describe("render check test",()=>{

    // const wrapper=shallow(<TakePart store={defaultProps}/>)
    const wrapper=setup();
    test("check component render",()=>{
        const component = findByTestAtrr(wrapper,'component-render');
        expect(component.length).toBe(1);
    })


  })

describe("testing for loading ",()=>{
    const wrapper=setup();

    test("when loading is true",()=>{
        const component = findByTestAtrr(wrapper,'check when loading is true');
        console.log("testing",component.debug())
        expect(component.length).toBe(1);
    })

    test("when loading is false",()=>{
        const component = findByTestAtrr(wrapper,'check when loading is false');
        expect(component.length).toBe(0);
    })
})
// describe("when loading",()=>{

//     test("when loading is true,render spinner",()=>{
//         const wrapper=shallow(<TakePart data={loading=true}/>)
//         const component=findByTestAtrr(wrapper,'render when loading spinner');
//         expect(component).toBe(1);


//     })

//     test("when loading is false,show the actual content",()=>{
//         const wrapper=shallow(<TakePart data={loading=false}/>)
//         const component=findByTestAtrr(wrapper,'render cards');
//         expect(component).toBe(1);
//     })
// })

