import React from 'react';
import Enzyme,{shallow} from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import { testStore , findByTestAtrr } from '../../../Utils';
import QuestionModal from './QuestionModal';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const defaultProps=
    {
    "id": "9ff01d20-c5d8-4209-b55d-534b19ae18bc",
      "card_type": "General Survey",
      "card_why": "When do you prefer to do the Business Workshop?",
      "card_answers": [
        {
          "id": "cb5dabac-f510-4971-a08e-cb3e7708fecf",
          "answer": "Monday",
          "total_vote": 1,
          "user_vote": [
            {
              "id": "1",
              "profileImg": "url",
              "name": {
                "fname": "gam",
                "lname": "tem"
              }
            }
          ]
        },
        {
          "id": "37e66612-ba33-475f-9f10-c8198cd66eef",
          "answer": "Friday",
          "total_vote": 0,
          "user_vote": []
        }
      ],
      "card_description": "Being able to generate and identify more ideas helps companies grow more quickly, stay relevant, and drive new trends.",
      "card_img": "url",
      "card_person_dept": [
        {
          "id": "1",
          "dept_name": "IT Department"
        }
      ],
      "user": {
        "id": "1",
        "profileImg": "url",
        "name": {
          "fname": "tom",
          "lname": "sam"
        }
      },
      "isvoted": true,
    }
const setup = () => {
    const store = testStore(initialState);
    const wrapper = shallow(<QuestionModal store={store}/>).dive().dive();
    return wrapper;
  }


describe("render check test",()=>{

    // const wrapper=shallow(<TakePart store={defaultProps}/>)
    const wrapper=shallow(<QuestionModal quizData={defaultProps}/>)
    console.log("check testing",wrapper.debug())
    test("check component render",()=>{
        const component = findByTestAtrr(wrapper,'component render');
        expect(component.length).toBe(1);
    })
    test("check component render",()=>{
        const component = findByTestAtrr(wrapper,'check render');
        expect(component.length).toBe(1);
    })
    test("check for card why",()=>{
        const component=findByTestAtrr(wrapper,'check card why');
        console.log("hurary",component.text());
        expect(component.length).toBe(1);

    })
  })

  describe("check values of the rendere page",()=>{
    const wrapper=shallow(<QuestionModal quizData={defaultProps}/>)

      test("check if card question is correct ",()=>{
        const component=findByTestAtrr(wrapper,'check card why');
        console.log("hurary",component.text());
        expect(component.text()).toBe("When do you prefer to do the Business Workshop?"); 
      })
  })

