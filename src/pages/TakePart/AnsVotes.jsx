import React from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { OverlayTrigger, Tooltip } from "react-bootstrap";

const AnsVotes = ({ votes, card, ans, isvoted, addVote ,answer_id, faCheck, qModel}) => {
  return (
    <div className={`card w-100 ${ans === 'ansB' && 'ml-2'}`}>
      {console.log("check answers",ans)}
        <div className={`card-body w-100 p-0 d-flex align-items-center justify-content-between ${isvoted ? `border border-success` : ``}`}>
          <OverlayTrigger
            placement="top"
            overlay={
              <Tooltip id={`tooltip-top`} style={{ fontSize: 14 }}>
                {ans}
              </Tooltip>
            }
          >
            <div className="d-flex align-items-center w-100 h-100 pl-2 overflow-hidden">
              {ans}
            </div>
          </OverlayTrigger>
          {isvoted ?
            <div className="text-white bg-success p-2 votes d-flex align-items-center h-100">
              <FontAwesomeIcon icon={faCheck} />
              <p className="p-0 m-0 pl-1" ><span onClick={() => addVote(card.id,answer_id,isvoted)}>{votes}</span></p>
            </div>
            :
            <div className="d-flex align-items-center text-white bg-success p-2 votes h-100" onClick={() => addVote(card.id,answer_id,isvoted)}>
            {votes}
            </div>
          }
        </div>
      </div>
  )
}

export default AnsVotes
