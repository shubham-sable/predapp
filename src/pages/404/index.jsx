import React from 'react'

const Page404 = () => {
  return (
    <div className="d-flex justify-content-center align-items-center w-100 mt-4">
      <h2>404 Not Found</h2>
    </div>
  )
}

export default Page404;
