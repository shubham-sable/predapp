import React, { useEffect } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../actions/PeopleDirectoryActions';
import { changeNavTitle } from '../../actions/common';

import { Table, Image } from 'react-bootstrap';
import ReactPagination from '../../components/Pagination';

import { I18n } from 'react-redux-i18n';

const PeopleDirectory = (props) => {
  const {
    peopleData,
    currentPage,
    totalItems,
    limit,
    loading,
    // searchString,
    changeNavTitle,
    getOrganisationPeopleDataAction
  } = props;

  useEffect(() => {
    const titleSettings = {
      title: 'sidebar.people_directory',
      icon: '/assets/icons/people-directory.svg'
    }
    changeNavTitle(titleSettings);
  }, [changeNavTitle]);

  useEffect(() => {
    if (loading) {
      getOrganisationPeopleDataAction(currentPage, limit);
    }
  }, [getOrganisationPeopleDataAction, currentPage, limit, loading])

  const handleChangePageNumber = (pageNumber) => {
    if (pageNumber !== currentPage) {
      getOrganisationPeopleDataAction(pageNumber, limit);
    }
  }

  const handleChangeSearch = (e) => {
    getOrganisationPeopleDataAction(1, limit, e.target.value.toLowerCase());
  }

  return (
    <div className="w-75 mx-auto mt-4">
      <div className="border">
        <input
          placeholder={I18n.t('people_directory.search')}
          type="text"
          id="search"
          name="search"
          className="form-control border-0"
          // value={searchString}
          onKeyUp={handleChangeSearch}
        />
        <div className="p-0 text-body font-size-12">
          <Table className="m-0" striped hover responsive>
            <thead>
              <tr>
                <th>{I18n.t('people_directory.name')}</th>
                <th>{I18n.t('people_directory.job_title_department')}</th>
                <th>{I18n.t('people_directory.location_phone')}</th>
                <th>{I18n.t('people_directory.skills')}</th>
              </tr>
            </thead>
            <tbody>
              {peopleData.map((tmpdata) => {
                return (
                  <tr key={tmpdata.id}>
                    <td className="align-middle">
                      <Image
                        src={tmpdata.image}
                        className="overflow-hidden rounded-circle"
                        height={36}
                      />
                      <span className="pl-2">{tmpdata.name}</span>
                    </td>
                    <td className="align-middle">{tmpdata.jobtitle_dept}</td>
                    <td className="align-middle">{tmpdata.lacation_phone}</td>
                    <td className="align-middle">{tmpdata.skills}</td>
                  </tr>
                )
              })}
            </tbody>
          </Table>
        </div>
      </div>
      <ReactPagination
        className="mt-3 float-right"
        currentPage={currentPage}
        totalItems={totalItems}
        perPage={limit}
        onClickPageNumber={handleChangePageNumber}
      />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    ...state.peopleDirectory
  };
}

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators({ ...Actions, changeNavTitle }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PeopleDirectory);
