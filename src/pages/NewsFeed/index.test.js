import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import NewsFeedPage from './';
import { testStore , findByTestAtrr } from '../../../Utils';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (initialState={}) =>{
  const store = testStore(initialState);
  const wrapper = shallow(<NewsFeedPage store={store}/>).dive().dive();
  return wrapper;
}

test('render NewsFeedPage without error', ()=>{
  const wrapper = setup();
  console.log(wrapper.debug());
  expect(wrapper);
})

describe('InfiniteScroll testing', ()=>{
  const wrapper = setup();
  test('render InfiniteScroll without error', ()=>{
    expect(wrapper.find('InfiniteScroll').length).toBe(1);
  })
  test('render InfiniteScroll with prop initialLoad', ()=>{
    expect(wrapper.find('InfiniteScroll').prop('initialLoad')).toBe(false);
  })
  test('render InfiniteScroll with prop pageStart', ()=>{
    expect(wrapper.find('InfiniteScroll').prop('pageStart')).toBe(0);
  })
})

describe('List testing', ()=>{
  const wrapper = setup();
  test('render List without error', ()=>{
    expect(wrapper.find('List').length).toBe(1);
  })
  test('render List with itemLayout', ()=>{
    expect(wrapper.find('List').prop('itemLayout')).toBe('vertical');
  })
  test('render List with size', ()=>{
    expect(wrapper.find('List').prop('size')).toBe('large');
  })
  test('render List with dataSource without undefined', ()=>{
    expect(wrapper.find('List').prop('dataSource')).not.toBeUndefined();
  })
})

describe('List Item testing', ()=>{
  const wrapper = setup();
  test('render List Item without error', ()=>{
    expect(wrapper.find('Item').length).toBe(1);
  })
})
