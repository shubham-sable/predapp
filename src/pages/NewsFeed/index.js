import React, {useEffect} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../actions/PostActions';
import { changeNavTitle } from '../../actions/common';

import InfiniteScroll from 'react-infinite-scroller';

import {
  List,
  Col,
  Avatar
} from 'antd';

const NewsFeedPage = props =>{
  const {
    changeNavTitle,
  } = props;

  useEffect(()=>{
    const titleSettings = {
      title: 'sidebar.news_feed',
      icon: '/assets/icons/news-feed.svg'
    }
    changeNavTitle(titleSettings);
  },[changeNavTitle]);

  const handleInfiniteOnLoad = () =>{

  }

  return(
    <>
      <Col span={17} style={{ border: '2px solid' }}>
        <InfiniteScroll
          initialLoad={false}
          pageStart={0}
          loadMore={handleInfiniteOnLoad}
          useWindow={false}
        >
          <List
            itemLayout='vertical'
            size='large'
            loading={false}
            dataSource={'hi'}
          >
            <List.Item
              key={'1'}
              avatar={<Avatar src={} />}
            >
              Hi
            </List.Item>
          </List>
        </InfiniteScroll>
      </Col>
    </>
  )
}

// {
//   id: 2,
//   user: {
//     userId: 2,
//     userName: 'Avery Walters',
//     userProfile: 'https://randomuser.me/api/portraits/women/21.jpg',
//   },
//   isLiked: true,
//   createdAt: 1597739484,
//   group: {
//     groupId: 1,
//     groupName: 'Social Group',
//   },
//   media: [],
//   description: `guilty lately... 🤦🏽‍♀️
//   As most of you know, I’m in hard-core creation mode right now getting The Blog & Email List Bootcamp course created for its September launch (wait-list in bio!) and getting my podcast ready for its October launch.
//   And I can’t get anything (like actually, ANYTHING 😂) done when I’m traveling, whiiiiich means I’ll be at home in NYC until the very end of September.
//   And as ridiculous as that sounds, for at least 2 weeks out of every month for the last 2 years, I’ve been on the road creating content and sharing epic destinations on here every single week. I’ve been so used to traveling constantly that staying put for two entire months feels really, really weird.
//   And I feel weirdly guilty not being able to share new travels with you all.
//   That being said, I want to know from you - what do you want to see here over the next two months?
//   Caption-wise: have you been loving my latest captions? And photo-wise: Old travel photos? New photos from New York?`,
//   totalComments: 1,
//   comments: [
//     {
//       commentId: 6,
//       isLiked: false,
//       userId: 4,
//       userName: 'Megan Ramos',
//       userProfile: 'https://randomuser.me/api/portraits/women/26.jpg',
//       comment: 'This is NY? Wow looks like the Caribbean?',
//       createdAt: 1567661136,
//       replies: [
//         {
//           commentId: 2,
//           isLiked: false,
//           userId: 3,
//           userName: 'Cameron Clark',
//           userProfile: 'https://randomuser.me/api/portraits/men/58.jpg',
//           comment: 'How can i become an influencer?',
//           createdAt: 1567661136,
//         },
//         {
//           commentId: 3,
//           isLiked: false,
//           userId: 2,
//           userName: 'Avery Walters',
//           userProfile: 'https://randomuser.me/api/portraits/women/21.jpg',
//           comment: 'We would love to see a mix of throwbacks and NYC content!!! anything you post is great 😍',
//           createdAt: 1567661136,
//         },
//       ]
//     }
//   ]
// }

// <InfiniteScroll
//           initialLoad={false}
//           pageStart={0}
//           loadMore={this.handleInfiniteOnLoad}
//           hasMore={!this.state.loading && this.state.hasMore}
//           useWindow={false}
//         >
//           <List
//             dataSource={this.state.data}
//             renderItem={item => (
//               <List.Item key={item.id}>
//                 <List.Item.Meta
//                   avatar={
//                     <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
//                   }
//                   title={<a href="https://ant.design">{item.name.last}</a>}
//                   description={item.email}
//                 />
//                 <div>Content</div>
//               </List.Item>
//             )}
//           >
//             {this.state.loading && this.state.hasMore && (
//               <div className="demo-loading-container">
//                 <Spin />
//               </div>
//             )}
//           </List>
//         </InfiniteScroll>

const mapStateToProps = (state) => {
  return {
    ...state.posts
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators({ ...Actions, changeNavTitle }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsFeedPage);
