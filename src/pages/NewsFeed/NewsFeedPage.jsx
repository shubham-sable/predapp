import React, { useEffect, useState } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../actions/PostActions';
import { changeNavTitle } from '../../actions/common';

import PostEditor from './../../components/PostEditor';
import NewsFeedSidebar from '../../components/UpcomingEvents/NewsFeedSidebar';

import InfiniteScroll from 'react-infinite-scroller';
import { Spinner } from 'react-bootstrap';
import SinglePost from './../../components/PostsListing/SinglePost';
// import EditPostModal from './../../components/PostsListing/EditPostModal';

const NewsFeedPage = props => {
  const {
    loading,
    postsList,
    getAllPostsListAction,
    savePostLikeUnlike,
    getPostCommentAction,
    getCommentReplies,
    postNewPostAction,
    postCommentAction,
    getRepliesReply,
    saveCommentLikeUnlike,
    deletePostAction,
    deleteCommentAction,
    updateCommentAction,
    resetPostListAction,
    currentPage,
    totalPosts,
    limit,
    commentsLimit,
    loadMoreCommentsAction,
    changeNavTitle,
  } = props;

  var postEditInit = {
    "object": "value",
    "document": {
      "object": "document",
      "nodes": [
        {
          "object": "block",
          "type": "paragraph",
          "nodes": []
        }
      ]
    }
  }

  useEffect(() => {
    const titleSettings = {
      title: 'sidebar.news_feed',
      icon: '/assets/icons/news-feed.svg'
    }
    changeNavTitle(titleSettings);
    return () => {
      resetPostListAction();
    };
  }, [changeNavTitle, resetPostListAction]);

  useEffect(() => {
    if (loading) {
      getAllPostsListAction(1,currentPage, limit); //currentPage=1 limit=20
    }
  }, [getAllPostsListAction, currentPage, limit, loading]);

  var tempDate = new Date();

  const handlePostSubmit = (value) => {
    const user={
      id: "1",
      profileImg: "xyz",
      name:{
        fname: "Virat",
        lname: "Kohli"
      }
    }

    var selectedGroup= undefined;
    if(value.selectedGroup !== undefined){
      selectedGroup={
        id: value.selectedGroup.id,
        name: value.selectedGroup.title,
        profile: value.selectedGroup.imageUrl
      }
    }

    const values={
      coWorkers:[
        value.coWorkers.map((coWorker) =>{
          return {
            id:coWorker.id,
            name:{
              fname:coWorker.firstName,
              lname:coWorker.lastName
            },
            profile: "url"
          }
        })
      ],
      mediaFiles:[
        value.mediaFiles.map((media) =>{
          return {
            media_type: media.type,
            url: "url"
          }
        })
      ],
      postContent:value.postContent,
      selectedGroup: selectedGroup
    }
    postNewPostAction(user,values);
  };

  const handleChangePageNumber = (pageNumber) => {
    if (pageNumber !== currentPage) {
      // getAllPostsListAction(1,pageNumber, limit);
    }
  }

  const handleGetComments = (postId) => {
    getPostCommentAction(postId,1);
  }

  const handleLoadReplies = (postId,parentId) => {
    const userId = 1;
    getCommentReplies(postId, parentId, userId);
  }

  const postLikeUnlikeHandle = (postId,isLiked) => {
    const user={
      id: "1",
      profileImg: "xyz",
      name:{
        fname: "Virat",
        lname: "Kohli"
      }
    }
    savePostLikeUnlike(user,postId,isLiked);
  }

  const commentLikeUnlikeHandle = (postId, commentId, isReply, replyCommentId, isliked) => {
    const user={
      id: "1",
      profileImg: "xyz",
      name:{
        fname: "Virat",
        lname: "Kohli"
      }
    }
    saveCommentLikeUnlike(postId, commentId, isReply, replyCommentId, isliked, user);
  }

  const handleLoadMoreComments = (postId, pageNumber) => {
    loadMoreCommentsAction(postId, pageNumber, commentsLimit);
  }

  const handleLoadReplyReplies = (postId, commentId, replyId) => {
    const userId = 1;
    getRepliesReply(postId,commentId,replyId,userId);
  }

  const commentPostHandle = (postId, comment, isReply, commentId, replyId) =>{
    const user={
      id: "1",
      profileImg: "xyz",
      name:{
        fname: "Virat",
        lname: "Kohli"
      }
    }
    postCommentAction(postId, comment, user, isReply, commentId, replyId);
  }

  const handleDeletePost = (postId) =>{
    deletePostAction(postId);
  }

  const handleDeleteComment = (postId, commentId, isReply, parentId) =>{
    deleteCommentAction(postId,commentId,isReply,parentId);
  }

  const handleEditComment = (postId, commentId, isReply, parentId, comment) =>{
    updateCommentAction(postId, commentId, isReply, parentId, comment);
  }

  const totalPages = Math.ceil(totalPosts / limit);

  return (
    <div className="posts-listing-newsfeed">
      <div style={{ marginLeft: 10 }}>
        <PostEditor onSubmit={handlePostSubmit} initialValue={postEditInit} />
        {loading ?
          <div className="d-flex justify-content-center pt-4" style={{ width: 650 }}>
            <Spinner animation="border" role="status">
              <span className="sr-only">Loading...</span>
            </Spinner>
          </div>
          :
          <InfiniteScroll
            pageStart={currentPage}
            initialLoad={false}
            loadMore={handleChangePageNumber}
            hasMore={currentPage + 1 <= totalPages}
            loader={
              <div className="d-flex card__box--post justify-content-center pt-2 pb-2" key={0}>
                <Spinner animation="border" role="status">
                  <span className="sr-only">Loading...</span>
                </Spinner>
              </div>
            }
          >
            {postsList.map((post, index) => {
              return (
                <SinglePost key={`post-${post.id}-${index}`} data={post} curUserId={1}
                commentsLimit={commentsLimit} postLikeUnlikeHandle={postLikeUnlikeHandle}
                commentLikeUnlikeHandle={commentLikeUnlikeHandle}
                handleLoadMoreComments={handleLoadMoreComments}
                commentPostHandle={commentPostHandle}
                handleGetComments={handleGetComments}
                handleLoadReplies={handleLoadReplies}
                handleLoadReplyReplies={handleLoadReplyReplies}
                handleDeletePost={handleDeletePost}
                handleDeleteComment={handleDeleteComment}
                handleEditComment={handleEditComment}
                />
              );
            })}
          </InfiniteScroll>
        }
      </div>
      <NewsFeedSidebar />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    ...state.posts
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators({ ...Actions, changeNavTitle }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsFeedPage);
