import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import NewsFeedPage from './NewsFeedPage';
import { testStore , findByTestAtrr } from '../../../Utils';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (initialState={}) =>{
  const store = testStore(initialState);
  const wrapper = shallow(<NewsFeedPage store={store}/>).dive().dive();
  return wrapper;
}

test('render NewsFeedPage without error', ()=>{
  const wrapper = setup();
  expect(wrapper);
})

test('render NewsFeedPage with loading value true', ()=>{
  const wrapper = setup(
    {
      posts:{
        loading:true
      }
    })
  // console.log(wrapper.debug())
  expect(wrapper);
})

test('render NewsFeedPage with loading value false', ()=>{
  const wrapper = setup(
    {
      posts:{
        loading:false,
        postsList: []
      }
    })
  // console.log(wrapper.debug())
  expect(wrapper);
})

describe('PostEditor testing',()=>{
  const wrapper = setup(
    {
      posts:{
        loading:true
      }
    })

  test('render PostEditor without error', ()=>{
    expect(wrapper.find('PostEditor').length).toBe(1);
  })
  test('render PostEditor with prop initialValue', ()=>{
    expect(wrapper.find('PostEditor').prop('initialValue')).not.toBeUndefined();
  })
})

describe('Spinner testing while loading true', ()=>{
  const wrapper = setup(
    {
      posts:{
        loading:true
      }
  })
  test('render Spinner without error', ()=>{
    expect(wrapper.find('Spinner').length).toBe(1);
  })
  test('render Spinner with prop animation', ()=>{
    expect(wrapper.find('Spinner').prop('animation')).toBe('border');
  })
  test('render Spinner with prop role', ()=>{
    expect(wrapper.find('Spinner').prop('role')).toBe('status');
  })
  test('render Spinner with text Loading...', ()=>{
    expect(wrapper.find('Spinner').text()).toBe('Loading...');
  })
})

test('Not to render Spinner while loading is false', ()=>{
  const wrapper = setup(
    {
      posts:{
        loading:false,
        postsList: []
      }
  })
  expect(wrapper.find('Spinner').length).toBe(0);
})

describe('InfiniteScroll testing', ()=>{
  const wrapper = setup(
    {
      posts:{
        loading:false,
        postsList: [],
        currentPage: 1
      }
  })
  test('render InfiniteScroll without error', ()=>{
    expect(wrapper.find('InfiniteScroll').length).toBe(1);
  })
  test('render InfiniteScroll with prop pageStart', ()=>{
    expect(wrapper.find('InfiniteScroll').prop('pageStart')).toBe(1);
  })
  test('render InfiniteScroll with prop initialLoad', ()=>{
    expect(wrapper.find('InfiniteScroll').prop('initialLoad')).toBe(false);
  })
  test('render InfiniteScroll with loader', ()=>{
    expect(wrapper.find('InfiniteScroll').prop('loader')).not.toBeUndefined();
  })
})
