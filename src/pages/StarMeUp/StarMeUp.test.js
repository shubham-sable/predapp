import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import StarMeUp from './StarMeUp';
import { testStore , findByTestAtrr } from '../../../Utils';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (initialState = {starMe: {
    recentStars: [],
    receivedStars: [],
    sentStars: [],
    loading: false,
    currentPage: 0,
    limit: 20,
    totalStars: 2,
    }}, 
    props={location: {pathname: '/star-me-up/recent'}}) =>{
  const store = testStore(initialState);
  const wrapper = shallow(<StarMeUp store={store} {...props}/>).dive().dive();
  return wrapper;
}

test('render StarMeUp without error', ()=>{
  const wrapper = setup();
  expect(wrapper);
})

test('render Loader when fetching data', () => {
  const wrapper = setup({starMe: {
    recentStars: [],
    receivedStars: [],
    sentStars: [],
    loading: true,
    currentPage: 0,
    limit: 20,
    totalStars: 2,
    }})
  expect(wrapper.find('Spinner').length).toBe(1);
})

describe('InfiniteScroll testing', () => {
  const wrapper = setup();
  test('render InfiniteScroll without error', () => {
      expect(wrapper.find('InfiniteScroll').length).toBe(1);
  })
  test('render InfiniteScroll with prop initialLoad', ()=>{
      expect(wrapper.find('InfiniteScroll').prop('initialLoad')).toBe(false);
  })
  test('render InfiniteScroll with prop pageStart', ()=>{
      expect(wrapper.find('InfiniteScroll').prop('pageStart')).toBe(0);
  })
})

describe('Card testing', ()=>{
    const post_example= {
        "id": "8d0a963d-fc0c-43e6-80fb-511a77b5253b",
        "sentTo": {
          "id": "3",
          "profileImg": "/assets/imgs/bradpitt.jpeg",
          "name": {
            "fname": "Fouad",
            "lname": "Omri"
          }
        },
        "description": "hjgjh",
        "createdAt": 1620377931.914311,
        "type": "team_player",
        "totalLikes": 1,
        "totalComments": 0,
        "postedBy": {
          "id": "chetan1",
          "profileImg": "https://www.gravatar.com/avatar/72D1A6CDBDFBE17B98B6510E908683C9.jpg",
          "name": {
            "fname": "Chetan",
            "lname": "Godhani"
          }
        },
        "isLiked": true,
        "stars_likes": [
          {
            "id": "chetan1",
            "profileImg": "image",
            "name": {
              "fname": "Chetan",
              "lname": "Godhani"
            }
          }
        ],
        "comment": []
      }
    const initialState= {starMe: {
        recentStars: [post_example],
        receivedStars: [],
        sentStars: [],
        loading: false,
        currentPage: 0,
        limit: 20,
        totalStars: 2,
        }}
    const wrapper = setup(initialState);
    
    test('render Reciever name', ()=>{
        const state= wrapper.find('h4').text()
        expect(state).toBe([post_example.sentTo.name.fname, post_example.sentTo.name.lname].join(" "));
    })
})

