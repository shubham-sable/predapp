import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Modal, Button, Card, Image } from "react-bootstrap";

import moment from "moment";
import CommentBox from "../../components/CommentBox";
import { LikeIcon, LikeFilledIcon, CommentIcon } from './../../svgs';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../actions/StarMeUpActions';

import { Translate } from "react-redux-i18n";
import { useEffect } from "react";

const StarViewModal = (props) => {

  const { 
    show, 
    onHide, 
    starData, 
    starTypeData, 
    handleStarPostLike, 
    starName, 
    getStarCommentsAction,
    handleSaveCommentLikeUnlike, 
    handleSaveCommentPost,
    handleLoadReplies,
    handleLoadReplyReplies,
    loadMoreCommentsClick,
    handleDeleteComment,
    handleEditComment,
    setCommentsLoadingAction
  } = props;

  useEffect(() => {
    if(starData) {
      getStarCommentsAction(starData.id, 0,10)
    }
  }, [show]);

  useEffect(() => {
    setCommentsLoadingAction()
  }, [show]);
  const commentLikeUnlikeHandle = (starPostId, commentId, isReply, replyCommentId) => {
    handleSaveCommentLikeUnlike(starPostId, commentId, isReply, replyCommentId);
  }

  const commentPostHandle = (starPostId, commentId, isReply, replyCommentId) => {
    handleSaveCommentPost(starPostId, commentId, isReply, replyCommentId);
  }

  let LikeComponent = LikeIcon;
  if (starData && starData.isLiked) {
    LikeComponent = LikeFilledIcon
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      className={`quiz-modal create-take-part`}
    >
      {starData &&
        <React.Fragment>
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              {[starData.sentTo.name.fname, starData.postedBy.name.lname].join(" ")}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body className="p-0">
            <Card className="mx-auto card border-0">
              <Card.Body className="p-0">
                <div className="position-sticky bg-white" style={{ top: 0, zIndex: 1 }}>
                  <div className="pl-3 pr-3 pt-3 bg-white border-bottom d-flex">
                    <div className="star-me-up--post-profile">
                      <Image className="rounded-circle" src={starData.sentTo.profile} height={50} />
                      <div className="star-me-up--post-column"></div>
                    </div>
                    <div className="pl-3 pr-3 pb-3 d-flex flex-column">
                      <div>
                        <h4 className="m-0"><b>{[starData.sentTo.name.fname, starData.sentTo.name.lname].join(" ")}</b></h4>
                        <h6 className="mt-0 font-weight-bold text-black-50 pt-2">{moment(new Date(starData.createdAt * 1000)).format("MM.DD.YYYY")} | <span style={{ color: '#dc3545b0' }}>{moment(new Date(starData.createdAt * 1000)).format("HH:MM")}</span></h6>
                      </div>
                      <div className={`${starTypeData.colorClassName} order-card mr-1 mb-2 rounded`}>
                        <div className="text-center d-flex align-items-center p-2">
                          <FontAwesomeIcon icon={starTypeData.icon} style={{ fontSize: 35, width: 35 }} />
                          <h3 style={{ fontSize: 18 }} className="font-weight-bold text-uppercase text-left pl-2 m-0"><Translate value={`star_me_up.${starTypeData.key}`} /></h3>
                        </div>
                      </div>
                      <div>
                        {starData.description}
                      </div>
                    </div>
                  </div>
                  <div className="take-part-footer border-top-0 d-flex align-items-center justify-content-between p-0 pl-3 pr-3 pt-2 pb-2">
                    <div>
                      <img src={starData.postedBy.profileImg} alt={starData.postedBy.name.fname} /><span>Sent By <b>{[starData.postedBy.name.fname, starData.postedBy.name.lname].join(" ")}</b></span>
                    </div>
                    <div className="d-flex user-select-none">
                      <span className="mr-2 d-flex align-items-center font-size-20 cursor-pointer" onClick={() => this.toggleStartMeUpViewModal(starData.id, starName)}>
                        <CommentIcon width={25} height={25} className="mr-2" /> <span className="font-size-14">{starData.totalComments}</span>
                      </span>
                      <span className="d-flex align-items-center font-size-20 cursor-pointer" onClick={() => handleStarPostLike(starData.id, starName)}>
                        <LikeComponent fill={starData.isMyLiked ? '#1F40E6' : '#1F40E6'} height={25} width={25} />&nbsp;
                        <span className="font-size-14">{starData.totalLikes}</span>
                      </span>
                    </div>
                  </div>
                </div>
                <CommentBox
                  comments={starData.comment}
                  user={starData.user}
                  postId={starData.id}
                  commentLikeUnlikeHandle={commentLikeUnlikeHandle}
                  hasMoreComments={false}
                  loadMoreCommentsClick={loadMoreCommentsClick}
                  commentPostHandle={commentPostHandle}
                  handleLoadReplies={handleLoadReplies}
                  handleLoadReplyReplies={handleLoadReplyReplies}
                  handleDeleteComment={handleDeleteComment}
                  handleEditComment={handleEditComment}
                />
              </Card.Body>
            </Card>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="default" className="text-danger" onClick={onHide}>Close</Button>
          </Modal.Footer>
        </React.Fragment>
      }
    </Modal>
  )
};

const mapStateToProps = (state) => ({
  ...state.starMe
})

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators({ ...Actions}, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(StarViewModal);