import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../actions/StarMeUpActions';
import { changeNavTitle } from '../../actions/common';

import { Nav, Card, Image, OverlayTrigger, Tooltip, Spinner } from "react-bootstrap";
import {
  faUsers,
  faRocket,
  faHands,
  faMagic,
  faBatteryEmpty,
  faPlus,
} from "@fortawesome/free-solid-svg-icons";
import {
  faGem,
  faLightbulb,
  faClock,
} from "@fortawesome/fontawesome-free-regular";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import SendStarModal from "./SendStarModal";
import StarViewModal from "./StarViewModal";

import moment from 'moment';
import { FloatingButton } from "../../components/Group/Group.style";
import { StickyNav } from '../../components/Header/GroupHeader';

import InfiniteScroll from 'react-infinite-scroller';
import { NavLink } from 'react-router-dom';
import { LikeIcon, LikeFilledIcon, CommentIcon } from './../../svgs';

import { Translate } from 'react-redux-i18n';

import './../TakePart/TakePart.css';

const starTypeArr = {
  kudos: {
    key: 'kudos',
    name: 'Kudos',
    className: 'bg-c-grey',
    colorClassName: 'text-color-grey',
    icon: faGem,
  },
  act_ethically: {
    key: 'act_ethically',
    name: 'Act Ethically',
    className: 'bg-c-blue',
    colorClassName: 'text-color-blue',
    icon: faHands,
  },
  team_player: {
    key: 'team_player',
    name: 'Team Player',
    className: 'bg-c-violate',
    colorClassName: 'text-color-violate',
    icon: faUsers,
  },
  constantly_innovate: {
    key: 'constantly_innovate',
    name: 'Constantly Innovate',
    className: 'bg-c-pink',
    colorClassName: 'text-color-pink',
    icon: faLightbulb,
  },
  excellence_in_your_work: {
    key: 'excellence_in_your_work',
    name: 'Excellence In Your Work',
    className: 'bg-c-lightblue',
    colorClassName: 'text-color-lightblue',
    icon: faClock,
  },
  think_big: {
    key: 'think_big',
    name: 'Think Big',
    className: 'bg-c-lightgreen',
    colorClassName: 'text-color-lightgreen',
    icon: faRocket,
  },
  have_fun: {
    key: 'have_fun',
    name: 'Have Fun',
    className: 'bg-c-yellow',
    colorClassName: 'text-color-yellow',
    icon: faMagic,
  },
};

class StarMeUp extends Component {

  constructor(props) {
    super(props);

    this.state = {
      starModalShow: false,
      starViewModalShow: false,
      starType: undefined,
      activeTab: 'available',
      selectedNav: 'recent',
      selectedStarId: undefined,
      selectedStarType: undefined,
      sidebarDrawerOpen: false,
    }
  }

  componentDidMount() {
    const { changeNavTitle, currentPage } = this.props;

    const titleSettings = {
      title: 'StarMeUp',
      icon: '/assets/icons/appreciation.svg'
    }

    changeNavTitle(titleSettings);
    this.loadMoreDataAPI(currentPage)
  }

  loadMoreDataAPI = (pageNo) => {
    const { location: { pathname }, loadAllStarsAction, limit } = this.props;
    let starName = '';
    if (pathname === '/star-me-up/recent') {
      starName = 'recent';
    } else if (pathname === '/star-me-up/received') {
      starName = 'received';
    } else if (pathname === '/star-me-up/sent') {
      starName = 'sent';
    }
    loadAllStarsAction(starName, pageNo, limit);
  }

  componentWillUnmount() {
    this.props.setLoadingFlagAction();
  }

  toggleStartMeUpModal = (type) => {
    this.setState((prevState) => ({
      starModalShow: !prevState.starModalShow,
      starType: type,
      sidebarDrawerOpen: false
    }))
  }

  toggleStartMeUpViewModal = (id, type) => {
    this.setState((prevState) => ({
      starViewModalShow: !prevState.starViewModalShow,
      selectedStarId: id,
      selectedStarType: type,
      sidebarDrawerOpen: false
    }))
  }

  handleTabChange = (tabName) => {
    this.setState({
      activeTab: tabName
    })
  }

  addVote = (questionData, ans) => {
    this.props.addVoteAction(questionData.id, ans);
  }

  handleSubmitSendStar = (values) => {
    console.log('handleSubmitSendStar', values);
    values.who.map((reciever) => {
      const h ={
        id: reciever.value.toString(),
        name: {
          fname: reciever.fname,
          lname: reciever.lname,
        },
        profileImg: reciever.imageUrl
      }
      const user={
        id: 'chetan1',
        name: {
          fname: 'Chetan',
          lname: 'Godhani'
        },
        profileImg: 'https://www.gravatar.com/avatar/72D1A6CDBDFBE17B98B6510E908683C9.jpg'
      }
      this.props.createStarMeUpCardAction(user, values.type, values.comment, h)
    })
    
  }

  handleNavSelect = (selectedNav) => {
    if (selectedNav !== this.state.selectedNav) {
      this.setState({
        selectedNav
      })
    }
  }

  handleGetComments = (postId) => {
    this.props.getStarCommentsAction(postId,1);
  }

  handleLoadReplies = (postId,parentId) => {
    const userId = "chetan1";
    this.props.getCommentReplies(postId, parentId, userId);
  }

  handleStarPostLike = (id, type) => {
    const user={
      id : "chetan1",
      name: {
        fname: "Chetan",
        lname: "Godhani"
      },
      profileImg: "image"
    }
    this.props.addStarLikeAction(id, type, user);
  }

  commentLikeUnlikeHandle = (postId, commentId, isReply, replyCommentId, isliked) => {
    const user={
      id: "1",
      profileImg: "xyz",
      name:{
        fname: "Virat",
        lname: "Kohli"
      }
    }
    this.props.saveCommentLikeUnlike(postId, commentId, isReply, replyCommentId, isliked, user);
  }

  handleLoadMoreComments = (postId, pageNumber) => {
    this.props.loadMoreCommentsAction(postId, pageNumber);
  }

  handleStarPostComment = (postId, comment, isReply, commentId, replyId) => {
    const user={
      id: "chetan1",
      profileImg: "Image",
      name:{
        fname: "Chetan",
        lname: "Godhani"
      }
    }
    this.props.postStarCommentAction(postId, comment, isReply, commentId, replyId, user);
  }

  handleLoadReplyReplies = (postId, commentId, replyId) => {
    const userId = 'chetan1';
    this.props.getRepliesReply(postId,commentId,replyId,userId);
  }

  commentPostHandle = (postId, comment, isReply, commentId, replyId) =>{
    const user={
      id: "chetan1",
      profileImg: "xyz",
      name:{
        fname: "Chetan",
        lname: "Godhani"
      }
    }
    this.props.postCommentAction(postId, comment, user, isReply, commentId, replyId);
  }

  handleDeletePost = (postId) =>{
    this.props.deletePostAction(postId);
  }

  handleDeleteComment = (postId, commentId, isReply, parentId) =>{
    this.props.deleteCommentAction(postId,commentId,isReply,parentId);
  }

  handleEditComment = (postId, commentId, isReply, parentId, comment) =>{
    this.props.updateCommentAction(postId, commentId, isReply, parentId, comment);
  }

  toggleDrawer = () => {
    this.setState(prevState => ({
      sidebarDrawerOpen: !prevState.sidebarDrawerOpen
    }))
  }

  handleChangePageNumber = (pageNumber) => {
    if (pageNumber !== this.props.currentPage) {
      this.loadMoreDataAPI(pageNumber)
    }
  }

  render() {
    const { loading, currentPage, totalStars, limit, recentStars, receivedStars, sentStars, location } = this.props;
    const { starModalShow, starType, selectedStarId, selectedStarType, starViewModalShow, sidebarDrawerOpen } = this.state;

    const totalPages = Math.ceil(totalStars / limit);

    const cardBoxStyle = {
      maxWidth: "650px"
    };

    let startsData = [];
    let starName = '';
    if (location.pathname === '/star-me-up/recent') {
      startsData = recentStars;
      starName = 'recentStars';
    } else if (location.pathname === '/star-me-up/received') {
      startsData = receivedStars;
      starName = 'receivedStars';
    } else if (location.pathname === '/star-me-up/sent') {
      startsData = sentStars;
      starName = 'sentStars';
    }

    let selectedStarTypeData = undefined;
    let selectedStarData = undefined;
    if (selectedStarId && selectedStarType) {
      selectedStarData = this.props[selectedStarType].find(x => x.id === selectedStarId);
      selectedStarTypeData = starTypeArr[selectedStarData.type];
    }

    return (
      <div style={{ minHight: 'calc(100vh - 60px)' }}>
        <StickyNav className="position-sticky star-me-up-nav shadow-sm" fill variant="tabs" defaultActiveKey="recent" onSelect={this.handleNavSelect}>
          <Nav.Item>
            <NavLink activeClassName="active" to="/star-me-up/recent" className="d-flex justify-content-center align-items-center betterme-link nav-link"><Translate value="star_me_up.recent" /></NavLink>
          </Nav.Item>
          <Nav.Item>
            <NavLink activeClassName="active" to="/star-me-up/received" className="d-flex justify-content-center align-items-center betterme-link nav-link"><Translate value="star_me_up.received" /></NavLink>
          </Nav.Item>
          <Nav.Item>
            <NavLink activeClassName="active" to="/star-me-up/sent" className="d-flex justify-content-center align-items-center betterme-link nav-link"><Translate value="star_me_up.sent" /></NavLink>
          </Nav.Item>
        </StickyNav>
        <div className="container-fluid star-me-up" style={{ padding: 10 }}>
          {loading ?
            <div className="d-flex justify-content-center pt-4">
              <Spinner animation="border" role="status">
                <span className="sr-only">Loading...</span>
              </Spinner>
            </div>
            :
            <InfiniteScroll
              pageStart={currentPage}
              initialLoad={false}
              loadMore={this.handleChangePageNumber}
              hasMore={currentPage + 1 < totalPages}
              loader={
                <div className="d-flex justify-content-center pt-2 pb-2" key={0}>
                  <Spinner animation="border" role="status">
                    <span className="sr-only">Loading...</span>
                  </Spinner>
                </div>
              }
            >
              <div className="pb-4">
                {startsData.length > 0 ?
                  startsData.map((recentStar, index) => {
                    const starData = starTypeArr[recentStar.type];

                    let LikeComponent = LikeIcon;
                    if (recentStar.isLiked) {
                      LikeComponent = LikeFilledIcon
                    }

                    return (
                      <Card className="mt-4 border mx-auto" style={cardBoxStyle} key={`${starName}-${recentStar.id}-${index}`}>
                        <Card.Body className="p-0">
                          <div className="d-flex" style={{ padding: 10 }}>
                            <div className="star-me-up--post-profile">
                              <Image className="rounded-circle" src={recentStar.sentTo.profileImg} height={50} />
                              <div className="star-me-up--post-column"></div>
                            </div>
                            <div className="pl-3 pr-3 pb-3 bg-white d-flex cursor-pointer" onClick={() => this.toggleStartMeUpViewModal(recentStar.id, starName)}>
                              <div className="d-flex flex-column">
                                <div>
                                  <h4 className="m-0"><b>{recentStar.sentTo.name.fname + " "+ recentStar.sentTo.name.lname}</b></h4>
                                  <h6 className="mt-0 font-weight-bold text-black-50 pt-2">{moment(new Date(recentStar.createdAt * 1000)).format("MM.DD.YYYY")} | <span style={{ color: '#dc3545b0' }}>{moment(new Date(recentStar.createdAt * 1000)).format("HH:MM")}</span></h6>
                                </div>
                                <div className="text-muted">
                                  {moment(new Date(recentStar.createdAt * 1000)).fromNow()}
                                </div>
                                <div className={`${starData.colorClassName} order-card mr-1 mb-2 rounded`}>
                                  <div className="card-block text-center d-flex align-items-center">
                                    <FontAwesomeIcon icon={starData.icon} style={{ fontSize: 35, width: 35 }} />
                                    <h3 style={{ fontSize: 18 }} className="font-weight-bold text-uppercase text-left pl-2 m-0"><Translate value={`star_me_up.${starData.key}`} /></h3>
                                  </div>
                                </div>
                                <div>
                                  {recentStar.description}
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="take-part-footer d-flex align-items-center justify-content-between border-top">
                            <div>
                              <img src={recentStar.postedBy.profile} alt={recentStar.postedBy.userName} /><span>Sent By <b>{[recentStar.postedBy.name.fname, recentStar.postedBy.name.lname].join(" ")}</b></span>
                            </div>
                            <div className="d-flex user-select-none align-items-center">
                              <span className="mr-2 d-flex align-items-center font-size-20 cursor-pointer" onClick={() => this.toggleStartMeUpViewModal(recentStar.id, starName)}>
                                <CommentIcon width={25} height={25} className="mr-2" /> <span className="font-size-14">{recentStar.totalComments}</span>
                              </span>
                              <span className="d-flex align-items-center font-size-20 cursor-pointer" onClick={() => {this.handleStarPostLike(recentStar.id, starName)}}>
                                <LikeComponent fill={recentStar.isLiked ? '#1F40E6' : '#1F40E6'} height={25} width={25} />&nbsp;
                                <span className="font-size-14">{recentStar.totalLikes}</span>
                              </span>
                            </div>
                          </div>
                        </Card.Body>
                      </Card>
                    );
                  })
                  :
                  <Card className="mt-4 border-0 mx-auto" style={cardBoxStyle}>
                    <Card.Body className="p-0">
                      <div className="p-5 bg-white border-bottom d-flex flex-column justify-content-center align-items-center">
                        <FontAwesomeIcon icon={faBatteryEmpty} style={{ fontSize: 40 }} />
                        <h2><Translate value="star_me_up.no_data_found" /></h2>
                      </div>
                    </Card.Body>
                  </Card>
                }
              </div>
            </InfiniteScroll>
          }
        </div>
        <SendStarModal
          show={starModalShow}
          onHide={() => this.toggleStartMeUpModal(undefined)}
          starType={starType}
          handleSubmitSendStar={this.handleSubmitSendStar}
        />
        <StarViewModal
          show={starViewModalShow}
          onHide={() => this.toggleStartMeUpViewModal(undefined, undefined)}
          starData={selectedStarData}
          starTypeData={selectedStarTypeData}
          starName={starName}
          handleStarPostLike={this.handleStarPostLike}
          handleSaveCommentLikeUnlike={this.handleSaveCommentLikeUnlike}
          handleSaveCommentPost={this.handleStarPostComment}
          loadMoreCommentsClick={this.handleLoadMoreComments}
          handleLoadReplies={this.handleLoadReplies}
          handleEditComment={this.handleEditComment}
          handleLoadReplyReplies={this.handleLoadReplyReplies}
          handleDeleteComment={this.handleDeleteComment}
        />
        <OverlayTrigger
          placement={"top"}
          overlay={
            <Tooltip className="font-size-14">
              <Translate value="star_me_up.send_star" />
            </Tooltip>
          }
        >
          <FloatingButton className="btn-primary" right={sidebarDrawerOpen ? 310 : 40} bottom={40} onClick={this.toggleDrawer} fontsize={25}>
            <FontAwesomeIcon icon={faPlus} />
          </FloatingButton>
        </OverlayTrigger>
        <div id="mySidenav" className="sidenav shadow-lg" style={{ width: sidebarDrawerOpen ? 300 : 0 }}>
          <div className="closebtn cursor-pointer" onClick={this.toggleDrawer}>&times;</div>
          {Object.keys(starTypeArr).map((key) => {
            const value = starTypeArr[key];
            return (
              <div key={value.key} className={`card ${value.className} order-card m-3 cursor-pointer`} onClick={() => this.toggleStartMeUpModal(value)} >
                <div className="card-block text-center d-flex align-items-center p-3">
                  <FontAwesomeIcon icon={value.icon} style={{ fontSize: 20, width: 35 }} />
                  <h3 className="m-0 font-weight-bold text-uppercase text-left pl-2 font-size-16"><Translate value={`star_me_up.${key}`} /></h3>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  ...state.starMe
})

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators({ ...Actions, changeNavTitle }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(StarMeUp)
