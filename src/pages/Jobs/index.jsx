import React, { useState, useEffect } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../actions/JobActions';
import { changeNavTitle } from '../../actions/common';

import { Container } from 'react-bootstrap';


import JobSearchBar from '../../components/Jobs/JobSearchBar';
import JobCard from '../../components/Jobs/JobCard';
import JobSaveModal from '../../components/Jobs/JobSaveModal';
import StickyBox from "react-sticky-box";

const JobsListingPage = (props) => {
  const {
    changeNavTitle,
    loading,
    fetchJobListAction,
    addSavedListAction,
    removedSavedListAction,
    addApplicationAction,
    getJobsApplied,
    jobs,
    jobsApplied,
  } = props;

  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    const titleSettings = {
      title: 'sidebar.jobs'
    }
    fetchJobListAction(0,50);
    getJobsApplied(6);
    changeNavTitle(titleSettings);
  }, [changeNavTitle]);

  const setIsJobApplied = (jobId) =>{
    let appliedFlag = false;
    if(jobsApplied.length > 0){
      jobsApplied.map((job) =>{
        if(job.id === jobId){
          appliedFlag = true
        }
      })
    }
    return appliedFlag;
  }

  return (
    <React.Fragment>
      <JobSearchBar />
      <Container className="my-3 mx-1200 d-flex">
        <div className="mr-3" style={{ flex: 1 }}>
          {loading ?
            <div className="border shadow px-3 py-5 text-center">
              <img src="/assets/no-jobs.svg" alt="no-jobs" />
              <h3 className="mt-2" data-test="headingInLoad">There are no jobs right now.</h3>
              <p className="font-size-14 m-0" data-test="descInLoad">Create a job alert and we’ll let you know when relevant jobs are posted.</p>
            </div>
            :
            jobs.map((job) =>{
                return(
                  <JobCard
                    key={job.id}
                    data={job}
                    isJobApplied={setIsJobApplied(job.id)}
                    createdAt={new Date()}
                    skills={['UI/UX', 'Sketch', 'Adobe CC']}
                    addSavedListAction={addSavedListAction}
                    removedSavedListAction={removedSavedListAction}
                    addApplicationAction={addApplicationAction}
                  />
                )
            })
          }
        </div>
        <StickyBox offsetTop={76} offsetBottom={10} style={{ flex: '0 0 362px', alignSelf: 'flex-start' }}>
          <div className="ui vertical menu">
            <div className="border shadow p-3 three wide column d-flex align-items-center justify-content-center" style={{ minHeight: 100 }}>
              <h4 className="mb-0" data-test="No_Filters">No Filters available</h4>
            </div>
            <div
              className="ui button border shadow p-3 d-flex align-items-center justify-content-center"
              style={{ minHeight: 100 }}
              data-test="SavedList"
              onClick={() => setIsOpen(true)}>
              <h4 className="mb-0">Saved List</h4>
            </div>
          </div>
        </StickyBox>
      </Container>
      <JobSaveModal
        show={isOpen}
        handleClose={() => setIsOpen(false)}
        data={props.jobs}
        setIsJobApplied={setIsJobApplied}
        removedSavedListAction={removedSavedListAction}
      />
    </React.Fragment>
  )
}

// <JobCard
//   jobTitle="UI Designer"
//   companyName="ABC LLC"
//   jobLocation="New York"
//   jobType="Contract"
//   description="Exp: 4-7 years; About the company At SalesHandy, were building the sales outreach platform. Since our founding in 2015, weve grown to become a profitable SaaS company. SalesHandy is run by 25 high-performing, happy people that are dedicated to building a product our customers love. Some of the key achievements We have more than 20k users using our tool on a regular basis with more than 2.5k paid users We are having customers from across the globe and from some of the fortune 500s too. We are a team of 25 who made this happen from Ahmedabad, India. Our marketing tactics have got a mention in a book With more than 250 reviews we are rated 4.5 on google chrome store and other online software discovery platforms We are adding hundreds of new customers & tons of new features every month What will I be doing? Developing user interface components and implementing them following well-known React.js workflows (such as Flux or Redux). Building reusable components and front-end libraries for future use. Optimizing components for maximum"
//   skills={['UI/UX Designer', 'Sketch', 'Adobe CC']}
//   createdAt={new Date()}
//   salaryPerYear={`$100,000 - $120,000 per annual`}
// />
// <JobCard
//   jobTitle="UI Designer"
//   companyName="ABC LLC"
//   jobLocation="Germany"
//   jobType="Temporary"
//   description="Exp: 4-7 years; About the company At SalesHandy, were building the sales outreach platform. Since our founding in 2015, weve grown to become a profitable SaaS company. SalesHandy is run by 25 high-performing, happy people that are dedicated to building a product our customers love. Some of the key achievements We have more than 20k users using our tool on a regular basis with more than 2.5k paid users We are having customers from across the globe and from some of the fortune 500s too. We are a team of 25 who made this happen from Ahmedabad, India. Our marketing tactics have got a mention in a book With more than 250 reviews we are rated 4.5 on google chrome store and other online software discovery platforms We are adding hundreds of new customers & tons of new features every month What will I be doing? Developing user interface components and implementing them following well-known React.js workflows (such as Flux or Redux). Building reusable components and front-end libraries for future use. Optimizing components for maximum"
//   skills={['UI/UX Designer', 'Sketch', 'Adobe CC']}
//   createdAt={new Date()}
//   salaryPerYear={`$100,000 - $120,000 per annual`}
// />
// <JobCard
//   jobTitle="UI Designer"
//   companyName="ABC LLC"
//   jobLocation="Remote"
//   jobType="Part-Time"
//   description="Exp: 4-7 years; About the company At SalesHandy, were building the sales outreach platform. Since our founding in 2015, weve grown to become a profitable SaaS company. SalesHandy is run by 25 high-performing, happy people that are dedicated to building a product our customers love. Some of the key achievements We have more than 20k users using our tool on a regular basis with more than 2.5k paid users We are having customers from across the globe and from some of the fortune 500s too. We are a team of 25 who made this happen from Ahmedabad, India. Our marketing tactics have got a mention in a book With more than 250 reviews we are rated 4.5 on google chrome store and other online software discovery platforms We are adding hundreds of new customers & tons of new features every month What will I be doing? Developing user interface components and implementing them following well-known React.js workflows (such as Flux or Redux). Building reusable components and front-end libraries for future use. Optimizing components for maximum"
//   skills={['UI/UX Designer', 'Sketch', 'Adobe CC']}
//   createdAt={new Date()}
//   salaryPerYear={`$100,000 - $120,000 per annual`}
// />
// <JobCard
//   jobTitle="Consultant Gynaecology"
//   companyName="Indira IVF"
//   jobLocation="Jamshedpur, Jharkhand"
//   jobType="Full Time"
//   description="Exp: 4-7 years; About the company At SalesHandy, were building the sales outreach platform. Since our founding in 2015, weve grown to become a profitable SaaS company. SalesHandy is run by 25 high-performing, happy people that are dedicated to building a product our customers love. Some of the key achievements We have more than 20k users using our tool on a regular basis with more than 2.5k paid users We are having customers from across the globe and from some of the fortune 500s too. We are a team of 25 who made this happen from Ahmedabad, India. Our marketing tactics have got a mention in a book With more than 250 reviews we are rated 4.5 on google chrome store and other online software discovery platforms We are adding hundreds of new customers & tons of new features every month What will I be doing? Developing user interface components and implementing them following well-known React.js workflows (such as Flux or Redux). Building reusable components and front-end libraries for future use. Optimizing components for maximum"
//   skills={['UI/UX Designer', 'Sketch', 'Adobe CC']}
//   createdAt={new Date()}
//   salaryPerYear={`Rs.120,000 - Rs.140,000 per month`}
// />

const mapStateToProps = (state) => {
  return {
    loading: state.jobs.loading,
    jobs:state.jobs.jobs,
    jobsApplied: state.jobs.jobsApplied
  };
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ ...Actions, changeNavTitle }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(JobsListingPage);
