import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import JobsListingPage from './';
import { testStore , findByTestAtrr } from '../../../Utils';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (initialState={}) =>{
  const store = testStore(initialState);
  const wrapper = shallow(<JobsListingPage store={store}/>).dive().dive();
  return wrapper;
}

test('render without error', ()=>{
  const wrapper = setup();
  // console.log(wrapper.debug());
  expect(wrapper);
})

describe('JobsListingPage testing while loading true', ()=>{
  const wrapper = setup({
    jobs:{
      loading: true
    }
  });
  test('render JobSearchBar without error', ()=>{
    expect(wrapper.find('JobSearchBar').length).toBe(1);
  })
  test('render Container without error',()=>{
    expect(wrapper.find('Container').length).toBe(1);
  })
  describe('Heading in Container testing', ()=>{
    const component = findByTestAtrr(wrapper,'headingInLoad');
    test('render Heading in Container',()=>{
      expect(component.length).toBe(1);
    })
    test('render Heading text',()=>{
      expect(component.text()).toBe("There are no jobs right now.");
    })
  })
  describe('Description in Container testing', ()=>{
    const component = findByTestAtrr(wrapper, 'descInLoad');
    test('render description in Container',()=>{
      expect(component.length).toBe(1);
    })
    test('render description text', ()=>{
      expect(component.text()).toBe("Create a job alert and we’ll let you know when relevant jobs are posted.");
    })
  })
  describe('StickyBox testing',()=>{
    test('render StickyBox without error', ()=>{
      expect(wrapper.find('StickyBox').length).toBe(1);
    })
    test('render No Filters without error', ()=>{
      const component = findByTestAtrr(wrapper,'No_Filters');
      expect(component.length).toBe(1);
    })
    test('render SavedList without error', ()=>{
      const component = findByTestAtrr(wrapper,'SavedList');
      expect(component.length).toBe(1);
    })
  })
  describe('JobSaveModal testing', ()=>{
    test('render JobSaveModal without error',()=>{
      expect(wrapper.find('JobSaveModal').length).toBe(1);
    })
    test('render JobSaveModal with show as prop', ()=>{
      expect(wrapper.find('JobSaveModal').prop('show')).toBe(false);
    })
    test('render JobSaveModal with data as prop', ()=>{
      expect(wrapper.find('JobSaveModal').prop('data')).toBe();
    })
  })
})
