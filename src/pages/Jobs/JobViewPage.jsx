import React, { useEffect, useState } from 'react'

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addApplicationAction } from '../../actions/JobActions';
import * as Actions from '../../actions/JobViewActions';
import { changeNavTitle } from '../../actions/common';

import { Container, Card, Image, Carousel, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight, faShareAlt, faExternalLinkAlt } from "@fortawesome/free-solid-svg-icons";
import { Spinner } from 'react-bootstrap';

import StickyBox from "react-sticky-box";
import JobViewWidget from '../../components/Jobs/JobViewWidget';
import JobApplyModal from '../../components/Jobs/JobApplyModal';

const JobViewPage = (props) => {
  const [isApplyNowOpen, setIsApplyNowOpen] = useState(false);

  const {
    changeNavTitle,
    loading,
    jobs,
    jobsApplied,
    id,
    fetchJobDetailAction,
    addApplicationAction,
  } = props;

  useEffect(() => {
    const titleSettings = {
      title: 'UI Designer Job'
    }
    changeNavTitle(titleSettings);
    fetchJobDetailAction(id);
  }, [changeNavTitle]);

  const setIsJobApplied = (jobId) =>{
    let appliedFlag = false;
    if(jobsApplied.length > 0){
      jobsApplied.map((job) =>{
        if(job.id === jobId){
          appliedFlag = true
        }
      })
    }
    return appliedFlag;
  }

  return (
    <Container className="my-3 mx-1200 d-flex">
      {loading?
        <div className="d-flex justify-content-center pt-4" style={{ width: 650 }}>
          <Spinner animation="border" role="status">
            <span className="sr-only">Loading...</span>
          </Spinner>
        </div>
        :
        <div className="mr-3" style={{ flex: 1 }}>
          <Card className="shadow border-0 rounded-0 job-detail">
            <Carousel>
              <Carousel.Item style={{ height: 325 }}>
                <img
                  className="d-block w-100"
                  src="https://plexusmd.s3-ap-southeast-1.amazonaws.com/Images/Orgimages/5838/SIKKIM-MANIPAL-INSTITUTE-OF-TECHNOLOGY-EAST-SIKKIM.jpg"
                  alt="First slide"
                />
                <Carousel.Caption>
                  <h3>First slide label</h3>
                  <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                </Carousel.Caption>
              </Carousel.Item>
              <Carousel.Item style={{ height: 325 }}>
                <img
                  className="d-block w-100"
                  src="https://plexusmd.s3-ap-southeast-1.amazonaws.com/Images/Orgimages/5838/a68e214690ce4ef89521ab927ccc537a.jpeg"
                  alt="Third slide"
                />

                <Carousel.Caption>
                  <h3>Second slide label</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </Carousel.Caption>
              </Carousel.Item>
            </Carousel>
            <Card.Body className="border-left border-right border-bottom">
              <Image className="job-detail__company-logo" src="https://www.plexusmd.com/PlexusMDAPI/Images/Provider/5838/5838.png" roundedCircle />

              <div className="pb-3 border-bottom">
                <h3 className="font-weight-600 mt-3 font-size-22 text-info">{jobs.JobName + "/ " + jobs.Company}</h3>
                <p className="text-color-grey font-weight-600 pt-1 font-size-14">{jobs.location.address}<a className="text-info" href="https://maps.google.com/?saddr=Current+Location&daddr=5th%20Mile,%20NH%2031A,%20Tadong,%20Gangtok,%20Sikkim%20737102">Get Directions <FontAwesomeIcon icon={faChevronRight} /></a></p>
                <div className="pt-2">
                  {setIsJobApplied(jobs.id)?
                    <Button
                      variant="outline-primary"
                      className="rounded shadow-sm px-4 font-size-14 text-uppercase"
                    >
                      <FontAwesomeIcon className="mr-2" icon={faExternalLinkAlt} /> Applied
                    </Button>
                    :
                    <Button
                      variant="outline-primary"
                      className="rounded shadow-sm px-4 font-size-14 text-uppercase"
                      onClick={() => setIsApplyNowOpen(true)}
                    >
                      <FontAwesomeIcon className="mr-2" icon={faExternalLinkAlt} /> Apply
                    </Button>
                  }
                  <Button variant="default" className="rounded ml-2 font-size-14 text-uppercase"><FontAwesomeIcon className="mr-2" icon={faShareAlt} /> Share Opening</Button>
                </div>
              </div>

              <div className="py-3 border-bottom">
                <h5 className="font-weight-bold font-size-18 text-info m-0">Required Profile</h5>
                <div className="d-flex mt-4">
                  <div>
                    <p className="m-0 font-size-13 text-muted">Qualification</p>
                    <p className="mb-0 mt-2 font-size-16 font-weight-bold">{jobs.Profile.Qualification}</p>
                  </div>
                  <div className="ml-5">
                    <p className="m-0 font-size-13 text-muted">Experience</p>
                    <p className="mb-0 mt-2 font-size-16 font-weight-bold">{jobs.Profile.Experience} Years</p>
                  </div>
                </div>
              </div>

              <div className="py-3 border-bottom">
                <h5 className="font-weight-bold font-size-18 text-info m-0">Offer</h5>
                <div className="d-flex mt-4">
                  <div>
                    <p className="m-0 font-size-13 text-muted">Salary ( ₹ )</p>
                    <p className="mb-0 mt-2 font-size-16 font-weight-bold">{jobs.Offer.Salary}L per month</p>
                  </div>
                  <div className="ml-5">
                    <p className="m-0 font-size-13 text-muted">Remuneration</p>
                    <p className="mb-0 mt-2 font-size-16 font-weight-bold">{jobs.Offer.Remuneration}</p>
                  </div>
                </div>
              </div>

              <div className="py-3 border-bottom">
                <h5 className="font-weight-bold font-size-18 text-info m-0">About the role</h5>
                <div className="d-flex mt-4">
                  <div>
                    <p className="m-0 font-size-13 text-muted">Job Description</p>
                    <p className="mb-0 mt-2 font-size-15">{jobs.Desc}</p>
                  </div>
                </div>
              </div>

              <div className="py-3 border-bottom">
                <h4 className="font-weight-600 mt-3 font-size-22 text-info m-0">About {jobs.Company}</h4>
                <h5 className="font-weight-bold font-size-16 text-info mt-4 mb-0">Address</h5>
                <p className="text-color-grey pt-3 font-size-13">{jobs.location.address}<a className="text-info" href="https://maps.google.com/?saddr=Current+Location&daddr=5th%20Mile,%20NH%2031A,%20Tadong,%20Gangtok,%20Sikkim%20737102">Get Directions <FontAwesomeIcon icon={faChevronRight} /></a></p>
              </div>

              <div className="py-3">
                <h5 className="font-weight-bold font-size-18 text-info m-0">Infrastructure</h5>
                <div className="d-flex flex-wrap mt-2">
                  <div className="mr-5 mt-3">
                    <p className="m-0 font-size-13 font-weight-bold text-muted">No. of ICU beds</p>
                    <p className="mb-0 mt-2 font-size-16">{jobs.Infrastructure.no_of_ICU_beds}</p>
                  </div>
                  <div className="mr-5 mt-3">
                    <p className="m-0 font-size-13 font-weight-bold text-muted">No. of PICU beds</p>
                    <p className="mb-0 mt-2 font-size-16">{jobs.Infrastructure.no_of_PICU_beds}</p>
                  </div>
                  <div className="mr-5 mt-3">
                    <p className="m-0 font-size-13 font-weight-bold text-muted">No. of NICU beds</p>
                    <p className="mb-0 mt-2 font-size-16">10</p>
                  </div>
                  <div className="mr-5 mt-3">
                    <p className="m-0 font-size-13 font-weight-bold text-muted">No. of OTs</p>
                    <p className="mb-0 mt-2 font-size-16">{jobs.Infrastructure.no_of_ot}</p>
                  </div>
                  <div className="mr-5 mt-3">
                    <p className="m-0 font-size-13 font-weight-bold text-muted">No. of beds</p>
                    <p className="mb-0 mt-2 font-size-16">{jobs.Infrastructure.no_of_beds}</p>
                  </div>
                </div>
              </div>
            </Card.Body>
          </Card>
        </div>
      }
      <StickyBox offsetTop={76} offsetBottom={10} style={{ flex: '0 0 362px', alignSelf: 'flex-start' }}>
        <h4 className="font-weight-bold m-0">Recommended jobs</h4>
        <JobViewWidget
          logo="https://www.plexusmd.com/PlexusMDAPI/Images/Provider/5838/5838.png"
          title="Consultant Cornea & Refractive Surgery at Nandadeep Eye Hospital"
          company="PlexusMD Careers"
          location="Belgaum"
          qualification="DNB,MS (3-6 Years)"
          salary="1.50L-2.00L per month"
          slug="ui-designer-job-5df4edr6g4"
          handleOpen={() => setIsApplyNowOpen(true)}
        />
        <JobViewWidget
          logo="https://www.plexusmd.com/PlexusMDAPI/Images/Provider/5838/5838.png"
          title="Consultant Cornea & Refractive Surgery at Nandadeep Eye Hospital"
          company="PlexusMD Careers"
          location="Belgaum"
          qualification="DNB,MS (3-6 Years)"
          salary="1.50L-2.00L per month"
          slug="ui-designer-job-5df4edr6g4"
          handleOpen={() => setIsApplyNowOpen(true)}
        />
        <JobViewWidget
          logo="https://www.plexusmd.com/PlexusMDAPI/Images/Provider/5838/5838.png"
          title="Consultant Cornea & Refractive Surgery at Nandadeep Eye Hospital"
          company="PlexusMD Careers"
          location="Belgaum"
          qualification="DNB,MS (3-6 Years)"
          salary="1.50L-2.00L per month"
          slug="ui-designer-job-5df4edr6g4"
          handleOpen={() => setIsApplyNowOpen(true)}
        />
      </StickyBox>
      <JobApplyModal
        show={isApplyNowOpen}
        handleClose={() => setIsApplyNowOpen(false)}
        data={{
          company: jobs.Company,
          id: jobs.id,
        }}
        addApplicationAction={addApplicationAction}
      />
    </Container>
  )
}

// <h5 className="font-weight-bold pt-3 font-size-18">Gangtok, Sikkim</h5>

const mapStateToProps = (state, ownProps) => {
  return {
    loading:state.jobView.loading,
    id: ownProps.match.params.id,
    jobs: state.jobView.jobs,
    jobsApplied: state.jobs.jobsApplied,
  };
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ ...Actions, addApplicationAction ,changeNavTitle }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(JobViewPage);
