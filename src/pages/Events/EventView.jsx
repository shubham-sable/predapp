import React, { Component } from 'react';

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../actions/EventActions';
import { postNewPostAction } from '../../actions/PostActions';
import UpcomingListing from '../../components/UpcomingEvents/UpcomingListing';
import EventInvitees from '../../components/EventInvitees/EventInvitees';
import { Spinner } from 'react-bootstrap';

// const eventData = {
//   id: '1',
//   eventName: 'World Dental Show 2020',
//   locationName: 'Bandra East, Mumbai',
//   groupId: 10,
//   groupName: 'Test Group 1',
//   latLong: '23.02579, 72.58727',
//   image: '/minion.jpeg',
//   allDay: false,
//   address: 'Bandra East, Mumbai',
//   end: 'June 30, 2019 11:00:00',
//   startDate: 'September 28, 2019 12:00:00',
//   title: 'World Dental Show 2020',
//   description: `WDS
//   Welcome to the 12th Annual World Dental Show! We invite you to participate and become an integral part of Asia's biggest dental event being held at Bandra-Kurla Complex,Mumbai. The annual event showcases all aspects of dental business world under one roof. WDS is also a trade and knowledge sharing platform that delivers the latest technologies, cutting-edge research and dentistry techniques to meet the growing demands of the ever-evolving world of dental science.
//   The event also acts as a meeting point uniting all relevant groups related to the world of dentistry from across the world. We urge you to partake in various panel discussions and lectures that will augment your dentistry skills and knowledge. Professionals are imparted continuing dental education via scientific learning sessions with speakers from the respective specialised field of dentistry. This will enable your dental team to excel in delivering the finest dental care in the world.`,
//   file: 'https://ts-production.imgix.net/images/e47ce064-3f85-4ace-afe8-af98d4c02cfa.jpg?auto=compress,format&w=800&h=450',
//   guestCanInvite: 'on',
//   showGuestList: 'on'
// };

class EventView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showInviteeModal: false
    };
  }

  componentDidMount(){
    const { eventId } = this.props;
    this.props.getEventByID(eventId);
  }

  toggleInviteeModal = () => {
    this.setState((prevState) => ({
      showInviteeModal: !prevState.showInviteeModal
    }));
  };

  handlePostSubmit = (value) => {
    const user={
      id: "1",
      profileImg: "xyz",
      name:{
        fname: "Virat",
        lname: "Kohli"
      }
    }

    var selectedGroup= undefined;
    if(value.selectedGroup !== undefined){
      selectedGroup={
        id: value.selectedGroup.id,
        name: value.selectedGroup.title,
        profile: value.selectedGroup.imageUrl
      }
    }

    const values={
      coWorkers:[
        value.coWorkers.map((coWorker) =>{
          return {
            id:coWorker.id,
            name:{
              fname:coWorker.firstName,
              lname:coWorker.lastName
            },
            profile: "url"
          }
        })
      ],
      mediaFiles:[
        value.mediaFiles.map((media) =>{
          return {
            media_type: media.type,
            url: "url"
          }
        })
      ],
      postContent:value.postContent,
      selectedGroup: selectedGroup
    }
    this.props.postNewPostAction(user,values);
  };

  render() {
    console.log(this.props.eventData);
    return (
      <React.Fragment>
        {this.props.eventView.loading ?
          <div className="d-flex justify-content-center pt-4" style={{ width: 650 }}>
            <Spinner animation="border" role="status">
              <span className="sr-only">Loading...</span>
            </Spinner>
          </div>
          :
          <div>
            <UpcomingListing eventData={this.props.eventView.eventData} handlePostSubmit={this.handlePostSubmit} />
            <EventInvitees
              show={this.state.showInviteeModal}
              onHide={this.toggleInviteeModal}
              eventData={this.props.eventView.eventData}
            />
          </div>
        }
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    eventView:state.events.eventView,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators({ ...Actions, postNewPostAction }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EventView);
