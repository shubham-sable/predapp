/* eslint-disable indent */
/* eslint-disable react/prop-types */
import React from 'react';

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeNavTitle } from '../../actions/common';

import UpcomingEvents from '../../components/UpcomingEvents/UpcomingEvents';
// import CalendarEvents from '../../components/CalendarEvents/CalendarEvents';
// import CalebrationEvents from '../../components/CalebrationEvents/CalebrationEvents';
import EventView from './EventView';

class EventsPage extends React.Component {

  componentDidMount() {
    const { changeNavTitle } = this.props;

    const titleSettings = {
      title: 'sidebar.events',
      icon: '/assets/icons/event.svg'
    }

    changeNavTitle(titleSettings);
  }

  render() {
    const { match, history, location } = this.props;
    const routeName = match.path;
    const eventId = match.params.eventId;

    return routeName === '/events/:eventId' &&
      (eventId === 'today' || eventId === 'this-week' || eventId === 'this-month' || eventId === 'previous') ?
        <UpcomingEvents match={match} history={history} location={location} />
        :
        <EventView {...this.props} eventId={eventId} />
  }
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators({ changeNavTitle }, dispatch)
  };
};

export default connect(null, mapDispatchToProps)(EventsPage);
