import React from 'react'

import { Container, Row, Col, Card } from 'react-bootstrap';

const InitHeader = ({ handleSelectType }) => {
  return (
    <React.Fragment>
      <h1 className="pt-4 pb-3">Create A Hospital / Health Business with Chain.care</h1>
      <p className="font-size-14">Connect with clients, employees, and the LinkedIn community. To get started, choose a page type.</p>

      <Container className="mt-1">
        <Row>
          <Col className="mt-4" xs={12} sm={6} md={3}>
            <button className="remove-link-style" onClick={() => handleSelectType('institute')}>
              <Card className="w-100 py-3">
                <Card.Body>
                  <img src="/assets/icons/camera.png" alt="" />
                  <h3 className="pt-3 mb-0">Education Institute</h3>
                  <h5 className="pt-2 font-weight-normal">Fewer than 200 employees</h5>
                </Card.Body>
              </Card>
            </button>
          </Col>
          <Col className="mt-4" xs={12} sm={6} md={3}>
            <button className="remove-link-style" onClick={() => handleSelectType('practice')}>
              <Card className="w-100 py-3">
                <Card.Body>
                  <img src="/assets/icons/camera.png" alt="" />
                  <h3 className="pt-3 mb-0">Practice</h3>
                  <h5 className="pt-2 font-weight-normal">Fewer than 200 employees</h5>
                </Card.Body>
              </Card>
            </button>
          </Col>
          <Col className="mt-4" xs={12} sm={6} md={3}>
            <button className="remove-link-style" onClick={() => handleSelectType('hospital')}>
              <Card className="w-100 py-3">
                <Card.Body>
                  <img src="/assets/icons/camera.png" alt="" />
                  <h3 className="pt-3 mb-0">Hospital</h3>
                  <h5 className="pt-2 font-weight-normal">Fewer than 200 employees</h5>
                </Card.Body>
              </Card>
            </button>
          </Col>
          <Col className="mt-4" xs={12} sm={6} md={3}>
            <button className="remove-link-style" onClick={() => handleSelectType('company')}>
              <Card className="w-100 py-3">
                <Card.Body>
                  <img src="/assets/icons/camera.png" alt="" />
                  <h3 className="pt-3 mb-0">Company</h3>
                  <h5 className="pt-2 font-weight-normal">Fewer than 200 employees</h5>
                </Card.Body>
              </Card>
            </button>
          </Col>
        </Row>
      </Container>
    </React.Fragment>
  )
}

export default InitHeader
