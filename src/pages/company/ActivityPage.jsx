import React, { useState } from 'react'

import { Container } from 'react-bootstrap';

import CompanyEditPageHeader from './edit/EditHeader';
import NavSidebar from './activity/NavSidebar';
import WidgetsRightSidebar from './activity/WidgetsRightSidebar';
import NotificationContents from './activity/NotificationContents';

const ActivityPage = () => {
  const [activeMenu, setActiveMenu] = useState('show_all');

  return (
    <React.Fragment>
      <CompanyEditPageHeader />
      <Container className="d-flex py-3 mx-1200">
        <NavSidebar activeMenu={activeMenu} setActiveMenu={setActiveMenu} />
        <NotificationContents />
        <WidgetsRightSidebar />
      </Container>
    </React.Fragment>
  )
}

export default ActivityPage;
