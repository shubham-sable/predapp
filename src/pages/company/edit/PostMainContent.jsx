import React, { useEffect } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../../actions/CompanyActions';
import { changeNavTitle } from '../../../actions/common';

import PostEditor from '../../../components/PostEditor';
import InfiniteScroll from 'react-infinite-scroller';
import { Spinner } from 'react-bootstrap';
import SinglePost from '../../../components/PostsListing/SinglePost';

const PostMainContent = (props) => {
  const {
    loading,
    postsList,
    savePostLikeUnlike,
    saveCommentLikeUnlike,
    getAllPostsListAction,
    resetPostListAction,
    currentPage,
    totalPosts,
    limit,
    commentsLimit,
    loadMoreCommentsAction,
    changeNavTitle,
  } = props;

  useEffect(() => {
    const titleSettings = {
      title: 'Freelance Man',
      icon: '/assets/icons/news-feed.svg'
    }
    changeNavTitle(titleSettings);
    return () => {
      resetPostListAction();
    };
  }, [changeNavTitle, resetPostListAction]);

  useEffect(() => {
    if (loading) {
      getAllPostsListAction(currentPage, limit);
    }
  }, [getAllPostsListAction, currentPage, limit, loading]);

  const handlePostSubmit = (value) => {
    const postContent = value;
    console.log('postContent', postContent);
  };

  const handleChangePageNumber = (pageNumber) => {
    if (pageNumber !== currentPage) {
      getAllPostsListAction(pageNumber, limit);
    }
  }

  const postLikeUnlikeHandle = (postId) => {
    savePostLikeUnlike(postId);
  }

  const commentLikeUnlikeHandle = (postId, commentId, isReply, replyCommentId) => {
    saveCommentLikeUnlike(postId, commentId, isReply, replyCommentId);
  }

  const handleLoadMoreComments = (postId, pageNumber) => {
    loadMoreCommentsAction(postId, pageNumber, commentsLimit)
  }

  const totalPages = Math.ceil(totalPosts / limit);

  return (
    <div className="mx-3" style={{ flex: 1 }}>
      <PostEditor onSubmit={handlePostSubmit} classNames="mt-0" />
      <InfiniteScroll
        pageStart={currentPage}
        initialLoad={false}
        loadMore={handleChangePageNumber}
        hasMore={currentPage + 1 <= totalPages}
        loader={
          <div className="d-flex card__box--post justify-content-center pt-2 pb-2" key={0}>
            <Spinner animation="border" role="status">
              <span className="sr-only">Loading...</span>
            </Spinner>
          </div>
        }
      >
        {postsList.map((post, index) => {
          return (
            <SinglePost key={`post-${post.id}-${index}`} data={post} commentsLimit={commentsLimit} postLikeUnlikeHandle={postLikeUnlikeHandle} commentLikeUnlikeHandle={commentLikeUnlikeHandle} handleLoadMoreComments={handleLoadMoreComments} />
          );
        })}
      </InfiniteScroll>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    ...state.companyAdminView
  };
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ ...Actions, changeNavTitle }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PostMainContent);
