import React, { useState } from 'react';

import { Button, Table } from 'react-bootstrap';

import LanguageForm from './LanguageForm';

const LanguageListing = ({ handleClose }) => {
  const [showForm, setShowForm] = useState(false);

  const handleToggle = () => setShowForm(!showForm);

  return (
    <React.Fragment>
      <div style={{ display: !showForm ? 'block' : 'none' }}>
        <div style={{ maxHeight: 500, minHeight: 289, overflowY: 'auto' }}>
          <div className="p-3 border-bottom">
            <h5>Manage other languages for page name and description.</h5>
            {/* <p className="m-0 font-size-16 font-weight-bold text-color-blue cursor-pointer">Learn more</p> */}
            <Button className="rounded-0 font-size-14 mt-2" variant="outline-primary" onClick={handleToggle}>+ Add a Lanauge</Button>
          </div>
          <Table responsive="sm">
            <thead>
              <tr>
                <th>#</th>
                <th>Language</th>
                <th>Name</th>
                <th>Tagline</th>
                <th>Description</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>English (Default)</td>
                <td>Institute</td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
          </Table>
        </div>
        <div className="position-sticky d-flex justify-content-end p-2 border-top" style={{ bottom: 0 }}>
          <Button className="rounded-0 font-size-14" variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </div>
      </div>
      <LanguageForm show={showForm} handleClose={handleToggle} />
    </React.Fragment>
  )
}

export default LanguageListing
