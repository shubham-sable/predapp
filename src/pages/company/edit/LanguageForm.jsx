import React from 'react'

import { Button, Form } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

import { Formik, Form as FormikForm } from 'formik';

const LanguageForm = ({ show, handleClose }) => {
  const handleFormSubmit = values => {
    console.log('values', values);
  }

  return (
    <div className="locations-form" style={{ display: show ? 'block' : 'none' }}>
      <div className="p-3 border-bottom">
        <h5>Add a language</h5>
        <Button variant="outline-primary" className="rounded-0 font-size-14" onClick={handleClose}>
          <FontAwesomeIcon style={{ fontSize: '1em' }} icon={faArrowLeft} className="default-icons mr-2" /> Back
        </Button>
      </div>
      <Formik
        enableReinitialize
        initialValues={{
          language: '',
          name: 'Institute',
          tagline: '',
          description: '',
          defaultLanguage: false,
        }}
        // validationSchema={LanguageSchema}
        onSubmit={handleFormSubmit}
      >
        {(renderProps) => {
          const { values: formValues } = renderProps;
          return (
            <FormikForm className="h-100" onSubmit={renderProps.handleSubmit}>
              <div className="p-3" style={{ maxHeight: 500, minHeight: 289, overflowY: 'auto' }}>
                <Form.Group>
                  <Form.Label className="font-size-14 font-weight-bold">Langauge</Form.Label>
                  <Form.Control
                    as="select"
                    custom
                    value={formValues.language}
                    onChange={(e) => renderProps.setFieldValue('language', e.target.value)}
                  >
                    <option value="">Select a Language</option>
                    <option value="English">English</option>
                    <option value="Hindi">Hindi</option>
                    <option value="German">German</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group>
                  <Form.Label className="font-size-14 font-weight-bold">Name</Form.Label>
                  <Form.Control
                    type="text"
                    value={formValues.name}
                    onChange={(e) => renderProps.setFieldValue('name', e.target.value)}
                  />
                  <Form.Text className="text-muted"></Form.Text>
                </Form.Group>
                <Form.Group controlId="tagline">
                  <Form.Label className="font-size-14">Tagline</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows="2"
                    placeholder="Enter a tagline"
                    value={formValues.tagline}
                    onChange={(e) => renderProps.setFieldValue('tagline', e.target.value)}
                  />
                </Form.Group>
                <Form.Group controlId="description">
                  <Form.Label className="font-size-14">Description</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows="4"
                    placeholder="Enter a description"
                    value={formValues.description}
                    onChange={(e) => renderProps.setFieldValue('description', e.target.value)}
                  />
                </Form.Group>
                <Form.Group controlId="formBasicCheckboxdefaultLanguage">
                  <Form.Check
                    type="checkbox"
                    label="Set as default language"
                    value={formValues.defaultLanguage}
                    onChange={e => renderProps.setFieldValue('defaultLanguage', e.target.checked)}
                  />
                </Form.Group>
              </div>
              <div className="position-sticky d-flex justify-content-end p-2 border-top" style={{ bottom: 0 }}>
                <Button className="rounded-0 font-size-14" variant="secondary" onClick={handleClose}>
                  Close
                </Button>
                <Button type="submit" className="rounded-0 font-size-14 ml-2" variant="primary" onClick={handleClose}>
                  Save Changes
                </Button>
              </div>
            </FormikForm>
          )
        }}
      </Formik>
    </div>
  )
}

export default LanguageForm;
