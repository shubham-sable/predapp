import React from 'react';

import { Button, Form } from 'react-bootstrap';
import CompanyProfileUpload from './CompanyProfileUpload';

import { Formik, Form as FormikForm } from 'formik';
// import * as Yup from "yup";

const PageInfoForm = ({ setActiveMenu, handleClose }) => {

  const handleFormSubmit = values => {
    console.log('value', values)
  }

  return (
    <Formik
      enableReinitialize
      initialValues={{
        name: '',
        tagline: ''
      }}
      // validationSchema={LanguageSchema}
      onSubmit={handleFormSubmit}
    >
      {(renderProps) => {
        const { values: formValues } = renderProps;
        return (
          <FormikForm className="h-100" onSubmit={renderProps.handleSubmit}>
            <div className="p-3" style={{ maxHeight: 500, minHeight: 289, overflowY: 'auto' }}>
              <h5>Update basic information to increase Page Discovery</h5>

              <div className="pt-3 pb-4">
                <h6 className="font-size-14">Page logo</h6>
                <CompanyProfileUpload
                  uploadFile={() => { }}
                  margintop="0"
                  width={80}
                />
              </div>

              <Form.Group controlId="companyName">
                <Form.Label className="font-size-14">Name</Form.Label>
                <Form.Control
                  type="text"
                  value={formValues.name}
                  onChange={(e) => renderProps.setFieldValue('name', e.target.value)}
                />
              </Form.Group>
              <Form.Group controlId="website">
                <Form.Label className="font-size-14">Tagline</Form.Label>
                <Form.Control
                  as="textarea"
                  rows="3"
                  placeholder="Add your slogan or mission statement"
                  value={formValues.tagline}
                  onChange={(e) => renderProps.setFieldValue('tagline', e.target.value)}
                />
              </Form.Group>

              <div className="pt-3">
                <p className="m-0 font-size-16 font-weight-bold text-color-blue cursor-pointer" onClick={() => setActiveMenu('manage_languages')}>Manage information in another language</p>
              </div>
            </div>
            <div className="position-sticky d-flex justify-content-end p-2 border-top" style={{ bottom: 0 }}>
              <Button className="rounded-0 font-size-14" variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <Button type="submit" className="rounded-0 font-size-14 ml-2" variant="primary" onClick={handleClose}>
                Save Changes
              </Button>
            </div>
          </FormikForm>
        )
      }}
    </Formik>
  )
}

export default PageInfoForm
