import React, { useState } from 'react'

import { Container, Badge, Button, Nav, Navbar, NavDropdown } from 'react-bootstrap';

import { Link } from 'react-router-dom';
import ManageAdminsModal from './ManageAdminsModal';
import PublicURLModal from './PublicURLModal';
import DeactivateModal from './DeactivateModal';

const CompanyEditPageHeader = () => {
  const [showModal, setShowModal] = useState('');

  const handleToggleModal = (modalName) => setShowModal(modalName);

  return (
    <React.Fragment>
      <div className="p-2 bg-white w-100 border-bottom">
        <Container className="d-flex align-items-center justify-content-between mx-1200">
          <div className="d-flex align-items-center">
            <div>
              <img src="/assets/icons/dummy-company.png" width={50} alt="dummy-company" />
            </div>
            <div className="ml-3 d-flex align-items-center">
              <h2 className="m-0 font-size-20">Institution Name</h2>
              <Badge className="ml-2 font-size-14" pill variant="secondary">
                Admin View
              </Badge>
            </div>
          </div>
          <Button className="rounded-0 font-size-14" variant="primary">View as Member</Button>
        </Container>
      </div>
      <div className="bg-white shadow w-100 border-bottom">
        <Container className="d-flex align-items-center justify-content-between mx-1200">
          <Navbar className="w-100 pl-0 font-size-14 text-dark company-admin-menu" collapseOnSelect expand="lg">
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link as={Link} to="/company/5466541/admin">Home</Nav.Link>
                <Nav.Link href="#pricing">Content</Nav.Link>
                <NavDropdown title="Analytics" id="collasible-nav-dropdown">
                  <NavDropdown.Item as={Link} to="/company/71131217/admin/analytics/visitors">Visitors</NavDropdown.Item>
                  <NavDropdown.Item as={Link} to="/company/71131217/admin/analytics/updates">Updates</NavDropdown.Item>
                  <NavDropdown.Item as={Link} to="/company/71131217/admin/analytics/followers">Followers</NavDropdown.Item>
                </NavDropdown>
                <Nav.Link as={Link} to="/company/71131217/admin/notifications/all/">Activity</Nav.Link>
              </Nav>
              <Nav>
                <NavDropdown title="Admin Tools" id="collasible-nav-dropdown">
                  <NavDropdown.Item href="#action/3.1">Invite Connections</NavDropdown.Item>
                  {/* <NavDropdown.Item href="#action/3.2">Post a Job</NavDropdown.Item> */}
                  <NavDropdown.Item href="#action/3.3">Create an Event</NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item onClick={() => handleToggleModal('manage_admins')}>Manage Admins</NavDropdown.Item>
                  <NavDropdown.Item onClick={() => handleToggleModal('edit_public_url')}>Edit Public URL</NavDropdown.Item>
                  <NavDropdown.Item onClick={() => handleToggleModal('deactivate')}>Deactivate Page</NavDropdown.Item>
                </NavDropdown>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </Container>
      </div>
      <ManageAdminsModal show={showModal === 'manage_admins'} handleClose={() => handleToggleModal('')} />
      <PublicURLModal show={showModal === 'edit_public_url'} handleClose={() => handleToggleModal('')} />
      <DeactivateModal show={showModal === 'deactivate'} handleClose={() => handleToggleModal('')} />
    </React.Fragment>
  )
}

export default CompanyEditPageHeader
