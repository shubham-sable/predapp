import React from 'react';

import { Container, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen } from '@fortawesome/free-solid-svg-icons';

import styled from 'styled-components';
import CompanyProfileUpload from './CompanyProfileUpload';
import { checkEnterAndClick } from '../../../utils/helpers';

const HeaderCover = styled.div`
  height: 180px;
  width: 100%;
  background-color: #fff;
  position: relative;

  .edit-cover-icon {
    border: 0;
    box-shadow: none;
    height: 35px;
    width: 35px;
    position: absolute;
    right: 10px;
    top: 10px;
    border-radius: 50%;
    color: rgba(0,0,0,.6);
    outline: none;

    &:hover, &:focus, &:active {
      background-color: #e5e5e5;
      color: #000;
    }
  }
`;

const EditBasicDetails = ({ handleEditModalToggle }) => {
  return (
    <Container className="mt-3 mx-1200">
      <div className="shadow border">
        <HeaderCover className="" style={{ backgroundImage: 'url(/assets/profile-page/background.svg)' }}>
          <button className="edit-cover-icon shadow d-flex justify-content-center align-items-center font-size-16">
            <FontAwesomeIcon icon={faPen} />
          </button>
        </HeaderCover>
        <div className="p-3">
          <div className="d-flex align-items-center justify-content-between">

            <div className="d-flex align-items-center flex-grow-1">
              <CompanyProfileUpload
                uploadFile={() => { }}
              />
              <div className="ml-3">
                <h3 className="m-0">Freelance Man</h3>
                <p className="mt-1 font-size-14">
                  <span className="text-muted">Information Technology & Services - 0 Followers</span>
                </p>
              </div>
            </div>
            <div>
              <Button className="rounded-0 font-size-14" variant="primary" onClick={handleEditModalToggle}>
                <FontAwesomeIcon icon={faPen} />&nbsp; Edit Page
              </Button>
              <Button className="rounded-0 font-size-14 ml-2" variant="outline-primary" style={{ border: '1px solid' }}><FontAwesomeIcon icon={faPen} />&nbsp; Share Page</Button>
            </div>
          </div>
          <div className="tagline-box" role="button" tabIndex="0" aria-pressed="false" onClick={handleEditModalToggle} onKeyDown={checkEnterAndClick(handleEditModalToggle)}>
            Tagline: Add a short description or catchphrase about your Page
          </div>
        </div>
      </div>
    </Container>
  )
}

export default EditBasicDetails;
