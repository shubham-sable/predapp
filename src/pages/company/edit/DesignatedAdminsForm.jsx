import React from 'react';

import { Button } from 'react-bootstrap';

import Autocomplete from '../../../components/SelectAutocomplete';

const DesignatedAdminsForm = ({ setActiveMenu, handleClose }) => {

  return (
    <React.Fragment>
      <div className="p-3 border-bottom">
        <h5>You must be connected to a member to include them as an admin.</h5>
        <Autocomplete classes="mt-3" />
      </div>
      <div style={{ maxHeight: 500, minHeight: 289, overflowY: 'auto' }}>
      </div>
      <div className="position-sticky d-flex justify-content-end p-2 border-top" style={{ bottom: 0 }}>
        <Button className="rounded-0 font-size-14" variant="secondary" onClick={handleClose}>
          Close
        </Button>
        <Button type="submit" className="rounded-0 font-size-14 ml-2" variant="primary" onClick={handleClose}>
          Save Changes
        </Button>
      </div>
    </React.Fragment>
  )
}

export default DesignatedAdminsForm
