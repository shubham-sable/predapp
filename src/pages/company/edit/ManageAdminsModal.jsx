import React, { useState } from 'react';

import { Modal, ListGroup } from 'react-bootstrap';

import DesignatedAdminsForm from './DesignatedAdminsForm';
import RecruiterPosterForm from './RecruiterPosterForm';
import { checkEnterAndClick } from '../../../utils/helpers';

const ManageAdminsModal = ({ show, handleClose }) => {
  const [activeMenu, setActiveMenu] = useState('designated_modals');

  return (
    <Modal
      show={show}
      onHide={handleClose}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      className="edit-company-modal"
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">Admin Tools {">"} Manage Admins</Modal.Title>
      </Modal.Header>
      <Modal.Body className="p-0 d-flex">
        <div className="border-right" style={{ flex: '0 0 200px' }}>
          <ListGroup className="border-bottom" variant="flush">
            <ListGroup.Item
              className={`font-weight-bold font-size-15 activity-links ${activeMenu === 'designated_modals' ? 'active' : ''}`}
              role="button"
              tabIndex="0"
              aria-pressed="false"
              onClick={() => setActiveMenu('designated_modals')}
              onKeyDown={checkEnterAndClick(setActiveMenu, 'designated_modals')}
            >
              Designated Admins
            </ListGroup.Item>
            <ListGroup.Item
              className={`font-weight-bold font-size-15 activity-links ${activeMenu === 'recruiter_posters' ? 'active' : ''}`}
              role="button"
              tabIndex="0"
              aria-pressed="false"
              onClick={() => setActiveMenu('recruiter_posters')}
              onKeyDown={checkEnterAndClick(setActiveMenu, 'recruiter_posters')}
            >
              Recruiter Posters
            </ListGroup.Item>
          </ListGroup>
        </div>
        <div style={{ flexGrow: 1 }}>
          {activeMenu === 'designated_modals' ?
            <DesignatedAdminsForm setActiveMenu={setActiveMenu} handleClose={handleClose} />
            :
            activeMenu === 'recruiter_posters' ?
              <RecruiterPosterForm setActiveMenu={setActiveMenu} handleClose={handleClose} />
              :
              null
          }
        </div>
      </Modal.Body>
    </Modal>
  )
}

export default ManageAdminsModal;
