import React from 'react';

import { checkEnterAndClick } from './../../../utils/helpers';
import StickyBox from "react-sticky-box";

const AnalyticsLeftSidebar = ({ handleEditModalToggle }) => {
  return (
    <StickyBox className="border shadow" offsetTop={76} offsetBottom={10} style={{ flex: '0 0 216px', alignSelf: 'flex-start' }}>
      <div className="px-2">
        <div className="border-bottom py-3">
          <h5 className="mb-1">Analytics</h5>
          <p className="text-muted">Last 30 day activity</p>

          <div className="d-flex justify-content-between">
            <div>
              <p className="mb-1 text-color-blue font-size-16">1</p>
              <p className="font-size-14 mb-2">Unique visitors</p>
            </div>
            <p className="m-0">0%</p>
          </div>
          <p className="text-color-blue font-weight-bold m-0" role="button" tabIndex="0" aria-pressed="false">Share trending articles</p>
        </div>
        <div className="border-bottom py-3">
          <div className="d-flex justify-content-between">
            <div>
              <p className="mb-1 text-color-blue font-size-16">0</p>
              <p className="font-size-14 mb-2">New followers</p>
            </div>
            <p className="m-0">0%</p>
          </div>
        </div>
        <div className="py-3">
          <div className="d-flex justify-content-between">
            <div>
              <p className="mb-1 text-color-blue font-size-16">1</p>
              <p className="font-size-14 mb-2">Post impressions</p>
            </div>
            <p className="m-0">0%</p>
          </div>
          <p className="text-color-blue font-weight-bold m-0" role="button" tabIndex="0" aria-pressed="false">Start a post</p>
        </div>
      </div>
      <div className="bg-mgrey py-3 px-2">
        <p className="font-size-14 mb-2">Drive business actions via a custom button</p>
        <p className="text-color-blue font-size-14 font-weight-bold m-0 py-1" role="button" tabIndex="0" aria-pressed="false" onClick={handleEditModalToggle} onKeyDown={checkEnterAndClick(handleEditModalToggle)}>Add custom button</p>
      </div>
    </StickyBox>
  )
}

export default AnalyticsLeftSidebar
