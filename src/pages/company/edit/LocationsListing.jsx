import React, { useState } from 'react';

import { Button, Table } from 'react-bootstrap';

import LocationsForm from './LocationsForm';

const LocationsListing = ({ handleClose }) => {
  const [showForm, setShowForm] = useState(false);

  const handleToggle = () => setShowForm(!showForm);

  return (
    <React.Fragment>
      <div style={{ display: !showForm ? 'block' : 'none' }}>
        <div style={{ maxHeight: 500, minHeight: 289, overflowY: 'auto' }}>
          <div className="p-3 border-bottom">
            <h5>Update locations to let members know where you’re based</h5>
            <p className="font-size-14 text-muted">If you don’t have a street address, you can exclude it.</p>
            <Button className="rounded-0 font-size-14" variant="outline-primary" onClick={handleToggle}>+ Add a Location</Button>
          </div>
          <Table responsive="sm">
            <thead>
              <tr>
                <th>#</th>
                <th>Address</th>
                <th>Location Name</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Table cell</td>
                <td>Table cell</td>
                <td>Table cell</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Table cell</td>
                <td>Table cell</td>
                <td>Table cell</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Table cell</td>
                <td>Table cell</td>
                <td>Table cell</td>
              </tr>
              <tr>
                <td>1</td>
                <td>Table cell</td>
                <td>Table cell</td>
                <td>Table cell</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Table cell</td>
                <td>Table cell</td>
                <td>Table cell</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Table cell</td>
                <td>Table cell</td>
                <td>Table cell</td>
              </tr>
              <tr>
                <td>1</td>
                <td>Table cell</td>
                <td>Table cell</td>
                <td>Table cell</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Table cell</td>
                <td>Table cell</td>
                <td>Table cell</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Table cell</td>
                <td>Table cell</td>
                <td>Table cell</td>
              </tr>
              <tr>
                <td>1</td>
                <td>Table cell</td>
                <td>Table cell</td>
                <td>Table cell</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Table cell</td>
                <td>Table cell</td>
                <td>Table cell</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Table cell</td>
                <td>Table cell</td>
                <td>Table cell</td>
              </tr>
            </tbody>
          </Table>
        </div>
        <div className="position-sticky d-flex justify-content-end p-2 border-top" style={{ bottom: 0 }}>
          <Button className="rounded-0 font-size-14" variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </div>
      </div>
      <LocationsForm show={showForm} handleClose={handleToggle} />
    </React.Fragment>
  )
}

export default LocationsListing
