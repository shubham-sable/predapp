import React from 'react';

import { Button, Form } from 'react-bootstrap';

import { Formik, Form as FormikForm } from 'formik';
// import * as Yup from "yup";

const ButtonsForm = ({ handleClose }) => {

  const handleFormSubmit = values => {
    console.log('value', values)
  }

  return (
    <Formik
      enableReinitialize
      initialValues={{
        customButton: false,
        tagline: '',
        buttonName: '',
        url: '',
      }}
      // validationSchema={LanguageSchema}
      onSubmit={handleFormSubmit}
    >
      {(renderProps) => {
        const { values: formValues } = renderProps;
        return (
          <FormikForm className="h-100" onSubmit={renderProps.handleSubmit}>
            <div style={{ maxHeight: 500, minHeight: 289, overflowY: 'auto' }}>
              <div className="p-3 border-bottom">
                <h4>Select buttons to display on your page</h4>

                <div className="pt-3 position-relative check-switch-component d-flex align-items-center">
                  <div>
                    <h5 className="font-size-16">Custom Button</h5>
                    <h6 className="font-size-14 font-weight-normal">Add a custom button to drive business actions through your Chain.care space</h6>
                  </div>
                  <div className="d-flex align-items-center ml-3">
                    <p className="m-0 font-size-14">{formValues.customButton ? 'On' : 'Off'}</p>
                    <Form.Check
                      className="ml-1"
                      type="switch"
                      id="custom-switch"
                      label={''}
                      value={formValues.customButton}
                      onChange={(e) => renderProps.setFieldValue('customButton', e.target.checked)}
                    />
                  </div>
                </div>
                {formValues.customButton &&
                  <div className="pt-2">
                    <Form.Group>
                      <Form.Label className="font-size-14 font-weight-bold">Button name</Form.Label>
                      <Form.Control
                        as="select"
                        custom
                        value={formValues.buttonName}
                        onChange={(e) => renderProps.setFieldValue('buttonName', e.target.value)}
                      >
                        <option value="">Select Button Name</option>
                        <option value="contact_us">Contact Us</option>
                        <option value="learn_more">Learn More</option>
                        <option value="register">Register</option>
                        <option value="sign_up">Sign Up</option>
                        <option value="visit_website">Visit Website</option>
                      </Form.Control>
                    </Form.Group>
                    <Form.Group>
                      <Form.Label className="font-size-14 font-weight-bold">URL</Form.Label>
                      <Form.Control
                        type="text"
                        value={formValues.url}
                        onChange={(e) => renderProps.setFieldValue('url', e.target.value)}
                      />
                      <Form.Text className="text-muted"></Form.Text>
                    </Form.Group>
                  </div>
                }
              </div>

              <div className="p-3">
                <h4>Button Preview</h4>
                <div className="p-3 border">
                  <div className="d-flex">
                    <div>
                      <img src="/assets/icons/dummy-company.png" width={110} alt="dummy-company" />
                    </div>
                    <div className="ml-3 align-self-end">
                      <p className="font-size-18 mb-1">Institution Name</p>
                      <p className="font-size-14 mb-1">Industry</p>
                    </div>
                  </div>
                  <div className="mt-3">
                    <Button className="rounded-0 font-size-16 px-4" variant="primary">+ &nbsp; Follow</Button>
                    {formValues.buttonName !== '' ?
                      <Button className="ml-3 rounded-0 font-size-16 px-4" style={{ border: '1px solid' }} variant="outline-primary">
                        {formValues.buttonName === 'contact_us' ?
                          'Contact Us'
                          :
                          formValues.buttonName === 'learn_more' ?
                            'Learn More'
                            :
                            formValues.buttonName === 'register' ?
                              'Register'
                              :
                              formValues.buttonName === 'sign_up' ?
                                'Sign Up'
                                :
                                formValues.buttonName === 'visit_website' ?
                                  'Visit Website'
                                  :
                                  null
                        }
                      </Button>
                      :
                      null
                    }
                  </div>
                </div>
              </div>
            </div>
            <div className="position-sticky d-flex justify-content-end p-2 border-top" style={{ bottom: 0 }}>
              <Button className="rounded-0 font-size-14" variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <Button type="submit" className="rounded-0 font-size-14 ml-2" variant="primary" onClick={handleClose}>
                Save Changes
              </Button>
            </div>
          </FormikForm>
        )
      }}
    </Formik>
  )
}

export default ButtonsForm
