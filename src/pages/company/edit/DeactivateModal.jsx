import React from 'react';

import { Button, Modal } from 'react-bootstrap';

const DeactivateModal = ({ show, handleClose }) => {
  return (
    <Modal
      show={show}
      onHide={handleClose}
      size="md"
      aria-labelledby="contained-modal-title-vcenter"
      className="edit-company-modal"
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">Admin Tools {">"} Deactivate Page</Modal.Title>
      </Modal.Header>
      <Modal.Body className="text-center px-5"> 
        <img src="/assets/deactivate.svg" alt="deactivate" />
        <h4 className="font-size-22 mt-3">Are you sure you want to continue?</h4>
        <p className="mt-3 font-size-14" style={{ lineHeight: 1.5 }}>Deactivating a page will remove the page from Chain.care. The page will no longer appear in search results and existing employee associations will be removed.</p>
        <p className="mt-3 font-size-14" style={{ lineHeight: 1.5 }}>Once deactivated, you and other admins will no longer be able to manage this page. Learn more about deactivating your Page in our Help Center.</p>
      </Modal.Body>
      <Modal.Footer>
        <Button className="rounded-0 font-size-14" variant="outline-primary" onClick={handleClose}>Deactivate</Button>
        <Button className="rounded-0 font-size-14" variant="primary" onClick={handleClose}>Cancel</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default DeactivateModal;
