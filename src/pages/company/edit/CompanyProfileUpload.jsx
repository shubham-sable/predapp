import React, { useState, useRef } from 'react';

import styled from 'styled-components';

export const CompanyProfileImage = styled.div`
  background: transparent ${props => props.bgimage ? `${`url(${props.bgimage})`}` : 'url("/assets/icons/camera.png")' } 50% no-repeat;
  background-size: ${props => props.bgimage ? 'cover' : 'auto'};
  background-color: rgba(243,246,248,.94);
  max-width: ${props => props.width ? props.width + 'px' : '0px'};
  max-height: ${props => props.width ? props.width + 'px' : '0px'};
  width: 100%;
  height: ${props => props.width ? props.width + 'px' : '0px'};
  margin-top: ${props => props.margintop ? props.margintop : 0};
  position: relative;
`;

const CompanyProfileUpload = ({ uploadFile, margintop = "-90px", width = 120 }) => {

  const [uploadedProfileImg, setUploadedProfileImg] = useState(undefined);
  const fileUpload = useRef(null);

  return (
    <CompanyProfileImage className="shadow border" bgimage={uploadedProfileImg && uploadedProfileImg.value } margintop={margintop} width={width}>
      <span className={`profile-photo-edit__file-upload-input cursor-pointer user-select-none`} onClick={() => fileUpload.current.click()} />
      <input
        type="file"
        className="d-none"
        ref={fileUpload}
        accept="image/*"
        onChange={(e) => {
          if (e.target.files.length === 0) {
            setUploadedProfileImg(undefined);
            // setUploadedProfileImgObj(undefined);
            return;
          } else {
            const reader = new FileReader();
            const file = e.target.files[0];
            const filetype = file.type;
            reader.readAsDataURL(file);
            reader.onload = (event) => {
              let payload = undefined;

              if (filetype.includes('image/')) {
                payload = {
                  type: 'image',
                  value: event.target.result,
                }
              }

              setUploadedProfileImg(payload);
            };

            // setUploadedProfileImgObj(e.target.files[0]);
            uploadFile(e.target.files[0]);
          }
        }}
      />
    </CompanyProfileImage>
  )
}

export default CompanyProfileUpload
