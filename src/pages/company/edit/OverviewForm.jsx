import React from 'react';

import { Button, Form } from 'react-bootstrap';

import { Formik, Form as FormikForm } from 'formik';
// import * as Yup from "yup";

const OverviewForm = ({ setActiveMenu, handleClose }) => {

  const handleFormSubmit = values => {
    console.log('value', values)
  }

  return (
    <Formik
      enableReinitialize
      initialValues={{
        name: '',
        tagline: '',
        description: '',
        url: '',
        phoneNumber: '',
        yearFounded: '',
      }}
      // validationSchema={LanguageSchema}
      onSubmit={handleFormSubmit}
    >
      {(renderProps) => {
        const { values: formValues } = renderProps;
        return (
          <FormikForm className="h-100" onSubmit={renderProps.handleSubmit}>
            <div className="p-3" style={{ maxHeight: 500, minHeight: 339, overflowY: 'auto' }}>
              <h5>Provide details to display on your page</h5>

              <div className="pt-3">
                <Form.Group>
                  <Form.Label className="font-size-14">Description</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows="3"
                    placeholder="Add an About Us with a brief overview of your products and services"
                    value={formValues.description}
                    onChange={(e) => renderProps.setFieldValue('description', e.target.value)}
                  />
                </Form.Group>
              </div>
              <div className="pt-3 pb-4">
              <p className="m-0 font-size-16 font-weight-bold text-color-blue cursor-pointer" onClick={() => setActiveMenu('manage_languages')}>Manage information in another language</p>
              </div>

              <Form.Group>
                <Form.Label className="font-size-14">Website URL</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Add your website homepage (www.example.com)"
                  value={formValues.url}
                  onChange={(e) => renderProps.setFieldValue('url', e.target.value)}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label className="font-size-14">Phone</Form.Label>
                <Form.Control
                  type="number"
                  placeholder="Enter a Phone Number"
                  value={formValues.phoneNumber}
                  onChange={(e) => renderProps.setFieldValue('phoneNumber', e.target.value)}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label className="font-size-14">Year Founded</Form.Label>
                <Form.Control
                  type="number"
                  placeholder="YYYY"
                  value={formValues.yearFounded}
                  onChange={(e) => renderProps.setFieldValue('yearFounded', e.target.value)}
                />
              </Form.Group>
            </div>
            <div className="position-sticky d-flex justify-content-end p-2 border-top" style={{ bottom: 0 }}>
              <Button className="rounded-0 font-size-14" variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <Button type="submit" className="rounded-0 font-size-14 ml-2" variant="primary" onClick={handleClose}>
                Save Changes
              </Button>
            </div>
          </FormikForm>
        )
      }}
    </Formik>
  )
}

export default OverviewForm;
