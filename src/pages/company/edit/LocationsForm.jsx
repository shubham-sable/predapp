import React from 'react'

import { Button, Form } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

import { Formik, Form as FormikForm } from 'formik';

const LocationsForm = ({ show, handleClose }) => {
  const handleFormSubmit = values => {
    console.log('values', values);
  }

  return (
    <div className="locations-form" style={{ display: show ? 'block' : 'none' }}>
      <div className="p-3 border-bottom">
        <h5>Add a location</h5>
        <p className="font-size-14 text-muted">Let’s enter your primary location. You can edit, remove, or add more locations later.</p>
        <Button variant="outline-primary" className="rounded-0 font-size-14" onClick={handleClose}>
          <FontAwesomeIcon style={{ fontSize: '1em' }} icon={faArrowLeft} className="default-icons mr-2" /> Back
        </Button>
      </div>
      <Formik
        enableReinitialize
        initialValues={{
          country: '',
          doesnthaveAddress: false,
          streetAddress: '',
          aptAddress: '',
          city: '',
          state: '',
          pincode: '',
          locationName: '',
          makePrimary: false,
        }}
        // validationSchema={LanguageSchema}
        onSubmit={handleFormSubmit}
      >
        {(renderProps) => {
          const { values: formValues } = renderProps;
          return (
            <FormikForm className="h-100" onSubmit={renderProps.handleSubmit}>
              <div className="p-3" style={{ maxHeight: 500, minHeight: 289, overflowY: 'auto' }}>
                <Form.Group>
                  <Form.Label className="font-size-14 font-weight-bold">Country/Region</Form.Label>
                  <Form.Control
                    as="select"
                    custom
                    value={formValues.country}
                    onChange={(e) => renderProps.setFieldValue('country', e.target.value)}
                  >
                    <option value="">Select a Country</option>
                    <option value="India">India</option>
                    <option value="Germany">Germany</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group controlId="formBasicCheckboxdoesnthaveAddress">
                  <Form.Check
                    type="checkbox"
                    label="My organization doesn’t have a street address"
                    value={formValues.doesnthaveAddress}
                    onChange={e => renderProps.setFieldValue('doesnthaveAddress', e.target.checked)}
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label className="font-size-14 font-weight-bold">Street address</Form.Label>
                  <Form.Control
                    type="text"
                    value={formValues.streetAddress}
                    onChange={(e) => renderProps.setFieldValue('streetAddress', e.target.value)}
                  />
                  <Form.Text className="text-muted"></Form.Text>
                </Form.Group>
                <Form.Group>
                  <Form.Label className="font-size-14 font-weight-bold">Apt, suite, etc.</Form.Label>
                  <Form.Control
                    type="text"
                    value={formValues.aptAddress}
                    onChange={(e) => renderProps.setFieldValue('aptAddress', e.target.value)}
                  />
                  <Form.Text className="text-muted"></Form.Text>
                </Form.Group>
                <Form.Group>
                  <Form.Label className="font-size-14 font-weight-bold">City</Form.Label>
                  <Form.Control
                    type="text"
                    value={formValues.city}
                    onChange={(e) => renderProps.setFieldValue('city', e.target.value)}
                  />
                  <Form.Text className="text-muted"></Form.Text>
                </Form.Group>
                <div className="d-flex">
                  <Form.Group>
                    <Form.Label className="font-size-14 font-weight-bold">State/Province</Form.Label>
                    <Form.Control
                      type="text"
                      value={formValues.state}
                      onChange={(e) => renderProps.setFieldValue('state', e.target.value)}
                    />
                    <Form.Text className="text-muted"></Form.Text>
                  </Form.Group>
                  <Form.Group className="ml-3">
                    <Form.Label className="font-size-14 font-weight-bold">ZIP/Postal Code</Form.Label>
                    <Form.Control
                      type="text"
                      value={formValues.pincode}
                      onChange={(e) => renderProps.setFieldValue('pincode', e.target.value)}
                    />
                    <Form.Text className="text-muted"></Form.Text>
                  </Form.Group>
                </div>
                <Form.Group>
                  <Form.Label className="font-size-14 font-weight-bold">Location Name</Form.Label>
                  <Form.Control
                    type="text"
                    value={formValues.locationName}
                    onChange={(e) => renderProps.setFieldValue('locationName', e.target.value)}
                  />
                  <Form.Text className="text-muted"></Form.Text>
                </Form.Group>
                <Form.Group controlId="formBasicCheckboxmakePrimary">
                  <Form.Check
                    type="checkbox"
                    label="Make my primary location"
                    value={formValues.makePrimary}
                    onChange={e => renderProps.setFieldValue('makePrimary', e.target.checked)}
                  />
                </Form.Group>
              </div>
              <div className="position-sticky d-flex justify-content-end p-2 border-top" style={{ bottom: 0 }}>
                <Button className="rounded-0 font-size-14" variant="secondary" onClick={handleClose}>
                  Close
                </Button>
                <Button type="submit" className="rounded-0 font-size-14 ml-2" variant="primary" onClick={handleClose}>
                  Save Changes
                </Button>
              </div>
            </FormikForm>
          )
        }}
      </Formik>
    </div>
  )
}

export default LocationsForm
