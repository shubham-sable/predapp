import React, { useState } from 'react';

import { Modal, ListGroup } from 'react-bootstrap';

import PageInfoForm from './PageInfoForm';
import ButtonsForm from './ButtonsForm';
import OverviewForm from './OverviewForm';
import LocationsListing from './LocationsListing';
import LanguageListing from './LanguageListing';

const EditCompanyModal = ({ show, handleClose }) => {
  const [activeMenu, setActiveMenu] = useState('manage_languages');

  return (
    <Modal
      show={show}
      onHide={handleClose}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      className="edit-company-modal"
    // centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">Edit</Modal.Title>
      </Modal.Header>
      <Modal.Body className="p-0 d-flex">
        <div className="border-right" style={{ flex: '0 0 200px' }}>
          <ListGroup className="border-bottom" variant="flush">
            <ListGroup.Item className="font-weight-bold font-size-16">
              Header
            </ListGroup.Item>
            <ListGroup.Item className="p-0">
              <ListGroup variant="flush">
                <ListGroup.Item
                  className={`font-weight-bold font-size-15 inner-links ${activeMenu === 'page_info' ? 'active' : ''}`}
                  onClick={() => setActiveMenu('page_info')}
                >
                  Page Info
                </ListGroup.Item>
                <ListGroup.Item
                  className={`font-weight-bold font-size-15 inner-links ${activeMenu === 'buttons' ? 'active' : ''}`}
                  onClick={() => setActiveMenu('buttons')}
                >
                  Buttons
                </ListGroup.Item>
              </ListGroup>
            </ListGroup.Item>
            <ListGroup.Item className="font-weight-bold font-size-16">
              About
            </ListGroup.Item>
            <ListGroup.Item className="p-0">
              <ListGroup variant="flush">
                <ListGroup.Item
                  className={`font-weight-bold font-size-15 inner-links ${activeMenu === 'overview' ? 'active' : ''}`}
                  onClick={() => setActiveMenu('overview')}
                >
                  Overview
                </ListGroup.Item>
                <ListGroup.Item
                  className={`font-weight-bold font-size-15 inner-links ${activeMenu === 'locations' ? 'active' : ''}`}
                  onClick={() => setActiveMenu('locations')}
                >
                  Locations
                </ListGroup.Item>
              </ListGroup>
            </ListGroup.Item>
            <ListGroup.Item className="font-weight-bold font-size-16">
              Community
            </ListGroup.Item>
            <ListGroup.Item className="p-0">
              <ListGroup variant="flush">
                <ListGroup.Item
                  className={`font-weight-bold font-size-15 inner-links ${activeMenu === 'manage_languages' ? 'active' : ''}`}
                  onClick={() => setActiveMenu('manage_languages')}
                >
                  Manage Languages
                </ListGroup.Item>
              </ListGroup>
            </ListGroup.Item>
          </ListGroup>
        </div>
        <div style={{ flexGrow: 1 }}>
          {activeMenu === 'page_info' ?
            <PageInfoForm setActiveMenu={setActiveMenu} handleClose={handleClose} />
            :
            activeMenu === 'buttons' ?
              <ButtonsForm setActiveMenu={setActiveMenu} handleClose={handleClose} />
              :
              activeMenu === 'overview' ?
                <OverviewForm setActiveMenu={setActiveMenu} handleClose={handleClose} />
                :
                activeMenu === 'locations' ?
                  <LocationsListing handleClose={handleClose} />
                  :
                  activeMenu === 'manage_languages' ?
                    <LanguageListing handleClose={handleClose} />
                    :
                    null
          }
        </div>
      </Modal.Body>
    </Modal>
  )
}

export default EditCompanyModal
