import React from 'react';

import { Button, ListGroup } from 'react-bootstrap';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen } from "@fortawesome/free-solid-svg-icons";

import styled from 'styled-components';
import { EventIcon } from './../../../svgs';
import StickyBox from "react-sticky-box";

const EditButton = styled.button`
  border: 0;
  box-shadow: none;
  height: 35px;
  width: 35px;
  border-radius: 50%;
  color: rgba(0,0,0,.6);
  outline: none;

  &:hover, &:focus, &:active {
    background-color: #e5e5e5;
    color: #000;
  }
`;

const WidgetsRightSidebar = () => {
  return (
    <StickyBox offsetTop={76} offsetBottom={10} style={{ flex: '0 0 312px', alignSelf: 'flex-start' }}>
      <div className="border shadow">
        <div className="border-bottom py-3 px-3">
          <h4 className="mb-0">Invite Connections To Follow</h4>
        </div>
        <ListGroup className="border-bottom" variant="flush">
          <ListGroup.Item className="d-flex align-items-center">
            <img className="rounded-circle align-self-start" width={48} height={48} src="https://avatars1.githubusercontent.com/u/20254321?v=4" alt="chetan8300" />
            <div className="mx-2">
              <div className="font-weight-bold font-size-16 mb-1">Chetan Godhani</div>
              <div className="text-muted break-words lt-line-clamp lt-line-clamp--multi-line font-size-14 m-0" style={{ WebkitLineClamp: 2 }}>Freelance Full Stack Developer | React.JS | Node.js | Laravel | Express.JS</div>
            </div>
            <Button variant="outline-primary" className="rounded-0 font-size-12 px-2 py-1">Invite</Button>
          </ListGroup.Item>
          <ListGroup.Item className="d-flex align-items-center">
            <img className="rounded-circle align-self-start" width={48} height={48} src="https://avatars1.githubusercontent.com/u/20254321?v=4" alt="chetan8300" />
            <div className="mx-2">
              <div className="font-weight-bold font-size-16 mb-1">Chetan Godhani</div>
              <div className="text-muted break-words lt-line-clamp lt-line-clamp--multi-line font-size-14 m-0" style={{ WebkitLineClamp: 2 }}>Freelance Full Stack Developer | React.JS | Node.js | Laravel | Express.JS</div>
            </div>
            <Button variant="outline-primary" className="rounded-0 font-size-12 px-2 py-1">Invite</Button>
          </ListGroup.Item>
          <ListGroup.Item className="d-flex align-items-center">
            <img className="rounded-circle align-self-start" width={48} height={48} src="https://avatars1.githubusercontent.com/u/20254321?v=4" alt="chetan8300" />
            <div className="mx-2">
              <div className="font-weight-bold font-size-16 mb-1">Chetan Godhani</div>
              <div className="text-muted break-words lt-line-clamp lt-line-clamp--multi-line font-size-14 m-0" style={{ WebkitLineClamp: 2 }}>Freelance Full Stack Developer | React.JS | Node.js | Laravel | Express.JS</div>
            </div>
            <Button variant="outline-primary" className="rounded-0 font-size-12 px-2 py-1">Invite</Button>
          </ListGroup.Item>
        </ListGroup>
        <Button className="rounded-0 text-color-blue font-weight-bold" variant size="lg" block>
          Invite more connections
        </Button>
      </div>
      <div className="border shadow mt-3">
        <div className="border-bottom py-2 px-3 d-flex align-items-center justify-content-between">
          <h3 className="mb-0">Events</h3>
          <EditButton className="shadow justify-content-center align-items-center font-size-16">
            <FontAwesomeIcon icon={faPen} />
          </EditButton>
        </div>
        <div className="border-bottom p-3 text-center">
          <EventIcon fill="#e5e5e5" width="70px" height="70px" />
          <p className="mt-3 font-size-14">Events organized by Freelance Man will appear here</p>
        </div>
        <Button className="rounded-0 text-color-blue font-weight-bold" variant size="lg" block>
          Create Event
        </Button>
      </div>
    </StickyBox>
  )
}

export default WidgetsRightSidebar
