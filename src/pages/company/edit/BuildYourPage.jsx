import React, { useState } from "react";

import { Container, Card, Accordion, Button } from "react-bootstrap";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons';
import Slider from 'react-slick';
import { checkEnterAndClick } from "../../../utils/helpers";

const BuildYourPage = ({ handleEditModalToggle }) => {

  // const [currentIndex, setCurrentIndex] = useState(0);
  const [buildYourPageAccordionActiveKey, setBuildYourPageAccordionActiveKey] = useState('0')

  // const handleMeterSliderSelect = (direction) => {
  //   setCurrentIndex(direction === 'prev' ? currentIndex - 1 : currentIndex + 1);
  // }

  const isProfileBuildYourPageOpen = () => {
    setBuildYourPageAccordionActiveKey(!buildYourPageAccordionActiveKey ? '0' : null);
  }

  // let currentSlider = undefined;

  // if (profileBuildYourPage[currentIndex]) {
  //   currentSlider = profileBuildYourPage[currentIndex];
  // }

  const settings = {
    dots: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: false,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          arrows: false,
          slidesToScroll: 2,
          slidesToShow: 2
        }
      },
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          slidesToScroll: 1,
          slidesToShow: 1
        }
      },
    ]
  };

  return (
    <Container className="mx-1200">
      <Accordion activeKey={buildYourPageAccordionActiveKey}>
        <Card className="w-100 shadow profile-cards mt-3 p-4">
          <Card.Header className="bg-white border-0 p-0">
            <div className="d-flex align-items-center justify-content-between mb-3">
              <div>
                <h3 className="font-size-20">
                  <span className="text-body font-weight-bold">Build Your Page</span>
                </h3>
                <p className="m-0 font-size-13">Complete actions to unlock new features along the way. On average, completed pages get 30% more traffic.</p>
              </div>
              <Accordion.Toggle
                className="cursor-pointer user-select-none profile-header--edit ml-2 rounded-circle d-flex align-items-center justify-content-center border-0"
                style={{ flex: '0 0 40px' }}
                as="div"
                eventKey="0"
                role="button"
                tabIndex="0"
                aria-pressed="false"
                onClick={isProfileBuildYourPageOpen}
                onKeyDown={checkEnterAndClick(isProfileBuildYourPageOpen)}
              >
                <FontAwesomeIcon className="font-size-24 p-1 rounded-circle profile-header--edit-icon" style={{ zoom: 1.5 }} icon={!buildYourPageAccordionActiveKey ? faAngleDown : faAngleUp} />
              </Accordion.Toggle>
            </div>
            <div className="mb-3 profile-meter d-flex" style={{ marginRight: 1 }}>
              <div style={{ width: 'calc(100% / 6)' }} className="strength-meter"></div>
              <div style={{ width: 'calc(100% / 6)' }} className="strength-meter"></div>
              <div style={{ width: 'calc(100% / 6)' }} className="strength-meter bg-mgrey"></div>
              <div style={{ width: 'calc(100% / 6)' }} className="strength-meter bg-mgrey"></div>
              <div style={{ width: 'calc(100% / 6)' }} className="strength-meter bg-mgrey position-relative">
                {/* <div className="meter-lock-div position-absolute d-flex justify-content-center align-items-center cursor-pointer">
                  <FontAwesomeIcon className="font-size-24 p-1 rounded-circle profile-header--edit-icon" icon={faLock} />
                </div> */}
              </div>
              <div style={{ width: 'calc(100% / 6)' }} className="strength-meter bg-mgrey"></div>
            </div>
          </Card.Header>
          <Accordion.Collapse eventKey="0">
            <Card.Body className="p-0">
              <div className="d-flex justify-content-between">
                <p className="p-0 font-size-14">6 remaining to complete</p>
                {/* <div className="d-flex ml-3" style={{ color: 'rgba(0, 0, 0, 0.6)' }}>
                  <button onClick={() => handleMeterSliderSelect('prev')} className="meter-navigation-button d-inline-flex align-items-center" disabled={currentIndex === 0 ? true : false}>
                    <FontAwesomeIcon className="mr-2" icon={faAngleLeft} size="2x" />
                    <span className="font-size-16 font-weight-bold">Previous</span>
                  </button>
                  <button onClick={() => handleMeterSliderSelect('next')} className="meter-navigation-button ml-3 d-inline-flex align-items-center" disabled={currentIndex === (profileBuildYourPage.length - 1) ? true : false}>
                    <span className="font-size-16 font-weight-bold">Next</span><FontAwesomeIcon className="ml-2" icon={faAngleRight} size="2x" />
                  </button>
                </div> */}
              </div>
              <Slider className="build-your-page-slider" {...settings}>
                <div key={`slide-0`} style={{ outline: 'none' }}>
                  <div className="border d-flex p-2 pt-3 pb-3" style={{ minHeight: 180, margin: '0 8px' }}>
                    <img className="pt-3" src="/assets/icons/camera.png" alt="" style={{ alignSelf: 'flex-start' }} />
                    <div className="pl-3 pt-3 d-flex flex-column justify-content-between">
                      <div>
                        <p className="font-size-16 font-weight-bold mb-2">Website URL</p>
                        <p className="font-size-14 mb-0 build-step-description">Help members find your website to learn more about your organization</p>
                      </div>
                      <div className="mt-2">
                        <Button variant="primary" className="rounded-0 font-size-14 mr-2" onClick={handleEditModalToggle}>Add</Button>
                        <Button className="rounded-0 font-size-14" variant="default">Don't Have</Button>
                      </div>
                    </div>
                  </div>
                </div>
                <div key={`slide-0`} style={{ outline: 'none' }}>
                  <div className="border d-flex p-2 pt-3 pb-3" style={{ minHeight: 180, margin: '0 8px' }}>
                    <img className="pt-3" src="/assets/icons/camera.png" alt="" style={{ alignSelf: 'flex-start' }} />
                    <div className="pl-3 pt-3 d-flex flex-column justify-content-between">
                      <div>
                        <p className="font-size-16 font-weight-bold mb-2">Description</p>
                        <p className="font-size-14 mb-0 build-step-description">Improve discoverability in search results by adding keywords to your description</p>
                      </div>
                      <div>
                        <Button variant="primary" className="rounded-0 font-size-14" onClick={handleEditModalToggle}>Add</Button>
                      </div>
                    </div>
                  </div>
                </div>
                <div key={`slide-0`} style={{ outline: 'none' }}>
                  <div className="border d-flex p-2 pt-3 pb-3" style={{ minHeight: 180, margin: '0 8px' }}>
                    <img className="pt-3" src="/assets/icons/camera.png" alt="" style={{ alignSelf: 'flex-start' }} />
                    <div className="pl-3 pt-3 d-flex flex-column justify-content-between">
                      <div>
                        <p className="font-size-16 font-weight-bold mb-2">Logo</p>
                        <p className="font-size-14 mb-0 build-step-description">Appear in more recommendations to members by adding a logo</p>
                      </div>
                      <div>
                        <Button variant="primary" className="rounded-0 font-size-14" onClick={handleEditModalToggle}>Upload</Button>
                      </div>
                    </div>
                  </div>
                </div>
                <div key={`slide-0`} style={{ outline: 'none' }}>
                  <div className="border d-flex p-2 pt-3 pb-3" style={{ minHeight: 180, margin: '0 8px' }}>
                    <img className="pt-3" src="/assets/icons/camera.png" alt="" style={{ alignSelf: 'flex-start' }} />
                    <div className="pl-3 pt-3 d-flex flex-column justify-content-between">
                      <div>
                        <p className="font-size-16 font-weight-bold mb-2">Location</p>
                        <p className="font-size-14 mb-0 build-step-description">Let members know where you’re based by sharing a location</p>
                      </div>
                      <div>
                        <Button variant="primary" className="rounded-0 font-size-14" onClick={handleEditModalToggle}>Add</Button>
                      </div>
                    </div>
                  </div>
                </div>
                <div key={`slide-0`} style={{ outline: 'none' }}>
                  <div className="border d-flex p-2 pt-3 pb-3" style={{ minHeight: 180, margin: '0 8px' }}>
                    <img className="pt-3" src="/assets/icons/camera.png" alt="" style={{ alignSelf: 'flex-start' }} />
                    <div className="pl-3 pt-3 d-flex flex-column justify-content-between">
                      <div>
                        <p className="font-size-16 font-weight-bold mb-2">Custom button</p>
                        <p className="font-size-14 mb-0 build-step-description">Drive business actions from your page by adding a custom button</p>
                      </div>
                      <div>
                        <Button variant="primary" className="rounded-0 font-size-14" onClick={handleEditModalToggle}>Add</Button>
                      </div>
                    </div>
                  </div>
                </div>
                <div key={`slide-0`} style={{ outline: 'none' }}>
                  <div className="border d-flex p-2 pt-3 pb-3" style={{ minHeight: 180, margin: '0 8px' }}>
                    <img className="pt-3" src="/assets/icons/camera.png" alt="" style={{ alignSelf: 'flex-start' }} />
                    <div className="pl-3 pt-3 d-flex flex-column justify-content-between">
                      <div>
                        <p className="font-size-16 font-weight-bold mb-2">Your first post</p>
                        <p className="font-size-14 mb-0 build-step-description">Double page engagement by posting content weekly</p>
                      </div>
                      <div>
                        <Button variant="primary" className="rounded-0 font-size-14">Post</Button>
                      </div>
                    </div>
                  </div>
                </div>
              </Slider>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </Container>
  )
}

export default BuildYourPage;
