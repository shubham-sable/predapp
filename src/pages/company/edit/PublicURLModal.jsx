import React from 'react';

import { Button, Form, InputGroup, FormControl, Modal } from 'react-bootstrap';

import { Formik, Form as FormikForm } from 'formik';
import * as Yup from 'yup';

const SendFeedbackSchema = Yup.object().shape({
  // employee: Yup.object().required('Required'),
  // competencies: Yup.array().min(1).required('Required'),
  // comment: Yup.string().required('Required')
});

const PublicURLModal = ({ show, handleClose }) => {

  const handleSubmit = values => {
    console.log('values', values);
    handleClose();
  }

  return (
    <Modal
      show={show}
      onHide={handleClose}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      className="edit-company-modal"
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">Admin Tools {">"} Edit Public URL</Modal.Title>
      </Modal.Header>
      <Modal.Body className="d-flex flex-column align-items-center">
        <Formik
          enableReinitialize
          initialValues={{
            url: "",
          }}
          validationSchema={SendFeedbackSchema}
          onSubmit={handleSubmit}
        >
          {(renderProps) => {
            const { values: formValues } = renderProps;
            return (
              <FormikForm>
                <Form.Group>
                  <Form.Label className="font-size-14" htmlFor="inlineFormInputGroup">
                    Chain.care Space public URL
                  </Form.Label>
                  <InputGroup className="mb-2">
                    <InputGroup.Prepend>
                      <InputGroup.Text className="font-size-14">chain.care/company/</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                      id="inlineFormInputGroup"
                      placeholder="e.g. abc-clinic"
                      className="font-size-14"
                      value={formValues.url}
                      onChange={(e) => renderProps.setFieldValue('url', e.target.value)}
                    />
                  </InputGroup>
                </Form.Group>
                <div className="mt-3 d-flex justify-content-center">
                  <Button className="rounded-0 font-size-14" variant="outline-primary">Cancel</Button>
                  <Button className="ml-2 rounded-0 font-size-14" type="submit" variant="primary">Save Changes</Button>
                </div>
              </FormikForm>
            )
          }}
        </Formik>
      </Modal.Body>
    </Modal>
  )
}

export default PublicURLModal;
