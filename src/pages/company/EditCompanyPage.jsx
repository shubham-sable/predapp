import React, { useState } from 'react';

import { Container } from 'react-bootstrap';

import CompanyEditPageHeader from './edit/EditHeader';
import BuildYourPage from './edit/BuildYourPage';
import EditBasicDetails from './edit/BasicDetails';
import AnalyticsLeftSidebar from './edit/AnalyticsLeftSidebar';
import PostMainContent from './edit/PostMainContent';
import WidgetsRightSidebar from './edit/WidgetsRightSidebar';
import EditCompanyModal from './edit/EditCompanyModal';

const EditCompanyPage = () => {
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);

  const handleEditModalToggle = () => setIsEditModalOpen(!isEditModalOpen);

  return (
    <React.Fragment>
      <CompanyEditPageHeader />
      <BuildYourPage handleEditModalToggle={handleEditModalToggle} />
      <EditBasicDetails handleEditModalToggle={handleEditModalToggle} />
      <Container className="d-flex mt-3 mb-3 mx-1200">
        <AnalyticsLeftSidebar handleEditModalToggle={handleEditModalToggle} />
        <PostMainContent />
        <WidgetsRightSidebar />
      </Container>
      <EditCompanyModal show={isEditModalOpen} handleClose={handleEditModalToggle} />
    </React.Fragment>
  )
}

export default EditCompanyPage;
