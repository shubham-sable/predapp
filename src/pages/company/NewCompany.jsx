import React, { useState } from 'react';

import { Container, Button, Form, InputGroup, FormControl } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

import './company.scss';
import InitHeader from './initHeader';
import { Formik, Form as FormikForm } from 'formik';
import * as Yup from 'yup';

const SendFeedbackSchema = Yup.object().shape({
  // employee: Yup.object().required('Required'),
  // competencies: Yup.array().min(1).required('Required'),
  // comment: Yup.string().required('Required')
});

const NewCompany = () => {
  const [selectedType, setSelectedType] = useState('');

  const handleSelectType = type => setSelectedType(type);

  const handleSubmit = values => {
    console.log('value', values);
  }

  return (
    selectedType === '' ?
      <div className="p-4 text-center w-100" style={{ minHight: 'calc(100vh - 60px)' }}>
        <InitHeader handleSelectType={handleSelectType} />
      </div>
      :
      <React.Fragment>
        <div className="shadow">
          <Container className="mx-1200">
            <div className="p-4 bg-white w-100">
              <Button variant="outline-primary d-flex align-items-center font-size-14" onClick={() => handleSelectType('')}>
                <FontAwesomeIcon style={{ fontSize: '1em' }} icon={faArrowLeft} className="default-icons mr-2" /> Back
              </Button>
              <div className="d-flex align-items-center mt-3">
                <img src="/assets/business.svg" alt="Camera" className="mr-3" />
                <h3 className="font-size-22 m-0">Let’s get started with a few details about your small business.</h3>
              </div>
            </div>
          </Container>
        </div>
        <Formik
          enableReinitialize
          initialValues={{
            name: "",
            url: "",
            website: "",
            logo: "",
            tagline: "",
            termsAgree: false,
          }}
          validationSchema={SendFeedbackSchema}
          onSubmit={handleSubmit}
        >
          {(renderProps) => {
            const { values: formValues } = renderProps;
            return (
              <FormikForm>
                <Container className="mt-4 mx-1200">
                  <div className="d-flex">
                    <div style={{ flex: 1 }}>
                      <div className="border shadow p-3">
                        <h4 className="text-primary mb-3">
                          Page identity
                        </h4>
                        <Form.Group controlId="companyName">
                          <Form.Label>Name</Form.Label>
                          <Form.Control
                            type="text"
                            value={formValues.name}
                            onChange={(e) => renderProps.setFieldValue('name', e.target.value)}
                          />
                        </Form.Group>
                        <Form.Group>
                          <Form.Label htmlFor="inlineFormInputGroup">
                            Chain.care Space public URL
                          </Form.Label>
                          <InputGroup className="mb-2">
                            <InputGroup.Prepend>
                              <InputGroup.Text>chain.care/company/</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl
                              id="inlineFormInputGroup"
                              placeholder="e.g. abc-clinic"
                              value={formValues.url}
                              onChange={(e) => renderProps.setFieldValue('url', e.target.value)}
                            />
                          </InputGroup>
                        </Form.Group>
                        <Form.Group controlId="website">
                          <Form.Label>Website</Form.Label>
                          <Form.Control
                            type="text"
                            placeholder="Begin with http:// or https:// or www."
                            value={formValues.website}
                            onChange={(e) => renderProps.setFieldValue('website', e.target.value)}
                          />
                          <Form.Text className="text-muted">
                            This is a link to your external website.
                          </Form.Text>
                        </Form.Group>
                      </div>
                      <div className="border shadow p-3 mt-3">
                        <h4 className="text-primary mb-3">
                          Profile Details
                        </h4>
                        <Form.Group controlId="companyName">
                          <Form.Label>Logo</Form.Label>
                          <Form.Control
                            type="file"
                            className="form-control" style={{ height: 36 }}
                            onChange={(e) => renderProps.setFieldValue('website', e.target.files.length > 0 ? e.target.files[0] : null)}
                          />
                          <Form.Text className="text-muted">300 x 300px recommended. JPGs, JPEGs, and PNGs supported.</Form.Text>
                        </Form.Group>
                        <Form.Group controlId="website">
                          <Form.Label>Tagline</Form.Label>
                          <Form.Control
                            as="textarea"
                            rows="3"
                            placeholder="Example: A family-run accounting firm that promises you won’t lose sleep over filing your taxes."
                            value={formValues.tagline}
                            onChange={(e) => renderProps.setFieldValue('tagline', e.target.value)}
                          />
                        </Form.Group>
                      </div>
                      <div className="mt-3">
                        <Form.Check
                          className="agree-new-company"
                          type="checkbox"
                          label="I verify that I am an authorized representative of this organization and have the right to act on its behalf in the creation and management of this page. The organization and I agree to the additional terms for Pages."
                          value={formValues.termsAgree}
                          onChange={(e) => renderProps.setFieldValue('termsAgree', e.target.checked)}
                        />
                      </div>
                      <div className="mt-3 d-flex justify-content-end">
                        <Button type="submit" size="lg" variant="primary">Create Page</Button>
                      </div>
                    </div>
                    <div style={{ flex: 1, top: 70, alignSelf: 'flex-start' }} className="ml-3 position-sticky">
                      <div className="border shadow">
                        <h4 className="text-primary p-3 border-bottom">
                          Page Preview
                        </h4>
                        <div className="p-3">
                          <div className="d-flex">
                            <div>
                              <img src="/assets/icons/dummy-company.png" width={110} alt="dummy-company" />
                            </div>
                            <div className="ml-3">
                              <p className="text-muted font-size-18 mb-1">Institution Name</p>
                              <p className="text-muted font-size-14">Industry</p>
                              <p className="text-muted font-size-14">Tagline</p>
                            </div>
                          </div>
                          <div className="mt-3 blue-kind-box" />
                        </div>
                      </div>
                    </div>
                  </div>
                </Container>
              </FormikForm>
            )
          }}
        </Formik>
      </React.Fragment>
  )
}

export default NewCompany;
