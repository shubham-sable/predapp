import React from 'react';

import { Button } from 'react-bootstrap';

const NotificationContents = () => {
  return (
    <div className="mx-3" style={{ flex: 1 }}>
      <div className="border shadow w-100">
        <h3 className="border-bottom m-0 px-3 py-3">All Notifications for Freelance Man</h3>
        <div className="text-center px-3">
          <img src="/assets/notifications.svg" alt="notifications" />
          <h4 className="font-size-22 font-weight-normal">You don’t have any notifications</h4>
          <p className="font-size-16 mt-3">Posting an update is the most effective way to start a conversation, and create word or mouth for your business.</p>
          <Button className="rounded-0 my-2 font-size-14" variant="primary">Post an Update</Button>
        </div>
      </div>
    </div>
  )
}

export default NotificationContents
