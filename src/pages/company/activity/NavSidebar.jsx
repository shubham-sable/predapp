import React from 'react';

import { ListGroup } from 'react-bootstrap';

import StickyBox from 'react-sticky-box';
import { checkEnterAndClick } from '../../../utils/helpers';

const NavSidebar = ({ setActiveMenu, activeMenu }) => {
  return (
    <StickyBox className="border shadow" offsetTop={76} offsetBottom={10} style={{ flex: '0 0 216px', alignSelf: 'flex-start' }}>
      <div className="border-right" style={{ flex: '0 0 200px' }}>
        <ListGroup className="border-bottom" variant="flush">
          <ListGroup.Item
            className={`font-weight-bold font-size-15 activity-links ${activeMenu === 'show_all' ? 'active' : ''}`}
            role="button"
            tabIndex="0"
            aria-pressed="false"
            onClick={() => setActiveMenu('show_all')}
            onKeyDown={checkEnterAndClick(setActiveMenu, 'show_all')}
          >
            Show All
          </ListGroup.Item>
          <ListGroup.Item
            className={`font-weight-bold font-size-15 activity-links ${activeMenu === 'reactions' ? 'active' : ''}`}
            role="button"
            tabIndex="0"
            aria-pressed="false"
            onClick={() => setActiveMenu('reactions')}
            onKeyDown={checkEnterAndClick(setActiveMenu, 'reactions')}
          >
            Reactions
          </ListGroup.Item>
          <ListGroup.Item
            className={`font-weight-bold font-size-15 activity-links ${activeMenu === 'shares' ? 'active' : ''}`}
            role="button"
            tabIndex="0"
            aria-pressed="false"
            onClick={() => setActiveMenu('shares')}
            onKeyDown={checkEnterAndClick(setActiveMenu, 'shares')}
          >
            Shares
          </ListGroup.Item>
          <ListGroup.Item
            className={`font-weight-bold font-size-15 activity-links ${activeMenu === 'comments' ? 'active' : ''}`}
            role="button"
            tabIndex="0"
            aria-pressed="false"
            onClick={() => setActiveMenu('comments')}
            onKeyDown={checkEnterAndClick(setActiveMenu, 'comments')}
          >
            Comments
          </ListGroup.Item>
          <ListGroup.Item
            className={`font-weight-bold font-size-15 activity-links ${activeMenu === 'mentions' ? 'active' : ''}`}
            role="button"
            tabIndex="0"
            aria-pressed="false"
            onClick={() => setActiveMenu('mentions')}
            onKeyDown={checkEnterAndClick(setActiveMenu, 'mentions')}
          >
            Mentions
          </ListGroup.Item>
        </ListGroup>
      </div>
    </StickyBox>
  )
}

export default NavSidebar
