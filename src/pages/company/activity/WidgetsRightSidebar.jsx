import React from 'react';

import { Button } from 'react-bootstrap';

import StickyBox from "react-sticky-box";

const WidgetsRightSidebar = () => {
  return (
    <StickyBox offsetTop={76} offsetBottom={10} style={{ flex: '0 0 312px', alignSelf: 'flex-start' }}>
      <div className="border shadow">
        <div className="border-bottom py-2 px-3">
          <h3 className="mb-0">Post highlights</h3>
          <p className="m-0 mt-1 text-muted ">In the last 30 days</p>
        </div>
        <div className="text-center px-3">
          <img src="/assets/highlights.svg" alt="notifications" />
          <h4 className="font-size-20 font-weight-normal">No highlights</h4>
          <p className="font-size-14 mt-3">No recent post to highlight. Stuck on what to post? Browse our suggestions in Content Suggestions</p>
          <Button className="rounded-0 my-2 font-size-14" variant="outline-primary">Browser suggestions</Button>
        </div>
      </div>
    </StickyBox>
  )
}

export default WidgetsRightSidebar
