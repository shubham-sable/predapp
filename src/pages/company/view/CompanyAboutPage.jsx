import React, { useEffect } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../../actions/CompanyActions';
import { changeNavTitle } from '../../../actions/common';

import { Container, Table, Badge } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExternalLinkSquareAlt } from '@fortawesome/free-solid-svg-icons';

import BasicDetails from './BasicDetails';

const CompanyAboutPage = (props) => {
  const { changeNavTitle } = props;

  useEffect(() => {
    const titleSettings = {
      title: 'Freelance Man'
    }
    changeNavTitle(titleSettings);
  }, [changeNavTitle]);

  return (
    <div className="company-member-view">
      <BasicDetails />
      <Container className="my-3 mx-1200">
        <div className="border shadow p-3 w-100">
          <h3 className="font-size-22">Overview</h3>
          <p className="text-muted font-size-14">test</p>
          <Table className="font-size-15" style={{ maxWidth: 500 }} responsive="sm" borderless>
            <tbody>
              <tr>
                <td className="font-weight-bold pl-0">Website</td>
                <td className="text-muted">http://predapp.com</td>
              </tr>
              <tr>
                <td className="font-weight-bold pl-0">Industry</td>
                <td className="text-muted">Heart Institute</td>
              </tr>
              <tr>
                <td className="font-weight-bold pl-0">Hospital Size</td>
                <td className="text-muted">11-50 Employees</td>
              </tr>
              <tr>
                <td className="font-weight-bold pl-0">Headquarters</td>
                <td className="text-muted">Heidelberg, Germany</td>
              </tr>
              <tr>
                <td className="font-weight-bold pl-0">Type</td>
                <td className="text-muted">Sole Proprietorship</td>
              </tr>
            </tbody>
          </Table>
        </div>
        <div className="mt-3 border shadow p-3 w-100">
          <h3 className="font-size-22 pb-2">Locations (3)</h3>
          <div className="locations-listing border-top">
            <div className="border-bottom py-3">
              <Badge className="font-size-14" variant="primary">Primary</Badge>
              <p className="font-weight-bold font-size-16 mt-2 mb-1">Headquarters</p>
              <p className="font-size-16 mb-2">SGVP Circle, Gandhinagar, Gujarat IN</p>
              <a className="font-size-15" href="https://www.bing.com/maps?where=SGVP%20Circle%2C%20Gandhinagar%2C%20Gujarat%2C%20IN" style={{ fontWeight: 600 }}>Get directions &nbsp; <FontAwesomeIcon icon={faExternalLinkSquareAlt} /></a>
            </div>
            <div className="border-bottom py-3">
              <Badge className="font-size-14" variant="primary">Secondary</Badge>
              <p className="font-weight-bold font-size-16 mt-2 mb-1">Headquarters</p>
              <p className="font-size-16 mb-2">Kurfürsten-Anlage 61, 69115 Heidelberg, Germany</p>
              <a className="font-size-15" href="https://www.bing.com/maps?where=SGVP%20Circle%2C%20Gandhinagar%2C%20Gujarat%2C%20IN" style={{ fontWeight: 600 }}>Get directions &nbsp; <FontAwesomeIcon icon={faExternalLinkSquareAlt} /></a>
            </div>
            <div className="py-3">
              <Badge className="font-size-14" variant="primary">Secondary</Badge>
              <p className="font-weight-bold font-size-16 mt-2 mb-1">Headquarters</p>
              <p className="font-size-16 mb-2">Vastrapur, Ahmedabad, Gujarat IN</p>
              <a className="font-size-15" href="https://www.bing.com/maps?where=SGVP%20Circle%2C%20Gandhinagar%2C%20Gujarat%2C%20IN" style={{ fontWeight: 600 }}>Get directions &nbsp; <FontAwesomeIcon icon={faExternalLinkSquareAlt} /></a>
            </div>
          </div>
        </div>
      </Container>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    ...state.companyAdminView
  };
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ ...Actions, changeNavTitle }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CompanyAboutPage);
