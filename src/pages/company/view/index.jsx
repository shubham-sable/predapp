import React, { useEffect } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../../actions/CompanyActions';
import { changeNavTitle } from '../../../actions/common';

import { Spinner, Container, ListGroup, Button } from 'react-bootstrap';
import InfiniteScroll from 'react-infinite-scroller';
import StickyBox from "react-sticky-box";

import PostEditor from '../../../components/PostEditor';
import SinglePost from '../../../components/PostsListing/SinglePost';
import BasicDetails from './BasicDetails';

const CompanyViewPage = (props) => {
  const {
    loading,
    postsList,
    savePostLikeUnlike,
    saveCommentLikeUnlike,
    getAllPostsListAction,
    resetPostListAction,
    currentPage,
    totalPosts,
    limit,
    commentsLimit,
    loadMoreCommentsAction,
    changeNavTitle,
  } = props;

  useEffect(() => {
    const titleSettings = {
      title: 'Freelance Man',
      icon: '/assets/icons/news-feed.svg'
    }
    changeNavTitle(titleSettings);
    return () => {
      resetPostListAction();
    };
  }, [changeNavTitle, resetPostListAction]);

  useEffect(() => {
    if (loading) {
      getAllPostsListAction(currentPage, limit);
    }
  }, [getAllPostsListAction, currentPage, limit, loading]);

  const handlePostSubmit = (value) => {
    const postContent = value;
    console.log('postContent', postContent);
  };

  const handleChangePageNumber = (pageNumber) => {
    if (pageNumber !== currentPage) {
      getAllPostsListAction(pageNumber, limit);
    }
  }

  const postLikeUnlikeHandle = (postId) => {
    savePostLikeUnlike(postId);
  }

  const commentLikeUnlikeHandle = (postId, commentId, isReply, replyCommentId) => {
    saveCommentLikeUnlike(postId, commentId, isReply, replyCommentId);
  }

  const handleLoadMoreComments = (postId, pageNumber) => {
    loadMoreCommentsAction(postId, pageNumber, commentsLimit)
  }

  const totalPages = Math.ceil(totalPosts / limit);

  return (
    <div className="company-member-view">
      <BasicDetails />
      <Container className="mt-3 post-content d-flex mx-1200">
        <div className="mr-3 post-content-listing" style={{ flex: 1 }}>
          <PostEditor onSubmit={handlePostSubmit} classNames="mt-0" />
          <InfiniteScroll
            pageStart={currentPage}
            initialLoad={false}
            loadMore={handleChangePageNumber}
            hasMore={currentPage + 1 <= totalPages}
            loader={
              <div className="d-flex card__box--post justify-content-center pt-2 pb-2" key={0}>
                <Spinner animation="border" role="status">
                  <span className="sr-only">Loading...</span>
                </Spinner>
              </div>
            }
          >
            {postsList.map((post, index) => {
              return (
                <SinglePost key={`post-${post.id}-${index}`} data={post} commentsLimit={commentsLimit} postLikeUnlikeHandle={postLikeUnlikeHandle} commentLikeUnlikeHandle={commentLikeUnlikeHandle} handleLoadMoreComments={handleLoadMoreComments} />
              );
            })}
          </InfiniteScroll>
        </div>
        <StickyBox offsetTop={136} offsetBottom={10} style={{ flex: '0 0 362px', alignSelf: 'flex-start' }}>
          <div className="border shadow">
            <div className="border-bottom py-3 px-3">
              <h4 className="mb-0">People also viewed</h4>
            </div>
            <ListGroup className="border-bottom" variant="flush">
              <ListGroup.Item className="d-flex align-items-center">
                <img className="rounded-circle align-self-start" width={48} height={48} src="https://testcreative.co.uk/wp-content/uploads/2018/08/logo.png" alt="chetan8300" />
                <div className="mx-2" style={{ flex: 1 }}>
                  <div className="font-weight-bold font-size-16 mb-1">PredApp Hospital</div>
                  <div className="text-muted break-words lt-line-clamp lt-line-clamp--multi-line font-size-14 m-0" style={{ WebkitLineClamp: 2 }}>Orthopedic Hospital</div>
                </div>
                <Button variant="outline-primary" className="rounded-0 font-size-12 px-2 py-1 font-weight-bold">+ Follow</Button>
              </ListGroup.Item>
              <ListGroup.Item className="d-flex align-items-center">
                <img className="rounded-circle align-self-start" width={48} height={48} src="https://testcreative.co.uk/wp-content/uploads/2018/08/logo.png" alt="chetan8300" />
                <div className="mx-2" style={{ flex: 1 }}>
                  <div className="font-weight-bold font-size-16 mb-1">Falana Hospital</div>
                  <div className="text-muted break-words lt-line-clamp lt-line-clamp--multi-line font-size-14 m-0" style={{ WebkitLineClamp: 2 }}>Heart Institute</div>
                </div>
                <Button variant="outline-primary" className="rounded-0 font-size-12 px-2 py-1 font-weight-bold">+ Follow</Button>
              </ListGroup.Item>
              <ListGroup.Item className="d-flex align-items-center">
                <img className="rounded-circle align-self-start" width={48} height={48} src="https://testcreative.co.uk/wp-content/uploads/2018/08/logo.png" alt="chetan8300" />
                <div className="mx-2" style={{ flex: 1 }}>
                  <div className="font-weight-bold font-size-16 mb-1">ICM Hospital</div>
                  <div className="text-muted break-words lt-line-clamp lt-line-clamp--multi-line font-size-14 m-0" style={{ WebkitLineClamp: 2 }}>Prosthetic Legs Institute</div>
                </div>
                <Button variant="outline-primary" className="rounded-0 font-size-12 px-2 py-1 font-weight-bold">+ Follow</Button>
              </ListGroup.Item>
            </ListGroup>
            <Button className="rounded-0 text-color-blue font-weight-bold" variant size="lg" block>
              Show more...
            </Button>
          </div>
        </StickyBox>
      </Container>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    ...state.companyAdminView
  };
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ ...Actions, changeNavTitle }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CompanyViewPage);
