import React from 'react'

import { Container, Button, Nav } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faExternalLinkSquareAlt } from '@fortawesome/free-solid-svg-icons';

import { NavLink, useParams } from "react-router-dom";
import styled from 'styled-components';
import StickyBox from "react-sticky-box";

const CompanyProfileImage = styled.div`
  background: transparent ${props => props.bgimage ? `${`url(${props.bgimage})`}` : 'url("/assets/icons/camera.png")'} 50% no-repeat;
  background-size: ${props => props.bgimage && props.bgimage !== '' ? 'cover' : 'auto'};
  background-color: rgba(243,246,248);
  max-width: ${props => props.width ? props.width + 'px' : '0px'};
  max-height: ${props => props.width ? props.width + 'px' : '0px'};
  width: 100%;
  height: ${props => props.width ? props.width + 'px' : '0px'};
  margin-top: ${props => props.margintop ? props.margintop : 0};
  position: relative;
`;

const HeaderCover = styled.div`
  height: 180px;
  width: 100%;
  background-color: #fff;
  position: relative;
  background-repeat: no-repeat;
  background-position: center center;

  .edit-cover-icon {
    border: 0;
    box-shadow: none;
    height: 35px;
    width: 35px;
    position: absolute;
    right: 10px;
    top: 10px;
    border-radius: 50%;
    color: rgba(0,0,0,.6);
    outline: none;

    &:hover, &:focus, &:active {
      background-color: #e5e5e5;
      color: #000;
    }
  }
`;

const StickyNav = styled(Nav)`
  background-color: #fff;
  z-index: 1;

  &.nav-tabs .nav-link:not(.active) {
    border: 0 !important;
  }
`;

const BasicDetails = () => {
  const params = useParams();
  return (
    <React.Fragment>
      <Container className="mt-3 mx-1200">
        <div className="border">
          <HeaderCover className="" style={{ backgroundImage: 'url(https://images.pexels.com/photos/56759/pexels-photo-56759.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260)' }} />
          <div className="p-3">
            <div className="d-flex align-items-center justify-content-between">
              <div className="d-flex align-items-center flex-grow-1">
                <CompanyProfileImage className="shadow border" bgimage="/logo.svg" margintop={"-90px"} width={120} />
                <div className="ml-3">
                  <h3 className="m-0">Freelance Man</h3>
                  <p className="mt-1 font-size-14">
                    <span className="text-muted">Information Technology & Services - Gandhinaga, Gujarat</span>
                  </p>
                </div>
              </div>
            </div>
            <div className="mt-2">
              <Button className="rounded-0 font-size-14" variant="primary">
                <FontAwesomeIcon icon={faPlus} />&nbsp; Follow
              </Button>
              <Button className="rounded-0 font-size-14 ml-2" variant="outline-primary" style={{ border: '1px solid' }}>
                Visit Website &nbsp;
                <FontAwesomeIcon icon={faExternalLinkSquareAlt} />
              </Button>
            </div>
          </div>
        </div>
      </Container>
      <StickyBox className="bg-white" offsetTop={60} style={{ zIndex: 1 }}>
        <Container className="mx-1200 bg-white">
          <StickyNav className="shadow-sm border-left border-right border-bottom rounded-0" fill variant="tabs" defaultActiveKey="about">
            <Nav.Item>
              <NavLink to={`/company/${params.companyId}`} exact className="d-flex justify-content-center align-items-center betterme-link font-size-15" eventkey="about">Home</NavLink>
            </Nav.Item>
            <Nav.Item>
              <NavLink to={`/company/${params.companyId}/about`} exact className="d-flex justify-content-center align-items-center betterme-link font-size-15" eventkey="post">About</NavLink>
            </Nav.Item>
            <Nav.Item>
              <NavLink to={`/company/${params.companyId}/jobs`} exact className="d-flex justify-content-center align-items-center betterme-link font-size-15" eventkey="members">Jobs</NavLink>
            </Nav.Item>
            <Nav.Item>
              <NavLink to={`/company/${params.companyId}/people`} className="d-flex justify-content-center align-items-center betterme-link font-size-15" eventkey="events">People</NavLink>
            </Nav.Item>
          </StickyNav>
        </Container>
      </StickyBox>
    </React.Fragment>
  )
}

export default BasicDetails;
