import React, { useEffect } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../../actions/CompanyActions';
import { changeNavTitle } from '../../../actions/common';

import { Container, Row, Col } from 'react-bootstrap';

import BasicDetails from './BasicDetails';
import Autocomplete from '../../../components/SelectAutocomplete';
import PeopleCard from '../../../components/Company/PeopleCard';

const CompanyPeople = (props) => {
  const { changeNavTitle } = props;

  useEffect(() => {
    const titleSettings = {
      title: 'Freelance Man'
    }
    changeNavTitle(titleSettings);
  }, [changeNavTitle]);

  return (
    <div className="company-member-view">
      <BasicDetails />
      <Container className="my-3 mx-1200">
        <div className="p-3 border shadow">
          <h4>48 employees</h4>
          <Autocomplete classes="people-search-box mt-3" placeholder="Search employees by title, keyword or school..." />
        </div>

        <Row>
          <Col className="mt-3" sm={4} md={3}>
            <PeopleCard
              coverImage="/assets/company-people-bg.svg"
              profileImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
              id={1}
              name="Chetan Godhani"
              tagline="Freelance Full Stack Developer | React.JS | Node.js | Laravel | Express.JS"
              connections={0}
              connectionImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
            />
          </Col>
          <Col className="mt-3" sm={4} md={3}>
            <PeopleCard
              coverImage="/assets/company-people-bg.svg"
              profileImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
              id={1}
              name="Chetan Godhani"
              tagline="Freelance Full Stack Developer | React.JS | Node.js | Laravel | Express.JS"
              connections={14}
              connectionImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
            />
          </Col>
          <Col className="mt-3" sm={4} md={3}>
            <PeopleCard
              coverImage="/assets/company-people-bg.svg"
              profileImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
              id={1}
              name="Chetan Godhani"
              tagline="Freelance Full Stack Developer | React.JS | Node.js | Laravel | Express.JS"
              connections={14}
              connectionImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
            />
          </Col>
          <Col className="mt-3" sm={4} md={3}>
            <PeopleCard
              coverImage="/assets/company-people-bg.svg"
              profileImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
              id={1}
              name="Chetan Godhani"
              tagline="Freelance Full Stack Developer | React.JS | Node.js | Laravel | Express.JS"
              connections={14}
              connectionImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
            />
          </Col>
          <Col className="mt-3" sm={4} md={3}>
            <PeopleCard
              coverImage="/assets/company-people-bg.svg"
              profileImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
              id={1}
              name="Chetan Godhani"
              tagline="Freelance Full Stack Developer | React.JS | Node.js | Laravel | Express.JS"
              connections={14}
              connectionImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
            />
          </Col>
          <Col className="mt-3" sm={4} md={3}>
            <PeopleCard
              coverImage="/assets/company-people-bg.svg"
              profileImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
              id={1}
              name="Chetan Godhani"
              tagline="Freelance Full Stack Developer | React.JS | Node.js | Laravel | Express.JS"
              connections={14}
              connectionImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
            />
          </Col>
          <Col className="mt-3" sm={4} md={3}>
            <PeopleCard
              coverImage="/assets/company-people-bg.svg"
              profileImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
              id={1}
              name="Chetan Godhani"
              tagline="Freelance Full Stack Developer | React.JS | Node.js | Laravel | Express.JS"
              connections={14}
              connectionImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
            />
          </Col>
          <Col className="mt-3" sm={4} md={3}>
            <PeopleCard
              coverImage="/assets/company-people-bg.svg"
              profileImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
              id={1}
              name="Chetan Godhani"
              tagline="Freelance Full Stack Developer | React.JS | Node.js | Laravel | Express.JS"
              connections={14}
              connectionImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
            />
          </Col>
          <Col className="mt-3" sm={4} md={3}>
            <PeopleCard
              coverImage="/assets/company-people-bg.svg"
              profileImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
              id={1}
              name="Chetan Godhani"
              tagline="Freelance Full Stack Developer | React.JS | Node.js | Laravel | Express.JS"
              connections={14}
              connectionImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
            />
          </Col>
          <Col className="mt-3" sm={4} md={3}>
            <PeopleCard
              coverImage="/assets/company-people-bg.svg"
              profileImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
              id={1}
              name="Chetan Godhani"
              tagline="Freelance Full Stack Developer | React.JS | Node.js | Laravel | Express.JS"
              connections={14}
              connectionImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
            />
          </Col>
          <Col className="mt-3" sm={4} md={3}>
            <PeopleCard
              coverImage="/assets/company-people-bg.svg"
              profileImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
              id={1}
              name="Chetan Godhani"
              tagline="Freelance Full Stack Developer | React.JS | Node.js | Laravel | Express.JS"
              connections={14}
              connectionImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
            />
          </Col>
          <Col className="mt-3" sm={4} md={3}>
            <PeopleCard
              coverImage="/assets/company-people-bg.svg"
              profileImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
              id={1}
              name="Chetan Godhani"
              tagline="Freelance Full Stack Developer | React.JS | Node.js | Laravel | Express.JS"
              connections={14}
              connectionImage="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500"
            />
          </Col>
        </Row>
      </Container>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    ...state.companyAdminView
  };
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ ...Actions, changeNavTitle }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CompanyPeople);
