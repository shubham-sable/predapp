import React, { useEffect } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../../actions/CompanyActions';
import { changeNavTitle } from '../../../actions/common';

import { Container } from 'react-bootstrap';

import BasicDetails from './BasicDetails';
import JobCard from '../../../components/Jobs/JobCard';
import StickyBox from "react-sticky-box";

const CompanyJobListing = (props) => {
  const { changeNavTitle } = props;

  useEffect(() => {
    const titleSettings = {
      title: 'Freelance Man'
    }
    changeNavTitle(titleSettings);
  }, [changeNavTitle]);

  return (
    <div className="company-member-view">
      <BasicDetails />
      <Container className="my-3 mx-1200 d-flex">
        <div className="mr-3" style={{ flex: 1 }}>
          <div className="border shadow px-3 py-5 text-center">
            <img src="/assets/no-jobs.svg" alt="no-jobs" />
            <h3 className="mt-2">There are no jobs right now.</h3>
            <p className="font-size-14 m-0">Create a job alert and we’ll let you know when relevant jobs are posted.</p>
          </div>
          <JobCard
            jobTitle="UI Designer"
            companyName="ABC LLC"
            jobLocation="New York"
            jobType="Contract"
            description="Exp: 4-7 years; About the company At SalesHandy, were building the sales outreach platform. Since our founding in 2015, weve grown to become a profitable SaaS company. SalesHandy is run by 25 high-performing, happy people that are dedicated to building a product our customers love. Some of the key achievements We have more than 20k users using our tool on a regular basis with more than 2.5k paid users We are having customers from across the globe and from some of the fortune 500s too. We are a team of 25 who made this happen from Ahmedabad, India. Our marketing tactics have got a mention in a book With more than 250 reviews we are rated 4.5 on google chrome store and other online software discovery platforms We are adding hundreds of new customers & tons of new features every month What will I be doing? Developing user interface components and implementing them following well-known React.js workflows (such as Flux or Redux). Building reusable components and front-end libraries for future use. Optimizing components for maximum"
            skills={['UI/UX Designer', 'Sketch', 'Adobe CC']}
            createdAt={new Date()}
            salaryPerYear={`$100,000 - $120,000 per annual`}
          />
          <JobCard
            jobTitle="UI Designer"
            companyName="ABC LLC"
            jobLocation="Germany"
            jobType="Temporary"
            description="Exp: 4-7 years; About the company At SalesHandy, were building the sales outreach platform. Since our founding in 2015, weve grown to become a profitable SaaS company. SalesHandy is run by 25 high-performing, happy people that are dedicated to building a product our customers love. Some of the key achievements We have more than 20k users using our tool on a regular basis with more than 2.5k paid users We are having customers from across the globe and from some of the fortune 500s too. We are a team of 25 who made this happen from Ahmedabad, India. Our marketing tactics have got a mention in a book With more than 250 reviews we are rated 4.5 on google chrome store and other online software discovery platforms We are adding hundreds of new customers & tons of new features every month What will I be doing? Developing user interface components and implementing them following well-known React.js workflows (such as Flux or Redux). Building reusable components and front-end libraries for future use. Optimizing components for maximum"
            skills={['UI/UX Designer', 'Sketch', 'Adobe CC']}
            createdAt={new Date()}
            salaryPerYear={`$100,000 - $120,000 per annual`}
          />
          <JobCard
            jobTitle="UI Designer"
            companyName="ABC LLC"
            jobLocation="Remote"
            jobType="Part-Time"
            description="Exp: 4-7 years; About the company At SalesHandy, were building the sales outreach platform. Since our founding in 2015, weve grown to become a profitable SaaS company. SalesHandy is run by 25 high-performing, happy people that are dedicated to building a product our customers love. Some of the key achievements We have more than 20k users using our tool on a regular basis with more than 2.5k paid users We are having customers from across the globe and from some of the fortune 500s too. We are a team of 25 who made this happen from Ahmedabad, India. Our marketing tactics have got a mention in a book With more than 250 reviews we are rated 4.5 on google chrome store and other online software discovery platforms We are adding hundreds of new customers & tons of new features every month What will I be doing? Developing user interface components and implementing them following well-known React.js workflows (such as Flux or Redux). Building reusable components and front-end libraries for future use. Optimizing components for maximum"
            skills={['UI/UX Designer', 'Sketch', 'Adobe CC']}
            createdAt={new Date()}
            salaryPerYear={`$100,000 - $120,000 per annual`}
          />
          <JobCard
            jobTitle="Consultant Gynaecology"
            companyName="Indira IVF"
            jobLocation="Jamshedpur, Jharkhand"
            jobType="Full Time"
            description="Exp: 4-7 years; About the company At SalesHandy, were building the sales outreach platform. Since our founding in 2015, weve grown to become a profitable SaaS company. SalesHandy is run by 25 high-performing, happy people that are dedicated to building a product our customers love. Some of the key achievements We have more than 20k users using our tool on a regular basis with more than 2.5k paid users We are having customers from across the globe and from some of the fortune 500s too. We are a team of 25 who made this happen from Ahmedabad, India. Our marketing tactics have got a mention in a book With more than 250 reviews we are rated 4.5 on google chrome store and other online software discovery platforms We are adding hundreds of new customers & tons of new features every month What will I be doing? Developing user interface components and implementing them following well-known React.js workflows (such as Flux or Redux). Building reusable components and front-end libraries for future use. Optimizing components for maximum"
            skills={['UI/UX Designer', 'Sketch', 'Adobe CC']}
            createdAt={new Date()}
            salaryPerYear={`Rs.120,000 - Rs.140,000 per month`}
          />
        </div>
        <StickyBox offsetTop={136} offsetBottom={10} style={{ flex: '0 0 362px', alignSelf: 'flex-start' }}>
          <div className="border shadow p-3 d-flex align-items-center justify-content-center" style={{ minHeight: 100 }}>
            <h4 className="mb-0">No Filters available</h4>
          </div>
        </StickyBox>
      </Container>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    ...state.companyAdminView
  };
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ ...Actions, changeNavTitle }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CompanyJobListing);
