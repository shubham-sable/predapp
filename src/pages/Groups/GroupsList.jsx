
import React, { useState, useEffect } from 'react';

//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../actions/GroupActions';
import { changeNavTitle } from '../../actions/common';

import { Image, OverlayTrigger, Tooltip, Spinner, Container, InputGroup, Dropdown } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle, faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';

import InfiniteScroll from 'react-infinite-scroller';
import { Translate } from 'react-redux-i18n';
import { Link } from 'react-router-dom';

import { FloatingButton } from '../../components/Group/Group.style';
import CreateGroupModal from './../../components/GroupModal/CreateGroupModal';
import Autocomplete from '../../components/SelectAutocomplete';

const GroupsList = (props) => {
  const { groupList, groupListLoading, groupListCurrentPage, groupListLimit, fetchGroupListAction, resetLoadingFlagAction, groupListTotal, changeNavTitle } = props;
  const [showCreateGroupModal, setShowCreateGroupModal] = useState(false);

  const createGroupSubmit = (values) => {
    console.log('values', values);
    // props.createGroup();
  }

  useEffect(() => {
    const titleSettings = {
      title: 'Groups',
      icon: '/assets/icons/group.png'
    };
    changeNavTitle(titleSettings);
    return () => {
      resetLoadingFlagAction();
    };
  }, [changeNavTitle, resetLoadingFlagAction]);

  useEffect(() => {
    if (groupListLoading) {
      fetchGroupListAction(groupListCurrentPage, groupListLimit);
    }
  }, [fetchGroupListAction, groupListLoading, groupListCurrentPage, groupListLimit]);

  const handleChangePageNumber = (pageNumber) => {
    if (pageNumber !== groupListCurrentPage) {
      fetchGroupListAction(pageNumber, groupListLimit);
    }
  }

  const groupListTotalPages = Math.ceil(groupListTotal / groupListLimit);

  return (
    <React.Fragment>
      {groupListLoading ?
        <div className="d-flex justify-content-center pt-4">
          <Spinner animation="border" role="status">
            <span className="sr-only">Loading...</span>
          </Spinner>
        </div>
        :
        <React.Fragment>
          <div className="border-bottom shadow-sm p-4">
            <Container className="mx-1200">
              <div className="mx-2 d-flex justify-content-between align-items-center">
                <InputGroup className="header-search-bar">
                  <InputGroup.Prepend>
                    <InputGroup.Text>
                      <FontAwesomeIcon icon={faSearch} />
                    </InputGroup.Text>
                  </InputGroup.Prepend>
                  <Autocomplete classes="mt-0 font-size-14" placeholder="Search Groups..." />
                </InputGroup>
                <Dropdown className="group-sort-by-dropdown" drop="down">
                  <Dropdown.Toggle variant="default" id="group-sort-by-toggle">
                    Sort By
                  </Dropdown.Toggle>

                  <Dropdown.Menu className="p-0 group-sort-by-dropdown-menu">
                    <Dropdown.Item>Name</Dropdown.Item>
                    <Dropdown.Item>Number of Members</Dropdown.Item>
                    <Dropdown.Item>Most Recent</Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              </div>
            </Container>
          </div>
          <InfiniteScroll
            pageStart={groupListCurrentPage}
            initialLoad={false}
            loadMore={handleChangePageNumber}
            hasMore={groupListCurrentPage + 1 <= groupListTotalPages}
            loader={
              <div className="d-flex justify-content-center pt-2 pb-2" key={0}>
                <Spinner animation="border" role="status">
                  <span className="sr-only">Loading...</span>
                </Spinner>
              </div>
            }
          >
            <div className="d-flex flex-wrap container mx-1200">
              {groupList.map((group, index) => {
                return (
                  <div className="tile border m-2 mt-3 w-auto" key={`group-${group.groupId}-${index}`} style={{ flex: '1 0 300px' }}>
                    <div className="wrapper d-flex p-2">
                      <Link to={`/group/${group.groupId}`}>
                        <Image
                          src={group.iconImage}
                          className="rounded"
                          style={{ overflow: 'hidden', height: 100, width: 100 }}
                        />
                      </Link>
                      <div className="pl-3">
                        <div className="header border-bottom-0 p-0">
                          <Link className="pt-1 d-block font-size-16 text-primary" to={`/group/${group.groupId}`}>
                            {group.groupTitle}
                          </Link>
                        </div>
                        <div className="dates pt-2">
                          <strong><Translate value="groups.total_members" />: {group.totalMembersInGroup}</strong>
                        </div>
                        <div className="pt-2 pr-0" style={{ lineHeight: 1.4 }}>
                          <FontAwesomeIcon icon={faInfoCircle} style={{ color: '#59687f' }} />
                          <span className="pl-2">
                            This Group is created for Sales Team for communication purpose with the
                            supply chain department and Sales managers.
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </InfiniteScroll>
        </React.Fragment>
      }
      <OverlayTrigger
        placement={"top"}
        overlay={
          <Tooltip className="font-size-14">
            Create Group
          </Tooltip>
        }
      >
        <FloatingButton className="btn-primary" bottom={40} onClick={() => setShowCreateGroupModal(true)} fontsize={25}>
          <FontAwesomeIcon icon={faPlus} />
        </FloatingButton>
      </OverlayTrigger>
      <CreateGroupModal
        show={showCreateGroupModal}
        onHide={() => setShowCreateGroupModal(false)}
        handleSubmit={createGroupSubmit}
      />
    </React.Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    ...state.common,
    groupList: state.group.groupList,
    groupListLoading: state.group.groupListLoading,
    groupListCurrentPage: state.group.groupListCurrentPage,
    groupListLimit: state.group.groupListLimit,
    groupListTotal: state.group.groupListTotal,
  };
}

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators({ ...Actions, changeNavTitle }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GroupsList);
