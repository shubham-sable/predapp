import React, { useState, useEffect } from "react";

import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { changeNavTitle } from "../../actions/common";
import * as Actions from '../../actions/FollowCoworkersAction';

import { Card, Image, Button, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import styled from 'styled-components';
import axios from 'axios';

import { Translate } from "react-redux-i18n";

const SuggestionsImage = styled(Image)`
 height: 80px !important;
 width: 80px !important;
 object-fit: contain !important;
 border-radius: 50%;
`;

const FollowedByText = styled.div`
  color: #90949c;
  font-size: 12px;
  line-height: 16px;
`;

const FollowButton = styled(Button).attrs(props => ({
  nowidth: props.nowidth ? 'inherit' : '120px'
}))`
  transition: 200ms cubic-bezier(.08,.52,.52,1) background-color, 200ms cubic-bezier(.08,.52,.52,1) box-shadow, 200ms cubic-bezier(.08,.52,.52,1) transform !important;
  background-color: #f5f6f7;
  color: #4b4f56;
  padding: 4px 6px;
  border: 1px solid #ccd0d5;
  width: ${props => props.nowidth};
`;

const FollowCoworkers = props => {
  const {
    changeNavTitle,
    loadCoworkersAction,
    followAction,
    unfollowAction,
    coworkers
   } = props;

  useEffect(() => {
    const titleSettings = {
      title: 'sidebar.follow_coworkers',
      icon: '/assets/icons/follow-coworkers.png'
    }

    changeNavTitle(titleSettings);
    loadCoworkersAction();
  }, [changeNavTitle, loadCoworkersAction])

  const cardBoxStyle = {
    maxWidth: 700,
    minWidth: 600
  };

  const handleFollowButton = (value) =>{
    !value.follow?
    followAction(value._id)
    :
    unfollowAction(value._id)
  }

  return (
    <React.Fragment>
      <Card className="mt-4 card__box--post border" style={{ ...cardBoxStyle }} data-test="my_component">
        <Card.Body className="p-4">
          <div className="text-secondary font-size-14">
            <p className="m-0">
              <Translate value="follow_coworkers.heading_title" />
            </p>
          </div>
        </Card.Body>
      </Card>
      <Card className="card__box--post border" style={cardBoxStyle}>
        <Card.Header className="d-flex align-items-center">
          <FontAwesomeIcon className="font-size-16" icon={faStar} />
          <h5 className="m-0 pl-2 font-weight-bold">
            <Translate value="follow_coworkers.suggestions" />
          </h5>
        </Card.Header>
        <Card.Header className="p-4 border-bottom-0 bg-white">
          <div className="mb-3 d-flex justify-content-center">
            <Image src={`/FollowCoworkers.png`} />
          </div>
          <div className="text-body d-flex flex-column align-items-center">
            <p className="m-0">
              <strong><Translate value="follow_coworkers.keep_checking_back" /></strong>
            </p>
            <p><Translate value="follow_coworkers.keep_checking_back_description" /></p>
            <FollowButton variant="default" nowidth><strong><Translate value="follow_coworkers.invite_coworkers" /></strong></FollowButton>
          </div>
        </Card.Header>
      </Card>
      <Card className="card__box--post border" style={cardBoxStyle}>
        <Card.Header className="d-flex align-items-center">
          <FontAwesomeIcon className="font-size-14" icon={faStar} />
          <h5 className="m-0 pl-2 font-weight-bold"><Translate value="follow_coworkers.suggestions" /></h5>
        </Card.Header>
        <Card.Body className="p-0">
          <Row className="m-0">
            {coworkers.length > 0 && coworkers.map((value, index) => {
              return (
                <Col xs={6} key={`coworker-${value._id}`} className={`d-flex justify-content-center p-3 border-bottom ${index % 2 === 0 ? 'border-right' : ''}`} style={{ height: 120 }} data-test='render_cards'>
                  <div className="d-flex align-items-center w-100">
                    <div className="d-flex justify-content-center align-items-center" style={{ flex: '0 0 120px', height: 80 }}>
                      <Link to={`/profile/${value._id}`}>
                        <SuggestionsImage src={'https://randomuser.me/api/portraits/men/76.jpg'} />
                      </Link>
                    </div>
                    <div className="d-flex flex-column justify-content-center ml-3">
                      <Link to={`/profile/${value._id}`} className="font-weight-bold font-size-14">
                        {`${value.firstName + " " + value.lastName}`}
                      </Link>
                      {value.follow ?
                        <FollowButton
                          onClick ={ () => handleFollowButton(value) }
                          className="ui button mt-2" variant="default" data-test='my_render_button'>
                          <strong><Translate value="follow_coworkers.following" /></strong>
                        </FollowButton>
                        :
                        <FollowButton
                          onClick ={ () => handleFollowButton(value) }
                          className="ui facebook button mt-2" variant="default" data-test='my_render_button'>
                          <strong><Translate value="follow_coworkers.follow" /></strong>
                        </FollowButton>
                      }
                    </div>
                  </div>
                </Col>
              );
            })}
          </Row>
        </Card.Body>
      </Card>
    </React.Fragment>
  );
}

// {value.isFollowed ?
//   <FollowButton className="mt-2" variant="default">
//     <strong><Translate value="follow_coworkers.following" /></strong>
//   </FollowButton>
//   :
//   <FollowButton className="mt-2" variant="default">
//     <strong><Translate onClick ={async () => await axios.delete(`http://127.0.0.1:8000/api/following/${value._id} value="follow_coworkers.follow" /></strong>
//   </FollowButton>
// }

// <FollowButton onClick ={async () => await axios.post(`http://127.0.0.1:8000/api/following/${value._id}`,{ id: '602666bd0205032fc47ea80e' })} className="mt-2" variant="default">
//   <strong><Translate value="follow_coworkers.follow" /></strong>
// </FollowButton>
// <FollowButton className="mt-2" variant="default">
//     <strong><Translate onClick ={async () => await axios.delete(`http://127.0.0.1:8000/api/following/${value._id}`,{ id: '602666bd0205032fc47ea80e' })} value="follow_coworkers.follow" /></strong>
// </FollowButton>


const mapStateToProps = state => {
  return {
    ...state.common,
    ...state.followCoworkers
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators({ ...Actions, changeNavTitle }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FollowCoworkers);
