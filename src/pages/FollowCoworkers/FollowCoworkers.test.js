import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import FollowCoworkers from './FollowCoworkers';
import { testStore , findByTestAtrr } from '../../../Utils';
import { loadCoworkersAction } from '../../actions/FollowCoworkersAction';
import moxios from 'moxios';

Enzyme.configure({ adapter: new EnzymeAdapter() });

// const store = testStore();
test('Testing in FollowCoworkersAction',()=>{

});

// const setup = (initialState) => {
//   const store = testStore(initialState);
//   const wrapper = shallow(<FollowCoworkers store={store}/>).dive().dive();
//   return wrapper;
// }
//
// test('FollowCoworkers renders without error',() => {
//   const wrapper = setup();
//   const followCoworkers_component = findByTestAtrr(wrapper, 'my_component');
//   expect(followCoworkers_component.length).toBe(1);
// });
//
// test('FollowCoworkers existance',() => {
//   const wrapper = setup();
//   const followCoworkers_component = findByTestAtrr(wrapper, 'my_component');
//   expect(followCoworkers_component.exists()).toBe(true);
// });
//
// test('FollowCoworkers store is empty',() => {
//   const wrapper = setup();
//   const followCoworkers_component = findByTestAtrr(wrapper, 'render_cards');
//   expect(followCoworkers_component.exists()).toBe(false);
// });
//
// describe('FollowCoworkers store filled', () =>{
//   beforeEach(() => {
//     moxios.install();
//   });
//   afterEach(() => {
//     moxios.uninstall();
//   });
//   test('Store filling', () => {
//     const store = testStore();
//     moxios.wait(() => {
//       const request = moxios.requests.mostRecent();
//     });
//
//     return store.dispatch(loadCoworkersAction())
//       .then(() => {
//         const newState = store.getState()
//         const wrapper = setup(newState);
//         const appComponent = findByTestAtrr(wrapper, 'render_cards');
//         expect(appComponent.exists()).toBe(true);
//       })
//   });
// });
//
// describe('Buttons in Follow Coworkers', () =>{
//   beforeEach(() => {
//     moxios.install();
//   });
//   afterEach(() => {
//     moxios.uninstall();
//   });
//   test('Let me render', () => {
//     const store = testStore();
//     moxios.wait(() => {
//       const request = moxios.requests.mostRecent();
//     });
//
//     return store.dispatch(loadCoworkersAction())
//       .then(() => {
//         const newState = store.getState()
//         const wrapper = setup(newState);
//         const appComponent = findByTestAtrr(wrapper, 'my_render_button');
//         expect(appComponent.length).toBe(10);
//       })
//   });
// });
//
// describe('Cards in Follow Coworkers', () =>{
//   beforeEach(() => {
//     moxios.install();
//   });
//   afterEach(() => {
//     moxios.uninstall();
//   });
//   test('Let me render', () => {
//     const store = testStore();
//     moxios.wait(() => {
//       const request = moxios.requests.mostRecent();
//     });
//
//     return store.dispatch(loadCoworkersAction())
//       .then(() => {
//         const newState = store.getState()
//         const wrapper = setup(newState);
//         const appComponent = findByTestAtrr(wrapper, 'render_cards');
//         expect(appComponent.length).toBe(10);
//       })
//   });
// });
