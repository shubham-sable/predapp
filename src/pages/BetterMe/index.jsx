import React, { useEffect, useState, Fragment } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../actions/BetterMeActions';
import { changeNavTitle } from '../../actions/common';

import { Badge, CardColumns, Row, Col, Image, Nav, OverlayTrigger, Tooltip, Spinner } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRedo, faPlus, faInbox, faReceipt } from '@fortawesome/free-solid-svg-icons';
import { FloatingButton } from '../../components/Group/Group.style';
import ReactEcharts from 'echarts-for-react';

import RequestFeedbackModal from '../../components/BetterMe/RequestFeedbackModal';
import SendFeedbackModal from '../../components/BetterMe/SendFeedbackModal';
import FeedbackRequestModal from '../../components/BetterMe/FeedbackRequestModal';
import ReceivedFeedbacksModal from '../../components/BetterMe/ReceivedFeedbacksModal';
import { StickyNav } from '../../components/Header/GroupHeader';
import ListCard from '../../components/BetterMe/ListCard';

import InfiniteScroll from 'react-infinite-scroller';

import UserRequestCard from '../../components/BetterMe/UserRequestCard';
import { NavLink } from 'react-router-dom';

import { Translate } from 'react-redux-i18n';

const BetterMePage = (props) => {
  const [showRequestFeedbackModal, setShowRequestFeedbackModal] = useState(false);
  const [showSendFeedbackModal, setShowSendFeedbackModal] = useState(false);
  const [showFeedbackModal, setShowFeedbackModal] = useState(false);
  const [showReceivedFeedbacksModal, setShowReceivedFeedbacksModal] = useState(false);
  const [userFeedbackObj, setuserFeedbackObj] = useState({});
  const [receivedFeedback, setReceivedFeedback] = useState([]);
  const [pieUpdated, setPieUpdated] = useState(false);
  const updatedFeedbackList = [];

  const receivedListStyle = {
    width: '675px',
    height: 'auto'
  };

  const xAxisLineData = [];
  const seriesData = [];

  const {
    receivedList,
    sentList,
    requestsList,
    ratedFeedbackBarChart,
    weeklyFeedbackData,
    fetchFeedbacksAndRequestAction,
    loadRatedCompitenciesDataAction,
    cleanUpBetterMeDataAction,
    sendFeedbackAction,
    requestFeedbackAction,
    completeRequestFeedbackAction,
    changeNavTitle,
    currentPage,
    loading,
    limit,
    totalData,

    setCompetencyRating,
    totalFeedbacks,
    mainReceivedList,
    employees,
    match: { path }
  } = props;

  let { topEmployeesData } = props;

  let totalYouRock = 0;
  let totalLetWorkOnThis = 0;
  let totalCool = 0;
  let competencyRating = [];
  let totalFeedbackCounts = 0;
  let pieces = [];

  let pageType = 'received';
  if(path === '/better-me/received') {
    pageType = "received";
  } else if(path === '/better-me/sent') {
    pageType = "sent";
  } else if(path === '/better-me/rated') {
    pageType = "rated";
  } else if(path === '/better-me/requests') {
    pageType = "requests";
  }

  topEmployeesData = topEmployeesData.sort((a, b) => { return b.feedbacks.length - a.feedbacks.length });

  useEffect(() => {
    const titleSettings = {
      title: 'sidebar.better_me',
      icon: '/assets/icons/improvement.png'
    };
    changeNavTitle(titleSettings);

    return () => {
      cleanUpBetterMeDataAction();
    }
  }, [changeNavTitle, cleanUpBetterMeDataAction]);

  useEffect(() => {
    cleanUpBetterMeDataAction()
  }, [pageType]);

  useEffect(() => {
    if(loading) {
      if(pageType === 'rated') {
        loadRatedCompitenciesDataAction();
      } else {
        fetchFeedbacksAndRequestAction(pageType, currentPage, limit);
      }
    }
  }, [fetchFeedbacksAndRequestAction, loadRatedCompitenciesDataAction, currentPage, limit])

  useEffect(() => {
    if (pieUpdated) {
      setCompetencyRating([]);
    }
  }, [pieUpdated, setCompetencyRating]);

  const barOptions = {
    color: ['#53C7E6', '#EEDB1E', '#F3ACD1'],
    yAxisindex: '5',
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow'
      }
    },
    legend: {
      data: ['You Rock', 'Let\'s work on this', 'Cool']
    },
    grid: {
      left: '1%',
      right: '1%',
      bottom: '1%',
      containLabel: true
    },
    xAxis: {
      type: 'value'
    },
    yAxis: {
      type: 'category',
      data: ratedFeedbackBarChart.xAxis || []
    },
    series: [
      {
        name: 'You Rock',
        type: 'bar',
        stack: '总量',
        label: {
          normal: {
            show: true,
            position: 'insideRight',
            color: '#000'
          }
        },
        data: ratedFeedbackBarChart.yAxis ? ratedFeedbackBarChart.yAxis.youRockData : [],
        barMaxWidth: '30%'
      },
      {
        name: 'Let\'s work on this',
        type: 'bar',
        stack: '总量',
        label: {
          normal: {
            show: true,
            position: 'insideRight',
            color: '#000'
          }
        },
        data: ratedFeedbackBarChart.yAxis ? ratedFeedbackBarChart.yAxis.letsWorkOnThisData : [],
        barMaxWidth: '30%'
      },
      {
        name: 'Cool',
        type: 'bar',
        stack: '总量',
        label: {
          normal: {
            show: true,
            position: 'insideRight',
            color: '#000'
          }
        },
        data: ratedFeedbackBarChart.yAxis ? ratedFeedbackBarChart.yAxis.coolData : [],
        barMaxWidth: '30%'
      }
    ]
  };

  const getOption = {
    title: {
      text: 'Competency Rating Chart',
      x: 'center'
    },
    legend: {
      orient: 'horizontal',
      top: 30,
      bottom: 20,
      data: [{ name: 'You Rock', value: '#53C7E6' }, { name: 'Cool', value: '#EEDB1E' }, { name: 'Let\'s work on this', value: '#F3ACD1' }]
    },
    color: ['#53C7E6', '#EEDB1E', '#F3ACD1'],
    tooltip: {
      trigger: 'item',
      formatter: '{a} <br/>{b} : {c} ({d}%)'
    },
    series: [
      {
        name: `${totalFeedbackCounts}`,
        type: 'pie',
        center: ['50%', '60%'],
        data: competencyRating,
        radius: ['50%', '70%'],
        avoidLabelOverlap: false,
        label: {
          normal: {
            show: true,
            position: 'center',
            formatter: '{a}',
            color: '#000000',
            fontWeight: '900',
            fontSize: '40'
          }
        },
        itemStyle: {
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: 'rgba(0, 0, 0, 0.5)'
          }
        }
      }
    ],
    visualMap: { pieces }
  };

  const getLineOption = {
    xAxis: {
      type: 'category',
      boundaryGap: false,
      data: xAxisLineData
    },
    yAxis: {
      type: 'value'
    },
    tooltip: {
      trigger: 'axis',
      position: 'top'
    },
    title: {
      text: 'Weekly Feedbacks'
    },
    legend: {
      data: xAxisLineData
    },
    series: [
      {
        data: seriesData,
        type: 'line',
        areaStyle: {
          color: '#007bff'
        },
        lineStyle: {
          color: '#007bff'
        },
        smooth: true
      }
    ]
  };

  if(ratedFeedbackBarChart.xAxis && ratedFeedbackBarChart.yAxis) {
    const youRockData = ratedFeedbackBarChart.yAxis.youRockData;
    const letsWorkOnThisData = ratedFeedbackBarChart.yAxis.letsWorkOnThisData;
    const coolData = ratedFeedbackBarChart.yAxis.coolData;

    youRockData.forEach((value, index) => {
      totalYouRock = totalYouRock + value;
      totalLetWorkOnThis = totalLetWorkOnThis + letsWorkOnThisData[index]
      totalCool = totalCool + coolData[index]
    });

    totalFeedbackCounts = totalYouRock + totalLetWorkOnThis + totalCool;

    competencyRating = [
      {name: "You Rock", value: totalYouRock},
      {name: "Let's work on this", value: totalLetWorkOnThis},
      {name: "Cool", value: totalCool},
    ];

    pieces = [
      {value: totalYouRock, color: "#53C7E6"},
      {value: totalLetWorkOnThis, color: "#EEDB1E"},
      {value: totalCool, color: "#F3ACD1"},
    ];
  }

  weeklyFeedbackData.forEach((line) => {
    const DateObj = { value: line.date };
    xAxisLineData.push(DateObj);
    seriesData.push(line.feedbacks);
  });

  const toggleSendFeedbackModal = () => {
    setShowSendFeedbackModal(!showSendFeedbackModal);
  }

  const toggleReceivedFeedbackModal = () => {
    setShowReceivedFeedbacksModal(!showReceivedFeedbacksModal)
  }

  const toggleFeedbackRequestsModal = () => {
    setShowFeedbackModal(!showFeedbackModal);
  }

  const getFeedbackReqested = (feedbacks, id, postId, name) => {
    const currentuserFeedbackObj = {
      feedbacks,
      userId: id,
      userName: name,
      postId: postId
    };
    setuserFeedbackObj(currentuserFeedbackObj);
    toggleFeedbackRequestsModal();
  };

  const showReceivedFeedbacks = (receivedFrom) => {
    setReceivedFeedback(receivedFrom);
    toggleReceivedFeedbackModal();
  };

  const resetRequestedEmployeeData = () => {
    setShowRequestFeedbackModal(false);
  };

  const onChartClick = (param, echarts) => {
    const currentname = param.data.name;
    let feedbackName;

    if (currentname === 'You Rock') {
      feedbackName = 'star';
    } else if (currentname === 'Cool') {
      feedbackName = 'medal';
    } else {
      feedbackName = 'cap';
    }

    mainReceivedList.forEach((received) => {
      const { userId, name, img } = received;
      const Feedbacks = received.feedbacks.filter(x => x.icon === feedbackName);
      if (Feedbacks !== undefined) {
        const userObj = { userId, name, img, feedbacks: Feedbacks };
        updatedFeedbackList.push(userObj);
      }
    });

    if (updatedFeedbackList.length > 0) {
      props.updateReceivedFeedbacksList(updatedFeedbackList);
      setPieUpdated(true);
    }
  };

  const onChartLegendselectchanged = (param, echart) => {
    const currentname = param.name;
    let feedbackName;

    if (currentname === 'You Rock') {
      feedbackName = 'star';
    } else if (currentname === 'Cool') {
      feedbackName = 'medal';
    } else {
      feedbackName = 'cap';
    }

    mainReceivedList.forEach((received) => {
      const { userId, name, img } = received;
      const Feedbacks = received.feedbacks.filter(x => x.icon === feedbackName);
      if (Feedbacks !== undefined) {
        const userObj = { userId, name, img, feedbacks: Feedbacks };
        updatedFeedbackList.push(userObj);
      }
    });

    if (updatedFeedbackList.length > 0) {
      props.updateReceivedFeedbacksList(updatedFeedbackList);
      setPieUpdated(true);
    }
  };

  const onEvents = {
    'click': onChartClick,
    'legendselectchanged': onChartLegendselectchanged
  };

  const reRenderPieChart = () => {
    props.setCompetencyRating([]);
    setPieUpdated(false);
    props.getReceivedFeedbackList();
  };

  const handleChangePageNumber = (pageNumber) => {
    if(currentPage !== pageNumber) {
      fetchFeedbacksAndRequestAction(pageType, pageNumber, limit);
    }
  }

  const handleSendSubmit = (values) => {
    const feedbacks = values.competencies.map((feedback) => {
      return{
        Competencies: feedback.value,
        feedback: feedback.icon.value
      }
    })
    const obj={
      sender: {
        id: 'chetan1',
        name: 'Chetan Godhani',
        profile: 'https://randomuser.me/api/portraits/men/64.jpg'
      },
      sentTo: {
        id: values.employee.value,
        name: values.employee.label,
        profile: values.employee.imageUrl
      },
      feedbacks: feedbacks,
      comment: values.comment
    }
    sendFeedbackAction(obj)
  }

  const handleRequestSubmit = (values) => {
    console.log(values)
    const competencies = values.feedbacks.map((feedback)=> {
      return(feedback.label)
    })
    const obj={
      sender: {
        id: 'chetan1',
        name: 'Chetan Godhani',
        profile: 'https://randomuser.me/api/portraits/men/64.jpg'
      },
      sentTo: {
        id: values.employee.value,
        name: values.employee.label,
        profile: values.employee.imageUrl
      },
      competencies: competencies,
      comment: values.comment
    }
    requestFeedbackAction(obj)
  }

  const handleFeedbackRequestSubmit =(id, feedbacks) => {
    console.log(id, feedbacks)
    completeRequestFeedbackAction(id,{feedbacks: feedbacks})
  }

  const totalPages = Math.ceil(totalData / limit);

  return (
    <Fragment>
      <StickyNav className="position-sticky" fill variant="tabs" defaultActiveKey="received">
        <Nav.Item>
          <NavLink activeClassName="active" to="/better-me/received" className="d-flex justify-content-center align-items-center betterme-link  nav-link font-size-14">
            <Translate value="better_me.received" />
          </NavLink>
        </Nav.Item>
        <Nav.Item>
          <NavLink activeClassName="active" to="/better-me/sent" className="d-flex justify-content-center align-items-center betterme-link  nav-link font-size-14">
            <Translate value="better_me.sent" />
          </NavLink>
        </Nav.Item>
        <Nav.Item>
          <NavLink activeClassName="active" to="/better-me/rated" className="d-flex justify-content-center align-items-center betterme-link  nav-link font-size-14">
            <Translate value="better_me.rated_compitencies" /><Badge variant="info" style={{ marginLeft: '10px' }}>9</Badge>
          </NavLink>
        </Nav.Item>
        <Nav.Item>
          <NavLink to="/better-me/requests" activeClassName="active" className="d-flex justify-content-center align-items-center betterme-link  nav-link font-size-14">
            <Translate value="better_me.feedback_requests" />
          </NavLink>
        </Nav.Item>
      </StickyNav>
      {loading ?
        <div className="d-flex justify-content-center pt-4 mx-auto" style={{ width: 650 }}>
          <Spinner animation="border" role="status">
            <span className="sr-only">Loading...</span>
          </Spinner>
        </div>
        :
        pageType === 'received' ?
          <InfiniteScroll
            pageStart={currentPage}
            loadMore={handleChangePageNumber}
            hasMore={currentPage + 1 <= totalPages}
            loader={
              <div className="d-flex justify-content-center pt-2 pb-2" key={0}>
                <Spinner animation="border" role="status">
                  <span className="sr-only">Loading...</span>
                </Spinner>
              </div>
            }
          >
            <div className="mt-4 mb-4 mx-auto" style={receivedListStyle}>
              {receivedList.map((receive, index) => {
                const { userId } = receive;
                return (
                  <ListCard key={`receive-${userId}-${index}`} data={receive} showReceivedFeedbacks={showReceivedFeedbacks} isReceived={true} />
                );
              })}
            </div>
          </InfiniteScroll>
        :
        pageType === 'sent' ?
          <InfiniteScroll
            pageStart={currentPage}
            loadMore={handleChangePageNumber}
            hasMore={currentPage + 1 <= totalPages}
            loader={
              <div className="d-flex justify-content-center pt-2 pb-2" key={0}>
                <Spinner animation="border" role="status">
                  <span className="sr-only">Loading...</span>
                </Spinner>
              </div>
            }
          >
            <div className="mt-4 mb-4 mx-auto" style={receivedListStyle}>
              {sentList.map((receive, index) => {
                const { userId } = receive;
                return (
                  <ListCard key={`sent-${userId}-${index}`} data={receive} showReceivedFeedbacks={showReceivedFeedbacks} />
                );
              })}
            </div>
          </InfiniteScroll>
        :
        pageType === 'rated' ?
          <Row className="container-fluid pt-4 pb-4 m-0" style={{ width: '100%', height: 'calc(100vh - 120px)' }}>
            <Col sm={5} className="h-100">
              <div className="chart-col mb-4 shadow p-1 bg-white rounded h-100">
                <ReactEcharts
                  option={barOptions}
                  lazyUpdate
                  className="h-100"
                />
              </div>
            </Col>
            <Col sm={4} className='d-flex flex-column justify-content-between h-100'>
              <Row className='flex-grow-1'>
                <Col sm={12}>
                  <div className="chart-col mb-4 shadow p-1 bg-white rounded w-100 d-flex  align-items-center">
                    <div
                      style={{ zIndex: '1000' }}
                      className="align-self-start cursor-pointer position-absolute p-2 text-primary"
                      onClick={reRenderPieChart}
                    >
                      <FontAwesomeIcon icon={faRedo} />
                    </div>
                    <ReactEcharts
                      option={getOption}
                      style={{ height: 280, position: 'relative' }}
                      notMerge
                      lazyUpdate
                      onEvents={onEvents}
                      className="w-100"
                    />
                  </div>
                </Col>
              </Row>
              <Row className='linerow flex-grow-1'>
                <Col sm={12}>
                  <div className="chart-col mb-4 shadow p-1 bg-white rounded w-100 d-flex align-items-center">
                    {
                      xAxisLineData.length > 0 &&
                      <ReactEcharts
                        option={getLineOption}
                        style={{ height: 300 }}
                        className="w-100"
                      />
                    }
                  </div>
                </Col>
              </Row>
            </Col>
            <Col sm={3} className="h-100 rounded">
              <div className="h-100 mb-4 shadow bg-white rounded overflow-y-auto ">
                <div className="d-flex p-3 justify-content-between position-sticky bg-white mb-2 border-bottom" style={{ top: 0 }}>
                  <div className="competency">
                    <Translate value="better_me.employee_list" />
                  </div>
                  <div className="competency">
                    <Translate value="better_me.feedbacks" />
                  </div>
                </div>
                {topEmployeesData.map((employee, index) => {
                  return (
                    <div key={`employee-${index}`} className='pb-2 px-3 user-competency-list mt-0'>
                      <div className='d-flex'>
                        <Image src={employee.img} roundedCircle style={{ maxWidth: '40px' }} />
                        <div className='employee-name ml-2 align-self-center'>
                          <div>{employee.name}</div>
                          <div className='employee-post'>
                            <Translate value="better_me.employee" />
                          </div>
                        </div>
                      </div>
                      <div className="d-flex justify-content-center align-self-center feedback-number">
                        {employee.feedbacks.length}
                      </div>
                    </div>
                  );
                })}
              </div>
            </Col>
          </Row>
        :
        pageType === 'requests' &&
          <InfiniteScroll
            pageStart={currentPage}
            loadMore={handleChangePageNumber}
            hasMore={currentPage + 1 <= totalPages}
            loader={
              <div className="d-flex justify-content-center pt-2 pb-2" key={0}>
                <Spinner animation="border" role="status">
                  <span className="sr-only">Loading...</span>
                </Spinner>
              </div>
            }
          >
            <Row className="pt-4 pb-2 container mx-auto">
              <Col sm={12} className='w-100'>
                <CardColumns>
                  {requestsList.map((request, index) => {
                    return (
                      <UserRequestCard key={`card-${index}`} data={request} getFeedbackReqested={getFeedbackReqested} />
                    );
                  })}
                </CardColumns>
              </Col>
            </Row>
          </InfiniteScroll>
      }
      <OverlayTrigger
        placement={"top"}
        trigger="click"
        overlay={
          <Tooltip className="font-size-14 betterme-tootltip">
            <OverlayTrigger
              placement={"top"}
              trigger={['hover', 'focus']}
              overlay={
                <Tooltip className="font-size-14">
                  <Translate value="better_me.request_feedback" />
                </Tooltip>
              }
            >
              <FloatingButton className="btn-primary feedback-btn" right={-21} bottom={86} onClick={() => setShowRequestFeedbackModal(true)}>
                <FontAwesomeIcon icon={faReceipt} />
              </FloatingButton>
            </OverlayTrigger>
            <OverlayTrigger
              placement={"bottom"}
              trigger={['hover', 'focus']}
              overlay={
                <Tooltip className="font-size-14">
                  <Translate value="better_me.send_feedback" />
                </Tooltip>
              }
            >
              <FloatingButton className="btn-primary feedback-btn" right={-21} bottom={12} onClick={toggleSendFeedbackModal}>
                <FontAwesomeIcon icon={faInbox} />
              </FloatingButton>
            </OverlayTrigger>
          </Tooltip>
        }
      >
        <FloatingButton className="btn-primary" bottom={40} fontsize={25}>
          <FontAwesomeIcon icon={faPlus} />
        </FloatingButton>
      </OverlayTrigger>
      <RequestFeedbackModal
        show={showRequestFeedbackModal}
        employees={employees}
        totalFeedbacks={totalFeedbacks}
        onHide={resetRequestedEmployeeData}
        handleSubmit={handleRequestSubmit}
      />
      <SendFeedbackModal
        show={showSendFeedbackModal}
        employees={employees}
        totalFeedbacks={totalFeedbacks}
        onHide={toggleSendFeedbackModal}
        handleSubmit={handleSendSubmit}
      />
      {pageType === 'requests' &&
        <FeedbackRequestModal
          show={showFeedbackModal}
          userFeedbackObj={userFeedbackObj}
          totalFeedbacks={totalFeedbacks}
          onHide={toggleFeedbackRequestsModal}
          handleSubmit={handleFeedbackRequestSubmit}
        />
      }
      {(pageType === 'sent' || pageType === 'received') &&
        <ReceivedFeedbacksModal
          receivedfeedback={receivedFeedback}
          show={showReceivedFeedbacksModal}
          onHide={toggleReceivedFeedbackModal}
        />
      }
    </Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    ...state.betterMe
  };
}

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators({ ...Actions, changeNavTitle }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BetterMePage);
