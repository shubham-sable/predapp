export const GET_ALL_COMPANY_POSTS_LIST = 'CompanyMember/GET_ALL_COMPANY_POSTS_LIST';
export const SAVE_COMPANY_POST_LIKE_UNLIKE = 'CompanyMember/SAVE_COMPANY_POST_LIKE_UNLIKE';
export const SAVE_COMPANY_COMMENT_LIKE_UNLIKE = 'CompanyMember/SAVE_COMPANY_COMMENT_LIKE_UNLIKE';
export const RESET_COMPANY_POST_LIST_NEWSFEED = 'CompanyMember/RESET_COMPANY_POST_LIST_NEWSFEED';
export const LOAD_MORE_COMMENTS_COMPANY = 'CompanyMember/LOAD_MORE_COMMENTS_COMPANY';