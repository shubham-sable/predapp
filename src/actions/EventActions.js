import {
  SET_LOADING_FLAG_EVENTS,
  FETCH_TODAY_EVENT,
  FETCH_THIS_WEEK_EVENT,
  FETCH_THIS_MONTH_EVENT,
  FETCH_PREVIOUS_EVENT,
  CREATE_NEW_EVENT,
  GET_EVENT_BY_ID,
  JOIN_EVENT,
  GET_DATE_EVENTS,
  FETCH_EVENT_LIST,
  CREATE_EVENT_WITH_INVITEES,
  SAVE_NEW_POST,
  SET_LOADING_STATE_CLEANUP_EVENTS,
} from '../types/event';

import eventApi from '../apis';

export const setLoadingTrueAction = (value) => dispatch => {
  dispatch({
    type: SET_LOADING_FLAG_EVENTS,
    value
  })
}

export const getTodayEventAction = (pageNumber,limit,userId) => async dispatch =>{
  const response = await eventApi.get('/events/today/', {
    params: {
      userId: userId,
      skip: pageNumber,
      limit: limit
    }
  });
  dispatch({
    type: FETCH_TODAY_EVENT,
    data: response.data.data
  })
}

export const getThisWeekEventAction = (pageNumber,limit,userId) => async dispatch =>{
  const response = await eventApi.get('/events/this_week/', {
    params: {
      userId: userId,
      skip: pageNumber,
      limit: limit
    }
  });
  dispatch({
    type: FETCH_THIS_WEEK_EVENT,
    data: response.data.data
  })
}

export const getThisMonthEventAction = (pageNumber,limit,userId) => async dispatch =>{
  const response = await eventApi.get('/events/this_month/', {
    params: {
      userId: userId,
      skip: pageNumber,
      limit: limit
    }
  });
  dispatch({
    type: FETCH_THIS_MONTH_EVENT,
    data: response.data.data
  })
}

export const getPrevEventAction = (pageNumber,limit,userId) => async dispatch =>{
  const response = await eventApi.get('/events/previous/', {
    params: {
      userId: userId,
      skip: pageNumber,
      limit: limit
    }
  });
  dispatch({
    type: FETCH_PREVIOUS_EVENT,
    data: response.data.data
  })
}

export const postEventAction = (data, user) => async dispatch =>{
  const response = await eventApi.post('/events/new',{
    eventName: data.title,
    user:{
      id: user.id,
      profileImg: user.profileImg,
      name:{
        fname: user.fname,
        lname: user.lname,
      }
    },
    location: {
      address: data.address,
      coordinate:{
        lat: 0,
        lng: 0
      }
    },
    media:[],
    allDay: false,
    startDate: data.startDate,
    endDate: data.endDate,
    description: data.description,
    guestCanInvite: data.guestCanInvite,
    showGuestList: data.showGuestList
  })
  dispatch({
    type: CREATE_NEW_EVENT,
  })
}

export const getEventByID = (id) => async dispatch => {
  const response = await eventApi.get(`/events/${id}`,{
    params:{
      userId: "1"
    }
  });
  console.log("Executed")
  if(response.data.data !== undefined){
    dispatch({
      type: GET_EVENT_BY_ID,
      data: response.data.data[0]
    })
  }
}

export const joinEventbyId = (eventId) => async dispatch =>{
  await eventApi.post(`/events/${eventId}/join_or_leave`,{
    id:"1",
    profileImg:'url',
    name: {
      fname: 'Virat',
      lname: 'Kohli'
    }
  });
  dispatch({
    type: JOIN_EVENT,
    data:{
      id:"1",
      profileImg:'url',
      name: {
        fname: 'Virat',
        lname: 'Kohli'
      }
    },
    eventId
  })
}

export const getEventByDate = (startDate, skip, limit) => async dispatch =>{
  const response = await eventApi.get(`/events/start_date/${startDate}`,{
    params:{
      skip: skip,
      limit: limit,
    }
  })
  if(response.data.data !== undefined){
    dispatch({
      type: GET_DATE_EVENTS,
      data: response.data.data
    })
  }
}

export const clearAllDataAfterUnMountAction = () => dispatch => {
  dispatch({
    type: SET_LOADING_STATE_CLEANUP_EVENTS
  })
}

export const getEventListFromDate = (date, pageNumber, limit) => dispatch => {
  setTimeout(() => {
    dispatch({
      type: FETCH_EVENT_LIST,
      data: {
        events: [
          {
            id: '1',
            eventName: 'World Dental Show 2020',
            locationName: 'Bandra East, Mumbai',
            latLong: '23.02579, 72.58727',
            image: 'https://ts-production.imgix.net/images/e47ce064-3f85-4ace-afe8-af98d4c02cfa.jpg?auto=compress,format&w=800&h=450',
            allDay: false,
            address: '123 Street, Amd, Gujarat 380001',
            end: 'October 30, 2019 11:00:00',
            startDate: 'October 20, 2019 12:00:00',
            title: 'hello !!',
            description: `WDS
            Welcome to the 12th Annual World Dental Show! We invite you to participate and become an integral part of Asia's biggest dental event being held at Bandra-Kurla Complex,Mumbai. The annual event showcases all aspects of dental business world under one roof. WDS is also a trade and knowledge sharing platform that delivers the latest technologies, cutting-edge research and dentistry techniques to meet the growing demands of the ever-evolving world of dental science.
            The event also acts as a meeting point uniting all relevant groups related to the world of dentistry from across the world. We urge you to partake in various panel discussions and lectures that will augment your dentistry skills and knowledge. Professionals are imparted continuing dental education via scientific learning sessions with speakers from the respective specialised field of dentistry. This will enable your dental team to excel in delivering the finest dental care in the world.
            Furthermore, visitors get an opportunity to interact with exhibitors and dental material and equipment suppliers to touch, use and compare the newest equipment and technology in dentistry today. Experience hands-on training, workshops and demonstrations that will help you reap rewards in your professional career. Exhibitors can also benefit from the show as it attracts a high percentage of private dentists, practice and laboratory owners who have the resource and authority to make better decisions to purchase dentistry-related products and equipment.
            Every year, WDS attracts more than 20,000 dental professionals, academicians, scientists, clinicians, research workers and enthusiasts from across the globe. The event brings together more than 303 dental institutions, 9 speciality dental associations, 200 plus material and equipment manufacturers and 3000 plus dental laboratories. The event is also attended by policy makers from the Indian government and allied health professionals from throughout Asia.
            Through WDS 2020 - Mumbai - Indian Dental Association (IDA) offers an unparalleled opportunity to learn, share knowledge, socialise and to experience the state-of-the-art technology and equipment related to the world of dentistry in the heart of the financial hub of India.
            Why Participate?
            The show offers high-quality and updated research information for practising dental professionals, dental students and enthusiasts alike.
            Be a part of panel discussion, seminars, workshop sessions and other educational programmes to expand your professional dentistry knowledge.
            Listen to the industry’s most honoured speakers.
            Participate in the hands-on sessions and get real-time experience on handling latest dental equipment and materials.
            Meet more than 200 dental equipment and material suppliers in person and get the best deals rightaway.
            The show also offers an unrivalled opportunity for exhibitors to reach out to the dental fraternity with products in a prolific ambience and a steadfast approach to the business of dentistry.
            Provide a common platform for networking and brand promotions to host key brands and companies in dentistry.
            Get together with old friends and colleagues to develop personal business relationships.`,
            file: '/nature.jpg',
            guestCanInvite: 'on',
            showGuestList: 'on',
            participates: [
              '/assets/participate-profile2.jpeg',
              '/assets/participate-profile.jpeg',
              '/assets/participate-profile2.jpeg',
            ],
          },
          {
            id: '2',
            eventName: 'International Symposium Recent Advances in Neonatal Medicine',
            locationName: 'Würzburg, Germany',
            latLong: '23.02579, 72.58727',
            image: 'https://www.med.uio.no/klinmed/english/research/groups/neonatal-medicine/neochibs.jpg',
            allDay: false,
            address: '123 Street, Baroda, Gujarat 390001',
            end: 'October 21, 2019 12:00:00',
            startDate: 'October 18, 2019 12:00:00',
            title: 'All Day Event',
            description: `Our research group has focus on neonatology. Main topics of interest have been the newborn circulation and myocardial function in healthy and sick term and preterm newborns.

            The transitional phase from fetal to newborn circulation is a vulnerable period for the newborn baby and has been of main interest.

            Other focus have been brain damages as a consequence of prematurity and perinatal asphyxia and serious infections in neonates.

            We have had six PhD-studies over the last four years. Three are related to newer modalities of studying myocardial function (Tissue Doppler Imaging) in the neonates, one is focusing on continuous EEG-monitoring in the early neonatal period in extreme preterm babies, and two focus on intervention programs and follow up after prematurity.

            Our research group is cooperating with several other departments within Oslo University Hospital, as well as other institutions and health regions in Norway and abroad.`,
            file: '/nature2.jpeg',
            guestCanInvite: 'off',
            showGuestList: 'on',
            participates: [
              '/assets/participate-profile2.jpeg',
              '/assets/participate-profile.jpeg',
              '/assets/participate-profile2.jpeg',
            ],
          },
          {
            id: '3',
            eventName: 'World Health Summit',
            locationName: 'Kosmos, Berlin, Germany',
            latLong: '23.02579, 72.58727',
            image: 'https://img.10times.com/event/5a6d53cdee65a2e2959476436c274ce/1513505685630/1513450584.jpg?imgeng=/w_230/h_150/m_stretch/cmpr_30/f_jpg',
            allDay: false,
            address: '123 Street, Amd, Gujarat 380001',
            end: 'October 30, 2019 11:00:00',
            startDate: 'October 27, 2019 12:00:00',
            title: 'hello !!',
            description: `The World Health Summit is the world's most prominent forum for addressing global health issues. It brings together key leaders from academia, politics, civil society, and the private sector to address the most pressing health-related challenges on the planet.`,
            file: '/nature.jpg',
            guestCanInvite: 'on',
            showGuestList: 'on',
            participates: [
              '/assets/participate-profile2.jpeg',
              '/assets/participate-profile.jpeg',
              '/assets/participate-profile2.jpeg',
            ],
          },
        ],
        currentPage: pageNumber,
        totalEvents: 3,
        selectedDate: date,
      }
    })
  }, 1000);
};

export const createNewEvent = (event) => (dispatch) => {
  dispatch({
    type: CREATE_NEW_EVENT,
    data: event
  });
};

export const saveNewEvent = (event) => (dispatch) => {
  dispatch({
    type: CREATE_EVENT_WITH_INVITEES,
    data: event
  });
};

export const getDateEventsAction = (currentdate, pageNumber, limit, events) => async dispatch => {
  const response = await eventApi.get(`/events/start_date/${currentdate}`, {
    params:{
      page: pageNumber,
      size: limit
    }
  });
  dispatch({
    type: GET_DATE_EVENTS,
    payload: response.data.data.items,
    currentdate,
    pageNumber
  });
};

export const postNewPostAction = (value, mediaFiles, tag_coworker, date, id) => async dispatch => {
  await eventApi.post(`/events/create_post/${id}`, {
    "user": [{
        "id": "string",
        "name": "string",
        "profile": "string"
      }],
    "isLiked": true,
    "group": [{
        "id": "string",
        "name": "string",
        "profile": "string"
      }],
    "media": mediaFiles,
    "description": value.postContent.document.nodes[0].nodes[0].text,
    "taggedCoWorkers": tag_coworker,
    "totalComments": 0,
    "comments": [{
        "commentId": "string",
        "isLiked": "string",
        "user": [{
            "id": "string",
            "name": "string",
            "profile": "string"
          }],
        "comment": "string",
        "createdAt": 0,
        "replies": [{
            "id": "string",
            "reply": "string"
          }]
      }]
  });
  dispatch({
    type: SAVE_NEW_POST,
    data: {
      "user": [{
          "id": "string",
          "name": "string",
          "profile": "string"
        }],
      "isLiked": true,
      "group": [{
          "id": "string",
          "name": "string",
          "profile": "string"
        }],
      "media": mediaFiles,
      "description": value.postContent.document.nodes[0].nodes[0].text,
      "taggedCoWorkers": tag_coworker,
      "totalComments": 0,
      "comments": [{
          "commentId": "string",
          "isLiked": "string",
          "user": [{
              "id": "string",
              "name": "string",
              "profile": "string"
            }],
          "comment": "string",
          "createdAt": 0,
          "replies": [{
              "id": "string",
              "reply": "string"
            }]
        }]
    }
  });
};
