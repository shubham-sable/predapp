import { FETCH_ORGANISATION_PEOPLE_DATA } from "../types/peopleDirectory"

export const getOrganisationPeopleDataAction = (pageNumber, limit, searchString = '') => dispatch => {
  dispatch({
    type: FETCH_ORGANISATION_PEOPLE_DATA,
    data: {
      peopleData:[
        {
          id: 1,
          image: 'https://randomuser.me/api/portraits/women/85.jpg',
          name: 'Amanda Gordon',
          jobtitle_dept: 'Admin',
          lacation_phone: 'NA',
          skills: ''
        },
        {
          id: 2,
          image: 'https://randomuser.me/api/portraits/women/63.jpg',
          name: 'Sandra Hunt',
          jobtitle_dept: 'Sub Admin',
          lacation_phone: 'NA',
          skills: ''
        },
        {
          id: 3,
          image: 'https://randomuser.me/api/portraits/women/21.jpg',
          name: 'Avery Walters',
          jobtitle_dept: 'Director',
          lacation_phone: '+49 176 25105753',
          skills: ''
        },
        {
          id: 4,
          image: 'https://randomuser.me/api/portraits/women/39.jpg',
          name: 'Tamara Mitchelle',
          jobtitle_dept: 'Director',
          lacation_phone: '+49 176 25105753',
          skills: ''
        },
        {
          id: 5,
          image: 'https://randomuser.me/api/portraits/women/49.jpg',
          name: 'Loretta Jacobs',
          jobtitle_dept: 'Director',
          lacation_phone: '+49 176 25105753',
          skills: ''
        },
        {
          id: 6,
          image: 'https://randomuser.me/api/portraits/women/46.jpg',
          name: 'Bonnie Price',
          jobtitle_dept: 'Employee',
          lacation_phone: '+49 176 25105756',
          skills: ''
        },
        {
          id: 7,
          image: 'https://randomuser.me/api/portraits/women/26.jpg',
          name: 'Megan Ramos',
          jobtitle_dept: 'Director',
          lacation_phone: '+49 176 25105753',
          skills: ''
        },
        {
          id: 8,
          image: 'https://randomuser.me/api/portraits/men/58.jpg',
          name: 'Cameron Clark',
          jobtitle_dept: 'Director',
          lacation_phone: '+49 176 25105753',
          skills: ''
        },
        {
          id: 9,
          image: 'https://randomuser.me/api/portraits/men/12.jpg',
          name: 'Virgil Carpenter',
          jobtitle_dept: 'Director',
          lacation_phone: '+49 176 25105753',
          skills: ''
        },
        {
          id: 10,
          image: 'https://randomuser.me/api/portraits/men/97.jpg',
          name: 'Howard Bailey',
          jobtitle_dept: 'Director',
          lacation_phone: '+49 176 25105753',
          skills: ''
        },
        {
          id: 11,
          image: 'https://randomuser.me/api/portraits/women/54.jpg',
          name: 'Robin Gregory',
          jobtitle_dept: 'Director',
          lacation_phone: '+49 176 25105753',
          skills: ''
        }
      ],
      currentPage: pageNumber,
      searchString
    }
  })
}