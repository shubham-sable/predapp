import {
  LOAD_COWORKERS,
  FOLLOW_UNFOLLOW
} from '../types/followCoworkers';
import followCoworkersapi from '../apis';


export const loadCoworkersAction = () => async dispatch => {
  const response = await followCoworkersapi.get(`/people/60263c56fe615b45578b026b`);
  console.log(response.data.people_data)
  dispatch({
    type: LOAD_COWORKERS,
    data: response.data.people_data
  });
};

export const followAction = (id) => dispatch =>{
  followCoworkersapi.post(`http://127.0.0.1:8000/api/following/${id}`,{
    id: '60263c56fe615b45578b026b',
    firstName:'Chetan',
    lastName: 'Godhani'
  });
  dispatch({
    type: FOLLOW_UNFOLLOW,
    id
  })
}

export const unfollowAction = (id) => dispatch =>{
  followCoworkersapi.delete(`http://127.0.0.1:8000/api/delete/${id}`,{
    data:{
      id: '60263c56fe615b45578b026b',
      firstName:'Chetan',
      lastName: 'Godhani'
    }
  });
  dispatch({
    type: FOLLOW_UNFOLLOW,
    id
  })
}

export const timepass = id =>{
}

// export const addFollowingAction = id => async dispatch => {
//   await followCoworkersapi.post(`/following/${id}`,{ id: '602666bd0205032fc47ea80e' });
//   dispatch({
//     type: FOLLOW_COWORKERS
//   });
// };
//
// export const unFollowingAction = id => async dispatch => {
//   await followCoworkersapi.delete(`/delete/${id}`);
//   dispatch({
//     type: UNFOLLOW_COWORKERS,
//   });
// };
    // data: [
    //   {
    //     id: 1,
    //     profileImage: 'https://randomuser.me/api/portraits/men/76.jpg',
    //     name: 'Chetan Godhani',
    //     followedBy: [2,3,4],
    //     isFollowed: false,
    //   },
    //   {
    //     id: 3,
    //     profileImage: 'https://randomuser.me/api/portraits/women/21.jpg',
    //     name: 'Avery Walters',
    //     followedBy: [1,5],
    //     isFollowed: false,
    //   },
    //   {
    //     id: 4,
    //     profileImage: 'https://randomuser.me/api/portraits/women/39.jpg',
    //     name: 'Tamara Mitchelle',
    //     followedBy: [],
    //     isFollowed: true,
    //   },
    //   {
    //     id: 5,
    //     profileImage: 'https://randomuser.me/api/portraits/women/49.jpg',
    //     name: 'Loretta Jacobs',
    //     followedBy: [],
    //     isFollowed: true,
    //   },
    //   {
    //     id: 6,
    //     profileImage: 'https://randomuser.me/api/portraits/women/46.jpg',
    //     name: 'Bonnie Price',
    //     followedBy: [],
    //     isFollowed: true,
    //   },
    //   {
    //     id: 7,
    //     profileImage: 'https://randomuser.me/api/portraits/women/26.jpg',
    //     name: 'Megan Ramos',
    //     followedBy: [],
    //     isFollowed: true,
    //   },
    //   {
    //     id: 8,
    //     profileImage: 'https://randomuser.me/api/portraits/men/58.jpg',
    //     name: 'Cameron Clark',
    //     followedBy: [],
    //     isFollowed: true,
    //   },
    //   {
    //     id: 9,
    //     profileImage: 'https://randomuser.me/api/portraits/men/12.jpg',
    //     name: 'Virgil Carpenter',
    //     followedBy: [],
    //     isFollowed: true,
    //   },
    //   {
    //     id: 10,
    //     profileImage: 'https://randomuser.me/api/portraits/men/97.jpg',
    //     name: 'Howard Bailey',
    //     followedBy: [],
    //     isFollowed: true,
    //   },
    //   {
    //     id: 11,
    //     profileImage: 'https://randomuser.me/api/portraits/women/54.jpg',
    //     name: 'Robin Gregory',
    //     followedBy: [],
    //     isFollowed: true,
    //   }
    // ]
