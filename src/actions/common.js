import { UPDATE_NAV_TITLE } from '../types';

export const changeNavTitle = (title) => (dispatch) => {
  dispatch({
    type: UPDATE_NAV_TITLE,
    title
  });
};