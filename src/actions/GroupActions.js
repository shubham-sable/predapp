import {
  FETCH_GROUP_LIST,
  CREATE_GROUP_TITLE,
  RECEIVED_GROUP_DATA,
  FETCH_GROUP_POSTS,
  SAVE_GROUP_POST_LIKE_UNLIKE,
  SAVE_GROUP_POST_COMMENT_LIKE_UNLIKE,
  RESET_LOADING_FLAG_GROUP_LIST,
  RESET_LOADING_FLAG_GROUP_POST,
} from './../types/groupTypes';

export const createGroupTitle = (title) => (dispatch) => {
  dispatch({
    type: CREATE_GROUP_TITLE,
    title
  });
};

export const receivedGroupData = (data) => (dispatch) => {
  dispatch({
    type: RECEIVED_GROUP_DATA,
    data
  });
};

const groupsInAction = [
  {
    id: 1,
    groupId: 1,
    groupTitle: 'General Purpose',
    name: 'General Purpose',
    iconImage: 'https://picsum.photos/seed/picsum/200/200',
    groupCoverImage: 'https://picsum.photos/seed/picsum/1920/1080',
    totalMembersInGroup: 99,
    type: 'closed',
    groupType: {
      id: 1,
      name: 'Team	& Projects'
    }
  },
  {
    id: 2,
    groupId: 2,
    groupTitle: 'Internal Meetings',
    name: 'Internal Meetings',
    iconImage: 'https://picsum.photos/200/200',
    groupCoverImage: 'https://picsum.photos/1920/1080',
    totalMembersInGroup: 99,
    type: 'closed',
    groupType: {
      id: 1,
      name: 'Team	& Projects'
    }
  },
  {
    id: 3,
    groupId: 3,
    groupTitle: 'Daily Standups',
    name: 'Daily Standups',
    iconImage: 'https://picsum.photos/id/101/200/200',
    groupCoverImage: 'https://picsum.photos/id/101/1920/1080',
    totalMembersInGroup: 99,
    type: 'closed',
    groupType: {
      id: 1,
      name: 'Team	& Projects'
    }
  },
  {
    id: 4,
    groupId: 4,
    groupTitle: 'Ideas Sharing',
    name: 'Ideas Sharing',
    iconImage: 'https://picsum.photos/id/0/200/200',
    groupCoverImage: 'https://picsum.photos/id/0/1920/1080',
    totalMembersInGroup: 99,
    type: 'closed',
    groupType: {
      id: 1,
      name: 'Team	& Projects'
    }
  },
];

export const fetchGroupListAction = (pageNo, limit) => dispatch => {
  setTimeout(() => {
    dispatch({
      type: FETCH_GROUP_LIST,
      data: {
        groupList: groupsInAction,
        currentPage: pageNo,
        groupListTotal: 4
      }
    });
  }, 1000);
}

export const fetchGroupDataAction = (groupId) => (dispatch) => {
  //  return fetch(`https://newsapi.org/v1/articles? 
  //           source=${channel}&apiKey=${MY_API_KEY}`)
  //         .then(
  //           response => response.json(),
  //           error => console.log('An error occurred.', error),
  //         )
  //         .then((json) => {
  //           dispatch(receivedPosts(json));
  //         });
  dispatch(receivedGroupData(groupsInAction.find(x => x.id === Number(groupId)) || groupsInAction[0]));
};

export const fetchGroupPostsAction = (groupId, pageNumber, limit) => dispatch => {
  setTimeout(() => {
    dispatch({
      type: FETCH_GROUP_POSTS,
      data: {
        groupPosts: [
          {
            id: 1,
            user: {
              userId: 1,
              userName: 'Chetan Godhani',
              userProfile: '/minion.jpeg',
            },
            isLiked: true,
            createdAt: 1567661136,
            media: [
              {
                id: 1,
                type: 'image',
                path: '/postimage.jpeg',
              },
              {
                id: 2,
                type: 'image',
                path: '/nature.jpg',
              },
              {
                id: 3,
                type: 'image',
                path: '/postimage.jpeg',
              },
              {
                id: 4,
                type: 'image',
                path: '/nature.jpg',
              },
              {
                id: 5,
                type: 'image',
                path: '/nature1.jpeg',
              },
              {
                id: 1,
                type: 'video',
                path: '/demo.mp4'
              },
              {
                id: 2,
                type: 'video',
                path: '/demo2.mp4'
              }
            ],
            description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
            comments: [
              {
                commentId: 1,
                isLiked: false,
                userId: 1,
                userName: 'Chetan Godhani',
                userProfile: '/minion.jpeg',
                comment: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
                createdAt: 1567661136,
                replies: [
                  {
                    commentId: 2,
                    isLiked: false,
                    userId: 1,
                    userName: 'Chetan Godhani',
                    userProfile: '/minion.jpeg',
                    comment: 'Test 2',
                    createdAt: 1567661136,
                  },
                  {
                    commentId: 3,
                    isLiked: false,
                    userId: 1,
                    userName: 'Chetan Godhani',
                    userProfile: '/minion.jpeg',
                    comment: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
                    createdAt: 1567661136,
                  },
                ]
              },
              {
                commentId: 2,
                isLiked: false,
                userId: 1,
                userName: 'Chetan Godhani',
                userProfile: '/minion.jpeg',
                comment: 'Test 2',
                createdAt: 1567661136,
                replies: []
              },
              {
                commentId: 3,
                isLiked: false,
                userId: 1,
                userName: 'Chetan Godhani',
                userProfile: '/minion.jpeg',
                comment: 'Test 3',
                createdAt: 1567661136,
                replies: [
                  {
                    commentId: 2,
                    isLiked: false,
                    userId: 1,
                    userName: 'Chetan Godhani',
                    userProfile: '/minion.jpeg',
                    comment: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
                    createdAt: 1567661136,
                  },
                  {
                    commentId: 3,
                    isLiked: false,
                    userId: 1,
                    userName: 'Chetan Godhani',
                    userProfile: '/minion.jpeg',
                    comment: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
                    createdAt: 1567661136,
                  },
                ]
              },
              {
                commentId: 4,
                isLiked: false,
                userId: 1,
                userName: 'Chetan Godhani',
                userProfile: '/minion.jpeg',
                comment: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
                createdAt: 1567661136,
                replies: []
              },
              {
                commentId: 5,
                isLiked: false,
                userId: 1,
                userName: 'Chetan Godhani',
                userProfile: '/minion.jpeg',
                comment: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
                createdAt: 1567661136,
                replies: []
              }
            ]
          },
          {
            id: 2,
            user: {
              userId: 1,
              userName: 'Chetan Godhani',
              userProfile: '/minion.jpeg',
            },
            isLiked: true,
            createdAt: 1567661136,
            media: [
              {
                id: 1,
                type: 'image',
                path: 'https://via.placeholder.com/1500',
              }
            ],
            description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
            comments: [
              {
                commentId: 6,
                isLiked: false,
                userId: 1,
                userName: 'Chetan Godhani',
                userProfile: '/minion.jpeg',
                comment: 'Test 3',
                createdAt: 1567661136,
                replies: [
                  {
                    commentId: 2,
                    isLiked: false,
                    userId: 1,
                    userName: 'Chetan Godhani',
                    userProfile: '/minion.jpeg',
                    comment: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
                    createdAt: 1567661136,
                  },
                  {
                    commentId: 3,
                    isLiked: false,
                    userId: 1,
                    userName: 'Chetan Godhani',
                    userProfile: '/minion.jpeg',
                    comment: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
                    createdAt: 1567661136,
                  },
                ]
              }
            ]
          },
          {
            id: 3,
            user: {
              userId: 1,
              userName: 'Chetan Godhani',
              userProfile: '/minion.jpeg',
            },
            isLiked: false,
            createdAt: 1567661136,
            media: [
              {
                id: 1,
                type: 'vidoe',
                path: '/demo.mp4'
              },
              {
                id: 2,
                type: 'vidoe',
                path: '/demo2.mp4'
              }
            ],
            description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
            comments: [
              {
                commentId: 7,
                isLiked: false,
                userId: 1,
                userName: 'Chetan Godhani',
                userProfile: '/minion.jpeg',
                comment: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
                createdAt: 1567661136,
                replies: []
              }
            ]
          }
        ],
        groupPostsCurrentPage: pageNumber,
        groupPostsTotal: 50
      }
    })
  }, 1000);
}

export const saveGroupPostLikeUnlikeAction = (postId) => (dispatch) => {
  dispatch({
    type: SAVE_GROUP_POST_LIKE_UNLIKE,
    postId
  });
};

export const saveGroupPostCommentLikeUnlikeAction = (postId, commentId, isReply, replyCommentId) => (dispatch) => {
  dispatch({
    type: SAVE_GROUP_POST_COMMENT_LIKE_UNLIKE,
    postId,
    commentId,
    isReply,
    replyCommentId
  });
};

export const resetLoadingFlagAction = () => dispatch => {
  dispatch({
    type: RESET_LOADING_FLAG_GROUP_LIST
  })
}

export const resetGroupPostListAction = () => dispatch => {
  dispatch({
    type: RESET_LOADING_FLAG_GROUP_POST
  })
}