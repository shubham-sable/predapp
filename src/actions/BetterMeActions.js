import {
  // New Consts
  FETCH_RECEIVED_FEEDBACKS,
  FETCH_SENT_FEEDBACKS,
  FETCH_FEEDBACK_REQUESTS,
  CLEAN_UP_BETTERME_DATA,
  FETCH_RATED_COMPITENCIES_DATA,
  SEND_FEEDBACK,
  COMPLETE_REQUEST,

  // Old Consts
  GET_RECEIVED_FEEDBACK_LIST,
  UPDATE_RECEIVED_FEEDBACK_LIST,
  SET_COMPITENCY_RATING,
} from '../types/betterMe';
import bettermeApi from './../apis';

const receivedFeedbackList = [
  {
    id: 1,
    userId: 1,
    name: 'Chetan Godhani',
    img: 'https://randomuser.me/api/portraits/men/76.jpg',
    description: 'Good work, as always',
    feedbacks: [
      {
        name: 'orientation',
        icon: 'star',
        date: '26-09-2019',
      },
      {
        name: 'Relationships',
        icon: 'cap',
        date: '27-09-2019',
      },
      {
        name: 'Service',
        icon: 'star',
        date: '28-09-2019',
      },
      {
        name: 'ownership',
        icon: 'medal',
        date: '30-09-2019',
      },
      {
        name: 'proactive',
        icon: 'star',
        date: '29-09-2019',
      }
    ]
  }]

export const fetchFeedbacksAndRequestAction = (type, pageNumber, limit) => async dispatch => {
  if (type === 'received') {
    const receivedFeedbackList = await bettermeApi.get('/betterme/received/chetan1', {
      params: {
        skip:pageNumber,
        limit:limit,
      }     
    });
    setTimeout(() => {
      dispatch({
        type: FETCH_RECEIVED_FEEDBACKS,
        data: {
          receivedList: receivedFeedbackList.data.data ? receivedFeedbackList.data.data : [],
          currentPage: pageNumber+1,
          totalData: 10,
        }
      })
    }, 1000);
  } else if (type === 'sent') {
    const sentFeedbackList = await bettermeApi.get('/betterme/sent/chetan1', {
      params: {
        skip:pageNumber,
        limit:limit,
      }     
    });
    setTimeout(() => {
      dispatch({
        type: FETCH_SENT_FEEDBACKS,
        data: {
          sentList: sentFeedbackList.data.data ? sentFeedbackList.data.data : [],
          currentPage: pageNumber+1,
          totalData: 10,
        }
      })
    }, 1000);
  } else if (type === 'requests') {
    const feedbackRequestsList = await bettermeApi.get('/betterme/requests/chetan1', {
      params: {
        skip:pageNumber,
        limit:limit,
      }     
    });
    setTimeout(() => {
      dispatch({
        type: FETCH_FEEDBACK_REQUESTS,
        data: {
          requestsList: feedbackRequestsList.data.data ? feedbackRequestsList.data.data : [],
          currentPage: pageNumber+1,
          totalData: 5,
        }
      })
    }, 1000);
  }
}

export const loadRatedCompitenciesDataAction = () => dispatch => {
  setTimeout(() => {
    dispatch({
      type: FETCH_RATED_COMPITENCIES_DATA,
      data: {
        ratedFeedbackBarChart: {
          xAxis: ["Agility", "Orientation", "Relationships", "Creative", "Ownership", "Pro-Active", "Service", "Opener", "Emotional Intelligence"],
          yAxis: {
            youRockData: [5, 10, 15, 0, 0, 20, 25, 30, 35],
            letsWorkOnThisData: [7, 10, 17, 20, 27, 34, 38, 45, 50],
            coolData: [2, 5, 7, 0, 0, 0, 20, 27, 39],
          },
        },
        weeklyFeedbackData: [
          { date: '29 Sep - 05 Oct', feedbacks: 10 },
          { date: '06 Oct - 12 Oct', feedbacks: 5 },
          { date: '13 Oct - 19 Oct', feedbacks: 20 },
          { date: '20 Oct - 26 Oct', feedbacks: 15 }
        ],
        topEmployeesData: receivedFeedbackList
      }
    });
  }, 1000);
}

export const sendFeedbackAction = (obj) => async dispatch => {
  const response = await bettermeApi.post('/betterme/send', obj)
  dispatch({
    type: SEND_FEEDBACK,
    data: response.data.data
  })
}

export const requestFeedbackAction = (obj) => async dispatch => {
  await bettermeApi.post('/betterme/request', obj)
}

export const completeRequestFeedbackAction = (id, obj) => async dispatch => {
  const response = await bettermeApi.put(`/betterme/ansreq/${id}`, obj)
  dispatch({
    type: COMPLETE_REQUEST,
    data: response.data.data
  })
}

export const cleanUpBetterMeDataAction = () => dispatch => {
  dispatch({
    type: CLEAN_UP_BETTERME_DATA
  })
}

export const getReceivedFeedbackList = () => (dispatch) => {
  dispatch({
    type: GET_RECEIVED_FEEDBACK_LIST,
    receivedFeedbackList
  });
};

export const updateReceivedFeedbacksList = (updatedFeedbackList) => (dispatch) => {
  dispatch({
    type: UPDATE_RECEIVED_FEEDBACK_LIST,
    updatedFeedbackList
  });
};

export const setCompetencyRating = (newArray) => (dispatch) => {
  dispatch({
    type: SET_COMPITENCY_RATING,
    newArray
  });
}