import {
  GET_ALL_COMPANY_POSTS_LIST,
  SAVE_COMPANY_POST_LIKE_UNLIKE,
  SAVE_COMPANY_COMMENT_LIKE_UNLIKE,
  RESET_COMPANY_POST_LIST_NEWSFEED,
  LOAD_MORE_COMMENTS_COMPANY,
} from '../types/companyMember';

export const getAllPostsListAction = (pageNumber, limit) => async (dispatch) => {
  setTimeout(() => {
    dispatch({
      type: GET_ALL_COMPANY_POSTS_LIST,
      data: {
        postsList: [
          {
            id: 1,
            user: {
              userId: 1,
              userName: 'Amanda Gordon',
              userProfile: 'https://randomuser.me/api/portraits/women/85.jpg',
            },
            isLiked: true,
            createdAt: 1601128539,
            group: {
              groupId: 1,
              groupName: 'Social Group',
            },
            media: [
              {
                id: 1,
                type: 'image',
                path: 'https://picsum.photos/200',
              },
              {
                id: 2,
                type: 'image',
                path: 'https://picsum.photos/220',
              },
              {
                id: 3,
                type: 'image',
                path: 'https://picsum.photos/240',
              },
              {
                id: 4,
                type: 'image',
                path: 'https://picsum.photos/260',
              },
              {
                id: 5,
                type: 'image',
                path: 'https://picsum.photos/280',
              },
              {
                id: 1,
                type: 'video',
                path: '/demo.mp4'
              },
              {
                id: 2,
                type: 'video',
                path: '/demo2.mp4'
              }
            ],
            description: "But that was life: Nobody got a guided tour to their own theme park. You had to hop on the rides as they presented themselves, never knowing whether you would like the one you were in line for...or if the bastard was going to make you throw up your corn dog and your cotton candy all over the place.",
            totalComments: 2,
            comments: [
              {
                commentId: 1,
                isLiked: false,
                userId: 1,
                userName: 'Sandra Hunt',
                userProfile: 'https://randomuser.me/api/portraits/women/63.jpg',
                comment: 'I Like this place and Beautyful rainfall thanks for travels tourism,s people',
                createdAt: 1567661136,
                replies: [
                  {
                    commentId: 2,
                    isLiked: false,
                    userId: 2,
                    userName: 'Avery Walters',
                    userProfile: 'https://randomuser.me/api/portraits/women/21.jpg',
                    comment: 'One of the best place to hang while travelling through this route during rainy season... 👏🏻👏🏻',
                    createdAt: 1567661136,
                  },
                  {
                    commentId: 3,
                    isLiked: false,
                    userId: 1,
                    userName: 'Sandra Hunt',
                    userProfile: 'https://randomuser.me/api/portraits/women/63.jpg',
                    comment: 'Where is this place?',
                    createdAt: 1567661136,
                  },
                ]
              },
              {
                commentId: 4,
                isLiked: false,
                userId: 2,
                userName: 'Avery Walters',
                userProfile: 'https://randomuser.me/api/portraits/women/21.jpg',
                comment: 'Beautifùl in rainy season risky as well.',
                createdAt: 1567661136,
                replies: []
              },
            ]
          },
          {
            id: 2,
            user: {
              userId: 2,
              userName: 'Avery Walters',
              userProfile: 'https://randomuser.me/api/portraits/women/21.jpg',
            },
            isLiked: true,
            createdAt: 1597739484,
            group: {
              groupId: 1,
              groupName: 'Social Group',
            },
            media: [],
            description: `guilty lately... 🤦🏽‍♀️
            As most of you know, I’m in hard-core creation mode right now getting The Blog & Email List Bootcamp course created for its September launch (wait-list in bio!) and getting my podcast ready for its October launch.
            And I can’t get anything (like actually, ANYTHING 😂) done when I’m traveling, whiiiiich means I’ll be at home in NYC until the very end of September.
            And as ridiculous as that sounds, for at least 2 weeks out of every month for the last 2 years, I’ve been on the road creating content and sharing epic destinations on here every single week. I’ve been so used to traveling constantly that staying put for two entire months feels really, really weird.
            And I feel weirdly guilty not being able to share new travels with you all.
            That being said, I want to know from you - what do you want to see here over the next two months?
            Caption-wise: have you been loving my latest captions? And photo-wise: Old travel photos? New photos from New York?`,
            totalComments: 1,
            comments: [
              {
                commentId: 6,
                isLiked: false,
                userId: 4,
                userName: 'Megan Ramos',
                userProfile: 'https://randomuser.me/api/portraits/women/26.jpg',
                comment: 'This is NY? Wow looks like the Caribbean?',
                createdAt: 1567661136,
                replies: [
                  {
                    commentId: 2,
                    isLiked: false,
                    userId: 3,
                    userName: 'Cameron Clark',
                    userProfile: 'https://randomuser.me/api/portraits/men/58.jpg',
                    comment: 'How can i become an influencer?',
                    createdAt: 1567661136,
                  },
                  {
                    commentId: 3,
                    isLiked: false,
                    userId: 2,
                    userName: 'Avery Walters',
                    userProfile: 'https://randomuser.me/api/portraits/women/21.jpg',
                    comment: 'We would love to see a mix of throwbacks and NYC content!!! anything you post is great 😍',
                    createdAt: 1567661136,
                  },
                ]
              }
            ]
          },
          {
            id: 3,
            user: {
              userId: 5,
              userName: 'Howard Bailey',
              userProfile: 'https://randomuser.me/api/portraits/men/97.jpg',
            },
            isLiked: false,
            createdAt: 1598602087,
            group: {
              groupId: 1,
              groupName: 'Social Group',
            },
            media: [
              {
                id: 1,
                type: 'video',
                path: '/demo.mp4'
              },
              {
                id: 2,
                type: 'video',
                path: '/demo2.mp4'
              }
            ],
            description: `Do you often struggle memorizing the syntax?⁠
            I get this question so often. Nobody can memorize every single command of a programming language and luckily that's not necessary. ⁠
            What you should focus on instead is to understand what happens behind the scene. Be curious and learn about how computers work, how compilers work, how computers understand the code we type in, what specific keywords really do and much more. That's so much more helpful than just memorizing whatever you write.⁠
            It's just important that you're able to understand code when you see it, not that you can write it on your own from memory. So that's what you should focus on.⁠`,
            totalComments: 1,
            comments: [
              {
                commentId: 6,
                isLiked: false,
                userId: 1,
                userName: 'Robin Gregory',
                userProfile: 'https://randomuser.me/api/portraits/women/54.jpg',
                comment: 'Ohh this is so very helpful for me , thanks so much dude',
                createdAt: 1567661136,
                replies: []
              }
            ]
          }
        ],
        currentPage: pageNumber,
        limit,
        totalPosts: 3,
      }
    });
  }, 1000);
};

export const loadMoreCommentsAction = (postId, pageNumber, limit) => dispatch => {
  dispatch({
    type: LOAD_MORE_COMMENTS_COMPANY,
    data: {
      comments: [
        {
          commentId: pageNumber * 2 + 10,
          isLiked: false,
          userId: 1,
          userName: 'Chetan Godhani',
          userProfile: '/minion.jpeg',
          comment: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
          createdAt: 1567661136,
          replies: []
        }
      ],
      postId,
      currentPage: pageNumber
    }
  })
}

export const savePostLikeUnlike = (postId) => (dispatch) => {
  dispatch({
    type: SAVE_COMPANY_POST_LIKE_UNLIKE,
    postId
  });
};

export const saveCommentLikeUnlike = (postId, commentId, isReply, replyCommentId) => (dispatch) => {
  dispatch({
    type: SAVE_COMPANY_COMMENT_LIKE_UNLIKE,
    postId,
    commentId,
    isReply,
    replyCommentId
  });
};

export const resetPostListAction = () => dispatch => {
  dispatch({
    type: RESET_COMPANY_POST_LIST_NEWSFEED
  })
}