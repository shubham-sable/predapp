import {
  LOAD_RECENT_STARS,
  LOAD_SENT_STARS,
  LOAD_RECEIVED_STARS,
  ADD_LIKE,
  SAVE_COMMENT_LIKE_UNLIKE,
  SET_LOADING_FLAG_STARMEUP,
  CHANGE_PAGE_NUMBER_STARMEUP,
  ADD_STARMEUP_CARD,
  LOAD_COMMENTS,
  LOAD_MORE_COMMENTS,
  SET_COMMENTS_LOADING_STARMEUP,
  ADD_COMMENT,
  EDIT_COMMENT,
  DELETE_COMMENT
} from '../types/starMeUp';

import starmeupApi from './../apis'

export const loadAllStarsAction = (type, pageNo, limit) => async dispatch => {
  const response = await starmeupApi.get(`/starmeup/count`)
  const totalStars = response.data
  switch (type){
    case 'recent':
      const recentStars = await starmeupApi.get('/starmeup/recent/', {
        params: {
          skip:pageNo,
          limit:limit,
          userId: 'chetan1'
        }     
      });
      setTimeout(() => {
        dispatch({
          type: LOAD_RECENT_STARS,
          data: {
            recentStars: Array.isArray(recentStars.data.data) ? recentStars.data.data : [],
            totalStars: totalStars,
            currentPage: pageNo,
            limit,
          }
        })});
      break;
    case 'sent':
      const sentStars = await starmeupApi.get('/starmeup/sent/chetan1', {
        params: {
          skip:pageNo,
          limit:limit
        }     
      });
      setTimeout(() => {
        dispatch({
          type: LOAD_SENT_STARS,
          data: {
            sentStars: Array.isArray(sentStars.data.data) ? sentStars.data.data : [],
            totalStars: totalStars,
            currentPage: pageNo,
            limit,
            totalStars: totalStars
          }
        })});
      break;
    case 'received':
      const receivedStars = await starmeupApi.get('/starmeup/received/chetan1', {
        params: {
          skip:pageNo,
          limit:limit
        }     
      });
      setTimeout(() => {
        dispatch({
          type: LOAD_RECEIVED_STARS,
          data: {
            receivedStars: Array.isArray(receivedStars.data.data) ? receivedStars.data.data : [],
            currentPage: pageNo,
            limit,
            totalStars: totalStars
          }
        })});
      break;
    default:
      break;
  }
}

export const addStarLikeAction = (id, type, user) => async dispatch => {
  const response = await starmeupApi.put(`/starmeup/${id}/like_or_unlike`, user)
  dispatch({
    type: ADD_LIKE,
    data: {
      id,
      type
    }
  });
}

export const getStarCommentsAction = (id, pageNo, limit) => async dispatch => {
  const response = await starmeupApi.get(`/starmeup/${id}/posts/${id}/comments`, {
    params: {
      skip:0,
      limit:limit,
      userId: 'chetan1'
    }
  });
  dispatch({
    type: LOAD_COMMENTS,
    parentId: id,
    postId: id,
    data: response.data
  })
}

export const getCommentReplies = (postId, parentId, userId) => async dispatch => {
  const response = await starmeupApi.get(`/starmeup/${postId}/posts/${parentId}/comments`, {
    params: {
      skip:0,
      limit:10,
      userId: userId
    }
  });
  dispatch({
    type: LOAD_COMMENTS,
    parentId: parentId,
    postId: postId,
    data: response.data
  })
}

export const saveCommentLikeUnlike = (starPostId, commentId, isReply, replyCommentId) => (dispatch) => {
  dispatch({
    type: SAVE_COMMENT_LIKE_UNLIKE,
    starPostId,
    commentId,
    isReply,
    replyCommentId
  });
};

export const loadMoreCommentsAction = (postId, pageNumber, limit) => dispatch => {
  const response = starmeupApi.get(`/starmeup/${postId}/posts/${postId}/comments`,{
    params: {
      skip: pageNumber,
      limit: limit
    }
  })
  dispatch({
    type: LOAD_MORE_COMMENTS,
    data: {
      ...response.data.data,
      currentPage: pageNumber
    }
  })
}

export const createStarMeUpCardAction = (user, type, comment, receiver) => async dispatch => {
  const starObj={
    sentTo: receiver,
    type: type,
    description: comment,
    postedBy: user
  }
  const response = await starmeupApi.post('/starmeup/create/', starObj)
  dispatch({
    type: ADD_STARMEUP_CARD,
    data: response.data.data
  })
}

export const postStarCommentAction = (postId, comment, isReply, commentId, replyId, user) => async dispatch => {
  console.log(commentId, replyId, isReply, postId, comment)
  if(isReply) {
    const commentObj ={
      parentId: commentId,
      user: user,
      comment_text : comment
    }
    const response = await starmeupApi.post(`/starmeup/${postId}/comments/new`, commentObj)
    dispatch({
      type: ADD_COMMENT,
      data: response.data,
      parentId: commentId,
      postId: postId,
    })
  }
  else {
    const commentObj = {
      parentId: postId,
      user: user,
      comment_text: comment
    }
    const response = await starmeupApi.post(`/starmeup/${postId}/comments/new`, commentObj)
    dispatch({
      type: ADD_COMMENT,
      data: response.data,
      parentId: postId,
      postId: postId,
    })
  }  
}
export const getRepliesReply = (postId, commentId, replyId, userId) => async dispatch =>{
  const response = await starmeupApi.get(`/starmeup/${postId}/posts/${replyId}/comments`,{
    params: {
      skip: 0,
      limit: 20,
      userId: userId
    }
  });
  if(response.data.error!==undefined){
    console.log(response.data.error);
  }else{
    dispatch({
      type: LOAD_COMMENTS,
      data: response.data,
      postId,
      commentId
    })
  }
}

export const updateCommentAction = (postId, commentId, isReply, parentId, comment) => async dispatch => {
  if(isReply) {
    const response = await starmeupApi.put(`/starmeup/${postId}/comments/${commentId}/edit`,{
      params: {
        userId: 'chetan1'
      }
    })
    dispatch({
      type: EDIT_COMMENT,
      data: response.data,
      postId,
      commentId,
      isReply,
      parentId,
      comment
    })
  }
  else {
    const response = await starmeupApi.put(`/starmeup/${postId}/comment/${commentId}/edit?userId=chetan1`,{comment_text: comment})
    dispatch({
      type: EDIT_COMMENT,
      data: response.data,
      postId,
      commentId,
      isReply,
      parentId,
      comment
    })
  }
}

export const deleteCommentAction = (postId, commentId, isReply, parentId) => async dispatch =>{
  await starmeupApi.delete(`/starmeup/${postId}/comment/${commentId}/delete`)
  dispatch({
    type: DELETE_COMMENT,
    postId,
    commentId,
    isReply,
    parentId
  });
}

export const setLoadingFlagAction = () => dispatch => {
  dispatch({
    type: SET_LOADING_FLAG_STARMEUP
  })
}

export const setCommentsLoadingAction = () => dispatch => {
  dispatch({
    type: SET_COMMENTS_LOADING_STARMEUP
  })
}

export const changePageNumberAction = (pageNumber) => dispatch => {
  dispatch({
    type: CHANGE_PAGE_NUMBER_STARMEUP,
    data: pageNumber
  })
}