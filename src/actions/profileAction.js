import {
GET_PROFILE_DATA,
EDIT_CONTACT,
ADD_WORK_EXPERIENCE,
ADD_EDUCATION,
ADD_CERTIFICATION,
ADD_VOLUNTEER,
ADD_SKILL
} from '../types/profile';


import profileApi from '../apis';
import ActionButton from 'antd/lib/modal/ActionButton';

export const getProfileData = (userId) => async dispatch =>{
    const response = await profileApi.get(`/profile/${userId}`);
    dispatch({
      type: GET_PROFILE_DATA,
      data: response.data.data
    })
}

export const editContactAction = (profile_id,email,mobileNumber) => async dispatch =>{
    await profileApi.put(`/contact/${profile_id}`,{
        "email":email,
        "mobileNumber":mobileNumber
    });
    dispatch({
        type:EDIT_CONTACT,
        data:{
            email,
            mobileNumber,
        }
    });
}

export const addWorkExperience = (values,profile_id) => async dispatch =>{
    console.log("action values",values);
    const response = await profileApi.post(`/workExperience/${profile_id}`,{
        "title": values.title,
        "startDate": values.startDate,
        "endDate": values.endDate,
        "isCurrentCompany": true,
        "company": {
          "id": "1",
          "name": values.company,
          "profile": "companyProfile",
          "location": values.location
        }
    })
    console.log("this is response",response);
    dispatch({
        type: ADD_WORK_EXPERIENCE,
        data:{
            "title": values.title,
            "startDate": values.startDate,
            "endDate": values.endDate,
            "isCurrentCompany": true,
            "company": {
              "id": "1",
              "name": values.company,
              "profile": "companyProfile",
              "location": values.location
        }
    }
    })
}
export const addEducation =(values,profile_id) => async dispatch =>{
    await profileApi.post(`/education/${profile_id}`,{
        "school": values.school,
        "degree": values.degree,
        "fieldOfStudy": values.fieldOfStudy,
        "startYear":values.startYear,
        "endYear": values.endYear,
        "grade": values.grade,
        "activites": values.activites,
    })

    dispatch({
        type:ADD_EDUCATION,
        data:{
            "school": values.school,
            "degree": values.degree,
            "fieldOfStudy": values.fieldOfStudy,
            "startYear":values.startYear,
            "endYear": values.endYear,
            "grade": values.grade,
            "activites": values.activites,
        }
    })
}

export const addCertification = (values,profile_id) => async dispatch=>{
    await profileApi.post(`/licenseCertification/${profile_id}`,{
        "name": values.name ,
        "issuingOrganization": values.issuingOrganization,
        "isExpirable": values.isExpirable,
        "issueDate":values.issueDate,
        "expirationDate": values.expirationDate,
        "credentialId": values.credentialId,
        "credentialUrl": values.credentialUrl,
    })

    dispatch({
        type:ADD_CERTIFICATION,
        data:{
            "name": values.name ,
            "issuingOrganization": values.issuingOrganization,
            "isExpirable": values.isExpirable,
            "issueDate":values.issueDate,
            "expirationDate": values.expirationDate,
            "credentialId": values.credentialId,
            "credentialUrl": values.credentialUrl,
        }

    })
}

export const addVolunteer =(values,profile_id) =>async dispatch =>{
    await profileApi.post(`/volunteerExperience/${profile_id}`,{
        "organization": values.organization,
        "role": values.role,
        "cause": values.cause,
        "isCurrentlyVolunteering": values.isCurrentlyVolunteering,
        "startDate": values.startDate,
        "endDate": values.endDate,
        "description": values.description,
    })

    dispatch({
        type:ADD_VOLUNTEER,
        data:{
            "organization": values.organization,
            "role": values.role,
            "cause": values.cause,
            "isCurrentlyVolunteering": values.isCurrentlyVolunteering,
            "startDate": values.startDate,
            "endDate": values.endDate,
            "description": values.description,
        }
    })
}

export const addSkill = (values,profile_id)=>async dispatch =>{
    await profileApi.post(`/profile/${profile_id}/skills/add`,{
        "name":values.skills,
    })
    dispatch({
        type:ADD_SKILL,
        data:{
            "name":values.skills,
        }
    })
}

