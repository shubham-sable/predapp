import { FETCH_JOB_DETAIL } from "../types/jobsView";
import jobApi from '../apis';

export const fetchJobDetailAction = (id) => async dispatch => {
  const response = await jobApi.get(`/jobs/${id}`);
  dispatch({
    type: FETCH_JOB_DETAIL,
    data: response.data.data
  })
}
