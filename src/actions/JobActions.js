import {
  FETCH_SEARCHED_JOBS,
  FETCH_ALL_JOBS,
  ADD_REMOVED_SAVED_LIST,
  ADD_APPLICATION,
  GET_JOBS_APPLIED,
 } from "../types/jobs";

import jobApi from '../apis';

export const fetchJobListAction = (pageNumber, limit) => async dispatch => {
  const response = await jobApi.get('/jobs',{
    params:{
      skip:pageNumber,
      limit:limit
    }
  });
  dispatch({
    type: FETCH_ALL_JOBS,
    data: response.data.data
  })
}

export const addSavedListAction = (id) => async dispatch => {
  await jobApi.put(`/jobs/saved/${id}`);
  dispatch({
    type: ADD_REMOVED_SAVED_LIST,
    id
  })
}

export const removedSavedListAction = (id) => async dispatch =>{
  await jobApi.put(`/jobs/unsaved/${id}`);
  dispatch({
    type: ADD_REMOVED_SAVED_LIST,
    id
  })
}

export const addApplicationAction = (data,id) => async (dispatch) =>{
  await jobApi.post(`jobs/new/${id}`,{
    "id": "6" ,
    "name": "Chetan Godhani",
    "url": "",
    "Email": data.email,
    "Mobile": data.phoneNumber,
    "Comfortable": true,
    "Resume": "abc",
    "Urgently": true,
    "nodejs_exp": data.nodeEx,
    "reactjs_exp": data.reactEx
  });
  dispatch({
    type: ADD_APPLICATION,
    id
  });
}

export const getJobsApplied = (id) => async dispatch =>{
  const response = await jobApi.get(`jobs/applicants/${id}`,{
    params:{
      skip: 0,
      limit:10
    }
  });
  dispatch({
    type: GET_JOBS_APPLIED,
    data: response.data.data
  })
}
