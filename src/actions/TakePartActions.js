import {
  LOAD_TAKE_PART_QUIZ,
  ADD_VOTE,
  SET_LOADING_FLAG_TAKE_PART,
  LOAD_MY_CARDS,
  CREATE_CARD,
  DELETE_CARD,
  UPDATE_CARD,
  MY_TOTAL_VOTE,
  MY_TOTAL_COMMENTS,
  GET_COMMENT_OF_CARD,
  SAVE_NEW_COMMENT,
  SAVE_REPLIES_REPLY,
  SAVE_COMMENT_REPLY,
  DELETE_COMMENT,
  UPDATE_COMMENT,
  SAVE_COMMENT_LIKE_UNLIKE,
  GET_COMMENT_REPLIES
} from '../types/takePart';
import takePartApi from '../apis';
// import { valueContainerCSS } from 'react-select/src/components/containers';

export const loadAllQuizesAction = (userId,skip, limit) =>async dispatch => {
  const response= await takePartApi.get(`/get_all_cards/${userId}`,{
    params:{
      skip:skip,
      limit:limit
    }
  });
  setTimeout(()=>{
  dispatch({
    type: LOAD_TAKE_PART_QUIZ,
    data: response.data.data
  })
  },1000);

}
export const loadMyCards = (userId,skip, limit) =>async dispatch => {
  const response= await takePartApi.get(`/get_user_cards/${userId}`,{
    params:{
      skip:skip,
      limit:limit
    }
  });
  setTimeout(()=>{
  dispatch({
    type: LOAD_MY_CARDS,
    data: response.data.data
  })
  },1000);

}
// setTimeout(() => {
  //   dispatch({
  //     type: GET_ALL_POSTS_LIST,
  //     payload: response.data.Data
  //   });
  // }, 1000);

export const addRemoveVoteAction = (card_id, ans_id,isVoted,user_id) => dispatch => {
  console.log("this is what i wanted",card_id,ans_id);
  if (!isVoted){
    const response=takePartApi.post(`add_answer_vote/${card_id}/${ans_id}`,{
          "id": "1",
          "profileImg": "string",
          "name": {
            "fname": "Shubham",
            "lname": "Sable"
          }
      });
  }
  else{
    const response=takePartApi.post(`remove_answer_vote/${card_id}/${ans_id}/${user_id}`);
  }
  dispatch({
    type: ADD_VOTE,
    data: {
      "id": "1",
      "profileImg": "string",
      "name": {
        "fname": "Shubham",
        "lname": "Sable"
        }
    },
    card_id,
    ans_id,
    isVoted
  });
}
export const getTotalVotesOfUser = (user_id) => async dispatch => {
  const response= await takePartApi.get(`/total_user_vote/${user_id}`)
  dispatch({
    type:MY_TOTAL_VOTE,
    data:response.data.data.total_vote
  })
};

export const getTotalCommentsOfUser = (user_id) => async dispatch => {
  const response= await takePartApi.get(`/total_comment_like/${user_id}`)
  dispatch({
    type:MY_TOTAL_COMMENTS,
    data:response.data.data.total_comments
  })
};

export const getCommentsofCard = (parent_id,user_id) => async dispatch =>{
  const response = await takePartApi.get(`/get_comments/${parent_id}/${user_id}`)
  dispatch({
    type:GET_COMMENT_OF_CARD,
    data:response.data.data,
    parent_id
  })
}


export const getCommentReplies = (card_id, commentId, userId) => async dispatch => {
  const response=await takePartApi.get(`/get_comments/${commentId}/${userId}`)
  if(response.data.error!==undefined){
    console.log(response.data.error);
  }else{
    dispatch({
      type: GET_COMMENT_REPLIES,
      data: response.data.data,
      card_id,
      commentId
    })
  }
}

// export const getRepliesReply = (postId, commentId, replyId, userId) => async dispatch =>{
//   const response = await postapi.get(`/get_comments/${replyId}/${userId}`);
//   if(response.data.error!==undefined){
//     console.log(response.data.error);
//   }else{
//     dispatch({
//       type: GET_COMMENT_REPLIES,
//       data: response.data.data,
//       postId,
//       commentId
//     })
//   }
// }

export const editCommentAction =(card_id, commentId, isReply, parentId, comment) => async dispatch =>{
  await takePartApi.put(`/edit_comment/${card_id}/${commentId}`,{
    comment_text: comment
  })
  dispatch({
    type: UPDATE_COMMENT,
    card_id,
    commentId,
    isReply,
    parentId,
    comment
  })
}

export const saveCommentLikeUnlike = (card_id, commentId, isReply, replyCommentId, isliked, user) => async (dispatch) => {
  if(!isliked){
    if(replyCommentId!== undefined){
      const response=await takePartApi.post(`/add_comment_like/${card_id}/${replyCommentId}`,{
        "user_id": user.id,
        "profile_url": user.profileImg,
        "name": {
          "fname": user.name.fname,
          "lname": user.name.lname
        }
      })
    }else {
      await takePartApi.post(`/add_comment_like/${card_id}/${commentId}`,{
        "user_id": user.id,
        "profile_url": user.profileImg,
        "name": {
          "fname": user.name.fname,
          "lname": user.name.lname
        }
      })
    }
  }else{
    if(replyCommentId!== undefined){
      await takePartApi.delete(`/delete_comment_like/${card_id}/${replyCommentId}/${user.id}`)
    }else {
      await takePartApi.delete(`/delete_comment_like/${card_id}/${commentId}/${user.id}`)
    }
  }
  dispatch({
    type: SAVE_COMMENT_LIKE_UNLIKE,
    commentId,
    isReply,
    replyCommentId
  });
};

export const deleteCommentAction = (card_id, commentId, isReply, parentId) => async dispatch =>{
  await takePartApi.delete(`/delete_comment/${card_id}/${commentId}`)
  dispatch({
    type: DELETE_COMMENT,
    card_id,
    commentId,
    isReply,
    parentId
  });
}
export const postCommentAction = (card_id, comment, user, isReply, commentId, replyId) => async dispatch => {
  if(!isReply){
    //it's comment
    // console.log("comment");
    const response=await takePartApi.post(`/add_comment/${card_id}`,{
      "parent_id": card_id,
      "user": {
        "id": user.id,
        "profileImg": user.profileImg,
        "name": {
          "fname": user.name.fname,
          "lname": user.name.lname
        }
      },
      "comment_text": comment,
      "comment_likes": []
    });
    dispatch({
      type: SAVE_NEW_COMMENT,
      data: {
        "id":response.data.data.comment_id,
        "parent_id": card_id,
        "user": {
          "id": user.id,
          "profileImg": user.profileImg,
          "name": {
            "fname": user.name.fname,
            "lname": user.name.lname
          }
        },
        "comment_text": comment,
        "comment_likes": [],
        "createdAt":"string",
        "isliked":false,
        "replies": []
      },
      card_id
    });
  }else if(commentId !== undefined && replyId !== undefined){
    //it's reply's @reply
    // console.log("Reply @reply");
    const response= await takePartApi.post(`/add_comment/${card_id}`,{
      "parent_id": replyId,
      "user": {
        "id": user.id,
        "profileImg": user.profileImg,
        "name": {
          "fname": user.name.fname,
          "lname": user.name.lname
        }
      },
      "comment_text": comment,
      "comment_likes": []
    });
    dispatch({
      type: SAVE_REPLIES_REPLY,
      data: {
        "id":response.data.data.comment_id,
        "parent_id": replyId,
        "user": {
          "id": user.id,
          "profileImg": user.profileImg,
          "name": {
            "fname": user.name.fname,
            "lname": user.name.lname
          }
        },
        "comment_text": comment,
        "comment_likes": [],
        "isliked":false,
        "replies": []
      },
      card_id,
      commentId,
      replyId
    });
  }else{
    //it's comment @reply
    // console.log("Reply @comment");
    const response=await takePartApi.post(`/add_comment/${card_id}`,{
      "parent_id": commentId,
      "user": {
        "id": user.id,
        "profileImg": user.profileImg,
        "name": {
          "fname": user.name.fname,
          "lname": user.name.lname
        }
      },
      "comment_text": comment,
      "comment_likes": []
    });
    dispatch({
      type: SAVE_COMMENT_REPLY,
      data: {
        "id":response.data.data.comment_id,
        "parent_id": commentId,
        "user": {
          "id": user.id,
          "profileImg": user.profileImg,
          "name": {
            "fname": user.name.fname,
            "lname": user.name.lname
          }
        },
        "comment_text": comment,
        "comment_likes": []
      },
      card_id,
      commentId
    });
  }
}

export const createTakePartCardAction = (values) => async dispatch => {
  console.log("values",values);
  const response =await takePartApi.post('/create_card',{
    "card_type": values.what.value,
    "card_why": values.why,
    "card_answers": [
      {
        "answer": values.answers[0],
        "total_vote": 0,
        "user_vote": []
      },
      {
        "answer": values.answers[1],
        "total_vote": 0,
        "user_vote": []
      }
    ],
    "card_description": values.whyDescription,
    "card_img": values.whyImage,
    "card_person_dept": [
      {
        "id": "changes should be made",
        "dept_name": "take array"
      }
    ],
    "user": {
      "id": "1",
      "profileImg": "Goku",
      "name": {
        "fname": "Rushabh",
        "lname": "Bokade"
      }
    },
    "comments": [],
    "total_comment": 0
  })
  console.log("power cut console",response);
  dispatch({
    type: CREATE_CARD,
    data:{
        "id":response.data.data.card_id,
        "card_type": values.what.value,
        "card_why": values.why,
        "card_answers": [
          {
            "answer": values.answers[0],
            "total_vote": 0,
            "user_vote": []
          },
          {
            "answer": values.answers[1],
            "total_vote": 0,
            "user_vote": []
          }
        ],
        "card_description": values.whyDescription,
        "card_img": values.whyImage,
        "card_person_dept": [
          {
            "id": "changes should be made",
            "dept_name": "take array"
          }
        ],
        "user": {
          "id": "1",
          "profileImg": "Goku",
          "name": {
            "fname": "Rushabh",
            "lname": "Bokade"
          }
        },
        "comments": [],
        "total_comment": 0
      },
    },
  )
}
export const updateTakePartCardAction = (values,card_id) => async dispatch => {
  console.log("check is DAAN values",values);
  await takePartApi.put(`/update_card/${card_id}`,{
    "card_type": values.what,
    "card_why": values.why,
    "card_description": values.whyDescription,
    "card_img": values.whyImage
  })

  dispatch({
    type: UPDATE_CARD,
    data:{
      "card_type": values.what.value,
      "card_why": values.why,
      "card_description": values.whyDescription,
      "card_img": values.whyImage
      },
      card_id
    }
  )
}


export const deleteCard = (card_id) =>async dispatch => {
  await takePartApi.delete(`/delete_card/${card_id}`)
  dispatch({
    type: DELETE_CARD,
    card_id
  })
}

export const setLoadingFlagTakePartAction = () => dispatch => {
  dispatch({
    type: SET_LOADING_FLAG_TAKE_PART
  })
}


