import moxios from 'moxios';
import React from 'react';
import { shallow } from 'enzyme';


test('Testing in FollowCoworkersAction',()=>{

});


// import {loadCoworkersAction} from './FollowCoworkersAction';
// import {testStore} from '../../Utils';
//
// import {
//   LOAD_COWORKERS,
//   FOLLOW_UNFOLLOW
// } from '../types/followCoworkers';
//
// import * as actions from './FollowCoworkersAction';
//
// // describe('timepass action', () => {
//     it('should create an action to request a advice', () => {
//         const expectedAction = undefined;
//         expect(actions.timepass()).toEqual(expectedAction);
//     });
// });
//
// // describe('actions loadCoworkersAction', () => {
// //     it('should create an action to request a advice', () => {
// //         const expectedAction = {
// //         type: "FollowCoworkers/LOAD_COWORKERS",
// //         payload: "Success"
// //         };
// //         expect(actions.loadCoworkersAction('60263c56fe615b45578b026b')).toEqual(expectedAction);
// //     });
// // });
//
// describe('loadCoworkersAction action creator', () => {
//   beforeEach(() => {
//     moxios.install();
//   });
//   afterEach(() => {
//     moxios.uninstall();
//   });
//   test('testing', () => {
//     const data =[
//         {
//           "_id": "60263c56fe615b45578b026b",
//           "profileImg": "imageurl",
//           "firstName": "Chetan",
//           "lastName": "Godhani",
//           "following": [
//             {
//               "id": "string"
//             },
//             {
//               "id": "602666bd0205032fc47ea80e"
//             }
//           ],
//           "follow": false
//         },
//         {
//           "_id": "602666bd0205032fc47ea80e",
//           "profileImg": "imageurl",
//           "firstName": "Avery",
//           "lastName": "Walters",
//           "following": [
//             {
//               "id": "string"
//             },
//             {
//               "id": "string"
//             },
//             {
//               "id": "string"
//             },
//             {
//               "id": "602666bd0205032fc47ea80e"
//             },
//             {
//               "id": "602666bd0205032fc47ea80e"
//             },
//             {
//               "id": "602666bd0205032fc47ea80e"
//             },
//             {
//               "id": "string",
//               "firstName": "string",
//               "lastName": "string"
//             }
//           ],
//           "follow": false
//         },
//         {
//           "_id": "602666f40205032fc47ea80f",
//           "profileImg": "imageurl",
//           "firstName": "Tamara",
//           "lastName": "Mitchelle",
//           "following": [
//             {
//               "id": "602666bd0205032fc47ea80e"
//             }
//           ],
//           "follow": false
//         },
//         {
//           "_id": "602667180205032fc47ea810",
//           "profileImg": "imageurl",
//           "firstName": "Loretta",
//           "lastName": "Jacobs",
//           "following": [
//             {
//               "id": "602666bd0205032fc47ea80e"
//             },
//             {
//               "id": "602666bd0205032fc47ea80e"
//             }
//           ],
//           "follow": false
//         },
//         {
//           "_id": "6026673d0205032fc47ea811",
//           "profileImg": "imageurl",
//           "firstName": "Bonnie",
//           "lastName": "Price",
//           "following": [
//             {}
//           ],
//           "follow": false
//         },
//         {
//           "_id": "6026676a0205032fc47ea812",
//           "profileImg": "imageurl",
//           "firstName": "Megan",
//           "lastName": "Ramos",
//           "following": [],
//           "follow": false
//         },
//         {
//           "_id": "6026678a0205032fc47ea813",
//           "profileImg": "imageurl",
//           "firstName": "Cameron",
//           "lastName": "Clark",
//           "following": [],
//           "follow": false
//         },
//         {
//           "_id": "602667ac0205032fc47ea814",
//           "profileImg": "imageurl",
//           "firstName": "Virgil",
//           "lastName": "Carpenter",
//           "following": [],
//           "follow": false
//         },
//         {
//           "_id": "602667c80205032fc47ea815",
//           "profileImg": "imageurl",
//           "firstName": "Howard",
//           "lastName": "Bailey",
//           "following": [],
//           "follow": false
//         },
//         {
//           "_id": "602667e40205032fc47ea816",
//           "profileImg": "imageurl",
//           "firstName": "Robin",
//           "lastName": "Gregory",
//           "following": [],
//           "follow": false
//         }
//       ];
//     const store = testStore();
//
//     moxios.wait(() => {
//       const request = moxios.requests.mostRecent();
//     });
//
//     return store.dispatch(actions.loadCoworkersAction())
//       .then(() => {
//         const newState = store.getState()
//         expect(newState.followCoworkers.coworkers).toStrictEqual(data)
//       })
//   });
// });
