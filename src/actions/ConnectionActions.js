import { FETCH_SEARCHED_JOBS } from "../types/connections";

export const fetchAllConnectionsAction = () => dispatch => {
  dispatch({
    type: FETCH_SEARCHED_JOBS,
  })
}
