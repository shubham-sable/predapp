export const checkEnterAndClick = (triggerFunc, ...args) => e => {
  e && e.preventDefault();
  if(e.keyCode === 13) {
    triggerFunc(...args);
  }
}