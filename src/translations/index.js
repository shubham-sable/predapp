// Translations JSON
import EnglishJSON from './en/common.json';
import GermanJSON from './de/common.json';
import ChineseJSON from './cn/common.json';
import HindiJSON from './hi/common.json';
import ArabicJSON from './ar/common.json';

const translations = {
  'en-US': EnglishJSON,
  de: GermanJSON,
  'zh-CN': ChineseJSON,
  hi: HindiJSON,
  ar: ArabicJSON
};

export default translations;
