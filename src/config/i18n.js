export const supportedLocales = {
  'en-US': "English",
  'de': "German",
  'zh-CN': "Chinese",
  'hi' : "Hindi",
  'ar' : "Arabic",
};

export const fallbackLocale = "en";
