/* eslint-disable func-names */
import React from 'react';
import ReactDOM from 'react-dom';

import { store } from './createStore';
import { Provider } from 'react-redux';

import axios from 'axios';
import Moment from 'moment'
// import Keycloak from 'keycloak-js';
import momentLocalizer from 'react-widgets-moment';

import App from './App';
import * as serviceWorker from './serviceWorker';

import './index.scss';
import 'antd/dist/antd.css';
import 'react-bootstrap-typeahead/css/Typeahead.css';

Moment.locale('en');
momentLocalizer();

axios.defaults.baseURL = 'https://idm.prenigma.com/socialnetwork/';
axios.defaults.headers.post['Content-Type'] = 'application/json';

// TODO move that to webpack ENV
// const keycloak = Keycloak({
//   url: 'http://35.225.64.127/auth/',
//   realm: 'prenigma',
//   clientId: 'web_app',
//   redirectUri: 'http://localhost:3001'
// });

// function loadApp(keyCloakObj) {
//   let script = document.createElement('script');
//   script.onload = function () {
//     initiializeApp(keyCloakObj)
//   }
//   script.src = keycloakObj.url.split("auth")[0] + "auth/js/keycloak.js";
//   script.onerror = function () {
//     let script = document.createElement('script');
//     script.onload = function () {
//       initiializeApp(keyCloakObj)
//     }
//     script.src = "app/auth/keycloak.js"
//     document.head.appendChild(script);
//   }
//   document.head.appendChild(script);
// }

// keycloak.init({ onLoad: 'login-required' }).success(authenticated => {
//   console.log('authenticated', authenticated);

//   if (authenticated) {
//     console.log('keycloak.token', keycloak.token);
//     // loadApp(keycloak)
//     axios.defaults.headers.common.Authorization = `Bearer ${keycloak.token}`;

//     // TODO may need to move ReactDOM.render here
//   }

//   ReactDOM.render(
//     <Provider store={store}>
//       <App keycloak={keycloak} />
//     </Provider>, document.getElementById('root')
//   );

//   // If you want your app to work offline and load faster, you can change
//   // unregister() to register() below. Note this comes with some pitfalls.
//   // Learn more about service workers: https://bit.ly/CRA-PWA
// }).error(error => {
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>, document.getElementById('root')
  );
// });

if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("./firebase-messaging-sw.js")
    .then(function (registration) {
      console.log("Registration successful, scope is:", registration.scope);
    })
    .catch(function (err) {
      console.log("Service worker registration failed, error:", err);
    });
}

// ReactDOM.render(
//   <Provider store={store}>
//     <App />
//   </Provider>,
//   document.getElementById('root')
// );

serviceWorker.unregister();
