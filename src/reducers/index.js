import { combineReducers } from 'redux';

import common from './common';
import group from './groupReducer';
import events from './eventReducer';
import posts from './postReducers';
import companyAdminView from './companyAdminViewReducer';
import companyMemberView from './companyMemberViewReducer';
import takePart from './takePartReducer';
import notification from './notificationReducer';
import starMe from './starMeUpReducer';
import betterMe from './betterMeReducer';
import followCoworkers from './followCoworkersReducer';
import peopleDirectory from './peopleDirectoryReducer';
import jobs from './jobReducer';
import jobView from './jobViewReducer';
import connections from './connectionReducer';
import { i18nReducer } from "react-redux-i18n";

export default combineReducers({
  i18n: i18nReducer,
  common,
  group,
  events,
  posts,
  jobs,
  jobView,
  connections,
  companyAdminView,
  companyMemberView,
  takePart,
  notification,
  starMe,
  betterMe,
  followCoworkers,
  peopleDirectory,
});