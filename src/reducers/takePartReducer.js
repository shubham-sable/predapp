/* eslint-disable no-case-declarations */
import { Card } from 'antd';
import {
  LOAD_TAKE_PART_QUIZ,
  ADD_VOTE,
  SET_LOADING_FLAG_TAKE_PART,
  LOAD_MY_CARDS,
  CREATE_CARD,
  UPDATE_CARD,
  MY_TOTAL_VOTE,
  MY_TOTAL_COMMENTS,
  GET_COMMENT_OF_CARD,
  SAVE_NEW_COMMENT,
  DELETE_COMMENT,
  SAVE_COMMENT_LIKE_UNLIKE,
  GET_COMMENT_REPLIES,
  SAVE_REPLIES_REPLY,
  UPDATE_COMMENT,
  DELETE_CARD,
} from '../types/takePart';

const initialState = {
  availableCards: [],
  myCards: [],
  loading: true,
  currentPage: 1,
  limit: 20,
  totalCards: 45,
  myTotalVotes:0,
  myTotalComments:0,
  comment:[],
};

export default function common(state = initialState, action = {}) {
  let availableCards = state.availableCards;
  let myCard=state.myCards;
  let comment = state.comment;  
  switch (action.type) {
    case LOAD_TAKE_PART_QUIZ:
      // console.log("buss it ",action.data)
      // return {
      //   ...state,
      //   availableCards:action.data,
      //   // availableCards: [...state.availableCards, ...action.data],
      //   // myCards: [...state.myCards, ...action.data.myCards],
      //   // currentPage: action.data.currentPage,
      //   // totalCards: action.data.totalCards,
      //   loading: false,
      // }
      if(action.data.length > 0){
        return{
          ...state,
          availableCards: action.data,
          loading: false
        };
      }else{
        return{
          ...state,
          loading: true
        };
      }
    case CREATE_CARD:
      return{
        ...state,
        availableCards: [...state.availableCards, action.data],
        myCards: [...state.myCards, action.data]

      }
    case UPDATE_CARD:
      availableCards=state.availableCards.map((card)=>{
        if(card.id===action.card_id)
        {
          return{
            ...state,
            card:[...state,action.data]
          }
        }
        else{
          return{
            card,
          }
        }
      })
      myCard=state.myCards.map((card)=>{
        if(card.id===action.card_id)
        {
          return{
            ...state,
            card:[action.data]
          }
        }
        else{
          return{
            card,
          }
        }
      })
      return{
        ...state,
        availableCards,
        myCards:myCard
      }
    case DELETE_CARD:
      availableCards = availableCards.filter((value) => value.id !== action.card_id)
      myCard = state.myCards.filter((value) => value.id !== action.card_id)
      return{
        ...state,
        availableCards,
        myCards:myCard,
      }
    case LOAD_MY_CARDS:
      return{
        ...state,
        myCards:action.data,
        loading:false,
      }
    case MY_TOTAL_VOTE:
      return{
        ...state,
        myTotalVotes:action.data
      }
    case MY_TOTAL_COMMENTS:
      return{
        ...state,
        myTotalComments:action.data
      }
    case ADD_VOTE:
      availableCards = state.availableCards.map((card) => {
        if(card.id === action.card_id) {
          if(!card.isvoted) {
            const answer=card.card_answers.map((answer)=>{
              if(answer.id==action.ans_id){
                return{
                  ...answer,
                  user_vote: [...answer.user_vote,action.data],
                  total_vote: answer.total_vote + 1
                }
              }
              else{
                return answer;
              }

            })
            return {
              ...card,
              card_answers:answer,
              isvoted: !action.isVoted,

            }
          }else{
            const answer=card.card_answers.map((answer)=>{
              if(answer.id==action.ans_id){
                const user_vote = answer.user_vote.filter((value) => value.id !== action.data.id)
                return{
                  ...answer,
                  user_vote: user_vote,
                  total_vote: answer.total_vote - 1
                }
              }
              else{
                return answer;
              }
            })
            return {
              ...card,
              card_answers:answer,
              isvoted: !action.isVoted,
            }
          }
        } else {
          return card;
        }
      });
      const myCards = state.myCards.map((card) => {
        if(card.id === action.card_id) {
          if(!card.isvoted) {
            const answer=card.card_answers.map((answer)=>{
              if(answer.id==action.ans_id){
                return{
                  ...answer,
                  user_vote: [...answer.user_vote,action.data],
                  total_vote: answer.total_vote + 1
                }
              }
              else{
                return answer;
              }

            })
            return {
              ...card,
              card_answers:answer,
              isvoted: !action.isVoted,

            }
          }else{
            const answer=card.card_answers.map((answer)=>{
              if(answer.id==action.ans_id){
                const user_vote = answer.user_vote.filter((value) => value.id !== action.data.id)
                return{
                  ...answer,
                  user_vote: user_vote,
                  total_vote: answer.total_vote - 1
                }
              }
              else{
                return answer;
              }
            })
            return {
              ...card,
              card_answers:answer,
              isvoted: !action.isVoted,
            }
          }
        } else {
          return card;
        }
      });
      return { ...state, availableCards:availableCards,myCards:myCards };
    case GET_COMMENT_OF_CARD:
      availableCards = state.availableCards.map((card)=>{
        if(card.id==action.parent_id)
        {
          return{
            ...card,
            comment:action.data,
          }
        }
        else{
          return{
            card,
          }
        }
      });
      if (action.data){
        return {
          ...state,
          comment:action.data
        }
      }
      else{
        return{
          ...state,
        }
      }  
    case SAVE_NEW_COMMENT:
      return{
        ...state,
       comment:[...state.comment,action.data]
      }
    case UPDATE_COMMENT:
      if(!action.isReply){
        const comment = state.comment.map((comment) =>{
          if(comment.id === action.commentId){
            return{
              ...comment,
              comment_text: action.comment
            }
          }else{
            return comment;
          }
        })
        return{
          ...state,
          comment: comment
        }
      }else{
        const comment = state.comment.map((comment) =>{
          if(comment.id === action.parentId){
            const replies = comment.replies.map((reply) =>{
              if(reply.id === action.commentId){
                return{
                  ...reply,
                  comment_text: action.comment
                }
              }else{
                return reply;
              }
            })
            return{
              ...comment,
              replies: replies
            }
          }else{
            return comment;
          }
        })
        return{
          ...state,
          comment: comment
        }
      }
    case DELETE_COMMENT:
      if(!action.isReply){
        comment = comment.filter((value) => value.id !== action.commentId)
        return{
          ...state,
          comment,
        }
      }
      else{
          comment= state.comment.map((comment)=>{
          if(comment.id==action.parentId)
          {
            const replies = comment.replies.filter((value) => value.id !== action.commentId)
                return{
                  ...comment,
                  replies: replies
                }
          }
          else {
            return comment;
          }
        })
      }
      return {
        ...state,comment
      }
    case SAVE_COMMENT_LIKE_UNLIKE:
      comment = state.comment.map((comment) =>{
          if (comment.id === action.commentId){
            if(!action.isReply){
              return{
                ...comment,
                isliked:!comment.isliked,

              }
            }
            else{
              const replies=comment.replies.map((reply)=>{
                  if(reply.id === action.replyCommentId){
                    return{
                      ...reply,
                      isliked:!reply.isliked,
                    }
                  }
                  else{
                    return reply
                  }
              }
              )
              return{
                ...comment,
                replies:replies
              }
            }
          }
        else{
          return comment
        }
      });
      return { ...state, comment };
    case GET_COMMENT_REPLIES:
      comment = state.comment.map((comment) =>{
        if(comment.id === action.commentId){
          return{
            ...comment,
            replies: [...comment.replies,...action.data]
          }
        }else{
          return comment;
        }
      })
      return {...state,comment};
    case SAVE_REPLIES_REPLY:
      comment = state.comment.map((comment) =>{
        if(comment.id === action.commentId){
          return{
            ...comment,
            replies: [...comment.replies,action.data]
          }
        }else{
          return comment;
        }
      })
      return {...state,comment};
    case SET_LOADING_FLAG_TAKE_PART:
      return {
        ...state,
        loading: true,
        availableCards: [],
        myCards: [],
      }
    default: return state;
  }
}