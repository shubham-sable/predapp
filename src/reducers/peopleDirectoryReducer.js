import { FETCH_ORGANISATION_PEOPLE_DATA } from "../types/peopleDirectory";

const initialState = {
  loading: true,
  peopleData: [],
  currentPage: 1,
  limit: 20,
  totalItems: 200,
  searchString: '',
}

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ORGANISATION_PEOPLE_DATA:
      return {
        ...state,
        ...action.data,
        loading: false
      }
    default:
      return state
  }
}
