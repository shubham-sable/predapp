/* eslint-disable no-case-declarations */
import {
  GET_ALL_COMPANY_POSTS_LIST,
  SAVE_COMPANY_POST_LIKE_UNLIKE,
  SAVE_COMPANY_COMMENT_LIKE_UNLIKE,
  RESET_COMPANY_POST_LIST_NEWSFEED,
  LOAD_MORE_COMMENTS_COMPANY,
} from '../types/companyAdmin';

const initialState = {
  postsList: [],
  currentPage: 1,
  limit: 20,
  loading: true,
  totalPosts: 200,
  commentsLimit: 15,
};

export default function common(state = initialState, action = {}) {
  let postsList = state.postsList;
  switch (action.type) {
    case GET_ALL_COMPANY_POSTS_LIST:
      return {
        ...state,
        postsList: [...state.postsList, ...action.data.postsList],
        currentPage: action.data.currentPage,
        limit: action.data.limit,
        totalPosts: action.data.totalPosts,
        loading: false,
      };
    case SAVE_COMPANY_POST_LIKE_UNLIKE:
      postsList = state.postsList.map((post) => {
        if(post.id === action.postId) {
          return {
            ...post,
            isLiked: !post.isLiked
          }
        } else {
          return post;
        }
      });
      return { ...state, postsList };
    case SAVE_COMPANY_COMMENT_LIKE_UNLIKE:
      postsList = state.postsList.map((post) => {
        if(post.id === action.postId) {
          const comments = post.comments.map(comment => {
            if(comment.commentId === action.commentId) {
              if(!action.isReply) {
                return {
                  ...comment,
                  isLiked: !comment.isLiked
                }
              } else {
                return {
                  ...comment,
                  replies: comment.replies.map(reply => {
                    if(reply.commentId === action.replyCommentId) {
                      return {
                        ...reply,
                        isLiked: !reply.isLiked
                      }
                    } else {
                      return reply;
                    }
                  })
                }
              }
            } else {
              return comment;
            }
          });
          return {
            ...post,
            comments
          }
        } else {
          return post;
        }
      });
      return { ...state, postsList };
    case LOAD_MORE_COMMENTS_COMPANY:
      const newPostList = postsList.map(post => {
        if(post.id === action.data.postId) {
          return {
            ...post,
            comments: [...post.comments, ...action.data.comments],
            currentPage: action.data.currentPage
          }
        }

        return post
      })

      return {
        ...state,
        postsList: newPostList
      }
    case RESET_COMPANY_POST_LIST_NEWSFEED:
      return {
        ...state,
        postsList: [],
        currentPage: 1,
        limit: 20,
        loading: true,
        totalPosts: 200,
      }
    default:
      return state;
  }
}