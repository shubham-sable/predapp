import {
    ADD_COMMENT,
    ADD_LIKE,
    CHANGE_PAGE_NUMBER_STARMEUP,
    LOAD_COMMENTS,
    LOAD_RECEIVED_STARS,
    LOAD_RECENT_STARS,
    LOAD_SENT_STARS,
    SET_COMMENTS_LOADING_STARMEUP
  } from '../types/starMeUp';
  
  import common from './starMeUpReducer';

  const post_example= {
    "id": "8d0a963d-fc0c-43e6-80fb-511a77b5253b",
    "sentTo": {
      "id": "3",
      "profileImg": "/assets/imgs/bradpitt.jpeg",
      "name": {
        "fname": "Fouad",
        "lname": "Omri"
      }
    },
    "description": "hjgjh",
    "createdAt": 1620377931.914311,
    "type": "team_player",
    "totalLikes": 1,
    "totalComments": 0,
    "postedBy": {
      "id": "chetan1",
      "profileImg": "https://www.gravatar.com/avatar/72D1A6CDBDFBE17B98B6510E908683C9.jpg",
      "name": {
        "fname": "Chetan",
        "lname": "Godhani"
      }
    },
    "isLiked": true,
    "stars_likes": [
      {
        "id": "chetan1",
        "profileImg": "image",
        "name": {
          "fname": "Chetan",
          "lname": "Godhani"
        }
      }
    ],
    "comment": []
  }

  const comments_example = [
    {
      "comments": {
        "parentId": "1",
        "user": {
          "id": "chetan1",
          "profileImg": "string",
          "name": {
            "fname": "Chetan",
            "lname": "Godhani"
          }
        },
        "comment_text": "Congratulations",
        "id": "1",
        "createdAt": 1620327428.264914,
        "totalCommentLikes": 1,
        "comments_likes": [
          {
            "id": "satvik1",
            "profileImg": "string",
            "name": {
              "fname": "Satvik",
              "lname": "Sahore"
            }
          }
        ],
        "replies": [],
        "isLiked": false
      }
    }
  ]
  
  test('returns default initial state when no action is passed', () => {
    const newState = common(undefined, {});
    const expectedState = {
        recentStars: [],
        receivedStars: [],
        sentStars: [],
        loading: true,
        currentPage: 0,
        limit: 20,
        totalStars: 2,
      }
    expect(newState).toEqual(expectedState);
  });
  test('returns state upon receiving an action of type LOAD_RECENT_STARS', () => {
    const newState = common(undefined, { type: LOAD_RECENT_STARS, data: {recentStars: [post_example]}})
    const expectedState = [post_example]
    expect(newState.recentStars).toEqual(expectedState);
  });
  test('returns state upon receiving an action of type LOAD_SENT_STARS', () => {
    const newState = common(undefined, { type: LOAD_SENT_STARS, data: {sentStars: [post_example]}})
    const expectedState = [post_example]
    expect(newState.sentStars).toEqual(expectedState);
  });
  test('returns state upon receiving an action of type LOAD_RECEIVED_STARS', () => {
    const newState = common(undefined, { type: LOAD_RECEIVED_STARS, data: {receivedStars: [post_example]}})
    const expectedState = [post_example]
    expect(newState.receivedStars).toEqual(expectedState);
  });
  test('returns state upon receiving an action of type ADD_LIKE', () => {
    const newState = common({recentStars: [post_example]}, { type: ADD_LIKE, data: {id: post_example.id, type: 'recentStars'}})
    const expectedState = [post_example]
    expect(newState.recentStars[0].isLiked).toEqual(!post_example.isLiked);
    expect(newState.recentStars[0].stars_likes).toEqual([]);
    expect(newState.recentStars[0].totalLikes).toEqual(0);
  });
  test('returns state upon receiving an action of type LOAD_COMMENTS', () => {
    const newState = common({recentStars: [post_example]}, { type: LOAD_COMMENTS, parentId: post_example.id, data: {data: comments_example}})
    const expectedState = [comments_example[0].comments]
    expect(newState.recentStars[0].comment).toEqual(expectedState);
  });
  test('returns state upon receiving an action of type ADD_COMMENT', () => {
    const newState = common({recentStars: [post_example]}, { type: ADD_COMMENT, data: {data: comments_example}, parentId: post_example.id, postId: post_example.id})
    const expectedState = comments_example.comments
    expect(newState.recentStars[0].comments).toEqual(expectedState);
  });
  test('returns state upon receiving an action of type SET_LOADING_FLAG_STARMEUP', () => {
    const newState = common(undefined, { type: SET_COMMENTS_LOADING_STARMEUP})
    expect(newState.loading).toEqual(true);
  });
  test('returns state upon receiving an action of type CHANGE_PAGE_NUMBER_STARMEUP', () => {
    const newState = common(undefined, { type: CHANGE_PAGE_NUMBER_STARMEUP, data: 1})
    expect(newState.currentPage).toEqual(1);
  });
