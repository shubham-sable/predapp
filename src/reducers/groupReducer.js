import {
  FETCH_GROUP_LIST,
  CREATE_GROUP_TITLE,
  RECEIVED_GROUP_DATA,
  FETCH_GROUP_POSTS,
  SAVE_GROUP_POST_LIKE_UNLIKE,
  SAVE_GROUP_POST_COMMENT_LIKE_UNLIKE,
  RESET_LOADING_FLAG_GROUP_LIST,
  RESET_LOADING_FLAG_GROUP_POST,
} from '../types/groupTypes';

const initialState = {
  groupData: {},
  groupPosts: [],
  groupList: [],
  groupPostsLoading: true,
  groupPostsCurrentPage: 1,
  groupPostsLimit: 20,
  groupPostsTotal: 100,
  groupListLoading: true,
  groupListCurrentPage: 1,
  groupListLimit: 30,
  groupListTotal: 50,
};

export default function common(state = initialState, action = {}) {
  let groupPosts = [];
  switch (action.type) {
    case FETCH_GROUP_LIST:
      return {
        ...state,
        groupList: [...state.groupList, ...action.data.groupList],
        groupListCurrentPage: action.data.currentPage,
        groupListLoading: false,
        groupListTotal: action.data.groupListTotal
      }
    case CREATE_GROUP_TITLE:
      return { ...state, title: action.title };
    case RECEIVED_GROUP_DATA:
      return {
        ...state,
        groupData: action.data
      };
    case FETCH_GROUP_POSTS:
      return {
        ...state,
        groupPosts: [...state.groupPosts, ...action.data.groupPosts],
        groupPostsCurrentPage: action.data.groupPostsCurrentPage,
        groupPostsLoading: false,
        groupPostsTotal: action.data.groupPostsTotal
      };
    case SAVE_GROUP_POST_LIKE_UNLIKE:
      groupPosts = state.groupPosts.map((post) => {
        if(post.id === action.postId) {
          return {
            ...post,
            isLiked: !post.isLiked
          }
        } else {
          return post;
        }
      });
      return { ...state, groupPosts };
    case SAVE_GROUP_POST_COMMENT_LIKE_UNLIKE:
      groupPosts = state.groupPosts.map((post) => {
        if(post.id === action.postId) {
          const comments = post.comments.map(comment => {
            if(comment.commentId === action.commentId) {
              if(!action.isReply) {
                return {
                  ...comment,
                  isLiked: !comment.isLiked
                }
              } else {
                return {
                  ...comment,
                  replies: comment.replies.map(reply => {
                    if(reply.commentId === action.replyCommentId) {
                      return {
                        ...reply,
                        isLiked: !reply.isLiked
                      }
                    } else {
                      return reply;
                    }
                  })
                }
              }
            } else {
              return comment;
            }
          });
          return {
            ...post,
            comments
          }
        } else {
          return post;
        }
      });
      return { ...state, groupPosts };
    case RESET_LOADING_FLAG_GROUP_LIST:
      return {
        ...state,
        groupList: [],
        groupListLoading: true,
        groupListCurrentPage: 1,
        groupListLimit: 30,
        groupListTotal: 0,
      }
    case RESET_LOADING_FLAG_GROUP_POST:
      return {
        ...state,
        groupPosts: [],
        groupPostsLoading: true,
        groupPostsCurrentPage: 1,
        groupPostsLimit: 20,
        groupPostsTotal: 0,
      }
    default: return state;
  }
}
