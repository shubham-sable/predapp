/* eslint-disable no-case-declarations */
import {
  LOAD_COWORKERS,
  FOLLOW_UNFOLLOW
} from '../types/followCoworkers';

const initialState = {
  coworkers: [],
};

export default function common(state = initialState, action = {}) {
  let coworkers= state.coworkers;
  switch (action.type) {
    case LOAD_COWORKERS:
      if(action.data !== undefined){
        return {
          ...state,
          coworkers:action.data
        }
      }else{
        return {
          ...state
        }
      }
    case FOLLOW_UNFOLLOW:
      coworkers = state.coworkers.map((value)=>{
        if(action.id==value._id){
          return{
            ...value,
            follow: !value.follow
          }
        }else{
          return value;
        }
      });
      return{...state, coworkers}
    default: return state;
  }
}
