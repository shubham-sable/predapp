/* eslint-disable no-case-declarations */
import {
  GET_ALL_POSTS_LIST,
  SAVE_POST_LIKE_UNLIKE,
  GET_POST_COMMENT,
  GET_COMMENT_REPLIES,
  SAVE_NEW_COMMENT,
  SAVE_COMMENT_REPLY,
  SAVE_REPLIES_REPLY,
  SAVE_COMMENT_LIKE_UNLIKE,
  SAVE_NEW_POST,
  DELETE_POST,
  DELETE_COMMENT,
  UPDATE_COMMENT,
  RESET_POST_LIST_NEWSFEED,
  LOAD_MORE_COMMENTS_NEWSFEED,
} from '../types/posts';

const initialState = {
  postsList: [],
  currentPage: 0,
  limit: 20,
  loading: true,
  totalPosts: 200,
  commentsLimit: 15,
};

// if (action.payload === undefined){
//   return{
//     ...state,
//     loading: true,
//   }
// }
// else{
//   return {
//     ...state,
//     postsList: [...state.postsList, ...action.payload.items],
//     currentPage: action.payload.currentPage,
//     limit: action.payload.limit,
//     totalPosts: action.payload.totalPosts,
//     loading: false,
//   };
// }


export default function common(state = initialState, action = {}) {
  let postsList = state.postsList;
  switch (action.type) {
    case GET_ALL_POSTS_LIST:
      if(action.data !== undefined){
        return{
          ...state,
          postsList: [...state.postsList,...action.data],
          loading: false
        };
      }else{
        return{
          ...state,
          loading: true
        };
      }
    case SAVE_POST_LIKE_UNLIKE:
      postsList = state.postsList.map((post) => {
        if(post.id === action.postId) {
          if(!post.isliked) {
            return {
              ...post,
              isliked: !post.isliked,
              likes: [...post.likes,action.data],
              totallikes: post.totallikes + 1
            }
          }else{
            const likes = post.likes.filter((value) => value.id !== action.data.id)
            return {
              ...post,
              isliked: !post.isliked,
              likes: likes,
              totallikes: post.totallikes - 1
            }
          }
        } else {
          return post;
        }
      });
      return { ...state, postsList };
    case GET_POST_COMMENT:
      postsList = state.postsList.map((post) =>{
        if(post.id === action.postId){
          return{
            ...post,
            comment: [...post.comment, ...action.data]
          };
        }else{
          return post;
        }
      });
      return { ...state, postsList };
    case GET_COMMENT_REPLIES:
      postsList = state.postsList.map((post) =>{
        if(post.id === action.postId){
          const comment = post.comment.map((comment) =>{
            if(comment.id === action.commentId){
              return{
                ...comment,
                replies: [...comment.replies,...action.data]
              }
            }else{
              return comment;
            }
          })
          return{
            ...post,
            comment: comment
          }
        }else{
          return post;
        }
      });
      return { ...state, postsList };
    case SAVE_NEW_COMMENT:
      postsList = state.postsList.map((post) => {
        if(post.id === action.postId) {
          return{
            ...post,
            comment: [action.data,...post.comment]
          }
        } else {
          return post;
        }
      });
      return { ...state, postsList };
    case SAVE_COMMENT_REPLY:
      postsList = state.postsList.map((post) =>{
        if(post.id === action.postId){
          const comment = post.comment.map((comment) =>{
            if(comment.id === action.commentId){
              return{
                ...comment,
                replies: [...comment.replies,action.data]
              }
            }else{
              return comment;
            }
          })
          return{
            ...post,
            comment: comment
          }
        }else{
          return post;
        }
      });
      return { ...state, postsList };
    case SAVE_REPLIES_REPLY:
      postsList = state.postsList.map((post) =>{
        if(post.id === action.postId){
          const comment = post.comment.map((comment) =>{
            if(comment.id === action.commentId){
              return{
                ...comment,
                replies: [...comment.replies,action.data]
              }
            }else{
              return comment;
            }
          })
          return{
            ...post,
            comment: comment
          }
        }else{
          return post;
        }
      });
      return { ...state, postsList };
    case SAVE_COMMENT_LIKE_UNLIKE:
      postsList = state.postsList.map((post) => {
        if(post.id === action.postId) {
          const comment = post.comment.map(comment => {
            if(comment.id === action.commentId) {
              if(!action.isReply) {
                return {
                  ...comment,
                  isliked: !comment.isliked
                }
              } else {
                return {
                  ...comment,
                  replies: comment.replies.map(reply => {
                    if(reply.id === action.replyCommentId) {
                      return {
                        ...reply,
                        isliked: !reply.isliked
                      }
                    } else {
                      return reply;
                    }
                  })
                }
              }
            } else {
              return comment;
            }
          });
          return {
            ...post,
            comment
          }
        } else {
          return post;
        }
      });
      return { ...state, postsList };
    case DELETE_POST:
      postsList = state.postsList.filter((value) => value.id !== action.postId)
      return { ...state, postsList };
    case SAVE_NEW_POST:
      return {
        ...state,
        postsList: [action.data,...postsList]
      }
    case DELETE_COMMENT:
      postsList = state.postsList.map((post) =>{
        if(post.id === action.postId){
          if(!action.isReply){
            const comment = post.comment.filter((value) => value.id !== action.commentId)
            return{
              ...post,
              comment: comment
            }
          }else{
            const comment = post.comment.map((comment) => {
              if(comment.id === action.parentId){
                const replies = comment.replies.filter((value) => value.id !== action.commentId)
                return{
                  ...comment,
                  replies: replies
                }
              }else{
                return comment;
              }
            })
            return {
              ...post,
              comment: comment
            }
          }
        }else{
          return post;
        }
      })
      return { ...state, postsList };
    case UPDATE_COMMENT:
      postsList = state.postsList.map((post) => {
        if(post.id === action.postId){
          if(!action.isReply){
            const comment = post.comment.map((comment) =>{
              if(comment.id === action.commentId){
                return{
                  ...comment,
                  comment_text: action.comment
                }
              }else{
                return comment;
              }
            })
            return{
              ...post,
              comment: comment
            }
          }else{
            const comment = post.comment.map((comment) =>{
              if(comment.id === action.parentId){
                const replies = comment.replies.map((reply) =>{
                  if(reply.id === action.commentId){
                    return{
                      ...reply,
                      comment_text: action.comment
                    }
                  }else{
                    return reply;
                  }
                })
                return{
                  ...comment,
                  replies: replies
                }
              }else{
                return comment;
              }
            })
            return{
              ...post,
              comment: comment
            }
          }
        }else{
          return post;
        }
      })
      return { ...state, postsList };
    case LOAD_MORE_COMMENTS_NEWSFEED:
      const newPostList = postsList.map(post => {
        if(post.id === action.data.postId) {
          return {
            ...post,
            comments: [...post.comments, ...action.data.comments],
            currentPage: action.data.currentPage
          }
        }

        return post
      })

      return {
        ...state,
        postsList: newPostList
      }
    case RESET_POST_LIST_NEWSFEED:
      return {
        ...state,
        postsList: [],
        currentPage: 1,
        limit: 20,
        loading: true,
        totalPosts: 200,
      }
    default:
      return state;
  }
}
