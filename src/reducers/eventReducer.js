import {
  SET_LOADING_FLAG_EVENTS,
  FETCH_TODAY_EVENT,
  FETCH_THIS_WEEK_EVENT,
  FETCH_THIS_MONTH_EVENT,
  FETCH_PREVIOUS_EVENT,
  CREATE_NEW_EVENT,
  GET_EVENT_BY_ID,
  GET_DATE_EVENTS,
  JOIN_EVENT,
  FETCH_EVENT_LIST,
  SET_LOADING_STATE_CLEANUP_EVENTS,
} from '../types/event';

import moment from "moment";

const initialState = {
  renderEvents: [],
  todayEvents: [],
  thisWeekEvents: [],
  thisMonthEvents: [],
  prevEvents: [],
  eventsList:[],
  todayeventlist: [],
  eventView: {
    loading: true,
    eventData: {}
  },
  todaytotalevents: 0,
  page: 1,
  selectedDate: moment().format("YYYY-MM-DD"),
  currentPage: 1,
  limit: 15,
  totalEvents: 0,
  loading: true,
};

export default (state = initialState, action = {}) => {
  const { eventsList } = state;
  switch (action.type) {
    case SET_LOADING_FLAG_EVENTS:
      if(action.value.length === 0){
        return {
          ...state,
          loading: true,
          renderEvents: []
        }
      }else{
        return {
          ...state,
          loading: false
        }
      }
    case FETCH_TODAY_EVENT:
      if(action.data !== undefined && action.data.length > 0 ){
        return {
          ...state,
          renderEvents: action.data,
          todayEvents: action.data,
          loading: false
        }
      }else{
        return{
          ...state,
          loading: false
        }
      }
    case FETCH_THIS_WEEK_EVENT:
      if(action.data !== undefined && action.data.length > 0 ){
        return {
          ...state,
          renderEvents: action.data,
          thisWeekEvents: action.data,
          loading: false
        }
      }else{
        return{
          ...state,
          loading: false
        }
      }
    case FETCH_THIS_MONTH_EVENT:
      if(action.data !== undefined && action.data.length > 0 ){
        return {
          ...state,
          renderEvents: action.data,
          thisMonthEvents: action.data,
          loading: false
        }
      }else{
        return{
          ...state,
          loading: false
        }
      }
    case FETCH_PREVIOUS_EVENT:
      if(action.data !== undefined && action.data.length > 0 ){
        return {
          ...state,
          renderEvents: action.data,
          prevEvents: action.data,
          loading: false
        }
      }else{
        return{
          ...state,
          loading: false
        }
      }
    case CREATE_NEW_EVENT:
      return{
        ...state,
        loading: false
      }
    case GET_EVENT_BY_ID:
      return{
        ...state,
        eventView: {
          loading: false,
          eventData: action.data
        }
      }
    case SET_LOADING_STATE_CLEANUP_EVENTS:
      return{
        ...state,
        renderEvents: [],
        todayEvents: [],
        thisWeekEvents: [],
        thisMonthEvents: [],
        prevEvents: [],
        selectedDate: moment().format("YYYY-MM-DD"),
        loading: true
      }
    case JOIN_EVENT:
      const events = state.renderEvents.map((value) =>{
        if(value.id === action.eventId){
          return{
            ...value,
            participants: [...value.participants, action.data]
          }
        }else{
          return value
        }
      })
      return { ...state, renderEvents: events}
    case GET_DATE_EVENTS:
      if(action.data.length > 0){
        return{
          ...state,
          renderEvents: action.data,
          loading: false
        }
      }else{
        return{
          ...state,
          renderEvents: [],
          loading: false
        }
      }
    default:
      return state;
  }
}


// case FETCH_EVENT_LIST:
//   return {
//     ...state,
//     events: [...state.events, ...action.data.events],
//     currentPage: action.data.currentPage,
//     totalEvents: action.data.totalEvents,
//     selectedDate: action.data.selectedDate,
//     loading: false,
//   }
// case CREATE_NEW_EVENT:
//   const lastID = state.events.length + 1;
//   action.data.id = lastID;
//   return { state, events: [...state.events, action.data] };
// case GET_DATE_EVENTS:
//   return {
//     ...state,
//     loading: false,
//     events: action.payload,
//     selectedDate: action.currentdate,
//     page: action.pageNumber + 1
//   };
// case SET_LOADING_STATE_CLEANUP_EVENTS:
//   return {
//     ...state,
//     events: [],
//     selectedDate: moment().format("YYYY-MM-DD"),
//     currentPage: 1,
//     limit: 20,
//     totalEvents: 0,
//     loading: true,
//   }
// case FETCH_TODAY_EVENT:
//   if(action.payload !== undefined){
//     return{
//       ...state,
//       loading: false,
//       events: action.payload,
//       totalEvents: action.payload.length
//     }
//   }else{
//     return{
//       ...state,
//       loading: true
//     }
//   }
// case FETCH_THIS_WEEK_EVENT:
//   if(action.payload !== undefined){
//     return{
//       ...state,
//       loading: false,
//       events: action.payload,
//     }
//   }else{
//     return{
//       ...state,
//       loading: true
//     }
//   }


// [
//   {
//     id: '1',
//     eventName: 'Fun Event',
//     locationName: 'Ahmedabad, India',
//     latLong: '23.02579, 72.58727',
//     image: '/minion.jpeg',
//     allDay: false,
//     address: '123 Street, Amd, Gujarat 380001',
//     end: 'October 30, 2020 11:00:00',
//     startDate: 'October 20, 2020 12:00:00',
//     title: 'hello !!',
//     description: 'test description for Test Event',
//     file: '/nature.jpg',
//     guestCanInvite: 'on',
//     showGuestList: 'on',
//     participates: [
//       '/assets/participate-profile2.jpeg',
//       '/assets/participate-profile.jpeg',
//       '/assets/participate-profile2.jpeg',
//     ],
//   },
//   {
//     id: '2',
//     eventName: 'Brainstorming Event',
//     locationName: 'Ahmedabad Test, India',
//     latLong: '23.02579, 72.58727',
//     image: '/assets/imgs/group_1.jpg',
//     allDay: false,
//     address: '123 Street, Baroda, Gujarat 390001',
//     end: 'October 21, 2020 12:00:00',
//     startDate: 'October 18, 2020 12:00:00',
//     title: 'All Day Event',
//     description: 'test  description for Test Event 2',
//     file: '/nature2.jpeg',
//     guestCanInvite: 'off',
//     showGuestList: 'on',
//     participates: [
//       '/assets/participate-profile2.jpeg',
//       '/assets/participate-profile.jpeg',
//       '/assets/participate-profile2.jpeg',
//     ],
//   },
//   {
//     id: '3',
//     eventName: 'Innovation Event',
//     locationName: 'Ahmedabad, India',
//     latLong: '23.02579, 72.58727',
//     image: '/minion.jpeg',
//     allDay: false,
//     address: '123 Street, Amd, Gujarat 380001',
//     end: 'October 30, 2020 11:00:00',
//     startDate: 'October 20, 2020 12:00:00',
//     title: 'hello !!',
//     description: 'test description for Test Event',
//     file: '/nature.jpg',
//     guestCanInvite: 'on',
//     showGuestList: 'on',
//     participates: [
//       '/assets/participate-profile2.jpeg',
//       '/assets/participate-profile.jpeg',
//       '/assets/participate-profile2.jpeg',
//     ],
//   },
//   {
//     id: '4',
//     eventName: 'Yoga Event',
//     locationName: 'Ahmedabad, India',
//     latLong: '23.02579, 72.58727',
//     image: '/minion.jpeg',
//     allDay: false,
//     address: '123 Street, Amd, Gujarat 380001',
//     end: 'October 30, 2020 11:00:00',
//     startDate: 'October 20, 2020 12:00:00',
//     title: 'hello !!',
//     description: 'test description for Test Event',
//     file: '/nature.jpg',
//     guestCanInvite: 'on',
//     showGuestList: 'on',
//     participates: [
//       '/assets/participate-profile2.jpeg',
//       '/assets/participate-profile.jpeg',
//       '/assets/participate-profile2.jpeg',
//     ],
//   },
//   {
//     id: '5',
//     eventName: 'Workshop Event',
//     locationName: 'Ahmedabad, India',
//     latLong: '23.02579, 72.58727',
//     image: '/minion.jpeg',
//     allDay: false,
//     address: '123 Street, Amd, Gujarat 380001',
//     end: 'October 30, 2020 11:00:00',
//     startDate: 'October 20, 2020 12:00:00',
//     title: 'hello !!',
//     description: 'test description for Test Event',
//     file: '/nature.jpg',
//     guestCanInvite: 'on',
//     showGuestList: 'on',
//     participates: [
//       '/assets/participate-profile2.jpeg',
//       '/assets/participate-profile.jpeg',
//       '/assets/participate-profile2.jpeg',
//     ],
//   },
//
// ]

// return{
//   ...state,
//   loading: false,
//   events: action.payload,
//   todayeventlist: action.payload,
//   todaytotalevents: action.payload.length
// }

// events: action.payload.items,
