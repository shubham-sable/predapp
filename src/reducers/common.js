import { UPDATE_NAV_TITLE } from '../types';

const initialState = {
	titleSetting: {
    title: '',
    icon: ''
  },
  mygroups: [],
};

export default function common(state = initialState, action = {}) {
  switch (action.type) {
    case UPDATE_NAV_TITLE:
      return { ...state, titleSetting: action.title };
    default: return state;
  }
}
