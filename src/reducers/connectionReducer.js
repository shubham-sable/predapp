import { FETCH_ALL_CONNECTIONS } from "../types/connections";

const initialState = {
  loading: true,
  peopleData: [],
  currentPage: 1,
  limit: 20,
  totalItems: 200,
  searchString: '',
}

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ALL_CONNECTIONS:
      return {
        ...state,
        loading: false
      }
    default:
      return state
  }
}
