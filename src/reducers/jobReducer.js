import {
  FETCH_SEARCHED_JOBS,
  FETCH_ALL_JOBS,
  ADD_REMOVED_SAVED_LIST,
  ADD_APPLICATION,
  GET_JOBS_APPLIED,
 } from "../types/jobs";

const initialState = {
  loading: true,
  peopleData: [],
  currentPage: 1,
  limit: 20,
  totalItems: 200,
  searchString: '',
  jobs: [],
  jobsApplied: [],
}

export default (state = initialState, action) => {
  let jobs = state.jobs;
  switch (action.type) {
    case FETCH_SEARCHED_JOBS:
      return {
        ...state,
        loading: false
      }
    case FETCH_ALL_JOBS:
      if(action.data !== undefined){
        return {
          ...state,
          loading: false,
          jobs: action.data
        }
      }else{
        return{
          ...state
        }
      }
    case ADD_REMOVED_SAVED_LIST:
      jobs = state.jobs.map((job) =>{
        if(job.id === action.id){
          return {
            ...job,
            saved: !job.saved,
          }
        } else {
          return job;
        }
      });
      return { ...state, jobs };
    case ADD_APPLICATION:
      const data={ id: action.id}
      return{
        ...state,
        jobsApplied: [...state.jobsApplied, data]
      }
    case GET_JOBS_APPLIED:
      return{
        ...state,
        jobsApplied: action.data
      }
    default:
      return state
  }
}
