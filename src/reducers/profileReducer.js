import {
    GET_PROFILE_DATA,
    EDIT_CONTACT,
    ADD_WORK_EXPERIENCE,
    ADD_EDUCATION,
    ADD_CERTIFICATION,
    ADD_VOLUNTEER,
    ADD_SKILL,
} from '../types/profile';


const initialState = {
    loading: true,
    totalConnections: 0,
    profile:{
        id: "1",
        profileImg: "xyz",
        name:{
          fname: "Chetan",
          lname: "Godhani"
      }
    },
    user: {},
    contact:{},
    media:{},
    accomplishments:{
        courses:[],
        publications:[],
        patents:[],
        projects:[],
        honorAndAwards:[],
        testScore:[],
        languages:[],
        organizations:[],
    },
    workExperience:[],
    education:[],
    licenseCertification:[],
    volunteerExperience:[],
    skills:[],
    interests:[],
    connections:[],
    following:[],
};


export default function common(state=initialState,action={}){
    switch(action.type){
        case GET_PROFILE_DATA:
            return{
                ...action.data[0],
                profile: {
                    id: action.data[0].profile_id,
                    name: {
                        fname: action.data[0].user.firstName,
                        lname: action.data[0].user.lastName
                    },
                    profile: action.data[0].media.profileImg
                },
                
                loading: false
            }
        case EDIT_CONTACT:
            return{
                ...state,
                contact:action.data,
            }
        case ADD_WORK_EXPERIENCE:
            return{
                ...state,
                workExperience:[...state.workExperience,action.data],
            }
        case ADD_EDUCATION:
            return{
                ...state,
                education:[...state.education,action.data],
            }
        case ADD_CERTIFICATION:
            return{
                ...state,
                licenseCertification:[...state.licenseCertification,action.data],
            }
        case ADD_VOLUNTEER:
            return{
                ...state,
                volunteerExperience:[...state.volunteerExperience,action.data],
            }
        case ADD_SKILL:
            return{
                ...state,
                skills:[...state.skills,action.data],
            }
        default:
            return state
    }
}