/* eslint-disable indent */
/* eslint-disable no-case-declarations */
/* eslint-disable no-fallthrough */
import {
  // New Consts
  FETCH_RECEIVED_FEEDBACKS,
  FETCH_SENT_FEEDBACKS,
  FETCH_FEEDBACK_REQUESTS,
  CLEAN_UP_BETTERME_DATA,
  FETCH_RATED_COMPITENCIES_DATA,
  COMPLETE_REQUEST,

  // Old Consts
  GET_RECEIVED_FEEDBACK_LIST,
  UPDATE_RECEIVED_FEEDBACK_LIST,
  SET_COMPITENCY_RATING,
} from '../types/betterMe';

const initialState = {
  // New States
  receivedList: [],
  sentList: [],
  requestsList: [],
  ratedFeedbackBarChart: {},
  weeklyFeedbackData: [],
  topEmployeesData: [],
  loading: true,
  currentPage: 0,
  limit: 15,
  totalData: 0,

  // Old States
  userFeedbacks: {
    feedbacks: [],
    userId: null
  },
  selectedfeedbacks: [],
  mainReceivedList: [],
  receivedFeedbackList: [],
  selectedEmployee: {},
  requestedEmployee: {},
  requestedFeedbacks: [],
  requestedFeedbackComment: '',
  feedbackCommented: false,
  employeeFeedbackResponse: false,
  pieUpdated: false,
  currentEmployeeList: [],
  currentFeedback: '',
  competencyRating: [],
  defaultRewards: ['star', 'cap', 'medal'],
  icon: ['star', 'cap', 'medal'],
  FeedbackIcons: [],
  feedbackRequests: [],
  totalFeedbacks: [
    {
      id: 1,
      name: 'Agility',
      icon: []
    },
    {
      id: 2,
      name: 'Orientation',
      icon: []
    },
    {
      id: 3,
      name: 'Relationships',
      icon: []
    },
    {
      id: 4,
      name: 'Creative',
      icon: []
    },
    {
      id: 5,
      name: 'Ownership',
      icon: []
    },
    {
      id: 6,
      name: 'Pro-Active',
      icon: []
    },
    {
      id: 7,
      name: 'Service',
      icon: []
    },
    {
      id: 8,
      name: 'Opener',
      icon: []
    },
    {
      id: 9,
      name: 'Emotional Intelligence',
      icon: []
    }
  ],
  employees: [
    {
      id: 1,
      name: 'Abhishek Leuva',
      position: 'Full Stack Developer'
    },
    {
      id: 2,
      name: 'Chetan Godhani',
      position: 'Full Stack Developer'
    },
    {
      id: 3,
      name: 'Fouad Omri',
      position: 'Project Manager'
    },
    {
      id: 4,
      name: 'Anas',
      position: 'Data Analyst'
    },
    {
      id: 5,
      name: 'Safa Omri',
      position: 'Cheif Executive Officer'
    },
    {
      id: 6,
      name: 'Nirav Joshi',
      position: 'Business Owner'
    }
  ]
};

export default function betterme(state = initialState, action = {}) {
  switch (action.type) {
    case FETCH_RECEIVED_FEEDBACKS:
      return {
        ...state,
        receivedList: [...state.receivedList, ...action.data.receivedList],
        currentPage: action.data.currentPage,
        totalData: action.data.totalData,
        loading: false,
      }
    case FETCH_SENT_FEEDBACKS:
      return {
        ...state,
        sentList: [...state.sentList, ...action.data.sentList],
        currentPage: action.data.currentPage,
        totalData: action.data.totalData,
        loading: false,
      }
    case FETCH_FEEDBACK_REQUESTS:
      return {
        ...state,
        requestsList: [...state.requestsList, ...action.data.requestsList],
        currentPage: action.data.currentPage,
        totalData: action.data.totalData,
        loading: false,
      }
    case FETCH_RATED_COMPITENCIES_DATA:
      return {
        ...state,
        ...action.data,
        loading: false,
      }
    case CLEAN_UP_BETTERME_DATA:
      return {
        ...state,
        receivedList: [],
        sentList: [],
        requestsList: [],
        loading: true,
        currentPage: 0,
        limit: 15,
        totalData: 0,
      }

    case COMPLETE_REQUEST:
      console.log(action)
      return {
        ...state,
        requestsList: state.requestsList.filter((request)=> request.id===action.data.id),
        currentPage: action.data.currentPage,
        totalData: action.data.totalData,
        loading: false,
      }

    // Old Cases
    case GET_RECEIVED_FEEDBACK_LIST:
      return { ...state, receivedFeedbackList: action.receivedFeedbackList, mainReceivedList: action.receivedFeedbackList };
    // update received feedback list
    case UPDATE_RECEIVED_FEEDBACK_LIST:
      return { ...state, receivedFeedbackList: action.updatedFeedbackList, pieUpdated: true };
    // set competency rating
    case SET_COMPITENCY_RATING:
      return { ...state, competencyRating: action.newArray };
    default: return state;
  }
};
