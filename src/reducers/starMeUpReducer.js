/* eslint-disable no-case-declarations */
import {
  LOAD_RECENT_STARS,
  LOAD_SENT_STARS,
  LOAD_RECEIVED_STARS,
  ADD_LIKE,
  SAVE_COMMENT_LIKE_UNLIKE,
  SET_LOADING_FLAG_STARMEUP,
  CHANGE_PAGE_NUMBER_STARMEUP,
  ADD_STARMEUP_CARD,
  LOAD_COMMENTS ,
  SET_COMMENTS_LOADING_STARMEUP,
  ADD_COMMENT,
  EDIT_COMMENT,
  DELETE_COMMENT
} from '../types/starMeUp';

const initialState = {
  recentStars: [],
  receivedStars: [],
  sentStars: [],
  loading: true,
  currentPage: 0,
  limit: 20,
  totalStars: 2,
};

export default function common(state = initialState, action = {}) {
  const { recentStars } = state;
  let updatedRecentStars  = [];
  switch (action.type) {
    case LOAD_RECENT_STARS:
      return {
        ...state,
        ...action.data,
        recentStars: action.data.recentStars,
        currentPage: action.data.currentPage,
        totalStars: action.data.totalStars,
        loading: false,
      }
    case LOAD_SENT_STARS:
      return {
        ...state,
        ...action.data,
        sentStars: [...state.sentStars, ...action.data.sentStars],
        currentPage: action.data.currentPage,
        loading: false,
      }
    case LOAD_RECEIVED_STARS:
      return {
        ...state,
        ...action.data,
        receivedStars: [...state.receivedStars, ...action.data.receivedStars],
        currentPage: action.data.currentPage,
        loading: false,
      }
    case ADD_LIKE:
      const user ={
        id: 'chetan1',
        name: {
          fname: "Chetan",
          lname: "Godhani"
        },
          profileImg: "Image"
      }
      const newData = state[action.data.type].map((starData) => {
        if(starData.id === action.data.id) {
          return {
            ...starData,
            isLiked: !starData.isLiked,
            stars_likes: !starData.isLiked ? [...starData.stars_likes, user] : starData.stars_likes.filter(item => item.id !== 'chetan1'),
            totalLikes: !starData.isLiked? starData.totalLikes + 1 : starData.totalLikes - 1
          }
        } else {
          return starData
        }
      })
      return {
        ...state,
        [action.data.type]: newData
      }
    case SAVE_COMMENT_LIKE_UNLIKE:
      updatedRecentStars = recentStars.map((starPost) => {
        if(starPost.id === action.starPostId) {
          const comments = starPost.comments.map(comment => {
            if(comment.commentId === action.commentId) {
              if(!action.isReply) {
                return {
                  ...comment,
                  isLiked: !comment.isLiked
                }
              } else {
                return {
                  ...comment,
                  replies: comment.replies.map(reply => {
                    if(reply.commentId === action.replyCommentId) {
                      return {
                        ...reply,
                        isLiked: !reply.isLiked
                      }
                    } else {
                      return reply;
                    }
                  })
                }
              }
            } else {
              return comment;
            }
          });
          return {
            ...starPost,
            comments
          }
        } else {
          return starPost;
        }
      });
      return { ...state, recentStars: updatedRecentStars };
    case SET_LOADING_FLAG_STARMEUP:
      return {
        ...state,
        loading: true,
      }
    case SET_COMMENTS_LOADING_STARMEUP:
      updatedRecentStars = recentStars.map((starPost)=> {
        return {
          ...starPost,
          comment:[]
        }
      })
      return {
        ...state,
        recentStars: updatedRecentStars
      }
    case CHANGE_PAGE_NUMBER_STARMEUP:
      return {
        ...state,
        currentPage: action.data
      }
    case ADD_STARMEUP_CARD:
      const newStar = action.data[0]
      console.log(newStar)
      return {
        ...state,
        recentStars: [newStar, ...state.recentStars],
        sentStars: [newStar, ...state.sentStars]
      }
    case LOAD_COMMENTS:
      if(action.data.errors) {
        return state;
      }
      updatedRecentStars = recentStars.map((starData) =>{
        if(starData.id === action.postId){
          if(starData.id === action.parentId){
            const newComment = action.data.data.map((comment) => {
              return {
                ...comment.comments
              }
            })
            return{
              ...starData,
              comment: [...starData.comment, ...newComment]
            };
          }
          else{
            const updatedComments= starData.comment.map((comment)=>{
              if(comment.id ===action.parentId){
                const newReplies = action.data.data.map((comment) => {
                  return {
                    ...comment.comments
                  }
                })
                return{
                  ...comment,
                  replies: newReplies
                };
              }
              else{
                return comment
              }
            })
            return{
              ...starData,
              comment: updatedComments
            }
          }
        }
        else{
          return starData;
        }
      });
      return { ...state, recentStars: updatedRecentStars };
    case ADD_COMMENT:
      if(action.data.errors) {
        return state;
      }
      updatedRecentStars = recentStars.map((starData) =>{
        if(starData.id === action.postId){
          if(starData.id == action.parentId) {
            const newComment = action.data.data.map((comment) => {
              return {
                ...comment.comments,
              }
            })
            return{
              ...starData,
              comment: [...starData.comment, ...newComment]
            };
          }
          else {
            const newComments = starData.comment.map((comment) => {
              if(comment.id == action.parentId) {
                const newComment = action.data.data.map((newComment) => {
                  return {
                    ...newComment.comments,
                  }
                })
                return {
                  ...comment,
                  replies: [...comment.replies, newComment]
                }
              }
              else {
                return comment
              }
            })
            return{
              ...starData,
              comment: [...starData.comment, ...newComments]
            };
          }
        }else{
          return starData;
        }
      });
      return { ...state, recentStars: updatedRecentStars };
    case EDIT_COMMENT:
      updatedRecentStars = recentStars.map((starData) => {
        if(starData.id === action.postId){
          if(!action.isReply){
            const comment = starData.comment.map((comment) =>{
              if(comment.id === action.commentId){
                return{
                  ...comment,
                  comment_text: action.comment
                }
              }else{
                return comment;
              }
            })
            return{
              ...starData,
              comment: comment
            }
          }else{
            const comment = starData.comment.map((comment) =>{
              if(comment.id === action.parentId){
                const replies = comment.replies.map((reply) =>{
                  if(reply.id === action.commentId){
                    return{
                      ...reply,
                      comment_text: action.comment
                    }
                  }else{
                    return reply;
                  }
                })
                return{
                  ...comment,
                  replies: replies
                }
              }else{
                return comment;
              }
            })
            return{
              ...starData,
              comment: comment
            }
          }
        }else{
          return starData;
        }
      })
      return { ...state, recentStars: updatedRecentStars };

    case DELETE_COMMENT:
      updatedRecentStars = recentStars.map((starData) =>{
        if(starData.id === action.postId){
          if(!action.isReply){
            const comment = starData.comment.filter((value) => value.id !== action.commentId)
            return{
              ...starData,
              comment: comment
            }
          }else{
            const comment = starData.comment.map((comment) => {
              if(comment.id === action.parentId){
                const replies = comment.replies.filter((value) => value.id !== action.commentId)
                return{
                  ...comment,
                  replies: replies
                }
              }else{
                return comment;
              }
            })
            return {
              ...starData,
              comment: comment
            }
          }
        }else{
          return starData;
        }
      })
      return { ...state, recentStars:updatedRecentStars };
    default: return state;
  }
}