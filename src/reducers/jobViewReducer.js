import { FETCH_JOB_DETAIL } from "../types/jobsView";

const initialState = {
  loading: true,
  jobs: [],
}

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_JOB_DETAIL:
      if(action.data !== undefined){
        return {
          ...state,
          loading: false,
          jobs: action.data[0]
        }
      }
    default:
      return state
  }
}
