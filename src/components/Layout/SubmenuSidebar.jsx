import React from 'react';
import styled from 'styled-components';

// import SearchBox from '../SubSidebar/SearchBox';
import SideNav, { NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import { Button } from "react-bootstrap";
import {
  NewsFeedIcon,
  EventIcon,
  GroupIcon,
  StarMeUpIcon,
  TakePartIcon,
  BetterMeIcon,
  PeopleDirectoryIcon,
  FollowCoworkersIcon,
  AddCoworkerIcon,
  JobsIcon,
} from './../../svgs';

import '@trendmicro/react-sidenav/dist/react-sidenav.css';

import { Translate, I18n } from "react-redux-i18n";
import { useLocation, useHistory } from 'react-router-dom';

export const Separator = styled.div`
  clear: both;
  position: relative;
  margin: .8rem 0;
  background-color: #ddd;
  height: 1px;
`;

const navWidthCollapsed = 64;
const navWidthExpanded = 240;

const Main = styled.main`
  position: relative;
  width: 100%;
  padding-left: ${props => props.expanded ? navWidthExpanded : navWidthCollapsed}px;
`;

const SubmenuSidebar = props => {
  const { children, onToggle, expanded, handleModalToggle } = props;
  const location = useLocation();
  const history = useHistory();

  const menuItems = [
    { path: '/', key: 'sidebar.news_feed', icon: NewsFeedIcon, notification: true },
    { path: '/events', key: 'sidebar.events', icon: EventIcon, notification: true },
    { path: '/groups', key: 'sidebar.groups', icon: GroupIcon, notification: false },
    { path: '/jobs', key: 'sidebar.jobs', icon: JobsIcon, notification: false },
    { path: '/star-me-up', key: 'sidebar.star_me_up', icon: StarMeUpIcon, notification: true },
    { path: '/take-part', key: 'sidebar.take_part', icon: TakePartIcon, notification: true },
    { path: '/better-me', key: 'sidebar.better_me', icon: BetterMeIcon, notification: true },
    { path: '/people-directory', key: 'sidebar.people_directory', icon: PeopleDirectoryIcon, notification: false },
    { path: '/follow-coworkers', key: 'sidebar.follow_coworkers', label: 'Follow Coworkers', icon: FollowCoworkersIcon, notification: false },
    // { path: '/', key: 'saved', label: 'Saved', imgUrl: 'subMenuListItem.png', bgPosition: '-45px -535px', isImage: false },
    // { path: '/', key: 'notes', label: 'Notes', imgUrl: 'subMenuListItem.png', bgPosition: '0 -712px', isImage: false },
    // { path: '/', key: 'org_chart', label: 'Org Chart', imgUrl: 'subMenuListItem.png', bgPosition: '-33px -712px', isImage: false },
    // { path: '/', key: 'interations', label: 'Interations', imgUrl: 'helperIcons.png', bgPosition: '0 -292px', isImage: false },
  ];

  return (
    <div className="d-flex">
      <SideNav
        onSelect={(selected) => {
          if (selected !== 'invite_colleague' && selected !== 'search' && location.pathname !== selected) {
            history.push(selected);
          } else if(selected === 'invite_colleague') {
            handleModalToggle();
          }
        }}
        onToggle={onToggle}
        className="sidebar-icon-collpase shadow"
        expanded={expanded}
      >
        <SideNav.Nav defaultSelected="/">
          {/* {expanded ?
            <div className="p-0 ml-2 mr-2" style={{ height: 50 }}>
              <SearchBox />
            </div>
            :
            <NavItem eventKey="search">
              <NavIcon>
                <FontAwesomeIcon icon={faSearch} color="#000" />
              </NavIcon>
            </NavItem>
          }
          <Separator /> */}
          {menuItems.map(({ path, key, icon: Icon, notification }) => {
            return (
              <NavItem eventKey={path} key={key}>
                <NavIcon>
                  <div className={notification ? "notification-badge position-relative" : ""} data-notification-count="10">
                    <Icon width="35" height="35" />
                  </div>
                </NavIcon>
                <NavText>
                  <Translate value={key} />
                </NavText>
              </NavItem>
            );
          })}
          <NavItem eventKey="invite_colleague" navitemClassName={`d-flex align-items-center invite-colleague-menu ${expanded ? 'invite-colleague-menu-text' : ''}`}>
            <NavIcon>
              <AddCoworkerIcon width="35" height="35" />
            </NavIcon>
            <NavText>
              <Button variant="secondary" className="shadow border rounded border-0 text-white mr-2" size="lg">
                {I18n.t('sidebar.follow_coworkers')}
              </Button>
            </NavText>
          </NavItem>
        </SideNav.Nav>
      </SideNav>
      <Main expanded={expanded}>
        {children}
      </Main>
    </div>
  );
}

export default SubmenuSidebar;
