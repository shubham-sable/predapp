import React from 'react';

import { Modal, Button, Col, FormLabel, Row } from 'react-bootstrap';
import { Formik, Form, Field, FieldArray, getIn } from 'formik';
import * as Yup from 'yup';

import { I18n } from 'react-redux-i18n';

const InviteColleagueSchema = Yup.object().shape({
  invitations: Yup.array().min(1, 'Add atleast 1 detail').required('Required')
});

const ErrorMessageArray = ({ name }) => (
  <Field name={name}>
    {({ form }) => {
      const error = getIn(form.errors, name);
      const touch = getIn(form.touched, name);
      return touch && error ?
        <div className="help-block text-danger pt-1">{error}</div>
        :
        null
    }}
  </Field>
);

const InviteColleagueModal = props => {
  const { show, onHide } = props;

  const handleFormSubmit = (values) => {
    console.log('values', values);
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
      className={`create-group-modal chetan-modal`}
    >
      <Modal.Header className="text-center" closeButton>
        <Modal.Title
          id="contained-modal-title-vcenter"
          className="w-100 font-size-20 modal-title font-weight-bold"
        >
          {I18n.t('header.invite_colleague')}
        </Modal.Title>
      </Modal.Header>
      <Formik
        enableReinitialize
        initialValues={{
          invitations: [{ fullName: '', email: '' }],
        }}
        validationSchema={InviteColleagueSchema}
        onSubmit={(values) => handleFormSubmit(values)}
      >
        {(renderProps) => {

          let checkIfAllFill = true;

          renderProps.values.invitations.forEach((invitation) => {
            if (checkIfAllFill && (invitation.fullName === '' || invitation.email === '')) {
              checkIfAllFill = false;
            }
          })

          return (
            <Form className="take-part-form" onSubmit={renderProps.handleSubmit}>
              <Modal.Body className="pt-4 pb-4 pl-3 pr-3 overflow-y-auto" style={{ maxHeight: '75vh' }}>
                <Row className="mb-4">
                  <Col sm={12}>
                    <FormLabel className="font-size-15 font-weight-bold">{I18n.t('header.name_emails')}</FormLabel>
                    <FieldArray
                      validateOnChange={true}
                      name="invitations"
                      render={arrayHelpers => {
                        if (checkIfAllFill) {
                          arrayHelpers.push({ fullName: '', email: '' })
                        }

                        return renderProps.values.invitations.map((answer, index) => (
                          <div key={index} className={`d-flex ${index > 0 ? 'pt-2' : ''}`}>
                            <div style={{ flex: 1 }}>
                              <Field name={`invitations[${index}].fullName`} className="form-control shadow-none" required placeholder={I18n.t('header.type_full_name')} />
                              <ErrorMessageArray name={`invitations[${index}].fullName`} />
                            </div>
                            <div style={{ flex: 1 }}>
                              <Field name={`invitations[${index}].email`} className="ml-2 form-control shadow-none" required placeholder={I18n.t('header.type_email_here')} />
                              <ErrorMessageArray name={`invitations[${index}].email`} />
                            </div>
                          </div>
                        ))
                      }}
                    />
                  </Col>
                </Row>
              </Modal.Body>
              <Modal.Footer className="p-1">
                <Button
                  className="font-weight-bold"
                  type="submit"
                  variant="primary"
                  size="lg"
                  disabled={renderProps.values.groupName === ''}
                  block
                >
                  {I18n.t('header.invite_colleague')}
                </Button>
              </Modal.Footer>
            </Form>
          );
        }}
      </Formik>
    </Modal>
  );
}

export default InviteColleagueModal;
