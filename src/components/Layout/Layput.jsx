import React, { Component, Fragment } from 'react';

import { connect } from 'react-redux';
import { setLocaleWithFallback } from './../../actions/i18n';

import { Dropdown, ListGroup, InputGroup } from "react-bootstrap";
import SideNav from '@trendmicro/react-sidenav';
import { Translate } from "react-redux-i18n";
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

import SubmenuSidebar from './SubmenuSidebar';
import Logout from './Logout';

import Notification from '../Notification';
import NotificationList from '../Notification/NotificationList';
import InviteColleagueModal from './InviteColleagueModal';
import Autocomplete from '../SelectAutocomplete';

import { checkEnterAndClick } from '../../utils/helpers';
import { supportedLocales } from './../../config/i18n';
import { NotificationIcon, ChatsIcon, DashboardIcon, LanguageIcon } from './../../svgs';

class Layout extends Component {
  constructor(props) {
    super(props);
    this.mql = window.matchMedia('(min-width: 1300px)');
    this.state = {
      showMenuText: this.mql.matches,
      logoutModal: false,
      sidebarDrawerOpen: false,
      expanded: false,
      inviteColleagueModel: false,
    };
  }

  componentDidMount() {
    this.mql.addListener(this.mediaQueryChanged);
  }

  componentWillUnmount() {
    this.mql.removeListener(this.mediaQueryChanged);
  }

  mediaQueryChanged = () => {
    this.setState({ showMenuText: this.mql.matches });
  };

  logoutModalCloseHandler = () => {
    this.setState({ logoutModal: false });
  };

  logoutModalOpenHandler = () => {
    this.setState({ logoutModal: true });
  };

  handleLogout = () => {
    const { keycloak } = this.props;
    keycloak && keycloak.logout();
  };

  handleSelectChats = () => {
    window.location.assign('https://messenger.prenigma.com');
  };

  handleModalToggle = () => {
    this.setState(prevState => ({
      inviteColleagueModel: !prevState.inviteColleagueModel
    }))
  }

  toggleDrawer = () => {
    this.setState(prevState => ({
      sidebarDrawerOpen: !prevState.sidebarDrawerOpen
    }))
  }

  onToggle = () => {
    this.setState(prevState => ({ expanded: !prevState.expanded }));
  };

  handleLanguageLinkClick = (e, code) => {
    e.preventDefault();
    this.props.setLocaleWithFallback(code);
  };

  getSidebarContent = () => {
    const { showMenuText, expanded } = this.state;
    const { titleSetting } = this.props;
    const sidebarIconItemClass = showMenuText ? 'sidebar-icon-item' : 'sidebar-icon-item sidebar-icon-padding';

    return (
      <div className="sidenav-wrapper shadow-sm position-sticky" style={{ minWidth: 520, top: 0 }}>
        <SideNav.Toggle tabIndex="0" expanded={expanded} onClick={this.onToggle} />
        <div className="menuToggle">
          <h1 className="m-0 ml-3 mr-3 font-size-16 header-main-title"><Translate value={titleSetting.title}/></h1>
          <InputGroup className="header-search-bar">
            <InputGroup.Prepend>
              <InputGroup.Text>
                <FontAwesomeIcon icon={faSearch} />
              </InputGroup.Text>
            </InputGroup.Prepend>
            <Autocomplete classes="mt-0 font-size-14" placeholder="Search" />
          </InputGroup>
        </div>
        <ul className="bottomMenu" tab-content="0">
          <li tabIndex="0" onClick={this.toggleDrawer}>
            <div className={`${sidebarIconItemClass}`}>
              <i className="material-icons user-select-none font-size-25">apps</i>
              {showMenuText && <Translate value="header.apps" />}
            </div>
          </li>
          <li>
            <Dropdown tabIndex="0" className={`sidebar-icon-item`}>
              <Dropdown.Toggle
                as={CustomToggle}
                id="dropdown-custom-components"
                tabIndex="0"
              >
                <NotificationIcon height={25} width={25} fill="#051c2c" />
                {showMenuText && <Translate value="header.notification" />}
              </Dropdown.Toggle>
              <Dropdown.Menu className="p-0 notification-dropdown">
                <NotificationList />
              </Dropdown.Menu>
            </Dropdown>
          </li>
          <li tabIndex="0" onClick={this.handleSelectChats} onKeyDown={checkEnterAndClick(this.handleSelectChats)}>
            <div className={`${sidebarIconItemClass}`}>
              <ChatsIcon height={25} width={25} fill="#051c2c" />
              {showMenuText && <Translate value="header.chat" />}
            </div>
          </li>
          <li tabIndex="0" onClick={this.logoutModalOpenHandler} onKeyDown={checkEnterAndClick(this.logoutModalOpenHandler)}>
            <div className="sidebar-icon-item helperIconsCommon">
              <DashboardIcon height={25} width={25} fill="#051c2c" />
              {showMenuText && <Translate value="header.dashboard" />}
            </div>
          </li>
          <li>
            <Link className="link-topbar sidebar-icon-item helperIconsCommon" to="/profile/1">
              <img src="/minion.jpeg" className="rounded-circle" alt="dashboard" height={40} />
            </Link>
          </li>
          <li>
            <Dropdown className={`sidebar-icon-item`} drop="left" tabIndex="0">
              <Dropdown.Toggle
                as={CustomToggle}
                id="dropdown-custom-components"
              >
                <LanguageIcon height={25} width={25} fill="#051c2c" />
                {showMenuText && <Translate value="header.language" />}
              </Dropdown.Toggle>
              <Dropdown.Menu className="p-0 language-dropdown">
                {Object.keys(supportedLocales).map(code => (
                  <Dropdown.Item key={code} eventKey={code} active={code === this.props.locale} onClick={e => this.handleLanguageLinkClick(e, code)}>{supportedLocales[code]}</Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
          </li>
        </ul>
      </div>
    );
  };

  render() {
    const { children } = this.props;
    const { logoutModal, sidebarDrawerOpen, expanded, inviteColleagueModel } = this.state;
    return (
      <Fragment>
        <Notification />
        {this.getSidebarContent()}
        <SubmenuSidebar onToggle={this.onToggle} expanded={expanded} handleModalToggle={this.handleModalToggle}>
          {children}
        </SubmenuSidebar>
        // logoutModal
        {logoutModal && (
          <Logout
            logoutModal={logoutModal}
            handleLogout={this.handleLogout}
            logoutModalCloseHandler={this.logoutModalCloseHandler}
          />
        )}
        //SideBarDrawer
        <div id="mySidenav" className="sidenav shadow-lg" style={{ width: sidebarDrawerOpen ? 300 : 0 }}>
          <div className="closebtn cursor-pointer" onClick={this.toggleDrawer}>&times;</div>
          <div>
            <div className="mx-2 border shadow-sm">
              <div className="p-3 border-bottom font-size-16 font-weight-bold">
                Visit More Prenigma Products
              </div>
              <div className="p-3 d-flex flex-wrap">
                <a className="product-link-box" href="http://localhost:3000">
                  <div className="text-center p-1 border shadow-sm cursor-pointer user-select-none">
                    <i className="material-icons user-select-none font-size-25">apps</i>
                    <div>Chain.care</div>
                  </div>
                </a>
              </div>
            </div>

            <div className="mx-2 mt-3 border shadow-sm">
              <div className="p-3 border-bottom font-size-16 font-weight-bold">
                Chain.care Spaces
              </div>
              <ListGroup className="font-size-14 border-0" style={{ borderRadius: 0 }}>
                <ListGroup.Item className="border-bottom d-flex justify-content-between align-items-center" style={{ border: 0 }}>
                  <Link to="/company/dr-omri-clinic" className="font-size-14 font-weight-normal text-underline p-0 product-link-box">Dr. Omri's Clinic</Link>
                  <Link to="/company/5466541/admin" className="font-size-14 font-weight-bold p-0 product-link-box">Edit</Link>
                </ListGroup.Item>
                <ListGroup.Item className="border-bottom d-flex justify-content-between align-items-center" style={{ border: 0 }}>
                  <Link to="/company/dr-chetan-clinic" className="font-size-14 font-weight-normal text-underline p-0 product-link-box">Dr. Chetan's Clinic</Link>
                  <Link to="/company/5466541/admin" className="font-size-14 font-weight-bold p-0 product-link-box">Edit</Link>
                </ListGroup.Item>
              </ListGroup>
              <div className="p-3 border-bottom text-center">
                <Link to="/company/new" className="font-size-14 font-weight-bold p-0 product-link-box">
                  + Create a Company Page
                </Link>
              </div>
            </div>
          </div>
        </div>
        <InviteColleagueModal show={inviteColleagueModel} onHide={this.handleModalToggle} />
      </Fragment>
    );
  }
}

const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
  <div
    ref={ref}
    onClick={(e) => {
      e.preventDefault();
      onClick(e);
    }}
    className="cursor-pointer user-select-none rounded-circle d-flex flex-column align-items-center justify-content-center">
    {children}
  </div>
));

const mapStateToProps = (state) => {
  return {
    ...state.common,
    locale: state.i18n.locale
  };
}
const mapDispatchToProps = { setLocaleWithFallback };

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
