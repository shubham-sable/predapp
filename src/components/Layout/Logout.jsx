import React, { Component } from "react";
import { Modal, Button } from "react-bootstrap";
import styled from "styled-components";

import { Translate } from "react-redux-i18n";

const { Header, Title, Body } = Modal;

const HeaderWrapper = styled(Header)`
  background-color: #f5f6f7;
  border-bottom: 1px solid #e5e5e5;
  border-radius: 3px 3px 0 0;
  color: #1d2129;
  font-weight: bold;
  line-height: 19px;
  padding: 10px 12px !important;
`;

class Logout extends Component {
  render() {
    const { logoutModal, handleLogout, logoutModalCloseHandler } = this.props;

    return (
      <Modal
        show={logoutModal}
        onHide={logoutModalCloseHandler}
        style={{ display: "flex", justifyContent: "center", top: "100px" }}
      >
        <HeaderWrapper closeButton>
          <Title as="h5">
            <Translate value="header.logout" tag="strong" />
          </Title>
        </HeaderWrapper>
        <Body><Translate value="header.are_you_sure_want_to_logout" /></Body>
        <Modal.Footer>
          <Button variant="default" size="sm" onClick={logoutModalCloseHandler}>
            <Translate value="header.cancel" />
          </Button>
          <Button variant="primary" size="sm" onClick={handleLogout}>
            <Translate value="header.confirm" />
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default Logout;
