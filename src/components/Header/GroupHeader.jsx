import React, { useEffect, useRef } from "react";

import { Nav, Container, Button } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faExternalLinkSquareAlt } from '@fortawesome/free-solid-svg-icons';

import { NavLink } from "react-router-dom";
import styled from "styled-components";
import StickyBox from "react-sticky-box";
import { Translate } from "react-redux-i18n";

import "./GroupHeader.css";

const CompanyProfileImage = styled.div`
  background: transparent ${props => props.bgimage ? `${`url(${props.bgimage})`}` : 'url("/assets/icons/camera.png")'} 50% no-repeat;
  background-size: ${props => props.bgimage && props.bgimage !== '' ? 'cover' : 'auto'};
  background-color: rgba(243,246,248);
  max-width: ${props => props.width ? props.width + 'px' : '0px'};
  max-height: ${props => props.width ? props.width + 'px' : '0px'};
  width: 100%;
  height: ${props => props.width ? props.width + 'px' : '0px'};
  margin-top: ${props => props.margintop ? props.margintop : 0};
  position: relative;
`;

const HeaderCover = styled.div`
  height: 180px;
  width: 100%;
  background-color: #fff;
  position: relative;
  background-repeat: no-repeat;
  background-position: center center;

  .edit-cover-icon {
    border: 0;
    box-shadow: none;
    height: 35px;
    width: 35px;
    position: absolute;
    right: 10px;
    top: 10px;
    border-radius: 50%;
    color: rgba(0,0,0,.6);
    outline: none;

    &:hover, &:focus, &:active {
      background-color: #e5e5e5;
      color: #000;
    }
  }
`;

export const StickyNav = styled(Nav)`
  background-color: #fff;
  top: 60px;
  z-index: 1;

  &.nav-tabs .nav-link:not(.active) {
    border: 0 !important;
  }
`;

const GroupHeader = (props) => {
  let headerRef = useRef(null);

  useEffect(() => {
    const stickyElm = document.querySelector('#sticky-group-header');

    console.log('stickyElm', stickyElm);

    const observer = new IntersectionObserver(
      ([e]) => {
        console.log('e', e)
        return e.target.classList.toggle('isSticky', e.intersectionRatio < 60)
      },
      { threshold: [1] }
    );

    stickyElm && observer.observe(stickyElm)
    return () => {
      stickyElm && observer.unobserve(stickyElm);
    }
  }, [headerRef]);

  console.log('headerRef', headerRef);

  const { groupData } = props;
  console.log('groupData', groupData);
  return (
    groupData && groupData.groupTitle ?
      <React.Fragment>
        <Container className="mt-3 mx-1200">
          <div ref={el => headerRef = el} className="border">
            <HeaderCover className="" style={{ backgroundImage: `url(${groupData.groupCoverImage})` }} />
            <div className="p-3">
              <div className="d-flex align-items-center justify-content-between">
                <div className="d-flex align-items-center flex-grow-1">
                  <CompanyProfileImage className="shadow border" bgimage="/logo.svg" margintop={"-90px"} width={120} />
                  <div className="ml-3">
                    <h3 className="m-0">{groupData.groupTitle}</h3>
                    <p className="mt-1 font-size-14">
                      <span className="text-muted">{groupData.groupType.name} - {groupData.totalMembersInGroup}</span>
                    </p>
                  </div>
                </div>
              </div>
              <div className="mt-2">
                <Button className="rounded-0 font-size-14" variant="primary">
                  <FontAwesomeIcon icon={faPlus} />&nbsp; Follow
                </Button>
                <Button className="rounded-0 font-size-14 ml-2" variant="outline-primary" style={{ border: '1px solid' }}>
                  Visit Website &nbsp;
                  <FontAwesomeIcon icon={faExternalLinkSquareAlt} />
                </Button>
              </div>
            </div>
          </div>
        </Container>
        <StickyBox id="sticky-group-header" className="bg-white container mx-1200 bg-white" offsetTop={60} offsetBottom={10} style={{ zIndex: 11 }}>
          <StickyNav className="position-sticky shadow-sm border-left border-bottom border-right" fill variant="tabs" defaultActiveKey="about">
            <Nav.Item>
              <NavLink to={`/group/${groupData.id}/about`} exact className="d-flex justify-content-center align-items-center betterme-link font-size-14" eventkey="about"><Translate value="groups.about" /></NavLink>
            </Nav.Item>
            <Nav.Item>
              <NavLink to={`/group/${groupData.id}`} exact className="d-flex justify-content-center align-items-center betterme-link font-size-14" eventkey="post"><Translate value="groups.post" /></NavLink>
            </Nav.Item>
            <Nav.Item>
              <NavLink to={`/group/${groupData.id}/members`} exact className="d-flex justify-content-center align-items-center betterme-link font-size-14" eventkey="members"><Translate value="groups.members" /></NavLink>
            </Nav.Item>
            <Nav.Item>
              <NavLink to={`/group/${groupData.id}/events`} className="d-flex justify-content-center align-items-center betterme-link font-size-14" eventkey="events"><Translate value="groups.events" /></NavLink>
            </Nav.Item>
            <Nav.Item>
              <NavLink to={`/group/${groupData.id}/integrations`} exact className="d-flex justify-content-center align-items-center betterme-link font-size-14" eventkey="integrations"><Translate value="groups.integrations" /></NavLink>
            </Nav.Item>
          </StickyNav>
        </StickyBox>
      </React.Fragment>
      :
      null
  );
}

export default GroupHeader;
