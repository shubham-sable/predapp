import React from 'react';

import styled from "styled-components";
import Slider from "react-slick";
import moment from "moment";


const SliderWrapper = styled(Slider)`
  .slick-arrow {
    &:before {
      color: #000 !important;
    }
  }

  .slick-prev {
    left: -35px !important;
    z-index: 1;
  }

  .slick-next {
    right: -35px !important;
    z-index: 1;
  }

  .slick-slide {
    transition: all 300ms ease;
    transform: scale(0.9);
  }
`;

const EventCalendar = ({ isMonth, which, selectedDate, onClick }) => {
  const type = isMonth ? 'month' : 'week';

  let start, end, startNumber, endNumber;

  let currentDate = moment();
  let currentFormattedDate = moment(selectedDate).format("YYYY-MM-DD");

  if(!isMonth) {
    if(which === 'current') {
      start = moment(currentDate).startOf('week');
      end = moment(currentDate).endOf('week');
    }
  } else {
    start = moment(currentDate).startOf('month');
    end = moment(currentDate).endOf('month');
  }

  startNumber = Number(moment(start).format('D'));
  endNumber = Number(moment(end).format('D'));

  let days = [...new Array(Math.abs(endNumber - startNumber + 1))].map((value, index) => {
    const date = startNumber + index;
    return {
      dateNumber: date,
      dayName: moment(start).add(index, 'days').format("ddd"),
      fullDate: moment(start).add(index, 'days').format("YYYY-MM-DD"),
    };
  });

  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    arrows: isMonth ? true : false,
    slidesToShow: 7,
    slidesToScroll: 7,
  };

  return (
    <SliderWrapper className="pt-3 pb-3" {...settings}>
      {days.map((day, index) => {
        return (
          <div key={`${type}-${day.fullDate}`}>
            <div
              className={`font-size-16 pt-2 pb-2 text-center cursor-pointer ${currentFormattedDate === day.fullDate ? `date-selected` : `date-not-selected`}`}
              onClick={() => onClick(day.fullDate)}
            >
              <div>{day.dayName}</div>
              <div>{day.dateNumber}</div>
            </div>
          </div>
        );
      })}
    </SliderWrapper>
  )
}

export default EventCalendar;
