import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import Layput from './';
import { testStore , findByTestAtrr } from '../../../Utils';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (initialState={}) => {
  const store = testStore(initialState);
  const wrapper = shallow(<Layput store={store}/>).dive().dive();
  return wrapper;
}

test('reders without error', ()=>{
  const wrapper = setup();
});

test('render HeaderBar without error', ()=>{
  const wrapper = setup();
  expect(wrapper.find('HeaderBar').length).toBe(1);
});

test('render SideBar without error', ()=>{
  const wrapper = setup();
  expect(wrapper.find('SideBar').length).toBe(1);
});

test('render AppDrawer without error', ()=>{
  const wrapper = setup();
  expect(wrapper.find('AppDrawer').length).toBe(1);
});

test('render Content without error', ()=>{
  const wrapper = setup();
  expect(wrapper.find('Content').length).toBe(1);
});

test('render Content with className site-layout-background ',()=>{
  const wrapper = setup();
  expect(wrapper.find('.site-layout-background').length).toBe(1);
});

test('render HeaderBar with prop toggleMenuView', ()=>{
  const wrapper = setup();
  expect(wrapper.find('HeaderBar').prop('toggleMenuView')).not.toBeUndefined();
});

test('render HeaderBar with prop setLocaleWithFallback', ()=>{
  const wrapper = setup();
  expect(wrapper.find('HeaderBar').prop('setLocaleWithFallback')).not.toBeUndefined();
});

test('render HeaderBar with prop toggleIsCollapsed', ()=>{
  const wrapper = setup();
  expect(wrapper.find('HeaderBar').prop('toggleIsCollapsed')).not.toBeUndefined();
});

test('render HeaderBar with prop toggleAppDrawer', ()=>{
  const wrapper = setup();
  expect(wrapper.find('HeaderBar').prop('toggleAppDrawer')).not.toBeUndefined();
});

test('render HeaderBar with prop title', ()=>{
  const wrapper = setup();
  expect(wrapper.find('HeaderBar').prop('title')).not.toBeUndefined();
});

test('render HeaderBar with prop locale', ()=>{
  const wrapper = setup();
  expect(wrapper.find('HeaderBar').prop('locale'))
});

test('render HeaderBar with prop state', ()=>{
  const wrapper = setup();
  expect(wrapper.find('HeaderBar').prop('state')).not.toBeUndefined();
});

test('render SideBar with prop toggleMenuView', ()=>{
  const wrapper = setup();
  expect(wrapper.find('SideBar').prop('toggleMenuView')).not.toBeUndefined();
});

test('render SideBar with prop state', ()=>{
  const wrapper = setup();
  expect(wrapper.find('SideBar').prop('state')).not.toBeUndefined();
});

test('render AppDrawer with prop toggleAppDrawer', ()=>{
  const wrapper = setup();
  expect(wrapper.find('AppDrawer').prop('toggleAppDrawer')).not.toBeUndefined();
});

test('render AppDrawer with prop visible', ()=>{
  const wrapper = setup();
  expect(wrapper.find('AppDrawer').prop('visible')).toBe(false);
});

test('render Switch for Route with error', ()=>{
  const wrapper = setup();
  expect(wrapper.find('Switch').length).toBe(1);
});

test('render Route with error', ()=>{
  const wrapper = setup();
  expect(wrapper.find('Route').length).not.toBe(0);
});
