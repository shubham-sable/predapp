import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import AppDrawer from './AppDrawer';

import { findByTestAtrr } from '../../../Utils';


Enzyme.configure({ adapter: new EnzymeAdapter() });

describe('Testing in AppDrawer', ()=>{
  const myFun = jest.fn();
  const wrapper = shallow(<AppDrawer visible={false} toggleAppDrawer={myFun} />);
  // console.log(wrapper.debug());
  test('render AppDrawer without error', ()=>{
    expect(wrapper);
  })
  test('render DrawerWrapper without error', ()=>{
    expect(wrapper.find('DrawerWrapper').length).toBe(1);
  })
  test('render DrawerWrapper with placement right', ()=>{
    expect(wrapper.find('DrawerWrapper').prop('placement')).toBe('right');
  })
  test('render DrawerWrapper with closable false', ()=>{
    expect(wrapper.find('DrawerWrapper').prop('closable')).toBe(false);
  })
  test('render DrawerWrapper with visible false', ()=>{
    expect(wrapper.find('DrawerWrapper').prop('visible')).toBe(false);
  })
  test('render DrawerWrapper with width', ()=>{
    expect(wrapper.find('DrawerWrapper').prop('width')).toBe(300);
  })
  describe('Close Button testing', ()=>{
    const component = findByTestAtrr(wrapper, 'Close');
    test('render Close Button without error', ()=>{
      expect(component.length).toBe(1);
    })
    test('onClick on Close button', ()=>{
      component.simulate('click');
      expect(myFun).toHaveBeenCalled();
    })
  })
  test('render Products card without error', ()=>{
    const component = findByTestAtrr(wrapper, 'Products');
    expect(component.length).toBe(1);
  })
  test('render Chain.care card without error', ()=>{
    const component = findByTestAtrr(wrapper, 'Chain.care');
    expect(component.length).toBe(1);
  })
});
