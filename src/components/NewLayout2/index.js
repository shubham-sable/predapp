import React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';

import HeaderBar from './HeaderBar';
import SideBar from './SideBar';
import AppDrawer from './AppDrawer';

import { setLocaleWithFallback } from './../../actions/i18n';

import { Layout } from 'antd';

// Common Pages
import NewsFeedPage from '../../pages/NewsFeed/NewsFeedPage';
import Group from '../../components/Group/index.jsx';
import GroupsList from '../../pages/Groups/GroupsList';
import EventsPage from '../../pages/Events';
import FollowCoworkers from '../../pages/FollowCoworkers/FollowCoworkers';
import PeopleDirectory from '../../pages/PeopleDirectory';

// Profile Related Pages
import Profile from '../../pages/Profile';
import ProfileConnections from '../../pages/Profile/Connections';

// Pages for Internal Company Setup on subdomain
import BetterMePage from '../../pages/BetterMe';
import TakePart from '../../pages/TakePart/TakePart';
import StarMeUp from '../../pages/StarMeUp/StarMeUp';

// Company View and Edit Page (Admin and Member View)
import NewCompany from '../../pages/company/NewCompany';
import EditCompanyPage from '../../pages/company/EditCompanyPage';
import ActivityPage from '../../pages/company/ActivityPage';
import CompanyViewPage from '../../pages/company/view';
import CompanyAboutPage from '../../pages/company/view/CompanyAboutPage';
import CompanyJobListing from '../../pages/company/view/CompanyJobListing';
import CompanyPeople from '../../pages/company/view/CompanyPeople';

// Job List Page Components
import JobsListingPage from '../../pages/Jobs';
import JobViewPage from '../../pages/Jobs/JobViewPage';

// 404 page
import Page404 from '../../pages/404';


class Layput extends React.Component{

  constructor(props){
    super(props);
    this.state={
      isCollapsed: false,
      visible: false,
      menuKey: 'NewsFeed'
    }
  }

  toggleMenuView = (value) =>{
    this.setState({ menuKey: value });
  }

  toggleIsCollapsed = () =>{
    this.setState({ isCollapsed: !this.state.isCollapsed });
  }

  toggleAppDrawer = (value) =>{
    console.log(this.state.visible);
    this.setState({ visible: value });
  }

  render(){
    return(
      <>
        <HeaderBar
          toggleMenuView={this.toggleMenuView}
          toggleIsCollapsed={this.toggleIsCollapsed}
          setLocaleWithFallback={setLocaleWithFallback}
          toggleAppDrawer={this.toggleAppDrawer}
          title={this.props.title}
          locale={this.props.locale}
          state={this.state}
          style={{ maxWidth: 520 }}       //Not working yet
        />
        <Layout
          style={{ marginTop: 64, paddingTop: 2}}>
          <SideBar toggleMenuView={this.toggleMenuView} state={this.state} />
          <Layout>
            <Layout.Content
              className="site-layout-background"
              style={{ marginLeft: 5, padding: 10 }}
            >
              <Switch>
                <Route exact path="/" component={NewsFeedPage} />

                {/* Coworker Listing Page */}
                <Route exact path="/follow-coworkers" component={FollowCoworkers} />

                {/* People Directory Page */}
                <Route exact path="/people-directory" component={PeopleDirectory} />

                {/* User Profile Page */}
                <Route exact path="/profile/:userId" component={Profile} />
                <Route exact path="/profile/:userId/connections" component={ProfileConnections} />

                {/* Event Feature Pages */}
                <Route exact path="/events">
                  <Redirect to="/events/today" />
                </Route>
                {/* <Route path="/events/calendar" component={EventsPage} /> */}
                <Route path="/events/:eventId" component={EventsPage} />

                {/* Group Feature Pages */}
                <Route path="/groups" component={GroupsList} />
                <Route exact path="/group/:id" component={props => <Group {...props} key={`group-home`} />} />
                <Route exact path="/group/:id/about" component={props => <Group {...props} key={`group-about`} />} />
                <Route exact path="/group/:id/members" component={props => <Group {...props} key={`group-members`} />} />
                <Route exact path="/group/:id/events" component={props => <Group {...props} key={`group-events`} />} />
                <Route exact path="/group/:id/events/:eventId" component={props => <Group {...props} key={`group-event-detail`} />} />
                {/* <Route path="/group/:id/photos" component={props => <Group {...props} key={`group-photos`} />} /> */}
                <Route path="/group/:id/integrations" component={props => <Group {...props} key={`group-integration`} />} />

                {/* TakePart Feature Pages */}
                <Route exact path="/take-part">
                  <Redirect to="/take-part/available-cards" />
                </Route>
                <Route path="/take-part/available-cards" component={TakePart} />
                <Route path="/take-part/my-cards" component={TakePart} />

                {/* StarMeUp Feature Pages */}
                <Route exact path="/star-me-up">
                  <Redirect to="/star-me-up/recent" />
                </Route>
                <Route path="/star-me-up/recent" component={props => <StarMeUp {...props} key="star-me-up-recent" />} />
                <Route path="/star-me-up/received" component={props => <StarMeUp {...props} key="star-me-up-received" />} />
                <Route path="/star-me-up/sent" component={props => <StarMeUp {...props} key="star-me-up-sent" />} />

                {/* BetterMe Feature Pages */}
                <Route exact path="/better-me">
                  <Redirect to="/better-me/received" />
                </Route>
                <Route path="/better-me/received" component={props => <BetterMePage {...props} key="better-me-received" />} />
                <Route path="/better-me/sent" component={props => <BetterMePage {...props} key="better-me-sent" />} />
                <Route path="/better-me/rated" component={props => <BetterMePage {...props} key="better-me-rated" />} />
                <Route path="/better-me/requests" component={props => <BetterMePage {...props} key="better-me-requests" />} />

                {/* Company Edit & View Pages */}
                <Route exact path="/company/new" component={NewCompany} />
                <Route exact path="/company/:companyId" component={CompanyViewPage} />
                <Route exact path="/company/:companyId/about" component={CompanyAboutPage} />
                <Route exact path="/company/:companyId/jobs" component={CompanyJobListing} />
                <Route exact path="/company/:companyId/people" component={CompanyPeople} />
                <Route exact path="/company/:companyId/:companyRole" component={EditCompanyPage} key="EditCompanyPage" />
                <Route exact path="/company/:companyId/:companyRole/notifications/all" component={ActivityPage} />

                {/* Jobs Listing */}
                <Route exact path="/jobs" component={JobsListingPage} />
                <Route exact path="/jobs/:id" component={JobViewPage} />

                {/* 404 Page */}
                <Route component={Page404} />
              </Switch>
            </Layout.Content>
            <AppDrawer visible={this.state.visible} toggleAppDrawer={this.toggleAppDrawer} />
          </Layout>
        </Layout>
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    ...state.common.titleSetting,
    locale: state.i18n.locale
  };
}

const mapDispatchToProps = { setLocaleWithFallback };

export default connect(mapStateToProps,mapDispatchToProps)(Layput);
