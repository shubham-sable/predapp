import React from 'react';

import { Translate } from 'react-redux-i18n';
import { Link } from 'react-router-dom';
import {
  Layout,
  Menu,
  Divider,
} from 'antd';

import {
  StarMeUpIcon,
  TakePartIcon,
  BetterMeIcon,
  PeopleDirectoryIcon,
  AddCoworkerIcon,
  JobsIcon,
} from './../../svgs';

const SideBar = (props) =>{

  const menuItems = [
    { path: '/jobs', key: 'Jobs', value: 'sidebar.jobs', icon: JobsIcon, notification: false },
    { path: '/star-me-up', key: 'StarMeUp', value: 'sidebar.star_me_up', icon: StarMeUpIcon, notification: true },
    { path: '/take-part', key: 'TakePart', value: 'sidebar.take_part', icon: TakePartIcon, notification: true },
    { path: '/better-me', key: 'BetterMe', value: 'sidebar.better_me', icon: BetterMeIcon, notification: true },
    { path: '/people-directory', key: 'PeopleDirectory', value: 'sidebar.people_directory', icon: PeopleDirectoryIcon, notification: false },
    // { path: '/', key: 'sidebar.news_feed', icon: NewsFeedIcon, notification: true },
    // { path: '/events', key: 'sidebar.events', icon: EventIcon, notification: true },
    // { path: '/groups', key: 'sidebar.groups', icon: GroupIcon, notification: false },
    // { path: '/follow-coworkers', key: 'sidebar.follow_coworkers', label: 'Follow Coworkers', icon: FollowCoworkersIcon, notification: false },
    // { path: '/', key: 'saved', label: 'Saved', imgUrl: 'subMenuListItem.png', bgPosition: '-45px -535px', isImage: false },
    // { path: '/', key: 'notes', label: 'Notes', imgUrl: 'subMenuListItem.png', bgPosition: '0 -712px', isImage: false },
    // { path: '/', key: 'org_chart', label: 'Org Chart', imgUrl: 'subMenuListItem.png', bgPosition: '-33px -712px', isImage: false },
    // { path: '/', key: 'interations', label: 'Interations', imgUrl: 'helperIcons.png', bgPosition: '0 -292px', isImage: false },
  ];

  return(
    <Layout.Sider width={250} trigger={null} collapsible collapsed={props.state.isCollapsed} style={{ minHeight: '100vh'}}>
      <Menu theme="dark" mode="inline" defaultSelectedKeys={props.state.menuKey} >
        {menuItems.map(({path, key, value, icon: Icon}) =>{
          return (
            <Menu.Item
              className="pl-4"
              style={{height:"50px"}}
              key={key}
              data-test={key}
              icon={<Icon width="35" height="35" />}
              onClick={() => props.toggleMenuView(key)}
            >
              <Link to={path}><Translate value={value} /></Link>
            </Menu.Item>
          );
        })}
          <Menu.Item className="pl-4" style={{height:"50px"}} key={'invite_colleague'} icon={<AddCoworkerIcon width="40" height="40" />}>
            <Translate value={'sidebar.follow_coworker'} />
          </Menu.Item>
      </Menu>
    </Layout.Sider>
  )
}

export default SideBar;
