import React from 'react';
import { Link } from 'react-router-dom';

import { supportedLocales } from './../../config/i18n';

import {
  Layout,
  Menu,
  Row,
  Col,
  Button,
  Typography,
  Dropdown,
 } from 'antd';

 import { Translate } from 'react-redux-i18n';
 import { AiOutlineAppstore, AiOutlineHome, AiOutlineDashboard} from 'react-icons/ai';
 import { MdEvent, MdGroup, MdNotifications, MdLanguage } from "react-icons/md";
 import { HiOutlineUserGroup, HiMenu } from "react-icons/hi";
 import { IoMdNotificationsOutline, IoIosChatboxes } from "react-icons/io";

 import {
   NewsFeedIcon,
   EventIcon,
   GroupIcon,
   FollowCoworkersIcon,
   NotificationIcon,
   ChatsIcon,
   DashboardIcon,
   LanguageIcon,
 } from './../../svgs';



 const HeaderBar = (props) =>{

   const dashboard = (
     <Menu>
      <Menu.Item>Profile</Menu.Item>
      <Menu.Item>Logout</Menu.Item>
     </Menu>
   );


   const language =(
    <Menu >
      {Object.keys(supportedLocales).map(code => (
           <Menu.Item key={code} eventKey={code} active={code === props.locale} onClick={e => handleLanguageLinkClick(code, e)}>{supportedLocales[code]}</Menu.Item>
       ))}
    </Menu>
  )

  const handleLanguageLinkClick = (code, e) => {
    props.setLocaleWithFallback(code);
  };

   return(
     <>
       <Layout.Header style={{background:'#fff', width:'100%', position:'fixed', zIndex: 1, paddingLeft: 5}} >
        <Row>
          <Col>
            <Button type='text' onClick={() => props.toggleIsCollapsed()} icon={<HiMenu style={{ fontSize: 25 }}/>} />
          </Col>
          <Col span={5}>
            <Typography.Title level={3} className='mt-3 p-1'><Translate value={props.title} /></Typography.Title>
          </Col>
          <Col>
            <Menu mode='horizontal' defaultSelectedKeys={props.state.menuKey}>
              <Menu.Item
                key='NewsFeed'
                data-test="NewsFeed"
                icon={<AiOutlineHome style={{fontSize: 30}} />}
                className='pl-3 pr-2'
                onClick={() => props.toggleMenuView("NewsFeed")}
              ><Link to="/" /></Menu.Item>
              <Menu.Item
                key='Events'
                data-test="Events"
                icon={<MdEvent style={{fontSize: 30}} />}
                className='pl-3 pr-2'
                onClick={() => props.toggleMenuView("Events")}
              ><Link to="/events" /></Menu.Item>
              <Menu.Item
                key='Groups'
                data-test="Groups"
                icon={<HiOutlineUserGroup style={{fontSize: 30}} />}
                className='pl-3 pr-2'
                onClick={() => props.toggleMenuView("Groups")}
              ><Link to="/groups" /></Menu.Item>
              <Menu.Item
                key='FollowCoworkers'
                data-test="FollowCoworkers"
                icon={<MdGroup style={{fontSize: 30}} />}
                className='pl-3 pr-2'
                onClick={() => props.toggleMenuView("FollowCoworkers")}
              ><Link to="/follow-coworkers" /></Menu.Item>
            </Menu>
          </Col>
          <Col push={4}>
            <Menu mode='horizontal'>
              <Menu.Item
                key='App'
                data-test='App'
                onClick={()=> props.toggleAppDrawer(true)}
                icon={<AiOutlineAppstore style={{ fontSize: 30 }} />}
              />
              <Menu.Item
                key='Notification'
                data-test='Notification'
                icon={<IoMdNotificationsOutline style={{ fontSize: 30 }} />}
              />
              <Menu.Item
                key="chats"
                data-test='chats'
                icon={<IoIosChatboxes style={{ fontSize: 30 }} />}
              />
              <Menu.Item
                key="dashboard"
                data-test='dashboard'
              >
                <Dropdown overlay={dashboard} trigger={['hover']}>
                  <AiOutlineDashboard style={{ fontSize: 30 }} />
                </Dropdown>
              </Menu.Item>
              <Menu.Item
                key="Profile"
                data-test='Profile'
                icon={<img src="/minion.jpeg" alt="logout" style={{ height: 30}} />}
              />
              <Menu.Item
                key="language"
                data-test='language'
              >
                <Dropdown overlay={language} trigger={['hover']}>
                  <MdLanguage style={{ fontSize: 30 }} />
                </Dropdown>
              </Menu.Item>
            </Menu>
          </Col>
        </Row>
       </Layout.Header>
     </>
   )
 }

 // <Dropdown overlay={menu} trigger={['click']}>
 //    <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
 //      Click me <DownOutlined />
 //    </a>
 //  </Dropdown>


 export default HeaderBar;
