import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import HeaderBar from './HeaderBar';

import { findByTestAtrr } from '../../../Utils';


Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (title='', locale='', state={}) => {
  const wrapper = shallow(<HeaderBar title={title} locale={locale} state={state} />);
  return wrapper;
}

test('render without error', ()=>{
  const wrapper = setup();
  expect(wrapper);
  // console.log(wrapper.debug());
});

test('render Header without error', ()=>{
  const wrapper = setup();
  expect(wrapper.find('Header').length).toBe(1);
});

test('render collapsed Button without error', ()=>{
  const wrapper = setup();
  expect(wrapper.find('Button').length).toBe(1);
});

test('render collapsed Button props', ()=>{
  const wrapper = setup();
  expect(wrapper.find('Button').prop('type')).toBe('text');
});

test('render title without error', ()=>{
  const wrapper = setup();
  expect(wrapper.find('Title').length).toBe(1);
});

test('render Menu without error', ()=>{
  const wrapper = setup();
  expect(wrapper.find('Menu').length).toBe(2);
});

test('render MenuItem without error', ()=>{
  const wrapper = setup();
  expect(wrapper.find('MenuItem').length).toBe(10);
});

test('render Translate without error', ()=>{
  const wrapper = setup();
  expect(wrapper.find('Translate').length).toBe(1);
});

test('render Translate with value', ()=>{
  const wrapper = shallow(<HeaderBar title='Testing' locale='' state={{}} />);
  expect(wrapper.find('Translate').prop('value')).toBe('Testing');
});

test('on Click in collapsed button', ()=>{
  const myFun = jest.fn();
  const wrapper = shallow(<HeaderBar toggleIsCollapsed={myFun} title='' locale='' state={{}}/>)
  const button = wrapper.find('Button');
  button.simulate('click');
  expect(myFun).toHaveBeenCalled();
});

describe('Testing for NewsFeed MenuItem', ()=>{
  const myFun = jest.fn();
  const wrapper = shallow(<HeaderBar toggleMenuView={myFun} title='' locale='' state={{}}/>)
  const component = findByTestAtrr(wrapper, 'NewsFeed');
  test('render NewsFeed MenuItem without error',()=>{
    expect(component.length).toBe(1);
  })
  test('onClick on toggleMenuView in NewsFeed', ()=>{
    component.simulate('click');
    expect(myFun).toHaveBeenCalled();
  })
});

describe('Testing for Events MenuItem', ()=>{
  const myFun = jest.fn();
  const wrapper = shallow(<HeaderBar toggleMenuView={myFun} title='' locale='' state={{}}/>)
  const component = findByTestAtrr(wrapper, 'Events');
  test('render Events MenuItem without error',()=>{
    expect(component.length).toBe(1);
  })
  test('onClick on toggleMenuView in Events', ()=>{
    component.simulate('click');
    expect(myFun).toHaveBeenCalled();
  })
});

describe('Testing for Groups MenuItem', ()=>{
  const myFun = jest.fn();
  const wrapper = shallow(<HeaderBar toggleMenuView={myFun} title='' locale='' state={{}}/>)
  const component = findByTestAtrr(wrapper, 'Groups');
  test('render Groups MenuItem without error',()=>{
    expect(component.length).toBe(1);
  })
  test('onClick on toggleMenuView in Groups', ()=>{
    component.simulate('click');
    expect(myFun).toHaveBeenCalled();
  })
});

describe('Testing for FollowCoworkers MenuItem', ()=>{
  const myFun = jest.fn();
  const wrapper = shallow(<HeaderBar toggleMenuView={myFun} title='' locale='' state={{}}/>)
  const component = findByTestAtrr(wrapper, 'FollowCoworkers');
  test('render FollowCoworkers MenuItem without error',()=>{
    expect(component.length).toBe(1);
  })
  test('onClick on toggleMenuView in FollowCoworkers', ()=>{
    component.simulate('click');
    expect(myFun).toHaveBeenCalled();
  })
});

describe('Testing for App MenuItem', ()=>{
  const myFun = jest.fn();
  const wrapper = shallow(<HeaderBar toggleAppDrawer={myFun} title='' locale='' state={{}}/>)
  const component = findByTestAtrr(wrapper, 'App');
  test('render App MenuItem without error',()=>{
    expect(component.length).toBe(1);
  })
  test('onClick on toggleAppDrawer in App', ()=>{
    component.simulate('click');
    expect(myFun).toHaveBeenCalled();
  })
});

describe('Testing for Notification MenuItem', ()=>{
  const wrapper = setup();
  const component = findByTestAtrr(wrapper, 'Notification');
  test('render Notification MenuItem without error', ()=>{
    expect(component.length).toBe(1);
  })
});

describe('Testing for chats MenuItem', ()=>{
  const wrapper = setup();
  const component = findByTestAtrr(wrapper, 'chats');
  test('render chats MenuItem without error', ()=>{
    expect(component.length).toBe(1);
  })
});

describe('Testing for dashboard MenuItem', ()=>{
  const wrapper = setup();
  const component = findByTestAtrr(wrapper, 'dashboard');
  test('render dashboard MenuItem without error', ()=>{
    expect(component.length).toBe(1);
  })
});

describe('Testing for Profile MenuItem', ()=>{
  const wrapper = setup();
  const component = findByTestAtrr(wrapper, 'Profile');
  test('render Profile MenuItem without error', ()=>{
    expect(component.length).toBe(1);
  })
});

describe('Testing for language MenuItem', ()=>{
  const wrapper = setup();
  const component = findByTestAtrr(wrapper, 'language');
  test('render language MenuItem without error', ()=>{
    expect(component.length).toBe(1);
  })
});
