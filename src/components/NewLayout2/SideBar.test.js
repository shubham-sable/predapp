import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import SideBar from './SideBar';

import { findByTestAtrr } from '../../../Utils';


Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (state={}) => {
  const wrapper = shallow(<SideBar state={state} />);
  return wrapper;
}

test('render SideBar without error', ()=>{
  const wrapper = setup();
  // console.log(wrapper.debug());
  expect(wrapper);
});

describe('Menu testing', ()=>{
  const wrapper = setup();
  test('render Menu without error', ()=>{
    expect(wrapper.find('Menu').length).toBe(1);
  })
  test('render Menu in dark theme', ()=>{
    expect(wrapper.find('Menu').prop('theme')).toBe('dark');
  })
  test('render Menu mode in inline', ()=>{
    expect(wrapper.find('Menu').prop('mode')).toBe('inline');
  })
});

describe('Testing on Jobs MenuItem', ()=>{
  const myFun = jest.fn();
  const wrapper = shallow(<SideBar toggleMenuView={myFun} state={{}} />);
  const component = findByTestAtrr(wrapper, 'Jobs');
  test('render Jobs Item without error',()=>{
    expect(component.length).toBe(1);
  })
  test('onClick on toggleMenuView in Jobs Item', ()=>{
    component.simulate('click');
    expect(myFun).toHaveBeenCalled();
  })
});

describe('Testing on StarMeUp MenuItem', ()=>{
  const myFun = jest.fn();
  const wrapper = shallow(<SideBar toggleMenuView={myFun} state={{}} />);
  const component = findByTestAtrr(wrapper, 'StarMeUp');
  test('render StarMeUp Item without error',()=>{
    expect(component.length).toBe(1);
  })
  test('onClick on toggleMenuView in StarMeUp Item', ()=>{
    component.simulate('click');
    expect(myFun).toHaveBeenCalled();
  })
});

describe('Testing on BetterMe MenuItem', ()=>{
  const myFun = jest.fn();
  const wrapper = shallow(<SideBar toggleMenuView={myFun} state={{}} />);
  const component = findByTestAtrr(wrapper, 'BetterMe');
  test('render BetterMe Item without error',()=>{
    expect(component.length).toBe(1);
  })
  test('onClick on toggleMenuView in BetterMe Item', ()=>{
    component.simulate('click');
    expect(myFun).toHaveBeenCalled();
  })
});

describe('Testing on TakePart MenuItem', ()=>{
  const myFun = jest.fn();
  const wrapper = shallow(<SideBar toggleMenuView={myFun} state={{}} />);
  const component = findByTestAtrr(wrapper, 'TakePart');
  test('render TakePart Item without error',()=>{
    expect(component.length).toBe(1);
  })
  test('onClick on toggleMenuView in TakePart Item', ()=>{
    component.simulate('click');
    expect(myFun).toHaveBeenCalled();
  })
});

describe('Testing on PeopleDirectory MenuItem', ()=>{
  const myFun = jest.fn();
  const wrapper = shallow(<SideBar toggleMenuView={myFun} state={{}} />);
  const component = findByTestAtrr(wrapper, 'PeopleDirectory');
  test('render PeopleDirectory Item without error',()=>{
    expect(component.length).toBe(1);
  })
  test('onClick on toggleMenuView in PeopleDirectory Item', ()=>{
    component.simulate('click');
    expect(myFun).toHaveBeenCalled();
  })
});
