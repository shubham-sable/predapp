import React from 'react';

import {
  Drawer,
  Card,
  Button,
  Row,
  Col,
  Popover,
  Space,
  Divider,
 } from 'antd';
import { MdClose, MdApps } from "react-icons/md";

const AppDrawer = (props) =>{

  return(
    <>
      <Drawer
        placement="right"
        closable={false}
        visible={props.visible}
        width={300}
      >
        <Col push={21} ><Button type='text' data-test='Close' onClick={() => props.toggleAppDrawer(false)} icon={<MdClose style={{ fontSize: 25 }} />} /></Col>
        <Card size="small" data-test='Products' title="Visit More Prenigma Products" style={{marginTop:20}}>
          <Popover content='Chain.care'><Button icon={<MdApps style={{ fontSize: 25 }} />} /></Popover>
        </Card>
        <Card size="small" data-test='Chain.care' title="Chain.care Spaces" style={{marginTop:20}}>
          <Row align='middle'>
            <Col>
              <p>Dr. Omri's Clinic</p>
            </Col>
            <Col push={7}>
              <Button type="text"><b>Edit</b></Button>
            </Col>
          </Row>
          <Divider style={{margin: 0}}/>
          <Row align='middle'>
            <Col>
              <p>Dr. Chetan's Clinic</p>
            </Col>
            <Col push={6}>
              <Button type="text"><b>Edit</b></Button>
            </Col>
          </Row>
          <Divider style={{margin: 0}}/>
          <Row justify='center' style={{marginTop: 8}}>
            <Button style={{padding: 5}}><b>+ Create a Company Page</b></Button>
          </Row>
        </Card>
      </Drawer>
    </>
  )
}

export default AppDrawer;
