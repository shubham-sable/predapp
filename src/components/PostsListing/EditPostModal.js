import React from "react";
import { Modal } from "react-bootstrap";
import PostEditor from './../PostEditor';

const EditPostModal = ({show, handleClose, data}) =>{

  var postEditInit = {
    "object": "value",
    "document": {
      "object": "document",
      "nodes": [
        {
          "object": "block",
          "type": "paragraph",
          "nodes": []
        }
      ]
    }
  }

  return (
    <Modal size="lg" show={show} onHide={handleClose} style={{ minWidth: 300}}>
      <Modal.Header closeButton>
        <Modal.Title>Edit</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <PostEditor onSubmit={handleClose} initialValue={postEditInit} />
      </Modal.Body>
    </Modal>
  )
};

export default EditPostModal;
