import React from "react";

const CustomMediaView = ({ data }) => {
  return (
    <div className="d-flex justify-content-center">
      {data.type === 'video' ? (
        <video className="card__box--post-custom-media" controls controlsList="nodownload">
          <source src={data.path} type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      ) : (
        <img className="card__box--post-custom-media" src={data.path} alt="" />
      )}
    </div>
  );
};

export default CustomMediaView;
