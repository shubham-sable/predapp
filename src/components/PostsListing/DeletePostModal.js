import React from 'react';
import { Modal, Button } from "react-bootstrap";

const DeletePostModal = ({ show, handleClose, handleDeletePost, postId }) =>{
  return(
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Warning!!</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        You really wanted to delete this post?
      </Modal.Body>
      <Modal.Footer>
        <Button variant="danger" onClick={() => {
          handleDeletePost(postId);
          handleClose();
        }} >Delete</Button>
        <Button variant="light" onClick={() => handleClose()} >Cancel</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default DeletePostModal;
