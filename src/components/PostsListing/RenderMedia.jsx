import React from 'react'

const RenderMedia = ({ data, classes }) => {
  return (
    data.type === 'image' ?
      <img className={`card__box--post-media ${classes}`} src={data.path} alt="" />
      :
      <video className={`card__box--post-media ${classes}`} controls>
        <source src={data.path} type="video/mp4" controlsList="nodownload" />
        Your browser does not support the video tag.
      </video>
  )
}

export default RenderMedia;
