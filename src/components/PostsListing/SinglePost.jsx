import React, { useState } from "react";

import Carousel, { Modal, ModalGateway } from 'react-images';
import { Card, Image, Button } from "react-bootstrap";
import moment from "moment";
import CommentBox from "./../CommentBox";
import PostTag from "./../PostTag";
import CustomMediaView from './CustomMediaView';
import RenderMedia from './RenderMedia';
import { CommentIcon, ShareIcon, LikeIcon, LikeFilledIcon } from './../../svgs';

import { I18n } from "react-redux-i18n";
import EditPostModal from './EditPostModal';
import DeletePostModal from './DeletePostModal';

import "./SinglePost.scss";

const SinglePost = (
  { data,
    curUserId,
    postLikeUnlikeHandle,
    commentLikeUnlikeHandle,
    commentsLimit,
    handleLoadMoreComments,
    commentPostHandle,
    handleGetComments,
    handleLoadReplies,
    handleLoadReplyReplies,
    handleDeletePost,
    handleDeleteComment,
    handleEditComment,
  }) => {
  const [isMediaModalOpen, setIsMediaModalOpen] = useState(false);
  const [isEditOpen, setIsEditOpen] = useState(false);
  const [isDeleteOpen, setIsDeleteOpen] = useState(false);

  const loadMoreCommentsClick = () => {
    const currentPage = data.currentPage ? ++data.currentPage : 1;
    handleLoadMoreComments(data.id, currentPage);
  }

  const focusOnComment = (value) => {
    handleGetComments(value);
    document.getElementById(`comment-content-editable-${data.id}`).focus()
  }

  let LikeComponent = LikeIcon;
  if (data.isliked) {
    LikeComponent = LikeFilledIcon
  }

  const mediaLength = data.media.length;

  return (
    <Card className="card__box--post bg-transparent border-0">
      <Card.Body className="d-flex">
        <div className="card__box--post-profile">
          <Image className="rounded-circle" src={data.user.profileImg} height={60} />
          <div className="card__box--post-column"></div>
        </div>
        <div className="pl-3 card__box--post-content">
          <div>
            <h3 className="font-size-16 m-0 font-weight-bold" data-test="createrName">{data.user.name.fname + " " + data.user.name.lname}</h3>
            <h6 className="m-0 font-weight-bold text-black-50 pt-2" data-test="createTime">{moment(new Date(data.createdAt)).format("MM.DD.YYYY")} | <span style={{ color: '#dc3545b0' }}>{moment(new Date(data.createdAt)).format("HH:MM")}</span></h6>
          </div>
          <p className="m-0 mt-3 font-size-14" style={{ lineHeight: 1.7 }} data-test="Descript">
            {data.description}
          </p>
          <div className="mt-4">
            {mediaLength === 1 ?
              <RenderMedia data={data.media[0]} />
              :
              mediaLength === 2 ?
                <div className="d-flex justify-content-between">
                  <RenderMedia data={data.media[0]} />
                  <RenderMedia classes="ml-2" data={data.media[1]} />
                </div>
                :
                mediaLength === 3 ?
                  <div className="d-flex justify-content-between">
                    <RenderMedia data={data.media[0]} />
                    <div className="ml-2">
                      <RenderMedia data={data.media[1]} />
                      <RenderMedia classes="mt-2" data={data.media[2]} />
                    </div>
                  </div>
                  :
                  mediaLength >= 4 &&
                  <React.Fragment>
                    <div className="d-flex">
                      <RenderMedia data={data.media[0]} />
                      <RenderMedia classes="ml-2" data={data.media[1]} />
                    </div>
                    <div className="d-flex mt-2">
                      <RenderMedia data={data.media[2]} />
                      {mediaLength === 4 ?
                        <RenderMedia classes="ml-2" data={data.media[2]} />
                        :
                        <div className="ml-2 card__box--post-media-more-posts" onClick={() => setIsMediaModalOpen(true)}>+{mediaLength - 3} {I18n.t('post_content.more')}</div>
                      }
                    </div>
                  </React.Fragment>
            }
          </div>
          {data.media.length > 0 &&
            <ModalGateway>
              {isMediaModalOpen ? (
                <Modal onClose={() => setIsMediaModalOpen(false)}>
                  <Carousel components={{ View: CustomMediaView }} views={data.media} />
                </Modal>
              ) : null}
            </ModalGateway>
          }
        </div>
        {data.user.id == curUserId ?
          <span>
            <Button variant="outline-dark" data-test="EditPost" onClick={() => setIsEditOpen(true)}><i className="edit icon"></i></Button>
            <Button variant="outline-danger" data-test="DeletePost" onClick={() => setIsDeleteOpen(true)}><i className="trash icon"></i></Button>
          </span>
          :
          <div />
        }
      </Card.Body>
      <PostTag data={data.taggedCoWorkers}/>
      <div className="take-part-footer d-flex align-items-center justify-content-between border-top p-0 pl-3 pr-3 pt-2 pb-2">
        <div className="d-flex user-select-none">
          <span className="mr-2 d-flex align-items-center font-size-20 cursor-pointer" onClick={() => postLikeUnlikeHandle(data.id,data.isliked)}>
            <LikeComponent fill={data.isliked ? '#1F40E6' : '#1F40E6'} height={30} width={30} />&nbsp;
            <span className="font-size-14 text-dark">{I18n.t('post_content.liked_by')}{data.totallikes}{I18n.t('post_content.users')}</span>
          </span>
        </div>
        <div>
          <Button
            size="sm"
            variant="default"
            className="d-inline-flex align-items-center text-dark card__box--post-comment-button"
            onClick={() => focusOnComment(data.id)}
          >
            <CommentIcon width={25} height={25} className="mr-2" /> <span>{I18n.t('post_content.go_to_comment')}</span>
          </Button>
          <Button
            size="sm"
            variant="default"
            className="d-inline-flex align-items-center ml-2 text-dark card__box--post-comment-button"
          >
            <ShareIcon width={25} height={25} className="mr-2" /> <span>{I18n.t('post_content.share')}</span>
          </Button>
        </div>
      </div>
      <CommentBox
        comments={data.comment}
        user={data.user}
        postId={data.id}
        commentLikeUnlikeHandle={commentLikeUnlikeHandle}
        hasMoreComments={false}
        loadMoreCommentsClick={loadMoreCommentsClick}
        commentPostHandle={commentPostHandle}
        handleLoadReplies={handleLoadReplies}
        handleLoadReplyReplies={handleLoadReplyReplies}
        handleDeleteComment={handleDeleteComment}
        handleEditComment={handleEditComment}
      />
      <EditPostModal
        show={isEditOpen}
        handleClose={() => setIsEditOpen(false)}
        data={data}
      />
      <DeletePostModal
        show={isDeleteOpen}
        handleClose={() => setIsDeleteOpen(false)}
        handleDeletePost={handleDeletePost}
        postId={data.id}
      />
    </Card>
  )
}

// data.totalComments - data.comments.length

export default SinglePost;
