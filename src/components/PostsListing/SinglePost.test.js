import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import SinglePost from './SinglePost';
import { findByTestAtrr } from '../../../Utils';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const initialState = {
  id: "5defdd76-2aea-4f72-8189-fec26be059cb",
  user: {
    id: "1",
    profileImg: "url",
    name: {
      fname: "sam",
      lname: "willings"
    }
  },
  isliked: false,
  createdAt: "2021-03-28T15:17:12.972000",
  group: {},
  media: [],
  description: "But that was life: Nobody got a guided tour to their own theme park. You had to hop on the rides as they presented themselves, never knowing whether you would like the one you were in line for...or if the bastard was going to make you throw up your corn dog and your cotton candy all over the place.",
  taggedCoWorkers: [],
  totalComments: 17,
  likes: [
    {
      id: "2",
      profileImg: "xyz",
      name: {
        fname: "Virat",
        lname: "Kohli"
      }
    }
  ],
  totallikes: 1,
  comment: []
}

const setup = (curUserId,comLimit, myFun) =>{
  const wrapper = shallow(
    <SinglePost
    data={initialState}
    curUserId={curUserId}
    commentsLimit={comLimit}
    postLikeUnlikeHandle={myFun}
    commentLikeUnlikeHandle={myFun}
    handleLoadMoreComments={myFun}
    commentPostHandle={myFun}
    handleGetComments={myFun}
    handleLoadReplies={myFun}
    handleLoadReplyReplies={myFun}
    handleDeletePost={myFun}
    handleDeleteComment={myFun}
    handleEditComment={myFun}
    />
  );
  return wrapper;
}

test('render without error', ()=>{
  const myFun = jest.fn()
  const wrapper = setup(1,15,myFun);
  // console.log(wrapper.debug())
  expect(wrapper);
})

describe('Card Testing',()=>{
  const myFun = jest.fn();
  const wrapper = setup(1,15,myFun);
  test('render Card without error',()=>{
    expect(wrapper.find('Card').length).toBe(1);
  })
  test('render CardBody without error',()=>{
    expect(wrapper.find('CardBody').length).toBe(1);
  })
  describe('ProfileImg render testing', ()=>{
    test('render profileImg without error', ()=>{
      expect(wrapper.find('Image').length).toBe(1);
    })
    test('render profileImg with rounded-circle', ()=>{
      expect(wrapper.find('Image').prop('className')).toBe('rounded-circle');
    })
    test('render profileImg with src value', ()=>{
      expect(wrapper.find('Image').prop('src')).toBe('url');
    })
  })
  describe('Create Data Testing',()=>{
    describe('on Creater Name testing', ()=>{
      const component = findByTestAtrr(wrapper,'createrName');
      test('render Creater Name component without error', ()=>{
        expect(component.length).toBe(1);
      })
      test('render Creater Name with value', ()=>{
        expect(component.text()).toBe('sam willings');
      })
    })
    describe('on Create Time testing', ()=>{
      const component = findByTestAtrr(wrapper,'createTime');
      test('render Create Time component without error', ()=>{
        expect(component.length).toBe(1);
      })
      test('render Create Time with value', ()=>{
        expect(component.text()).toBe("03.28.2021 | 15:03");
      })
    })
    describe('on Description render',()=>{
      const component = findByTestAtrr(wrapper,'Descript');
      test('render Description component without error', ()=>{
        expect(component.length).toBe(1);
      })
      test('render Description with value', ()=>{
        expect(component.text()).toBe("But that was life: Nobody got a guided tour to their own theme park. You had to hop on the rides as they presented themselves, never knowing whether you would like the one you were in line for...or if the bastard was going to make you throw up your corn dog and your cotton candy all over the place.");
      })
    })
    test('render Media without error', ()=>{
      expect(wrapper.find('RenderMedia').length).toBe(0);
    })
  })
})

describe('Card testing for Login User as Creater', ()=>{
  const myFun = jest.fn();
  const wrapper = setup(1,15,myFun);
  describe('Edit Post Button testing', ()=>{
    const component = findByTestAtrr(wrapper,'EditPost');
    test('render Button without error', ()=>{
      expect(component.length).toBe(1);
    })
    test('onClick testing',()=>{
      component.simulate('click');
      expect(wrapper.find('EditPostModal').prop('show')).toBe(true);
    })
  })
  describe('Delete Post Button testing', ()=>{
    const component = findByTestAtrr(wrapper,'DeletePost');
    test('render Button without error', ()=>{
      expect(component.length).toBe(1);
    })
    test('onClick testing',()=>{
      component.simulate('click');
      expect(wrapper.find('DeletePostModal').prop('show')).toBe(true);
    })
  })
})

describe('Card testing for Login User as not Creater', ()=>{
  const myFun = jest.fn();
  const wrapper = setup(2,15,myFun);
  test('Not to render Edit post Button',()=>{
    const component = findByTestAtrr(wrapper,'EditPost');
    expect(component.length).toBe(0);
  })
  test('Not to render Delete post Button',()=>{
    const component = findByTestAtrr(wrapper,'DeletePost');
    expect(component.length).toBe(0);
  })
})

describe('PostTag testing', ()=>{
  const myFun = jest.fn();
  const wrapper = setup(1,15,myFun);
  test('render PostTag without error', ()=>{
    expect(wrapper.find('PostTag').length).toBe(1);
  })
  test('render PostTag with data', ()=>{
    expect(wrapper.find('PostTag').prop('data')).toEqual([]);
  })
})

describe('render Buttons without error', ()=>{
  const myFun = jest.fn();
  const wrapper = setup(1,15,myFun);
  test('render Comment Button without error', ()=>{
    expect(wrapper.find('CommentIcon').length).toBe(1);
  })
  test('render Share Button without error', ()=>{
    expect(wrapper.find('ShareIcon').length).toBe(1);
  })
})

describe('CommentBox Testing', ()=>{
  const myFun = jest.fn();
  const wrapper = setup(1,15,myFun);
  test('render CommentBox without error', ()=>{
    expect(wrapper.find('CommentBox').length).toBe(1);
  })
  test('render CommentBox with comments value', ()=>{
    expect(wrapper.find('CommentBox').prop('comments')).toEqual([]);
  })
  test('render CommentBox with user value', ()=>{
    expect(wrapper.find('CommentBox').prop('user')).toEqual({
      id: "1",
      profileImg: "url",
      name: {
        fname: "sam",
        lname: "willings"
      }
    });
  })
  test('render CommentBox with postId value', ()=>{
    expect(wrapper.find('CommentBox').prop('postId')).toEqual("5defdd76-2aea-4f72-8189-fec26be059cb");
  })
  test('render CommentBox with hasMoreComments value', ()=>{
    expect(wrapper.find('CommentBox').prop('hasMoreComments')).toBe(false);
  })
  test('render CommentBox with commentLikeUnlikeHandle', ()=>{
    expect(wrapper.find('CommentBox').prop('commentLikeUnlikeHandle')).toBe(myFun);
  })
  test('render CommentBox with loadMoreCommentsClick', ()=>{
    expect(wrapper.find('CommentBox').prop('loadMoreCommentsClick')).not.toBe(myFun);
  })
  test('render CommentBox with commentPostHandle', ()=>{
    expect(wrapper.find('CommentBox').prop('commentPostHandle')).toBe(myFun);
  })
  test('render CommentBox with handleLoadReplies', ()=>{
    expect(wrapper.find('CommentBox').prop('handleLoadReplies')).toBe(myFun);
  })
  test('render CommentBox with handleLoadReplyReplies', ()=>{
    expect(wrapper.find('CommentBox').prop('handleLoadReplyReplies')).toBe(myFun);
  })
  test('render CommentBox with handleDeleteComment', ()=>{
    expect(wrapper.find('CommentBox').prop('handleDeleteComment')).toBe(myFun);
  })
  test('render CommentBox with handleEditComment', ()=>{
    expect(wrapper.find('CommentBox').prop('handleEditComment')).toBe(myFun);
  })
})

describe('EditPostModal Testing', ()=>{
  const myFun = jest.fn();
  const wrapper = setup(1,15,myFun);
  test('render EditPostModal without error', ()=>{
    expect(wrapper.find('EditPostModal').length).toBe(1);
  })
  test('render EditPostModal with show value', ()=>{
    expect(wrapper.find('EditPostModal').prop('show')).toBe(false);
  })
  test('render EditPostModal with data value', ()=>{
    expect(wrapper.find('EditPostModal').prop('data')).toBe(initialState);
  })
})

describe('DeletePostModal Testing', ()=>{
  const myFun = jest.fn();
  const wrapper = setup(1,15,myFun);
  test('render DeletePostModal without error', ()=>{
    expect(wrapper.find('DeletePostModal').length).toBe(1);
  })
  test('render DeletePostModal with show value', ()=>{
    expect(wrapper.find('DeletePostModal').prop('show')).toBe(false);
  })
  test('render DeletePostModal with postId value', ()=>{
    expect(wrapper.find('DeletePostModal').prop('postId')).toEqual("5defdd76-2aea-4f72-8189-fec26be059cb");
  })
  test('render DeletePostModal with handleDeletePost', ()=>{
    expect(wrapper.find('DeletePostModal').prop('handleDeletePost')).toBe(myFun);
  })
})
