import React, { useState } from 'react'

import { AsyncTypeahead } from 'react-bootstrap-typeahead';

const SEARCH_URI = 'https://api.github.com/search/users';

const Autocomplete = ({ classes = '', placeholder = 'Search for a Admin user...', multiple = false }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [options, setOptions] = useState([]);

  const handleSearch = (query) => {
    setIsLoading(true);

    fetch(`${SEARCH_URI}?q=${query}+in:login&page=1&per_page=50`)
      .then((resp) => resp.json())
      .then(({ items }) => {
        const options = items.map((i) => ({
          avatar_url: i.avatar_url,
          id: i.id,
          login: i.login,
        }));

        setOptions(options);
        setIsLoading(false);
      });
  };

  return (
    <AsyncTypeahead
      id="async-example"
      isLoading={isLoading}
      labelKey="login"
      className={classes}
      minLength={3}
      onSearch={handleSearch}
      options={options}
      inputProps={{
        className: "rounded-0"
      }}
      multiple={multiple}
      placeholder={placeholder}
      renderMenuItemChildren={(option, props) => (
        <React.Fragment>
          <img
            alt={option.login}
            src={option.avatar_url}
            style={{
              height: '24px',
              marginRight: '10px',
              width: '24px',
            }}
          />
          <span>{option.login}</span>
        </React.Fragment>
      )}
    />
  );
}

export default Autocomplete;
