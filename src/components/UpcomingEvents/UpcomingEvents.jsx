import React, { useState, useEffect } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../actions/EventActions';

import { Card, Nav, OverlayTrigger, Tooltip, Image, Button, Spinner, Container, InputGroup, Dropdown } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';

import moment from 'moment';
import { NavLink, Link } from 'react-router-dom';
import InfiniteScroll from 'react-infinite-scroller';
import StickyBox from "react-sticky-box";
import { Translate } from "react-redux-i18n";

import CreateEventModal from '../CreateEventModal/CreateEventModal';
import { StickyNav } from '../Header/GroupHeader';
import EventCalendar from '../EventCalendar';
import Autocomplete from 'components/SelectAutocomplete';
import { FloatingButton } from '../Group/Group.style';

import './UpcomingEvents.css';

const cardBoxStyle = {
  maxWidth: 700,
  margin: '10px auto',
  float: 'none'
};

const UpcomingEvents = (props) => {
  const {
    match,
    isGroup,
    groupData,
    renderEvents,
    todayEvents,
    thisWeekEvents,
    thisMonthEvents,
    prevEvents,
    postEventAction,
    getEventListFromDate,
    getEventByDate,
    setLoadingTrueAction,
    joinEventbyId,
    selectedDate,
    currentPage,
    totalEvents,
    limit,
    loading,
    clearAllDataAfterUnMountAction,
    getTodayEventAction,
    getThisWeekEventAction,
    getThisMonthEventAction,
    getPrevEventAction,
  } = props;

  const [showCreateEventModal, setShowCreateEventModal] = useState(false);
  const [showTitle, setShowTitle] = useState('');

  useEffect(() => {
    const userId = 1;
    if (match.params.eventId === 'today'){
      setLoadingTrueAction(todayEvents);
      getTodayEventAction(0,limit,userId);
    }
    if (match.params.eventId === 'this-week'){
      setLoadingTrueAction(thisWeekEvents);
      getThisWeekEventAction(0,limit,userId);
    }
    if (match.params.eventId === 'this-month'){
      setLoadingTrueAction(thisMonthEvents);
      getThisMonthEventAction(0,limit,userId);
    }
    if (match.params.eventId === 'previous'){
      setLoadingTrueAction(prevEvents);
      getPrevEventAction(0,limit,userId);
    }
  }, [getEventListFromDate, match.params.eventId, currentPage, limit])


  useEffect(() => {
    return () => {
      clearAllDataAfterUnMountAction();
    };
  }, [clearAllDataAfterUnMountAction])

  const handleModalToggle = () => {
    setShowCreateEventModal(!showCreateEventModal);
    setShowTitle("Creat Event");
  };

  let urlPrefix = '/events';

  if (isGroup) {
    urlPrefix = `/group/${groupData.id}/events`;
  }

  const handleChangeDate = (date) => {
    if (selectedDate !== date) {
      setLoadingTrueAction(renderEvents);
      getEventByDate(date, 0, limit);
    }
  }

  const handleChangePageNumber = (pageNumber) => {
    // if (currentPage !== pageNumber) {
    //   getEventListFromDate(selectedDate, pageNumber, limit);
    // }
  }

  const handleClick = (id) =>{
    joinEventbyId(id);
  }

  const checkJoin = (participants,id) =>{
    var join = false;
    if(participants!==undefined){
      participants.map((value) =>{
        if(value.id==id){
          join = true;
        }
      })
    }
    return join;
  }

  const handleEdit = (id) =>{
    setShowCreateEventModal(!showCreateEventModal);
    setShowTitle("Edit Event");
  }

  let eventId = match.params.eventId;

  const selectedNav = eventId === 'today' ? 'today' : eventId === 'this-week' ? 'this_week' : eventId === 'this-month' ? 'this_month' : eventId === 'previous' ? 'previous' : '';

  const totalPages = Math.ceil(totalEvents / limit);

  return (
    <React.Fragment>
      <div className="border-bottom shadow-sm p-4">
        <Container className="mx-1200">
          <div className="mx-2 d-flex justify-content-between align-items-center">
            <InputGroup className="header-search-bar">
              <InputGroup.Prepend>
                <InputGroup.Text>
                  <FontAwesomeIcon icon={faSearch} />
                </InputGroup.Text>
              </InputGroup.Prepend>
              <Autocomplete classes="mt-0 font-size-14" placeholder="Search Events..." />
            </InputGroup>
            <Dropdown className="group-sort-by-dropdown" drop="down">
              <Dropdown.Toggle variant="default" id="group-sort-by-toggle">
                Sort By
              </Dropdown.Toggle>

              <Dropdown.Menu className="p-0 group-sort-by-dropdown-menu">
                <Dropdown.Item>Name</Dropdown.Item>
                <Dropdown.Item>Number of Members</Dropdown.Item>
                <Dropdown.Item>Most Recent</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
        </Container>
      </div>
      <StickyBox className={`bg-white ${!isGroup ? 'shadow-sm' : ''}`} offsetTop={isGroup ? 120 : 60} style={{ zIndex: 1 }}>
        <Container className="mx-1200">
          <StickyNav className={`position-sticky ${isGroup ? 'border-left border-right border-bottom shadow-sm' : 'border-bottom-0'}`} fill variant="tabs">
            <Nav.Item>
              <NavLink activeClassName="active" to={`${urlPrefix}/today`} className="d-flex justify-content-center align-items-center betterme-link font-size-14 nav-link">
                <Translate  value="events.today" />
              </NavLink>
            </Nav.Item>
            <Nav.Item>
              <NavLink activeClassName="active" to={`${urlPrefix}/this-week`} className="d-flex justify-content-center align-items-center betterme-link font-size-14 nav-link">
                <Translate value="events.this_week" />
              </NavLink>
            </Nav.Item>
            <Nav.Item>
              <NavLink activeClassName="active" to={`${urlPrefix}/this-month`} className="d-flex justify-content-center align-items-center betterme-link font-size-14 nav-link">
                <Translate value="events.this_month" />
              </NavLink>
            </Nav.Item>
            <Nav.Item>
              <NavLink activeClassName="active" to={`${urlPrefix}/previous`} className="d-flex justify-content-center align-items-center betterme-link font-size-14 nav-link">
                <Translate value="events.previous" />
              </NavLink>
            </Nav.Item>
          </StickyNav>
        </Container>
      </StickyBox>
      <div className="ml-2 mr-2">
        <Card style={cardBoxStyle} className="events-card shadow-sm border-0">
          <Card.Body className="m-0">
            {selectedNav === 'today' && (
              <div className="pt-3 pl-4 pb-2">
                <span style={{ fontSize: '20px', fontWeight: 'bold' }}>{moment().format('D dddd')}</span>
                <div style={{ color: '#adb8c2' }}>{moment().format('YYYY')}</div>
              </div>
            )}
            {selectedNav === 'this_week' && (
              <EventCalendar which="current" onClick={handleChangeDate} selectedDate={selectedDate} />
            )}
            {selectedNav === 'this_month' && (
              <EventCalendar which="current" isMonth onClick={handleChangeDate} selectedDate={selectedDate} />
            )}
          </Card.Body>
        </Card>
        {loading ?
          <div className="d-flex justify-content-center pt-4 mx-auto" style={{ width: 650 }}>
            <Spinner animation="border" role="status">
              <span className="sr-only">Loading...</span>
            </Spinner>
          </div>
          :
          <InfiniteScroll
            pageStart={currentPage}
            initialLoad={false}
            loadMore={handleChangePageNumber}
            hasMore={currentPage + 1 <= totalPages}
            loader={
              <div className="d-flex card__box--post justify-content-center pt-2 pb-2" key={0}>
                <Spinner animation="border" role="status">
                  <span className="sr-only">Loading...</span>
                </Spinner>
              </div>
            }
          >
            {renderEvents.length > 0 ?
              renderEvents.map((value) => {
                return (
                  <Card key={`event-${value.id}`} style={cardBoxStyle} className="events-card shadow-sm border-0 p-4">
                    <Card.Body className="d-flex justify-content-between">
                      <div className="d-flex flex-grow-1">
                        <Image
                          src={'/minion.jpeg'}
                          className="rounded"
                          width={130}
                          style={{ height: 130, flex: '0 0 130px', objectFit: 'cover' }}
                        />
                        <div className="pl-3 d-flex flex-column">
                          <div className="d-flex flex-row justify-content-between align-items-center">
                            <Link
                              to={`/events/${value.id}`}
                              className="font-size-20 font-weight-bold padding-top-bottom text-primary"
                            >
                              {value.eventName}
                            </Link>
                          </div>
                          <div className="d-flex flex-row justify-content-between align-items-center padding-top-bottom">
                            <Link
                              to={`/events/${value.id}`}
                              className="font-size-16 text-primary single-line-webkit"
                            >
                              {value.description}
                            </Link>
                          </div>
                          <div className="participate-maindiv">
                            <div className="participate-subdiv">
                              <Translate className="participate-subdiv padding-top-bottom" value="events.participates" />
                              <div className='d-flex flex-row'>
                                {value.participants!== undefined && value.participants.map(
                                  (participant, index) => {
                                    return (
                                      index <= 2 &&
                                      <div key={`participants-${value.id}-${index}`} className='d-flex flex-row'>
                                        <Image
                                          src={'/assets/participate-profile2.jpeg'}
                                          roundedCircle
                                          style={{
                                            height: '40px',
                                            width: '40px',
                                            marginRight: '10px'
                                          }}
                                        />
                                        {index === 2 && value.participants.length > 2 &&
                                          <div className="plus-participants">+5</div>
                                        }
                                      </div>
                                    );
                                  }
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="pl-3 d-flex flex-column" style={{ flex: '0 0 180px' }}>
                        <div className="d-flex flex-row justify-content-between align-items-center padding-top-bottom font-size-14 text-wrap">
                          {moment(new Date(value.startDate)).format('D MMMM, HH:mm a')}
                        </div>
                        <div className="padding-top-bottom">
                          <Button variant="outline-primary" disabled={checkJoin(value.participants,1)} onClick={() => handleClick(value.id)} style={{ borderRadius: 20 }}><Translate value="events.join" /></Button>
                          <Button variant="outline-secondary" className="ml-2" style={{ borderRadius: 20 }}><Translate value="events.cant_go" /></Button>
                        </div>
                        <span className="pt-2"><Translate value="events.location" /></span>
                        <h5 className="pt-1 m-0">
                          {value.location.address}
                        </h5>
                      </div>
                    </Card.Body>
                    <Button onClick={() => handleEdit(value.id)}>Edit</Button>
                  </Card>
                );
              })
              :
              <Card style={cardBoxStyle} className="events-card shadow-sm border-0">
                <Card.Body>
                  <div className="p-4 d-flex justify-content-center">
                    <div>
                      <Image src="/assets/imgs/noevents.jpeg" />
                    </div>
                  </div>
                </Card.Body>
              </Card>
            }
          </InfiniteScroll>
        }
      </div>
      <OverlayTrigger placement={'top'} overlay={<Tooltip className="font-size-14">Create Event</Tooltip>}>
        <FloatingButton className="btn-primary" bottom={40} onClick={handleModalToggle} fontsize={25}>
          <FontAwesomeIcon icon={faPlus} />
        </FloatingButton>
      </OverlayTrigger>
      <CreateEventModal
        showCreateEventModal={showCreateEventModal}
        onCreateEventModalClose={handleModalToggle}
        showCreateEventModalFooter
        showTitle={showTitle}
        postEventAction={postEventAction}
      />
    </React.Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    ...state.events
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators({ ...Actions }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UpcomingEvents);
