import React from 'react';

import { Button, Card, Image } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendarAlt, faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import moment from 'moment';

import PostEditor from '../PostEditor';
import { Translate } from "react-redux-i18n";
import './UpcomingListing.scss';
import SinglePost from './../PostsListing/SinglePost';

import InfiniteScroll from 'react-infinite-scroller';
import { Spinner } from 'react-bootstrap';

const UpcomingListing = (props) => {
  const {
    eventData,
    handlePostSubmit
 } = props;

 const handleChangePageNumber = () =>{
   console.log("working");
 }

  // const handlePostSubmit = (value) => {
  //   var tempDate = new Date();
  //   var date = tempDate.getFullYear() + '-' + (tempDate.getMonth()+1) + '-' + tempDate.getDate() +'T'+ tempDate.getHours()+':'+ tempDate.getMinutes()+':'+ tempDate.getSeconds();
  //   var mediaFiles=[];
  //   value.mediaFiles.map((media) => {
  //     mediaFiles=[...mediaFiles, {
  //       id: media.id,
  //       type: media.type,
  //       url: media.name,
  //     }]
  //   })
  //   var tag_coworker=[];
  //   value.coWorkers.map((coWorker) =>{
  //     tag_coworker=[...tag_coworker, {
  //       id: coWorker.id,
  //       firstname: coWorker.firstName,
  //       lastname: coWorker.lastName,
  //       profile: "string",
  //     }]
  //   })
  //   // postNewPostAction(value, mediaFiles, tag_coworker, date, id);
  // };

  return (
    <div>
      <Card className="card__box--post event-detail border-0 rounded-0">
        <Card.Img className="w-100 event-detail__image" src={'https://ts-production.imgix.net/images/e47ce064-3f85-4ace-afe8-af98d4c02cfa.jpg?auto=compress,format&w=800&h=450'} style={{ height: '200px', objectFit: 'cover', objectPosition: 'top'}} />
        <Card.Body className="border-left border-right border-bottom">
          <div className="event-detail__basic-info">
            <Card.Title className="font-size-20">{eventData.eventName}</Card.Title>

            <div className="event-detail__content">
              <div className="d-flex">
                <FontAwesomeIcon className="event-icons" icon={faCalendarAlt} />
                <span className="align-self-center">
                  {moment(new Date(eventData.startDate)).format('dddd, MMMM DD, HH:mma')}
                </span>
              </div>
              {/* <span className="cursor-pointer user-select-none font-size-14">
                More details
              </span> */}
            </div>

            <div className="event-detail__content">
              <div className="d-flex">
                <FontAwesomeIcon className="event-icons" icon={faMapMarkerAlt} />
                <span className="align-self-center">{eventData.location.address}</span>
              </div>
              <Translate className="cursor-pointer text-primary user-select-none font-size-14" value="events.show_map" />
            </div>

            <div className="event-detail__attendees justify-content-between">
              <div className="d-flex align-items-center">
                <Translate className="align-self-center" value="events.attendees" />
                <div className="event-detail__attending-details">
                  <Image
                    src="/assets/participate-profile2.jpeg"
                    roundedCircle
                    className="event-detail__attending-profile"
                  />
                </div>
                <div className="event-detail__attending-details">
                  <Image
                    src="/assets/participate-profile.jpeg"
                    roundedCircle
                    className="event-detail__attending-profile"
                  />
                </div>
                <div className="event-detail__attending-details">
                  <Image
                    src="/assets/participate-profile2.jpeg"
                    roundedCircle
                    className="event-detail__attending-profile"
                  />
                </div>
                <div className="event-detail__attending-details">
                  <Image
                    src="/assets/participate-profile.jpeg"
                    roundedCircle
                    className="event-detail__attending-profile"
                  />
                  <span className="event-last-image">+ 12</span>
                </div>
              </div>
              <div className="event-detail__join-pod">
                <Button className="event-detail__join-pod-link" variant="primary"><Translate value="events.join_event" /></Button>
              </div>
            </div>
          </div>
        </Card.Body>
      </Card>

      <Card className="card__box--post rounded-0">
        <Card.Body className="d-flex justify-content-between">
          <div className="font-size-14 font-weight-bold text-primary">
            <span className="cursor-pointer user-select-none text-primary">
              3 <Translate value="events.went" /> · 0 <Translate value="events.maybe" /> · 19 <Translate value="events.invited" />
            </span>
            <div className="font-size-12 text-secondary pt-1"><Translate value="events.invite_to_event" /></div>
          </div>
          <Button variant="primary" className="d-flex align-items-center p-1" style={{ borderRadius: 20 }}>
            <span className="material-icons text-white font-size-16 mr-1">share</span><Translate value="events.share" />
          </Button>
        </Card.Body>
      </Card>

      <Card className="card__box--post rounded-0">
        <Card.Header className="font-weight-bold bg-white font-size-16 border-0"><Translate value="events.details" /></Card.Header>
        <Card.Body className="font-weight-16 pt-0">{eventData.description}</Card.Body>
      </Card>
      <PostEditor onSubmit={handlePostSubmit} isEvent />
    </div>
  );
}

export default UpcomingListing;
