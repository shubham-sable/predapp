import React, { useRef, useState, useEffect, Fragment } from 'react';

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../actions/EventActions';

import { Badge, Image, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircle } from '@fortawesome/free-solid-svg-icons';
import './NewsFeedSidebar.scss';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import moment from 'moment';
import StickyBox from "react-sticky-box";

const NewsFeedSidebar = (props) => {
  const { eventsList, todayeventlist, page, todaytotalevents, selectedDate } = props;
  const [todaydate, settodaydate] = useState(new Date());
  const currentdate = moment(new Date()).format('LL');
  const eventCalendar = useRef();

  useEffect(() => {
    if (todaydate === new Date()) {
      props.getDateEventsAction(currentdate);
    }
  }, [todaydate, currentdate, props]);

  const onChange = (date) => {
    const thisday = moment(date).format('LL');
    console.log('thisday', thisday);
    if (thisday === selectedDate) {
      props.getDateEventsAction(thisday, page);
    } else {
      props.getDateEventsAction(thisday, 1);
    }
    settodaydate(date);
  };

  const onClickDay = (date) => {
    console.log('eventCalendar', eventCalendar);
  }

  const tileClassName = (date) => {
    const nowdate = new Date(todaydate);
    const newdate = date.date;
    const alldatesdate = newdate.getDate();
    const alldatesmonth = newdate.getMonth();
    const alldatesyear = newdate.getFullYear();

    let eventDates = [];
    eventsList.map(async (event) => {
      let eventdate = new Date(event.startDate).getDate();
      let eventmonth = new Date(event.startDate).getMonth();
      let eventyear = new Date(event.startDate).getFullYear();
      if (eventyear === newdate.getFullYear() && eventmonth === newdate.getMonth() && eventdate === newdate.getDate()) {
        eventDates.push(eventdate);
      }
    });
    if (eventDates.length > 0) {
      if (eventDates.includes(newdate.getDate()) && newdate.getDate() === nowdate.getDate()) {
        return 'events-selected-date';
      } else if (eventDates.includes(newdate.getDate())) {
        return 'events-date';
      } else {
        return '';
      }
    }
    if (alldatesyear === new Date().getFullYear() && alldatesmonth === new Date().getMonth() && alldatesdate === new Date().getDate()) {
      return 'today-active';
    }
  };

  return (
    <StickyBox offsetTop={70} offsetBottom={10} className="newsfeed-sidebar">
    {/* <div > */}
      <div className={`event-calendar bg-white`}>
        <Calendar
          onChange={onChange}
          value={todaydate}
          tileClassName={tileClassName}
          className={todayeventlist.length > 0 ? 'border-bottom' : ''}
          onClickDay={onClickDay}
          ref={eventCalendar}
        />
        <div style={{ maxHeight: '400px', overflowY: 'auto' }}>
          {todayeventlist.map((event, index) => {
            return (
              <Fragment key={`event-${event.id}`}>
                <div className={`d-flex pl-2 pr-2 pt-3 pb-3 justify-content-between ${todayeventlist.length - 1 !== index && 'border-bottom'} text-primary`}>
                  <span className="d-flex" style={{ width: '40%' }}>
                    <span className="pr-2">
                      <FontAwesomeIcon
                        icon={faCircle}
                        className="align-self-center font-size-12 text-primary"
                      />
                    </span>
                    <span className="pr-2">{event.eventName}</span>
                  </span>
                  <span className="pr-2 text-center" style={{ width: '20%' }}>{moment(event.startDate).format('LT')}</span>
                  <span className="text-right" style={{ width: '40%' }}>{event.locationName}</span>
                </div>
                {todaytotalevents > 3 && index === todayeventlist.length - 1 && todayeventlist.length !== todaytotalevents &&
                  <div className='d-flex pl-2 pr-2 pt-3 pb-3 justify-content-between border-bottom text-primary'>
                    <span className="d-flex justify-content-center w-100 cursor-pointer" onClick={() => onChange(selectedDate)}>Show More</span>
                  </div>
                }
              </Fragment>
            );
          })}
        </div>
      </div>
      <div className="w-100 bg-white mt-3">
        <div className="p-3 profile-widget-head d-flex flex-column justify-content-center align-items-center">
          <div className="profile-widget-photo text-center">
            C
          </div>
          <h3 className="text-white mt-3">Dr. Chetan Godhani <small className="text-color-grey">MS</small></h3>
          <p className="text-white text-uppercase font-size-12 text-center px-2 mb-0">Consultant at AA Rahim Memorial District Hospital</p>
          <div>
            <Badge className="text-uppercase font-size-14 my-2" variant="info">Anatomy</Badge>
          </div>
          <p className="text-color-grey font-size-14 mb-0">Mumbai, India</p>
        </div>
        <div className="bg-c-grey text-white d-flex w-100 position-relative" style={{ height: 42 }}>
          <div className="profile-completed-progress" style={{ width: '40%' }}></div>
          <div className="d-flex justify-content-between align-items-center p-2 position-absolute w-100 font-size-14" style={{ height: 42 }}>
            <div className="text-uppercase">40% Profile Completed</div>
            <div className="cursor-pointer user-select-none text-uppercase font-weight-bold font-size-12">Complete Now {">"}</div>
          </div>
        </div>
        <div className="w-100 profile-statistics bg-white border">
          <div className="d-flex">
            <div className="text-center profile-stat-cell">
              <h4 className="font-weight-bold font-size-16">0</h4>
              <div className="cursor-pointer user-select-none text-uppercase font-weight-bold font-size-12">Profile View {">"}</div>
            </div>
            <div className="text-center profile-stat-cell">
              <h4 className="font-weight-bold font-size-16">0</h4>
              <div className="cursor-pointer user-select-none text-uppercase font-weight-bold font-size-12">Connection {">"}</div>
            </div>
          </div>
          <div className="d-flex">
            <div className="text-center profile-stat-cell">
              <h4 className="font-weight-bold font-size-16">0</h4>
              <div className="cursor-pointer user-select-none text-uppercase font-weight-bold font-size-12">Post By You {">"}</div>
            </div>
            <div className="text-center profile-stat-cell">
              <h4 className="font-weight-bold font-size-16">0</h4>
              <div className="cursor-pointer user-select-none text-uppercase font-weight-bold font-size-12">Saved Post {">"}</div>
            </div>
          </div>
        </div>
      </div>
      <div className="w-100 mt-3">
        <h3 className="text-uppercase font-size-14">PEOPLE YOU MAY KNOW</h3>
        <div className="bg-white border">
          <div className="d-flex align-items-center w-100 p-2 border-bottom">
            <Image src="https://via.placeholder.com/150" rounded width={40} style={{ alignSelf: 'flex-start'}} />
            <div style={{ flex: 1, margin: '0 10px' }}>
              <p className="m-0 font-weight-bold font-size-14 mb-1">Dr. Binal Joshi</p>
              <p className="m-0 font-weight-bold"><span className="text-info">General Medicine,</span><span> Ahmedabad</span></p>
            </div>
            <Button variant="secondary">Connect</Button>
          </div>
          <div className="d-flex align-items-center w-100 p-2 border-bottom">
            <Image src="https://via.placeholder.com/150" rounded width={40} style={{ alignSelf: 'flex-start'}} />
            <div style={{ flex: 1, margin: '0 10px' }}>
              <p className="m-0 font-weight-bold font-size-14 mb-1">Dr. Binal Joshi</p>
              <p className="m-0 font-weight-bold"><span className="text-info">General Medicine,</span><span> Ahmedabad</span></p>
            </div>
            <Button variant="secondary">Connect</Button>
          </div>
          <div className="d-flex align-items-center w-100 p-2">
            <Image src="https://via.placeholder.com/150" rounded width={40} style={{ alignSelf: 'flex-start'}} />
            <div style={{ flex: 1, margin: '0 10px' }}>
              <p className="m-0 font-weight-bold font-size-14 mb-1">Dr. Binal Joshi</p>
              <p className="m-0 font-weight-bold"><span className="text-info">General Medicine,</span><span> Ahmedabad</span></p>
            </div>
            <Button variant="secondary">Connect</Button>
          </div>
        </div>
      </div>
      <div className="w-100 mt-4">
        <h3 className="text-uppercase font-size-14">RECOMMENDED JOBS FOR YOU</h3>
        <div className="bg-white border">
          <div className="d-flex align-items-center w-100 p-2 border-bottom">
            <Image src="https://via.placeholder.com/150" rounded width={40} style={{ alignSelf: 'flex-start'}} />
            <div style={{ flex: 1, margin: '0 10px' }}>
              <p className="m-0 font-weight-bold font-size-14">ICU Registrar at a Leading Hospital </p>
            </div>
            <Button variant="outline-secondary">View</Button>
          </div>
          <div className="d-flex align-items-center w-100 p-2 border-bottom">
            <Image src="https://via.placeholder.com/150" rounded width={40} style={{ alignSelf: 'flex-start'}} />
            <div style={{ flex: 1, margin: '0 10px' }}>
              <p className="m-0 font-weight-bold font-size-14">ICU Registrar at a Leading Hospital </p>
            </div>
            <Button variant="outline-secondary">View</Button>
          </div>
          <div className="d-flex align-items-center w-100 p-2">
            <Image src="https://via.placeholder.com/150" rounded width={40} style={{ alignSelf: 'flex-start'}} />
            <div style={{ flex: 1, margin: '0 10px' }}>
              <p className="m-0 font-weight-bold font-size-14">ICU Registrar at a Leading Hospital </p>
            </div>
            <Button variant="outline-secondary">View</Button>
          </div>
        </div>
      </div>
    {/* </div> */}
    </StickyBox>
  );
};

const mapStateToProps = (state) => {
  return {
    ...state.events
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators({ ...Actions }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsFeedSidebar);
