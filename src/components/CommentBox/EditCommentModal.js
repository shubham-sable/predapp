import React, { useState } from 'react';
import { Modal, Button } from "react-bootstrap";

const DeleteCommentModal = ({ show, handleClose, handleEditComment, postId, data, commentText, setCommentText}) =>{

  return(
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Edit Comment</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <input
          type="text"
          className="ml-2 pl-3 pr-3 pt-2 pb-2 d-flex font-size-14 flex-column"
          contentEditable={true}
          style={{
            minWidth: 420,
            minHeight: "35px",
            borderRadius: "10px",
            backgroundColor: "#F4F4F6",
            outline: "none",
            whiteSpace: "pre-wrap",
            wordBreak: "break-word",
            border: "1px solid #e5e5e5",
          }}
          value={commentText}
          onChange = {(e) => setCommentText(e.target.value)}
        />
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" onClick={() => {
          handleEditComment(postId, data.id, data.isReply, data.parentId, commentText);
          handleClose();
        }} >Save</Button>
        <Button variant="light" onClick={() => handleClose()} >Cancel</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default DeleteCommentModal;
