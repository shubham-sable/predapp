import React, { useRef, useState, useEffect } from "react";

import { Image, Button } from "react-bootstrap";
import styled from "styled-components";

import DeleteCommentModal from './DeleteCommentModal';
import EditCommentModal from './EditCommentModal';

import { I18n } from "react-redux-i18n";

import './comment-box.css';

const CommentBoxDiv = styled.div`
  min-height: 35px;

  .comment-box-editable {
    min-height: 35px;
    border-radius: 25px;
    background-color: #F4F4F6;
    outline: none;
    white-space: pre-wrap;
    word-break: break-word;
    border: 1px solid #e5e5e5;
  }

  .comment-box-editable:empty::before {
    content: attr(placeholder);
    display: block;
    color: rgb(151, 159, 180);
    padding-top: 2px;
  }

  img {
    top: 0;
  }
`;


const CommentBox = ({
  comments,
  user,
  commentLikeUnlikeHandle,
  postId,
  hasMoreComments,
  loadMoreCommentsClick,
  handleLoadReplies,
  handleLoadReplyReplies,
  commentPostHandle,
  handleDeleteComment,
  handleEditComment,
 }) => {
  const sendCommentRef = useRef(null);
  let scrollToCommentRef = useRef();
  const [replyingId, setReplyingId] = useState([]);
  const [newAddedId, setNewAddedId] = useState(undefined);
  const [isDeleteOpen, setIsDeleteOpen] = useState(false);
  const [isEditOpen, setIsEditOpen] = useState(false);
  const [reqData, setReqData] = useState(undefined);
  const [commentText, setCommentText] = useState("");

  useEffect(() => {
    if (newAddedId || newAddedId === 0) {
      if (scrollToCommentRef.current) {
        window.scrollTo({
          behavior: "smooth",
          top: scrollToCommentRef.current.offsetParent.offsetTop + scrollToCommentRef.current.offsetTop - 100
        });
        scrollToCommentRef.current.childNodes[1].focus()
      }
    }
  }, [newAddedId, scrollToCommentRef]);

  const readMessage = (event, isReply = false, commentId, replyId) => {
    if (event.keyCode === 13 && !event.shiftKey) {
      event.preventDefault();
      const comment = sendCommentRef.current.innerText;
      if(comment !== '') {
        commentPostHandle(postId, comment, isReply, commentId, replyId);
        sendCommentRef.current.innerText = '';
      }
    }
  }

  const addRemoveReplyingId = (id, replyId, replyingName = '') => {
    // {
    //   isReply: false,
    //   id: undefined
    //   replyId: undefined
    // }
    const isReply = id && replyId ? true : false;
    let findReplyIndex = replyingId.findIndex(x => x.id === id);
    if(findReplyIndex !== -1) {
      if(replyingId[findReplyIndex].replyId === replyId) {
        setReplyingId(replyingId.filter((x, index) => index !== findReplyIndex));
        setNewAddedId(undefined);
      } else {
        setReplyingId(replyingId.map((x, index) => {
          if(index === findReplyIndex) {
            setNewAddedId(id);
            return {
              ...x,
              replyId,
              isReply,
              replyingName: replyingName !== '' ? `@${replyingName} ` : ''
            }
          } else {
            return x;
          }
        }));
      }
    } else {
      setNewAddedId(id);
      setReplyingId([...replyingId, { isReply, id, replyId, replyingName: replyingName !== '' ? `@${replyingName} ` : '' }]);
    }
  }

  return (
    <React.Fragment>
      <div className="take-part-footer d-flex align-items-center justify-content-between border-top p-0 pl-3 pr-3 pt-2 pb-2 comment-write-box">
        <CommentBoxDiv className="position-relative w-100">
          <div id={`comment-content-editable-${postId}`} className="comment-box-editable ml-5 pl-3 pr-3 pt-2 pb-2 d-flex font-size-14 flex-column" contentEditable={true} placeholder={`${I18n.t('post_content.write_a_comment')}...`} onKeyDown={readMessage} ref={sendCommentRef} />
          <Image className="rounded-circle position-absolute" src={'https://randomuser.me/api/portraits/women/54.jpg'} />
        </CommentBoxDiv>
      </div>
      <div className="border-top p-0 comment-list-box bg-transparent comment-list-scrollbar">
        <div className="take-part-footer p-0 pl-3 pr-3 pb-3 border-0 font-size-13" style={{ lineHeight: 1.3 }}>
          {comments.map((comment, index) => {
            let foundIndex = replyingId.findIndex(x => x.id === comment.id);
            return (
              <div className="d-flex pt-3" key={`comment-${comment.id}`}>
                <Image className="rounded-circle commentIcon shadow-sm" src={'https://randomuser.me/api/portraits/men/97.jpg'} />
                <div className="w-100">
                  <div className="pb-2 border-bottom">
                    <div className="comment-content">
                      <span className="text-dark font-weight-bold cursor-pointer user-select-none">{comment.user.name.fname + " " + comment.user.name.lname}</span>
                      <span className="text-body pl-2">{comment.comment_text}</span>
                    </div>
                    <div className="font-weight-bold pt-1 pl-2">
                      <span className={`cursor-pointer user-select-none ${comment.isliked ? 'text-primary' : 'text-dark'}`} onClick={() => commentLikeUnlikeHandle(postId, comment.id, false, undefined, comment.isliked)}>{comment.isliked ? I18n.t('post_content.unlike') : I18n.t('post_content.like')}</span>
                       ·
                      <span className="cursor-pointer user-select-none text-dark" onClick={() => addRemoveReplyingId(comment.id)}>{I18n.t('post_content.reply')}</span>
                      <span className="cursor-pointer user-select-none text-dark float-right" onClick={() => handleLoadReplies(postId,comment.id)} >Load Replies</span>
                      {comment.user.id == 1 ?
                        <>
                          <span className="cursor-pointer user-select-none text-dark float-right"
                            onClick={() => {
                              setReqData({
                                id: comment.id,
                                isReply: false
                              })
                              setCommentText(comment.comment_text);
                              setIsEditOpen(true);
                            }}
                            ><i className="edit icon"></i></span>
                          <span className="cursor-pointer user-select-none text-dark float-right"
                            onClick={() =>{
                              setReqData({ id: comment.id, isReply: false });
                              setIsDeleteOpen(true);
                            }}><i className="trash icon"></i></span>
                        </>
                        :
                        <div />
                      }
                    </div>
                  </div>
                  {comment.replies.map((reply, repliedIndex) => {
                    return (
                      <div className="d-flex pt-3 pb-2 border-bottom" key={`reply-${repliedIndex}-${reply.id}`}>
                        <Image className="rounded-circle commentIcon shadow-sm replyIcon" src={'https://randomuser.me/api/portraits/women/21.jpg'} />
                        <div>
                          <div className="comment-content">
                            <span className="text-dark font-weight-bold cursor-pointer user-select-none">{reply.user.name.fname + " " + reply.user.name.lname}</span>
                            <span className="text-body pl-2">{reply.comment_text}</span>
                          </div>
                          <div className="font-weight-bold pt-1 pl-2">
                            <span className={`cursor-pointer user-select-none ${reply.isliked ? 'text-primary' : 'text-dark'}`} onClick={() => commentLikeUnlikeHandle(postId, comment.id, true, reply.id, reply.isliked)}>{reply.isliked ? I18n.t('post_content.unlike') : I18n.t('post_content.like')}</span>
                             ·
                            <span className="cursor-pointer user-select-none text-dark" onClick={() => addRemoveReplyingId(comment.id, reply.id, reply.user.name.fname + " " + comment.user.name.lname)}>{I18n.t('post_content.reply')}</span>
                            <span className="cursor-pointer user-select-none text-dark float-right" onClick={() => handleLoadReplyReplies(postId,comment.id,reply.id)} >Load Replies</span>
                            {reply.user.id == 1 ?
                              <>
                                <span className="cursor-pointer user-select-none text-dark float-right"
                                  onClick={() => {
                                    setReqData({
                                      id: reply.id,
                                      isReply: true,
                                      parentId: reply.parent_id
                                    })
                                    setCommentText(reply.comment_text);
                                    setIsEditOpen(true);
                                  }}
                                  ><i className="edit icon"></i></span>
                                <span className="cursor-pointer user-select-none text-dark float-right"
                                  onClick={() =>{
                                    setReqData({ id: reply.id, isReply: true, parentId: reply.parent_id });
                                    setIsDeleteOpen(true);
                                  }}><i className="trash icon"></i></span>
                              </>
                              :
                              <div />
                            }
                          </div>
                        </div>
                      </div>
                    );
                  })}
                  {foundIndex !== -1 &&
                    <CommentBoxDiv
                      className="d-flex pt-3"
                      id={'scrollToCommentRef'}
                      // ref={el => {
                      //   if(newAddedId && index === newAddedId) {
                      //     scrollToCommentRef = el;
                      //   }
                      //   return;
                      // }}
                      ref={scrollToCommentRef}
                    >
                      <Image className="rounded-circle commentIcon shadow-sm replyIcon" src={'https://randomuser.me/api/portraits/women/54.jpg'} />
                      <div
                        id="comment-content-editable"
                        className="comment-box-editable w-100 pl-3 pr-3 pt-2 pb-2 d-flex font-size-14 flex-column"
                        contentEditable={true}
                        placeholder={`${I18n.t('post_content.write_a_comment')}...`}
                        onKeyDown={(event) => readMessage(event, true, comment.id, replyingId[foundIndex].replyId)}
                        ref={sendCommentRef}
                        suppressContentEditableWarning={true}
                      >
                        {replyingId[foundIndex].replyingName}
                      </div>
                    </CommentBoxDiv>
                  }
                </div>
              </div>
            );
          })}
        </div>
        {hasMoreComments &&
          <Button
            variant="primary"
            className="border-0 rounded-0 pt-2 pb-2 shadow font-size-14 font-weight-bold"
            onClick={loadMoreCommentsClick}
            block
          >
            {I18n.t('post_content.load_more_comments')}
          </Button>
        }
      </div>
      <EditCommentModal
        show={isEditOpen}
        handleClose={() => setIsEditOpen(false)}
        handleEditComment={handleEditComment}
        postId={postId}
        data={reqData}
        commentText={commentText}
        setCommentText={setCommentText}
      />
      <DeleteCommentModal
        show={isDeleteOpen}
        handleClose={() => setIsDeleteOpen(false)}
        handleDeleteComment={handleDeleteComment}
        postId={postId}
        data={reqData}
      />
    </React.Fragment>
  )
}

export default CommentBox
