import React from 'react';
import { Modal, Button } from "react-bootstrap";

const DeleteCommentModal = ({ show, handleClose, handleDeleteComment, postId, data }) =>{
  return(
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Warning!!</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        You really wanted to delete this Comment?
      </Modal.Body>
      <Modal.Footer>
        <Button variant="danger" onClick={() => {
          handleDeleteComment(postId, data.id, data.isReply, data.parentId);
          handleClose();
        }} >Delete</Button>
        <Button variant="light" onClick={() => handleClose()} >Cancel</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default DeleteCommentModal;
