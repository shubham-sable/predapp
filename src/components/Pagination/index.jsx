import React from 'react';
import Pagination from 'react-bootstrap/Pagination';

const ReactPagination = ({ currentPage, totalItems, perPage, onClickPageNumber, className }) => {

  const totalPages = Math.ceil(totalItems / perPage);

  let numberOfArray = 5;

  let start = currentPage;

  if(start - 2 <= 0) {
    start = 1;
  } else {
    start = start - 2;
  }

  if((start + 5) > totalPages) {
    start = totalPages - 4;
  }

  if(totalPages <= 5) {
    numberOfArray = totalPages;
    start = 1;
  }

  return (
    <Pagination className={`pagination-custom ${className}`}>
      <Pagination.First
        onClick={() => onClickPageNumber(1)}
        disabled={currentPage === 1}
      />
      <Pagination.Prev
        onClick={() => onClickPageNumber(currentPage - 1)}
        disabled={currentPage === 1}
      />

      {[...new Array(numberOfArray)].map((value, index) => {
        const pageNumber = start;
        start++;
        return (
          <Pagination.Item
            key={pageNumber}
            onClick={() => onClickPageNumber(pageNumber)}
            active={pageNumber === currentPage ? true : false}
          >
            {pageNumber}
          </Pagination.Item>
        )
      })}

      <Pagination.Next
        onClick={() => onClickPageNumber(currentPage + 1)}
        disabled={currentPage === totalPages}
      />
      <Pagination.Last
        onClick={() => onClickPageNumber(totalPages)}
        disabled={currentPage === totalPages}
      />
    </Pagination>
  )
}

export default ReactPagination;
