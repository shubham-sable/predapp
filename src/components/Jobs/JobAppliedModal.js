import React from "react";
import { Modal } from "react-bootstrap";
import JobCard from "./JobCard";

const JobAppliedModal = ({ show, handleClose, data, removedSavedListAction }) =>{
  return (
    <Modal size="lg" show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Saved List</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {data.loading ?
          <div className="border shadow px-3 py-5 text-center">
            <img src="/assets/no-jobs.svg" alt="no-jobs" />
            <h3 className="mt-2">There are no jobs right now.</h3>
            <p className="font-size-14 m-0">Create a job alert and we’ll let you know when relevant jobs are posted.</p>
          </div>
          :
          data.map((job) =>{
            //here to edit
            if(job.saved){
              return(
                <JobCard
                  key={job.id}
                  data={job}
                  createdAt={new Date()}
                  skills={['UI/UX', 'Sketch', 'Adobe CC']}
                  jobType="Full Time"
                  removedSavedListAction={removedSavedListAction}
                />
              )
            }
          })
        }
      </Modal.Body>
    </Modal>
  )
};

export default JobAppliedModal;
