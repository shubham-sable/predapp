import React from 'react';
import { useState } from 'react';
import { Button, Badge } from 'react-bootstrap'

import { Link } from 'react-router-dom';
import moment from 'moment';
import JobApplyModal from './JobApplyModal';

const JobCard = ({
  data,
  isJobApplied,
  skills,
  createdAt,
  addSavedListAction,
  removedSavedListAction,
  addApplicationAction,
}) => {

  const [isApplyNowOpen, setIsApplyNowOpen] = useState(false);

  const middleDot = <span className="text-dark px-1">&#9679;</span>;
  const moment1 = moment(createdAt);
  const moment2 = moment();
  return (
    <div className="border shadow mt-3 p-4">
      <div className="d-flex justify-content-between align-items-center">
        <div>
          <p className="text-muted font-size-12 m-0 text-capitalize">{moment.duration(moment1.diff(moment2)).humanize(true)}</p>
          <Link to={`/jobs/${data.id}`}><h3 className="mt-1 mb-0 text-info">{data.JobName}</h3></Link>
          <p className="font-size-14 font-weight-bold pt-1" style={{ color: '#89878b' }}><span>{data.Company}</span> {middleDot} <span>{data.location.address}</span> {middleDot} <span>{data.Offer.Salary}</span></p>
        </div>
        <div>
          <Badge className="font-size-14 px-3 py-2" pill variant="primary">{data.Type}</Badge>
        </div>
      </div>
      <div className="job-description mt-2 font-size-14">
        <p className="break-words lt-line-clamp lt-line-clamp--multi-line m-0" style={{ WebkitLineClamp: 2, lineHeight: 1.4 }}>{data.Desc}</p>
        <br />
        <Link to={`/jobs/${data.id}`} className="text-muted"><u>see more...</u></Link>
      </div>
      <div className="mt-3">
        {isJobApplied ?
          <Button variant="primary" className="disabled font-size-14 px-4">Applied</Button>
          :
          <Button onClick={() => setIsApplyNowOpen(true)} variant="primary" className="font-size-14 px-4">Apply Now</Button>
        }
        { data.saved ?
          <Button onClick= { () => removedSavedListAction(data.id) } variant="outline-primary" className="ml-2 font-size-14 px-4">Unsave</Button>
          :
          <Button onClick= { () => addSavedListAction(data.id)} variant="outline-primary" className="ml-2 font-size-14 px-4">Add to Save</Button>
         }
      </div>
      <JobApplyModal
        show={isApplyNowOpen}
        handleClose={() => setIsApplyNowOpen(false)}
        data={{
          company: data.Company,
          id: data.id,
        }}
        addApplicationAction={addApplicationAction}
      />
    </div>
  )
}

export default JobCard;
