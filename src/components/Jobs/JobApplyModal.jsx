import React, { useState } from 'react';

import { Modal, Button, Image, Form, Row, Col } from 'react-bootstrap';


import { Formik, Form as FormikForm } from 'formik';
import { ErrorMessage } from 'formik';
import * as Yup from 'yup';
import options from './country.json';
import { fromJS } from 'immutable';

const FormStep1Schema = Yup.object({
  email: Yup.string().required('Required'),
  country: Yup.string().required('Required'),
  phoneNumber: Yup.string().required('Required')
});

const FormStep2Schema = Yup.object({

  // file: Yup.mixed().test((file)=>file),
  // resume: Yup.string().required('Required'),

});

const FormStep3Schema = Yup.object().shape({
  VueEx:Yup.string().required("required"),
  nodeEx:Yup.string().required("required"),
  reactEx:Yup.string().required("required"),
  locationComfortable:Yup.string("required")
  // resume: Yup.string().required('Required'),

});

const FormStep4Schema = Yup.object().shape({
  // resume: Yup.string().required('Required'),
  startImmediately:Yup.string().required("required",)
});

const JobApplyModal = ({ show, handleClose, data, addApplicationAction}) => {
  const [currentStep, setCurrentStep] = useState(1);
  const [formData, setFormData] = useState({
    email: 'chetangodhani9@gmail.com',
    country: 'in',
    phoneNumber: '7028302333'
  });


  const addApplication = (formData,id) => {
    addApplicationAction(formData,id);
    handleClose();
  };
  // const addNewApplication = (id) =>{
  //   addNewApplicationAction(id)
  // }

  const handleFormSubmit = async (values) => {
    setFormData(formData => ({
      ...formData,
      ...values
    }));
    if (currentStep !== 5) {
      handleStepChange('next');
    }
  }

  const handleStepChange = type => {
    if (type === 'prev') {
      setCurrentStep(currentStep => currentStep - 1);
    } else if (type === 'next') {
      setCurrentStep(currentStep => currentStep + 1);
    }
  }

  let submitFormSteps = null;

  const handleSubmitFormRef = (e) => {
    if (currentStep===5){
      addApplication(formData,data.id);
    }
    if (submitFormSteps) {
      submitFormSteps(e);
    }
  };

  const bindSubmitForm = (submitForm) => {
    submitFormSteps = submitForm;
  };

  const UserData = () => {
    return (
      <div className="d-flex align-items-center w-100">
        <Image src="https://www.gravatar.com/avatar/72d1a6cdbdfbe17b98b6510e908683c9?s=500" rounded width={56} style={{ alignSelf: 'flex-start' }} roundedCircle />
        <div style={{ flex: 1, margin: '0 10px' }}>
          <p className="m-0 font-weight-bold font-size-16">Chetan Godhani</p>
          <p className="m-0 font-size-14 mt-1">Freelance Full Stack Developer | React.JS | Node.JS | Next.JS | Laravel</p>
          <p className="m-0 font-weight-normal text-muted"><span className="text-info">Developer,</span><span> Ahmedabad</span></p>
        </div>
      </div>
    )
  };

  return (
    <Modal size="lg" show={show} onHide={handleClose}>
      {/* {console.log("this is form data",formData)} */}
      <Modal.Header closeButton>
        <Modal.Title>Apply to {data.company}</Modal.Title>
      </Modal.Header>
      <Modal.Body className="px-4 py-2 overflow-y-auto" style={{ maxHeight: '80vh' }}>
        <div className="d-flex align-items-center mb-3">
          <progress max="100" value={currentStep > 0 ? 25 + (currentStep - 2)*25 : 0} className="job-apply-progress" aria-valuetext="Current value: 0" style={{ '--offset-value': '0%' }}>
            Current value: 0
          </progress>
          <div className="ml-2 font-size-16">{currentStep > 0 ? 25 + (currentStep - 2)*25 : 0}%</div>
        </div>

        <p className="mb-2 font-size-16 font-weight-bold">
          {currentStep === 1 ?
            'Contact info'
            :
            currentStep === 2 ?
              'Resume Upload '
              :
              currentStep === 3 ?
                'Additional Questions'
                :
                currentStep === 4 ?
                  'Work authorization'
                  :
                  'Review your Application'
          }
        </p>
        <p className="font-size-14 text-muted">
          {currentStep === 4 ?
            'Not sure how to answer the following questions? Learn More.'
            :
            currentStep === 5 ? 'The employer will also receive a copy of your profile.' : ''
          }
        </p>

        {currentStep === 1 &&
          <UserData />
        }

        {currentStep === 1 ?
          <Formik
            enableReinitialize
            initialValues={{
              email: formData.email,
              country: formData.country,
              phoneNumber: formData.phoneNumber,
            }}
            validationSchema={FormStep1Schema}
            onSubmit={handleFormSubmit}
          >
            {(renderProps) => {
              const { values: formValues, submitForm } = renderProps;
              bindSubmitForm(submitForm);
              return (

                <FormikForm className="mt-3">
                  <Form.Group>
                    <Form.Label className="font-size-14" htmlFor="inlineFormInputGroup">Email address*</Form.Label>
                    <Form.Control
                      as="select"
                      value={formValues.email}
                      onChange={(e) => renderProps.setFieldValue('email', e.target.value)}
                    >
                      <option value="chetangodhani9@gmail.com">chetangodhani9@gmail.com</option>
                    </Form.Control>
                  </Form.Group>
                  <div className="d-flex">
                    <Form.Group className="flex-grow-1">
                      <Form.Label className="font-size-14" htmlFor="inlineFormInputGroup">Phone country code*</Form.Label>
                      <Form.Control
                        as="select"
                        value={formValues.country}
                        onChange={(e) => renderProps.setFieldValue('country', e.target.value)}
                      >
                        {options.map(country => <option value={country.value} key={country.value}>{country.label}</option>)}
                      </Form.Control>
                    </Form.Group>
                    <Form.Group controlId="formBasicMobile" className="ml-3">
                      <Form.Label className="font-size-14">Mobile phone number</Form.Label>
                      <Form.Control type="number" placeholder="Enter Mobile Number" className="remove-number-arrows" value={formValues.phoneNumber} onChange={(e)=>renderProps.setFieldValue('phoneNumber',e.target.value)}/>
                      <div className="red font-size-10" style={{ color:'red' }}>{renderProps.errors.phoneNumber}</div>
                      {/* <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                      </Form.Text> */}
                      {/* <p>{renderProps.errors}</p> */}

                      {/* <ErrorMessage name={errors}/> */}
                      {/* {console.log("this is myphone ",FormStep1Schema.isValidSync(formValues.phoneNumber))} */}
                    </Form.Group>

                  </div>

                </FormikForm>

              )
            }}
          </Formik>
          :
          currentStep === 2 ?
            <Formik
              enableReinitialize
              initialValues={{
                resume: null,
              }}
              validationSchema={FormStep2Schema}
              onSubmit={handleFormSubmit}
            >
              {(renderProps) => {
                const { values: formValues, submitForm } = renderProps;
                bindSubmitForm(submitForm);
                return (
                  <FormikForm className="mt-3">
                    <Form.Group controlId="formBasicResume">
                      <Form.Label className="font-size-14">Please include a resume</Form.Label>
                      <Form.Control type="file" required />
                      <div className="red font-size-10" style={{ color:'red' }}>{renderProps.errors.file}</div>
                      {/* <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                      </Form.Text> */}
                    </Form.Group>
                  </FormikForm>
                )
              }}
            </Formik>
            :
            currentStep === 3 ?
              <Formik
                enableReinitialize
                initialValues={{
                  answers: [],
                  VueEx:'',
                  nodeEx:'',
                  reactEx:'',
                  locationComfortable: "Yes"
                }}
                validationSchema={FormStep3Schema}
                onSubmit={handleFormSubmit}
              >
                {(renderProps) => {
                  const { values: formValues, submitForm } = renderProps;
                  bindSubmitForm(submitForm);
                  return (
                    <FormikForm className="mt-3">
                      <Form.Group controlId="formBasicQ1">
                        <Form.Label className="font-size-14">How many years of work experience do you have using Vue.js?</Form.Label>
                        <Form.Control type="number" className="remove-number-arrows" value={formValues.VueEx} onChange={(e)=>renderProps.setFieldValue('VueEx',e.target.value)} />

                      </Form.Group>
                      <Form.Group controlId="formBasicQ2">
                        <Form.Label className="font-size-14">How many years of work experience do you have using Node.js?</Form.Label>
                        <Form.Control type="number" className="remove-number-arrows"  value={formValues.nodeEx} onChange={(e)=>renderProps.setFieldValue('nodeEx',e.target.value)}/>
                        <div className="red font-size-10" style={{ color:'red' }}>{renderProps.errors.nodeEx}</div>
                      </Form.Group>
                      <Form.Group controlId="formBasicQ3">
                        <Form.Label className="font-size-14">How many years of work experience do you have using React.js?</Form.Label>
                        <Form.Control type="number" className="remove-number-arrows" value={formValues.reactEx} onChange={(e)=>renderProps.setFieldValue('reactEx',e.target.value)}/>
                        <div className="red font-size-10" style={{ color:'red' }}>{renderProps.errors.reactEx}</div>
                      </Form.Group>
                      {/* {console.log("These are the answers for JueEx",formValues.jueEx)}
                      {console.log("These are the answers for nodeJs",formValues.nodeEx)} */}
                      <fieldset>
                        <Form.Group as={Row}>
                          <Col sm={12}>
                            <Form.Label as="legend" className="font-size-14">
                              Are you comfortable commuting to this job's location?
                            </Form.Label>
                          </Col>

                          <Col sm={12} value={formValues.locationComfortable}  onChange={(e)=>renderProps.setFieldValue('locationComfortable',e.target.value)}>
                              <Form.Check
                                type="radio"
                                label="Yes"
                                name="formHorizontalRadios"
                                id="formHorizontalRadios1"
                                value="Yes"
                                checked="checked"
                                onChange={(e)=>renderProps.setFieldValue('locationComfortable',e.target.value)}
                              />
                              <Form.Check
                                type="radio"
                                label="No"
                                name="formHorizontalRadios"
                                id="formHorizontalRadios2"
                                value="No"
                                onChange={(e)=>renderProps.setFieldValue('locationComfortable',e.target.value)}
                              />
                          </Col>
                          <div className="red font-size-10" style={{ color:'red' }}>{renderProps.errors.locationComfortable}</div>

                          {/* {console.log("Location Comfortable",formValues.locationComfortable)} */}
                        </Form.Group>
                      </fieldset>
                      <div className="red font-size-10" style={{ color:'red' }}>{renderProps.errors.VueEx}</div>
                    </FormikForm>
                  )
                }}
              </Formik>
              :
              currentStep === 4 ?
                <Formik
                  enableReinitialize
                  initialValues={{
                    startImmediately: "Yes",
                  }}
                  validationSchema={FormStep4Schema}
                  onSubmit={handleFormSubmit}
                >
                  {(renderProps) => {
                    const { values: formValues, submitForm } = renderProps;
                    bindSubmitForm(submitForm);
                    return (
                      <FormikForm className="mt-3">
                        <fieldset>
                          <Form.Group as={Row}>
                            <Col sm={12}>
                              <Form.Label as="legend" className="font-size-14">
                                We must fill this position urgently. Can you start immediately?
                              </Form.Label>
                            </Col>
                            <Col sm={12} value={formValues.startImmediately} onChange={(e)=>renderProps.setFieldValue('startImmediately',e.target.value)}>
                              <Form.Check
                                type="radio"
                                label="Yes"
                                name="formHorizontalRadios"
                                id="formHorizontalRadios1"
                                value="Yes"
                                checked="checked"
                              />
                              <Form.Check
                                type="radio"
                                label="No"
                                name="formHorizontalRadios"
                                id="formHorizontalRadios2"
                                value="No"
                              />
                            </Col>
                            <div className="red font-size-10" style={{ color:'red' }}>{renderProps.errors.startImmediately}</div>
                          </Form.Group>
                        </fieldset>
                      </FormikForm>
                    )
                  }}
                </Formik>
                :
                <React.Fragment>
                  <div className="mt-3 border-top py-3">
                    <p className="mb-2 font-size-16 font-weight-bold">Contact Info</p>
                    <UserData />
                    <div className="mt-3">
                      <p className="m-0 font-size-13 text-muted">Email address</p>
                      <p className="mb-0 mt-2 font-size-15">{formData.email}</p>
                    </div>
                    <div className="mt-3">
                      <p className="m-0 font-size-13 text-muted">Phone country code</p>
                      <p className="mb-0 mt-1 font-size-15">{formData.country}</p>
                    </div>
                    <div className="mt-3">
                      <p className="m-0 font-size-13 text-muted">Mobile phone number</p>
                      <p className="mb-0 mt-1 font-size-15">{formData.phoneNumber}</p>
                    </div>
                  </div>
                  <div className="ðborder-top py-3">
                    <p className="mb-2 font-size-16 font-weight-bold">Resume</p>
                    <div className="mt-3 border d-flex" style={{ width: 'fit-content' }}>
                      <div className="m-0 font-size-16 font-weight-bold text-white bg-danger p-3 d-flex align-items-center">PDF</div>
                      <div className="border-left p-3">
                        <p className="mb-0 font-size-15">Resume File Name</p>
                        <p className="mb-0 mt-2 font-size-15">Last used on 9/30/2020</p>
                      </div>
                    </div>
                  </div>
                  <div className="border-top py-3">
                    <p className="mb-2 font-size-16 font-weight-bold">Additional Questions</p>
                    <div className="mt-3">
                      <p className="m-0 font-size-13 text-muted">How many years of work experience do you have using Vue.js?</p>
                      <p className="mb-0 mt-2 font-size-15">{formData.VueEx}</p>
                    </div>
                    <div className="mt-3">
                      <p className="m-0 font-size-13 text-muted">How many years of work experience do you have using Node.js?</p>
                      <p className="mb-0 mt-1 font-size-15">{formData.nodeEx}</p>
                    </div>
                    <div className="mt-3">
                      <p className="m-0 font-size-13 text-muted">How many years of work experience do you have using React.js?</p>
                      <p className="mb-0 mt-1 font-size-15">{formData.reactEx}</p>
                    </div>
                    <div className="mt-3">
                      <p className="m-0 font-size-13 text-muted">Are you comfortable commuting to this job's location?</p>
                      <p className="mb-0 mt-1 font-size-15">{formData.locationComfortable}</p>
                    </div>
                  </div>
                  <div className="border-top py-3">
                    <p className="mb-2 font-size-16 font-weight-bold">Work authorization</p>
                    <div className="mt-3">
                      <p className="m-0 font-size-13 text-muted">We must fill this position urgently. Can you start immediately?</p>
                      <p className="mb-0 mt-1 font-size-15">{formData.startImmediately}</p>
                    </div>
                  </div>
                  <div className="border-top pt-3">
                    <Form.Group as={Row} controlId="formHorizontalCheck">
                      <Col sm={{ span: 12 }}>
                        <Form.Check label={<p className="m-0">Follow <strong>Freelance Man</strong> to stay up to date with their page.</p>} />
                      </Col>
                    </Form.Group>
                  </div>
                  {console.log("This is formdata",formData)}
                </React.Fragment>

        }
      </Modal.Body>
      <Modal.Footer>
        {currentStep > 1 &&
          <Button className="rounded-0 font-size-14" variant="outline-primary" onClick={() => handleStepChange('prev')}>Back</Button>
        }
        <Button className="rounded-0 font-size-14" type="submit" variant="primary" onClick={handleSubmitFormRef}>{currentStep === 5 ? 'Submit Application': 'Next'}</Button>
      </Modal.Footer>
    </Modal>
  )
}


export default JobApplyModal;
