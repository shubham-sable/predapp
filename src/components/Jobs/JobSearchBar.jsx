import React from 'react';

import { Button, Container } from 'react-bootstrap';
import Autocomplete from '../SelectAutocomplete';

const JobSearchBar = () => {
  return (
    <div className="shadow p-3" style={{ backgroundColor: '#f3f6f8' }}>
      <Container className="mx-1200">
        <h2 className="text-center font-weight-normal">Search for your next job</h2>
        <div className="d-flex justify-content-center align-items-center mt-4 mx-auto" style={{ maxWidth: 800 }}>
          <Autocomplete classes="mt-0 w-100 font-size-14" placeholder="Search by title, skill, or company" />
          <Autocomplete classes="mt-0 w-100 ml-3" placeholder="Search Location" />
          <Button variant="primary" className="rounded-0 font-size-14 ml-3 px-4">Search</Button>
        </div>
      </Container>
    </div>
  )
}

export default JobSearchBar;
