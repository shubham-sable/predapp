import React from 'react';

import { Image, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBook, faMapMarker, faRupeeSign, faExternalLinkAlt, faShareAlt } from "@fortawesome/free-solid-svg-icons";

import { Link } from 'react-router-dom';

const JobViewWidget = ({ title, location, qualification, salary, company, logo, slug, handleOpen }) => {
  return (
    <div className="shadow border mt-3 p-3">
      <div className="d-flex">
        <Image src={logo} roundedCircle style={{ width: 50, height: 50 }} />
        <div className="ml-2">
          <Link to={`/jobs/${slug}`}>
            <h3 className="font-weight-600 font-size-16 text-info">{title}</h3>
          </Link>
          <p className="mb-0 font-size-14 font-weight-bold">at {company}</p>
        </div>
      </div>
      <div className="font-size-14 text-muted ml-2">
        <div className="mt-3">
          <FontAwesomeIcon className="mx-auto" style={{ width: 20, fontSize: 16 }} icon={faMapMarker} /> <span className="ml-2">{location}</span>
        </div>
        <div className="mt-3">
          <FontAwesomeIcon className="mx-auto" style={{ width: 20, fontSize: 16 }} icon={faBook} /> <span className="ml-2">{qualification}</span>
        </div>
        <div className="mt-3">
          <FontAwesomeIcon className="mx-auto" style={{ width: 20, fontSize: 16 }} icon={faRupeeSign} /> <span className="ml-2">{salary}</span>
        </div>
      </div>
      <div className="mt-3">
        <Button
          variant="outline-primary" 
          className="rounded shadow-sm font-size-14 text-uppercase"
          onClick={handleOpen}
        >
          <FontAwesomeIcon className="mr-2" icon={faExternalLinkAlt} /> Apply
        </Button>
        <Button variant="default" className="rounded ml-2 font-size-14 text-uppercase"><FontAwesomeIcon className="mr-2" icon={faShareAlt} /> Share Opening</Button>
      </div>
    </div>
  )
}

export default JobViewWidget;
