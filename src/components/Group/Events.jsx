import React from "react";

import UpcomingEvents from './../UpcomingEvents/UpcomingEvents';

import "./Events.css";
import "./EventListing.css";

class Events extends React.Component {

  removeEventData = eventData => {
    const { events } = this.state;

    const newEvents = events.filter(x => x.id !== eventData.id);

    this.setState({
      events: newEvents
    });
  };

  editCreatedEvent = () => {
    this.setState({
      showCreateEventModal: true
    });
  };

  render() {
    const { groupData, match, history, location } = this.props;
    const eventId = match.params.eventId;

    return (
      (eventId === 'today' || eventId === 'this-week' || eventId === 'this-month' || eventId === 'previous') &&
      <UpcomingEvents match={match} history={history} location={location} groupData={groupData} isGroup />
    );
  }
}

export default Events;