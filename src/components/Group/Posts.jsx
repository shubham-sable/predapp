import React, { Component } from 'react';

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../actions/GroupActions';

import { Spinner } from 'react-bootstrap';
import InfiniteScroll from 'react-infinite-scroller';

// Custom Components
import PostEditor from '../PostEditor';
import SinglePost from "./../PostsListing/SinglePost";

class Posts extends Component {

  componentDidMount() {
    const { fetchGroupPostsAction, groupData, groupPostsCurrentPage, groupPostsLimit } = this.props;
    fetchGroupPostsAction(groupData.id, groupPostsCurrentPage, groupPostsLimit);
  }

  componentWillUnmount() {
    this.props.resetGroupPostListAction();
  }

  handlePostSubmit = (value) => {
    console.log('value', value);
  }

  postLikeUnlikeHandle = (postId) => {
    const { saveGroupPostLikeUnlikeAction } = this.props;
    saveGroupPostLikeUnlikeAction(postId);
  }

  commentLikeUnlikeHandle = (postId, commentId, isReply, replyCommentId) => {
    const { saveGroupPostCommentLikeUnlikeAction } = this.props;
    saveGroupPostCommentLikeUnlikeAction(postId, commentId, isReply, replyCommentId);
  }

  handleChangePageNumber = (pageNumber) => {
    const { fetchGroupPostsAction, groupData, groupPostsCurrentPage, groupPostsLimit } = this.props;
    if (pageNumber !== groupPostsCurrentPage) {
      fetchGroupPostsAction(groupData.id, pageNumber, groupPostsLimit);
    }
  }

  render() {
    const { groupPosts, groupPostsLoading, groupPostsLimit, groupPostsTotal, groupPostsCurrentPage } = this.props;

    const groupPostTotalPages = Math.ceil(groupPostsTotal / groupPostsLimit);

    return (
      <div className="w-auto m-2" style={{ flex: 1 }}>
        <PostEditor onSubmit={this.handlePostSubmit} isGroup />
        {groupPostsLoading ?
          <div className="d-flex justify-content-center pt-4 mx-auto" style={{ width: 650 }}>
            <Spinner animation="border" role="status">
              <span className="sr-only">Loading...</span>
            </Spinner>
          </div>
          :
          <InfiniteScroll
            pageStart={groupPostsCurrentPage}
            initialLoad={false}
            loadMore={this.handleChangePageNumber}
            hasMore={groupPostsCurrentPage + 1 <= groupPostTotalPages}
            loader={
              <div className="d-flex card__box--post justify-content-center pt-2 pb-2" key={0}>
                <Spinner animation="border" role="status">
                  <span className="sr-only">Loading...</span>
                </Spinner>
              </div>
            }
          >
            {groupPosts.map((post, index) => {
              return (
                <SinglePost key={`post-${post.id}-${index}`} data={post} postLikeUnlikeHandle={this.postLikeUnlikeHandle} commentLikeUnlikeHandle={this.commentLikeUnlikeHandle} />
              );
            })}
          </InfiniteScroll>
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return state.group;
};

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators({ ...Actions }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Posts);
