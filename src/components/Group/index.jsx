import React, { Fragment, Component } from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../../actions/GroupActions';
import { changeNavTitle } from '../../actions/common';

import GroupHeader from '../Header/GroupHeader';
// import GroupSidebar from './GroupSidebar';
import Events from './Events';
import About from './About';
import Members from './Members';
import Posts from './Posts';

import { matchPath } from 'react-router-dom';
import { Redirect } from 'react-router-dom';

const allEventRoutes = ['today', 'this-week', 'this-month', 'previous'];

class Group extends Component {

  constructor(props) {
    super(props);
    this.state = {
      groupId: props.match.params.id
    };
  }

  componentDidMount() {
    const { fetchGroupDataAction, match, changeNavTitle, groupData } = this.props;
    fetchGroupDataAction(match.params.id);
    changeNavTitle({ title: groupData.name });
  }

  componentDidUpdate() {
    const { fetchGroupDataAction, match, changeNavTitle, groupData } = this.props;
    fetchGroupDataAction(match.params.id);
    changeNavTitle({ title: groupData.name });
  }

  shouldComponentUpdate(nextProps, nextState) {
    if ((nextProps.match.params.id !== this.props.match.params.id) || (this.props.groupData && (nextProps.groupData.id !== this.props.groupData.id)) || this.state !== nextState) {
      return true;
    }
    return false;
  }

  getParams = (pathname, params) => {
    let matchProfile = '404';
    if (matchPath(pathname, { path: '/group/:id', exact: true, strict: false })) {
      matchProfile = 'index';
    } else if (matchPath(pathname, { path: '/group/:id/events', exact: true, strict: false })) {
      matchProfile = 'redirect';
    } else if (matchPath(pathname, { path: '/group/:id/events/:eventId', exact: true, strict: false })) {
      
      if(allEventRoutes.includes(params.eventId)) {
        matchProfile = 'events';
      } else {
        matchProfile = 'redirect';
      }
    } else if (matchPath(pathname, { path: '/group/:id/about', exact: true, strict: false })) {
      matchProfile = 'about';
    } else if (matchPath(pathname, { path: '/group/:id/members', exact: true, strict: false })) {
      matchProfile = 'members';
      // } else if(matchPath(pathname, { path: '/group/:id/photos', exact: true, strict: false })) {
      //   matchProfile = 'photos';
    } else if (matchPath(pathname, { path: '/group/:id/integrations', exact: true, strict: false })) {
      matchProfile = 'integrations';
    }
    return matchProfile;
  };

  renderGroupPaths = (routeName) => {
    if (routeName === 'index') {
      return (
        <Posts {...this.props} />
      );
    } else if (routeName === 'events') {
      return (
        <Events {...this.props} />
      );
    } else if (routeName === 'about') {
      return (
        <About {...this.props} />
      );
    } else if (routeName === 'members') {
      return (
        <Members {...this.props} />
      );
      // } else if(routeName === 'photos') {
      //   return (
      //     <Photos {...this.props}/>
      //   );
    } else if (routeName === 'integrations') {
      return (
        <div></div>
      );
    }
  }

  // renderSidebar = (routeName) => {
  //   if(routeName === 'index' || routeName === 'about' || routeName === 'members' || routeName === 'integrations') {
  //     return (
  //       <GroupSidebar />
  //     );
  //   }
  // }

  render() {
    const { groupData, match } = this.props;
    console.log('this.props', this.props);

    const isRoute = this.getParams(this.props.location.pathname, match.params);

    return (
      isRoute === 'redirect' ?
        <Redirect to={`/group/${match.params.id}/events/today`} />
        :
        <Fragment>
          <GroupHeader groupData={groupData} />
          <div className="w-100">
            {this.renderGroupPaths(isRoute)}
            {/* { this.renderSidebar(isRoute) } */}
          </div>
        </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    groupData: state.group.groupData,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators({ ...Actions, changeNavTitle }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Group);
