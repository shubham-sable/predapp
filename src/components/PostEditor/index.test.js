import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import PostEditor from './';

import { findByTestAtrr } from '../../../Utils';

Enzyme.configure({ adapter: new EnzymeAdapter() });

describe('PostEditor Testing', ()=>{
  const myFun = jest.fn();
  const wrapper = shallow(<PostEditor onSubmit={myFun} />);
  console.log(wrapper.debug());
  test('render without error', ()=>{
    expect(wrapper);
  })
  test('render Formik without error', ()=>{
    expect(wrapper.find('Formik').length).toBe(1);
  })
  test('render Formik with initialValues', ()=>{
    expect(wrapper.find('Formik').prop('initialValues')).not.toBeUndefined();
  })
})
