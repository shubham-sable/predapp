import React from 'react';

import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const PeopleCard = ({ coverImage, profileImage, id, name, tagline, connections, connectionImage }) => {
  return (
    <div className="border shadow people-listing-card">
      <img className="poster-background" src={coverImage} alt="" />
      <div className="d-flex flex-column align-items-center people-card-data text-center px-2">
        <img src={profileImage} height={104} alt="" />
        <div className="pt-2">
          <Link className="font-size-16 font-weight-bold break-words lt-line-clamp lt-line-clamp--multi-line" to={`/profile/${id}`} style={{ WebkitLineClamp: 1 }}>{name}</Link>
          <div className="pt-1 text-muted break-words lt-line-clamp lt-line-clamp--multi-line font-size-14 m-0" style={{ WebkitLineClamp: 2, height: 36 }}>{tagline}</div>
        </div>
        <div className="people-connections d-flex pt-3 align-items-center px-3">
          {connections && connections > 0 ?
            <React.Fragment>
              <img src={connectionImage} height={104} alt="" />
              <span className="font-weight-bold">14 Shared Connections</span>
            </React.Fragment>
            :
            <span style={{ height: 30 }}> - </span>
          }
        </div>
      </div>
      <div className="p-3">
        <Button className="rounded-0 font-size-14" variant="outline-primary" block>Connect</Button>
      </div>
    </div>
  )
}

export default PeopleCard
