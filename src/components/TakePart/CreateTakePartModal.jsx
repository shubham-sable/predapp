import React from 'react';

import { Modal, Button, Col, Card, Row, FormLabel } from "react-bootstrap";
import Select from 'react-select';

import { Formik, Form, Field, ErrorMessage, FieldArray, getIn } from 'formik';
import * as Yup from "yup";

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../actions/TakePartActions';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faMinus } from "@fortawesome/free-solid-svg-icons";
import { Translate, I18n } from "react-redux-i18n";

const CardSchema = Yup.object().shape({
  what: Yup.string().required(I18n.t('take_part.required')),
  why: Yup.string().min(2, I18n.t('take_part.too_short')).max(50, I18n.t('take_part.too_long')).required(I18n.t('take_part.required')),
  answers: Yup.array().min(1, I18n.t('take_part.required')).required(I18n.t('take_part.required')),
  whyDescription: Yup.string().min(2, I18n.t('take_part.too_short')).max(140, I18n.t('take_part.too_long')).required(I18n.t('take_part.required')),
  whyImage: Yup.mixed()
    .test('fileType', I18n.t('take_part.unsupported_file_format'), value => {
      if (value) {
        return ['image/jpg', 'image/jpeg', 'image/png'].includes(value.type);
      } else {
        return true;
      }
    })
    .test('fileSize', I18n.t('take_part.file_size_is_too_large'), value => {
      if (value) {
        return value.size <= 1024000;
      } else {
        return true;
      }
    }),
  who: Yup.array().min(1, I18n.t('take_part.select_atleast_1_group')).required(I18n.t('take_part.required')),
});

const ErrorMessageArray = ({ name }) => (
  <Field name={name}>
    {({ form }) => {
      const error = getIn(form.errors, name);
      const touch = getIn(form.touched, name);
      return touch && error ?
        <div className="help-block text-danger pt-1">{error}</div>
        :
        null
    }}
  </Field>
);

const options = [
  { value: 'IT Department', label: 'IT Department' },
  { value: 'Designing', label: 'Designing' },
  { value: 'Marketing', label: 'Marketing' },
  { value: 'Management', label: 'Management' },
  { value: 'Admin', label: 'Admin' },
  { value: 'Mobile Department', label: 'Mobile Department' },
];

const proposalTypeOptions = [
  { value: 'Foo', label: 'Foo Proposal' },
  { value: 'StarMeUp', label: 'StarMeUp Proposal' },
  { value: 'BetterMe', label: 'BetterMe Proposal' },
  { value: 'BeThere', label: 'BeThere Proposal' },
  { value: 'TakePart', label: 'TakePart Proposal' },
];

const CreateTakePartModal = ({ show, onHide, createTakePartCardAction,updateTakePartCardAction,showTitle,cardData }) => {
  const handleFormSubmit = async (values) => {
    console.log('values', values);
    let data = new FormData();
    for (var key in values) {
      data.append(key, values[key]);
    }
    console.log('data', data);
    console.log("thiese are vlues",values);
    await onHide();
    {showTitle==="EDIT CARD"?
    updateTakePartCardAction(values,cardData.id):
    createTakePartCardAction(values)}
  }

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      className={`quiz-modal create-take-part`}
    >
      <Modal.Header className="text-center" closeButton>
        <Modal.Title
          id="contained-modal-title-vcenter"
          className="w-100 font-size-20 modal-title font-weight-bold"
        >
          {/* <Translate value="take_part.create_card" /> */}
          {showTitle}
        </Modal.Title>
      </Modal.Header>

      <Formik
        enableReinitialize
        initialValues=
        {
          showTitle==="EDIT CARD"?
          {
          what:cardData.card_type,
          why: cardData.card_why,
          answers: cardData.card_answers ,
          whyDescription: cardData.card_description,
          whyImage: cardData.card_img,
          who:cardData.card_person_dept,
        }:
        {
          what: '',
          why: '',
          answers: [''],
          whyDescription: '',
          whyImage: '',
          who: []
        }

      }
        validationSchema={CardSchema}
        onSubmit={(values) => handleFormSubmit(values)}
      >
        {(renderProps) => {
          return (
            <Form className="take-part-form" onSubmit={renderProps.handleSubmit}>
              <Modal.Body style={{ height: 'unset', maxHeight: '75vh', padding: '1rem' }}>
                <FormLabel className="font-size-15 font-weight-bold d-flex justify-content-between">
                  <Translate value="take_part.card_type" />
                </FormLabel>
                <Select
                  closeMenuOnSelect={true}
                  value={renderProps.values.what}
                  onChange={(selectedOption) => {
                  renderProps.setFieldValue('what', selectedOption);
                  }}
                  options={proposalTypeOptions}
                  menuContainerStyle={{ 'zIndex': 999 }}
                  isClearable
                  styles={{
                    menuPortal: base => {
                      const { zIndex, ...rest } = base;
                      return { ...rest, zIndex: 9999 };
                    }
                  }}
                  menuPortalTarget={document.body}
                  isSearchable
                  name="what"
                  id="what"
                  placeholder={I18n.t('take_part.select_card_type')}
                />
                <ErrorMessage className="help-block text-danger pt-1" name="what" component="div" />
                <Row className="mb-4 pt-4">
                  <Col sm={12}>
                    <FormLabel className="font-size-15 font-weight-bold">
                      <Translate value="take_part.why" />
                    </FormLabel>
                    <Field type="text" className="form-control shadow-none" name="why" placeholder={I18n.t('take_part.why_are_you_creating_this_card')} />
                    <ErrorMessage className="help-block text-danger pt-1" name="why" component="div" />
                  </Col>
                </Row>
                <Row className="mb-4">
                  <Col sm={12}>
                    <FormLabel className="font-size-15 font-weight-bold">
                      <Translate value="take_part.answers" />
                    </FormLabel>
                    <FieldArray
                      validateOnChange={true}
                      name="answers"
                      render={arrayHelpers => (
                        renderProps.values.answers && renderProps.values.answers.length > 0 ? (
                          <React.Fragment>
                            {renderProps.values.answers.map((answer, index) => (
                              <React.Fragment key={index}>
                                <div className={`d-flex ${index > 0 ? 'pt-1' : ''}`}>
                                  <Field name={`answers[${index}]`} className="form-control shadow-none" style={{ width: '80%' }} required />
                                  {index !== 0 &&
                                    <React.Fragment>
                                      {/* remove answer from the list */}
                                      <Button variant="outline-danger" className="ml-1 rounded-circle border" onClick={() => arrayHelpers.remove(index)}>
                                        <FontAwesomeIcon icon={faMinus} />
                                      </Button>
                                    </React.Fragment>
                                  }
                                </div>
                                <ErrorMessageArray name={`answers[${index}]`} />
                              </React.Fragment>
                            ))}
                            <Button block variant="outline-light" className="mt-2 text-body" style={{ width: '80%', border: '1px dashed' }} onClick={() => arrayHelpers.push('')}>
                              <FontAwesomeIcon icon={faPlus} />
                            </Button>
                          </React.Fragment>
                        ) : (
                            <button type="button" onClick={() => arrayHelpers.push('')}>
                              {/* show this when user has removed all answers from the list */}
                                Add a friend
                            </button>
                          )
                      )}
                    />
                  </Col>
                </Row>
                <Row className="mb-4">
                  <Col sm={12}>
                    <FormLabel className="font-size-15 font-weight-bold">
                      <Translate value="take_part.description" />
                    </FormLabel>
                    <Field type="text" className="form-control shadow-none" name="whyDescription" placeholder={I18n.t('take_part.give_more_detail_to_this_card')} component="textarea" rows="4" />
                    <ErrorMessage className="help-block text-danger pt-1" name="whyDescription" component="div" />
                  </Col>
                </Row>
                <Row className="mb-4">
                  <Col sm={12}>
                    <FormLabel className="font-size-15 font-weight-bold">
                      <Translate value="take_part.image" />
                    </FormLabel>
                    <input
                      type="file"
                      className="form-control shadow-none"
                      name="whyImage" style={{ height: 36 }}
                      onChange={(e) => {
                        if (!e.target.files) {
                          return;
                        }
                        renderProps.setFieldValue('whyImage', e.target.files[0])
                      }}
                    />
                    <ErrorMessage className="help-block text-danger pt-1" name="whyImage" component="div" />
                  </Col>
                </Row>
                <FormLabel className="font-size-15 font-weight-bold">
                  <Translate value="take_part.person_or_department" />
                </FormLabel>
                <Select
                  closeMenuOnSelect={false}
                  value={renderProps.values.who}
                  onChange={(selectedOption) => {
                    renderProps.setFieldValue('who', selectedOption);
                  }}
                  options={options}
                  menuContainerStyle={{ 'zIndex': 999 }}
                  isClearable
                  styles={{
                    menuPortal: base => {
                      const { zIndex, ...rest } = base;
                      return { ...rest, zIndex: 9999 };
                    }
                  }}
                  menuPortalTarget={document.body}
                  isSearchable
                  isMulti
                  name="who"
                  placeholder={I18n.t('take_part.select_person_or_department')}
                  menuPlacement="top"
                />
                <ErrorMessage className="help-block text-danger pt-1" name="who" component="div" />
                {renderProps.values.who &&
                  <Row className="p-1 text-center">
                    {renderProps.values.who.map((group) => {
                      return (
                        <Col key={group.value} className="m-1 shadow align-self-center">
                          <Card.Body className="pt-3 pb-3 pl-0 pr-0">
                            <b>{group.label}</b>
                          </Card.Body>
                        </Col>
                      );
                    })}
                  </Row>
                }
              </Modal.Body>
              <Modal.Footer className="p-1">
                <Button
                  className="font-weight-bold"
                  type="submit"
                  variant="primary"
                  size="lg"
                  block
                >
                  <Translate value="take_part.save" />
                </Button>
              </Modal.Footer>
            </Form>
          )
        }}
      </Formik>
    </Modal>
  )
}

const mapStateToProps = (state) => ({
  ...state.takePart
})

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators({ ...Actions }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateTakePartModal);