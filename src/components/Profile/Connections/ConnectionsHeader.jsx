import React from 'react';

import { Button, Container } from 'react-bootstrap';

import Autocomplete from '../../SelectAutocomplete';

const ConnectionsHeader = () => {
  return (
    <div className="shadow p-3" style={{ backgroundColor: '#f3f6f8' }}>
      <Container className="mx-1200 d-flex align-items-center">
        <Autocomplete classes="mt-0 font-size-14 flex-1" placeholder="Current Companies" multiple />
        <Autocomplete classes="mt-0 ml-2 font-size-14 flex-1" placeholder="Locations" multiple />
        <Button variant="primary" className="rounded-0 font-size-14 ml-2 px-4">Search</Button>
        <Button variant="outline-primary" className="rounded-0 font-size-14 ml-2 px-4">Clear</Button>
      </Container>
    </div>
  )
}

export default ConnectionsHeader;
