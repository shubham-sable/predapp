import React from 'react';

import { Button } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUsers } from '@fortawesome/free-solid-svg-icons';

import { Link } from 'react-router-dom';

const PeopleListingCard = ({
  classNames = '',
}) => {
  return (
    <div className={`border shadow p-4 ${classNames}`}>
      <div className="d-flex justify-content-between align-items-center">
        <div className="d-flex align-items-center">

          <img className="rounded-circle align-self-start" width={48} height={48} src="https://avatars1.githubusercontent.com/u/20254321?v=4" alt="chetan8300" />
          <div className="pl-2">
            <div className="font-weight-bold font-size-16 mb-1">Chetan Godhani</div>
            <div className="break-words lt-line-clamp lt-line-clamp--multi-line font-size-14 m-0" style={{ WebkitLineClamp: 2 }}>Freelance Full Stack Developer | React.JS | Node.js | Laravel | Express.JS</div>
            <div className="text-muted break-words lt-line-clamp lt-line-clamp--multi-line font-size-14 mt-1" style={{ WebkitLineClamp: 2 }}>Ahmedabad Area, India</div>
            <div className="job-description mt-2 font-size-12 pt-2">
              <FontAwesomeIcon icon={faUsers} className="mr-2 font-size-14" />
              <Link to="/profile/2" className="text-muted font-weight-bold user-select-none cursor-pointer">Piyush Banugariya</Link>,&nbsp;
              <Link to="/profile/3" className="text-muted font-weight-bold user-select-none cursor-pointer">HR Dipali</Link>,&nbsp;
              and&nbsp;
              <Link to="/profile/2/connections" className="text-muted font-weight-bold user-select-none cursor-pointer">185 other shared connections</Link>
            </div>
          </div>
        </div>
        <Button variant="outline-primary" className="rounded-0 font-size-14 font-weight-bold px-3 py-1 ml-2">Message</Button>
      </div>
    </div>
  )
}

export default PeopleListingCard;
