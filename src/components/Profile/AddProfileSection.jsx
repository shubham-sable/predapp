import React from "react";
import { Dropdown, Accordion, Card } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown, faPlus } from '@fortawesome/free-solid-svg-icons';

import { I18n, Translate } from "react-redux-i18n";

const AddProfileSection = ({ handleModalToggle }) => {
  return (
    <Dropdown className="profile-header--add-profile-dropdown">
      <Dropdown.Toggle className="font-size-16 profile-header--add-profile" variant="primary" id="dropdown-basic">
        {I18n.t('profile.add_profile_section')}
      </Dropdown.Toggle>
      <Dropdown.Menu>
        <Accordion defaultActiveKey="0">
          <Card className="border-top border-left-0 border-right-0 border-bottom-0 rounded-0">
            <Accordion.Toggle className="font-weight-bold cursor-pointer bg-white user-select-none font-size-14 card-header d-flex justify-content-between align-items-center" as="div" eventKey="1">
              <Translate value="profile.about" />
              <FontAwesomeIcon className="font-size-20" icon={faAngleDown} />
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="1">
              <Card.Body className="p-2">
                <div className="d-flex pb-3">
                  <img src="/assets/icons/briefcase.svg" height="30" width="30" alt="" />
                  <div className="ml-2 font-size-14 flex-grow-1 flex-shrink-1">
                    <div className="font-weight-bold">{I18n.t('profile.contact_info')}</div>
                    <div className="text-body pt-1">{I18n.t('profile.contact_info_description')}</div>
                  </div>
                  <div className="profile-section-faPlus cursor-pointer user-select-none profile-header--edit ml-2 rounded-circle d-flex align-items-center justify-content-center" onClick={() => handleModalToggle('editcontactinfo')}>
                    <FontAwesomeIcon className="font-size-22 p-1 rounded-circle profile-header--edit-icon" icon={faPlus} />
                  </div>
                </div>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
          <Card className="border-top border-left-0 border-right-0 border-bottom-0 rounded-0">
            <Accordion.Toggle className="font-weight-bold cursor-pointer bg-white user-select-none font-size-14 card-header d-flex justify-content-between align-items-center" as="div" eventKey="2">
              <Translate value="profile.background" />
              <FontAwesomeIcon className="font-size-20" icon={faAngleDown} />
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="2">
              <Card.Body className="p-2">
                <div className="d-flex pb-3">
                  <img src="/assets/icons/briefcase.svg" height="30" width="30" alt="" />
                  <div className="ml-2 font-size-14 flex-grow-1 flex-shrink-1">
                    <div className="font-weight-bold">{I18n.t('profile.work_experience')}</div>
                    <div className="text-body pt-1">{I18n.t('profile.work_experience_description')}</div>
                  </div>
                  <div className="profile-section-faPlus cursor-pointer user-select-none profile-header--edit ml-2 rounded-circle d-flex align-items-center justify-content-center" onClick={() => handleModalToggle('workExperience')}>
                    <FontAwesomeIcon className="font-size-22 p-1 rounded-circle profile-header--edit-icon" icon={faPlus} />
                  </div>
                </div>
                <div className="d-flex pb-3">
                  <img src="/assets/icons/university.svg" height="30" width="30" alt="" />
                  <div className="ml-2 font-size-14 flex-grow-1 flex-shrink-1">
                    <div className="font-weight-bold">{I18n.t('profile.education')}</div>
                    <div className="text-body pt-1">{I18n.t('profile.education_description')}</div>
                  </div>
                  <div className="profile-section-faPlus cursor-pointer user-select-none profile-header--edit ml-2 rounded-circle d-flex align-items-center justify-content-center" onClick={() => handleModalToggle('education')}>
                    <FontAwesomeIcon className="font-size-22 p-1 rounded-circle profile-header--edit-icon" icon={faPlus} />
                  </div>
                </div>
                <div className="d-flex pb-3">
                  <img src="/assets/icons/certification.svg" height="30" width="30" alt="" />
                  <div className="ml-2 font-size-14 flex-grow-1 flex-shrink-1">
                    <div className="font-weight-bold">{I18n.t('profile.license_certification')}</div>
                    <div className="text-body pt-1">{I18n.t('profile.license_certification_description')}</div>
                  </div>
                  <div className="profile-section-faPlus cursor-pointer user-select-none profile-header--edit ml-2 rounded-circle d-flex align-items-center justify-content-center" onClick={() => handleModalToggle('certification')}>
                    <FontAwesomeIcon className="font-size-22 p-1 rounded-circle profile-header--edit-icon" icon={faPlus} />
                  </div>
                </div>
                <div className="d-flex pb-3">
                  <img src="/assets/icons/volunteer.svg" height="30" width="30" alt="" />
                  <div className="ml-2 font-size-14 flex-grow-1 flex-shrink-1">
                    <div className="font-weight-bold">{I18n.t('profile.volunteer_experience')}</div>
                    <div className="text-body pt-1">{I18n.t('profile.volunteer_experience_description')}</div>
                  </div>
                  <div className="profile-section-faPlus cursor-pointer user-select-none profile-header--edit ml-2 rounded-circle d-flex align-items-center justify-content-center" onClick={() => handleModalToggle('volunteer')}>
                    <FontAwesomeIcon className="font-size-22 p-1 rounded-circle profile-header--edit-icon" icon={faPlus} />
                  </div>
                </div>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
          <Card className="border-top border-left-0 border-right-0 border-bottom-0 rounded-0">
            <Accordion.Toggle className="font-weight-bold cursor-pointer bg-white user-select-none font-size-14 card-header d-flex justify-content-between align-items-center" as="div" eventKey="3">
              <Translate value="profile.skills" />
              <FontAwesomeIcon className="font-size-20" icon={faAngleDown} />
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="3">
              <Card.Body className="p-2">
                <div className="d-flex pb-3">
                  <img src="/assets/icons/skills.svg" height="30" width="30" alt="" />
                  <div className="ml-2 font-size-14 flex-grow-1 flex-shrink-1">
                    <div className="font-weight-bold">{I18n.t('profile.skills')}</div>
                    <div className="text-body pt-1">{I18n.t('profile.skills_description')}</div>
                  </div>
                  <div className="profile-section-faPlus cursor-pointer user-select-none profile-header--edit ml-2 rounded-circle d-flex align-items-center justify-content-center" onClick={() => handleModalToggle('skill')}>
                    <FontAwesomeIcon className="font-size-22 p-1 rounded-circle profile-header--edit-icon" icon={faPlus} />
                  </div>
                </div>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
          <Card className="border-top border-left-0 border-right-0 border-bottom-0 rounded-0">
            <Accordion.Toggle className="font-weight-bold cursor-pointer bg-white user-select-none font-size-14 card-header d-flex justify-content-between align-items-center" as="div" eventKey="4">
              <Translate value="profile.accomplishments" />
              <FontAwesomeIcon className="font-size-20" icon={faAngleDown} />
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="4">
              <Card.Body className="p-2" style={{ maxHeight: 400, overflowY: 'auto' }}>
                <div className="d-flex pb-3">
                  <img src="/assets/icons/publication.svg" height="30" width="30" alt="" />
                  <div className="ml-2 font-size-14 flex-grow-1 flex-shrink-1">
                    <div className="font-weight-bold">{I18n.t('profile.publications')}</div>
                    <div className="text-body pt-1">{I18n.t('profile.publications_description')}</div>
                  </div>
                  <div className="profile-section-faPlus cursor-pointer user-select-none profile-header--edit ml-2 rounded-circle d-flex align-items-center justify-content-center" onClick={() => handleModalToggle('publication')}>
                    <FontAwesomeIcon className="font-size-22 p-1 rounded-circle profile-header--edit-icon" icon={faPlus} />
                  </div>
                </div>
                <div className="d-flex pb-3">
                  <img src="/assets/icons/patent.svg" height="30" width="30" alt="" />
                  <div className="ml-2 font-size-14 flex-grow-1 flex-shrink-1">
                    <div className="font-weight-bold">{I18n.t('profile.patents')}</div>
                    <div className="text-body pt-1">{I18n.t('profile.patents_description')}</div>
                  </div>
                  <div className="profile-section-faPlus cursor-pointer user-select-none profile-header--edit ml-2 rounded-circle d-flex align-items-center justify-content-center" onClick={() => handleModalToggle('patents')}>
                    <FontAwesomeIcon className="font-size-22 p-1 rounded-circle profile-header--edit-icon" icon={faPlus} />
                  </div>
                </div>
                <div className="d-flex pb-3">
                  <img src="/assets/icons/courses.svg" height="30" width="30" alt="" />
                  <div className="ml-2 font-size-14 flex-grow-1 flex-shrink-1">
                    <div className="font-weight-bold">{I18n.t('profile.courses')}</div>
                    <div className="text-body pt-1">{I18n.t('profile.courses_description')}</div>
                  </div>
                  <div className="profile-section-faPlus cursor-pointer user-select-none profile-header--edit ml-2 rounded-circle d-flex align-items-center justify-content-center" onClick={() => handleModalToggle('courses')}>
                    <FontAwesomeIcon className="font-size-22 p-1 rounded-circle profile-header--edit-icon" icon={faPlus} />
                  </div>
                </div>
                <div className="d-flex pb-3">
                  <img src="/assets/icons/project.svg" height="30" width="30" alt="" />
                  <div className="ml-2 font-size-14 flex-grow-1 flex-shrink-1">
                    <div className="font-weight-bold">{I18n.t('profile.projects')}</div>
                    <div className="text-body pt-1">{I18n.t('profile.projects_description')}</div>
                  </div>
                  <div className="profile-section-faPlus cursor-pointer user-select-none profile-header--edit ml-2 rounded-circle d-flex align-items-center justify-content-center" onClick={() => handleModalToggle('projects')}>
                    <FontAwesomeIcon className="font-size-22 p-1 rounded-circle profile-header--edit-icon" icon={faPlus} />
                  </div>
                </div>
                <div className="d-flex pb-3">
                  <img src="/assets/icons/star.svg" height="30" width="30" alt="" />
                  <div className="ml-2 font-size-14 flex-grow-1 flex-shrink-1">
                    <div className="font-weight-bold">{I18n.t('profile.honor_awards')}</div>
                    <div className="text-body pt-1">{I18n.t('profile.honor_awards_description')}</div>
                  </div>
                  <div className="profile-section-faPlus cursor-pointer user-select-none profile-header--edit ml-2 rounded-circle d-flex align-items-center justify-content-center" onClick={() => handleModalToggle('awards')}>
                    <FontAwesomeIcon className="font-size-22 p-1 rounded-circle profile-header--edit-icon" icon={faPlus} />
                  </div>
                </div>
                <div className="d-flex pb-3">
                  <img src="/assets/icons/test.svg" height="30" width="30" alt="" />
                  <div className="ml-2 font-size-14 flex-grow-1 flex-shrink-1">
                    <div className="font-weight-bold">{I18n.t('profile.test_scores')}</div>
                    <div className="text-body pt-1">{I18n.t('profile.test_scores_description')}</div>
                  </div>
                  <div className="profile-section-faPlus cursor-pointer user-select-none profile-header--edit ml-2 rounded-circle d-flex align-items-center justify-content-center" onClick={() => handleModalToggle('tests')}>
                    <FontAwesomeIcon className="font-size-22 p-1 rounded-circle profile-header--edit-icon" icon={faPlus} />
                  </div>
                </div>
                <div className="d-flex pb-3">
                  <img src="/assets/icons/language.svg" height="30" width="30" alt="" />
                  <div className="ml-2 font-size-14 flex-grow-1 flex-shrink-1">
                    <div className="font-weight-bold">{I18n.t('profile.languages')}</div>
                    <div className="text-body pt-1">{I18n.t('profile.languages_description')}</div>
                  </div>
                  <div className="profile-section-faPlus cursor-pointer user-select-none profile-header--edit ml-2 rounded-circle d-flex align-items-center justify-content-center" onClick={() => handleModalToggle('languages')}>
                    <FontAwesomeIcon className="font-size-22 p-1 rounded-circle profile-header--edit-icon" icon={faPlus} />
                  </div>
                </div>
                <div className="d-flex pb-3">
                  <img src="/assets/icons/organization.svg" height="30" width="30" alt="" />
                  <div className="ml-2 font-size-14 flex-grow-1 flex-shrink-1">
                    <div className="font-weight-bold">{I18n.t('profile.organizations')}</div>
                    <div className="text-body pt-1">{I18n.t('profile.organizations_description')}</div>
                  </div>
                  <div className="profile-section-faPlus cursor-pointer user-select-none profile-header--edit ml-2 rounded-circle d-flex align-items-center justify-content-center" onClick={() => handleModalToggle('organizations')}>
                    <FontAwesomeIcon className="font-size-22 p-1 rounded-circle profile-header--edit-icon" icon={faPlus} />
                  </div>
                </div>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>
      </Dropdown.Menu>
    </Dropdown>
  )
}

export default AddProfileSection;
