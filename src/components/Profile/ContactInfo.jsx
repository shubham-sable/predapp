import React from 'react'

import { Modal } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen, faEnvelope } from '@fortawesome/free-solid-svg-icons';

import { I18n } from 'react-redux-i18n';

const ContactInfoModal = ({ show, onHide,handleEditContactInfo }) => {
  return (
    <Modal
      show={show}
      onHide={onHide}
      dialogClassName="mw-100 mt-5"
      className="work-experience-modal"
      aria-labelledby="contained-modal-title-vcenter"
    >
      <Modal.Header className="p-4" closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          <h1 className="font-weight-normal mb-0 font-size-24">Chetan Godhani</h1>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="p-4">
        <h2 className="d-flex justify-content-between align-items-center font-weight-normal">
          <div>Contact Info</div>
          <div className="cursor-pointer user-select-none profile-header--edit ml-2 rounded-circle d-flex align-items-center justify-content-center font-size-20">
            <FontAwesomeIcon className="font-size-24 p-1 rounded-circle profile-header--edit-icon" icon={faPen} onClick={handleEditContactInfo}/>
          </div>
        </h2>

        <div className="d-flex align-items-center pt-3">
          <FontAwesomeIcon className="font-size-20 rounded-circle profile-header--edit-icon align-self-start" icon={faEnvelope} />
          <div className="ml-3 d-flex flex-column">
            <span className="font-weight-bold font-size-16">{I18n.t('profile.email')}</span>
            {/* <a href="mailto:chetan.godhani@gmail.com" target="_blank" rel="noopener noreferrer" className="mt-2 font-size-14" >
              chetan.godhani@gmail.com
            </a> */}
            <input
              type="text"
              className="ml-2 pl-3 pr-3 pt-2 pb-2 d-flex font-size-14 flex-column"
              contentEditable={true}
              style={{
                minWidth: 420,
                minHeight: "35px",
                borderRadius: "10px",
                backgroundColor: "#F4F4F6",
                outline: "none",
                whiteSpace: "pre-wrap",
                wordBreak: "break-word",
                border: "1px solid #e5e5e5",
              }}
              value={"chetanghodani@gmail.com"}
              onChange = {(e) =>handleEditContactInfo(e.target.value)}
        />
          </div>
        </div>
      </Modal.Body>
    </Modal>
  )
}

export default ContactInfoModal;