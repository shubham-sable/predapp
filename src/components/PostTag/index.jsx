import React from "react"

const PostTag = (data) => {
  return (
    <div className="pl-3 pr-3 pb-2 border-top">
      {data.data.length > 0 ?
        <div className="chip font-size-14 mt-2 mr-2 user-select-none cursor-pointer">
          <span className="chip-label">with {data.data[0].firstname + " " + data.data[0].lastname}</span>
        </div>
        :
        <div className="chip font-size-14 mt-2 mr-2 user-select-none cursor-pointer">
          <span className="chip-label">ABC</span>
        </div>
      }
    </div>
  )
}

// <div className="chip font-size-14 mt-2 mr-2 user-select-none cursor-pointer">
//   <span className="chip-label">ABCD</span>
// </div>

export default PostTag;
