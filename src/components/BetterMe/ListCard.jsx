import React from 'react';

import { Card, Image } from 'react-bootstrap';

import { Translate } from 'react-redux-i18n';

const ListCard = ({ data, isReceived, showReceivedFeedbacks }) => {
  const total = data.feedbacks.length;
  const { comment } = data;
  const { name, profile } = isReceived ? data.sender: data.sentTo;
  return (
    <Card className="mt-3 shadow-sm">
      <Card.Header className="d-flex p-2 pl-3">
        <span className="cursor-pointer" style={{ fontWeight: '900', fontSize: '30px', color: 'cornflowerblue' }} onClick={() => isReceived ? showReceivedFeedbacks(data) : showReceivedFeedbacks(data)}>{total}</span>
        <div className="ml-2 align-self-center">
          <Translate value="better_me.rated_compitencies" className="font-size-16 text-uppercase" style={{ fontWeight: '500', color: '#90949C' }} />
        </div>
      </Card.Header>
      <Card.Body className="p-2 pl-3 bg-white border-bottom d-flex flex-column">
        <span className="pt-1 pb-1">{comment}</span>
      </Card.Body>
      <Card.Footer className="d-flex align-items-center p-2 pl-3">
        <div className='mr-1'><Image src={profile} height={30} roundedCircle /></div>
        <div>
          {isReceived ?
            <Translate className="pl-1" value="better_me.received_from" />
            :
            <Translate className="pl-1" value="better_me.sent_to" />
          }
          <span className="font-weight-bold font-size-16 pl-1">{name}</span>
        </div>
      </Card.Footer>
    </Card>
  )
}

export default ListCard;