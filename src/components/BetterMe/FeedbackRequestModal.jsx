import React,{useState} from 'react';

import { Modal, Button, ListGroup } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMinusCircle, faPlusCircle, faMedal, faGraduationCap, faStar } from '@fortawesome/free-solid-svg-icons';

import './BetterMeModal.css';

import { Translate, I18n } from 'react-redux-i18n';

const FeedbackRequestModal = (props) => {
  const { totalFeedbacks, userFeedbackObj } = props;
  const [feedbacks,setFeedbacks] = useState([])

  let userFeedbacks = null;
  let userFeedbackArray = []
  let notUserFeedbackArray = [];
  let notUserFeedbacks = null;

  const feedbackStars = [
    {
      type: faStar,
      title: 'You Rock',
      backgroundColor: '#53C7E6',
      value: 'star'
    },
    {
      type: faGraduationCap,
      title: 'Let\'s work on this',
      backgroundColor: '#EEDB1E',
      value: 'cap',
    },
    {
      type: faMedal,
      title: 'Cool',
      backgroundColor: '#F3ACD1',
      value: 'medal',
    },
  ];

  const handleFeedbackSelect = (feedback, star) => {
    setFeedbacks([...feedbacks, {Competencies: feedback.name, feedback: star.value}])
  }

  const handleFeedbackDeselect = (feedback) => {
    setFeedbacks(feedbacks.filter((value)=> value.Competencies == feedback.name))
  }

  const handleSave = () => {
    props.handleSubmit(userFeedbackObj.postId,feedbacks)
    props.onHide()
  }

  if (userFeedbackObj.feedbacks && userFeedbackObj.feedbacks.length > 0) {

    notUserFeedbackArray = totalFeedbacks.filter((item) => {
      return userFeedbackObj.feedbacks.findIndex(x => x === item.name) === -1;
    });

    notUserFeedbacks = notUserFeedbackArray.map((feedback, index) => {
      return (
        <ListGroup.Item className='user-feedback-list' key={`not-feedback-${index}`}>
          <div className='feedback-name'>{feedback.name}</div>
          <div className="d-flex">
            <div className={feedback.icon.length > 0 ? "d-flex" : "d-none"}>
              {feedback.icon.includes('star') &&
                <div className='feedback-icons' style={{ backgroundColor: '#53C7E6' }}>
                  <FontAwesomeIcon icon={faStar} className="default-icons cursor-pointer" />
                </div>
              }
              {feedback.icon.includes('cap') &&
                <div className='feedback-icons' style={{ backgroundColor: '#EEDB1E' }}>
                  <FontAwesomeIcon icon={faGraduationCap} className="cursor-pointer font-size-20" />
                </div>
              }
              {feedback.icon.includes('medal') &&
                <div className='feedback-icons' style={{ backgroundColor: '#F3ACD1' }}>
                  <FontAwesomeIcon icon={faMedal} className="cursor-pointer font-size-20" />
                </div>
              }
            </div>
            <div className="d-flex justify-content-around pr-2 align-items-center">
              {feedback.icon.length > 0 ?
                <React.Fragment>
                  <span className="pr-2 font-weight-bold">{feedback.icon[0].title}</span>
                  <span
                    className="feedback-icons shadow position-relative"
                    style={{ backgroundColor: feedback.icon[0].backgroundColor }}
                  >
                    <FontAwesomeIcon icon={feedback.icon[0].type} className="font-size-20 cursor-pointer" />
                    <span
                      className="position-absolute remove-button cursor-pointer"
                      onClick={() => {
                        feedback.icon=[]
                        handleFeedbackDeselect(feedback)
                      }}
                    >&times;</span>
                  </span>
                </React.Fragment>
                : 
                feedbackStars.map((star, index) => {
                  return (
                    <div
                      key={`star-${feedback.id}-${index}`}
                      className="feedback-icons shadow mr-2"
                      style={{ backgroundColor: star.backgroundColor }}
                      onClick={() => {
                        feedback.icon=[star]
                        handleFeedbackSelect(feedback, star)
                      }}
                    >
                      <FontAwesomeIcon icon={star.type} className="font-size-20 cursor-pointer" />
                    </div>
                  );
                })
              }
            </div>
          </div>
        </ListGroup.Item>
      );
    });

    userFeedbackArray = totalFeedbacks.filter((item) => {
      return userFeedbackObj.feedbacks.findIndex(x => x === item.name) !== -1;
    });

    userFeedbacks = userFeedbackArray.map((feedback, index) => {
      let feedbackColor = { display: 'none' };
      let feedbacktext = '';
      if (feedback.icon.length > 0 && feedback.icon.length < 3) {
        if (feedback.icon[0] === 'star') {
          feedbackColor = { color: '#53C7E6', fontSize: '12px' };
          feedbacktext = 'You Rock';
        } else if (feedback.icon[0] === 'cap') {
          feedbackColor = { color: '#EEDB1E', fontSize: '12px' };
          feedbacktext = 'let\'s work on this';
        } else {
          feedbackColor = { color: '#F3ACD1', fontSize: '12px' };
          feedbacktext = 'Cool';
        }
      }

      return (
        <ListGroup.Item className='user-feedback-list' key={`feedback-${index}`}>
          <div>
            <div className='feedback-name'>{feedback.name}</div>
            <div style={feedbackColor}>{feedbacktext}</div>
          </div>
          <div className="d-flex">
            <div className={feedback.icon.length > 0 ? "d-flex" : "d-none"}>
              {
                feedback.icon.includes('star') &&
                <div className='feedback-icons' style={{ backgroundColor: '#53C7E6' }}>
                  <FontAwesomeIcon className="cursor-pointer default-icons" icon={faStar} />
                </div>
              }
              {
                feedback.icon.includes('cap') &&
                <div className='feedback-icons' style={{ backgroundColor: '#EEDB1E' }}>
                  <FontAwesomeIcon className="font-size-20 cursor-pointer" icon={faGraduationCap} />
                </div>
              }
              {
                feedback.icon.includes('medal') &&
                <div className='feedback-icons' style={{ backgroundColor: '#F3ACD1' }}>
                  <FontAwesomeIcon className="font-size-20 cursor-pointer" icon={faMedal} />
                </div>
              }
            </div>
            <div className="d-flex justify-content-around pr-2 align-items-center">
              {feedback.icon.length > 0 ?
                <React.Fragment>
                  <span className="pr-2 font-weight-bold">{feedback.icon[0].title}</span>
                  <span
                    className="feedback-icons shadow position-relative"
                    style={{ backgroundColor: feedback.icon[0].backgroundColor }}
                  >
                    <FontAwesomeIcon icon={feedback.icon[0].type} className="font-size-20 cursor-pointer" />
                    <span
                      className="position-absolute remove-button cursor-pointer"
                      onClick={() => {
                        feedback.icon=[]
                        handleFeedbackDeselect(feedback)
                      }}
                    >&times;</span>
                  </span>
                </React.Fragment>
                : 
                feedbackStars.map((star, index) => {
                  return (
                    <div
                      key={`star-${feedback.id}-${index}`}
                      className="feedback-icons shadow mr-2"
                      style={{ backgroundColor: star.backgroundColor }}
                      onClick={() => {
                        feedback.icon=[star]
                        handleFeedbackSelect(feedback, star)
                      }}
                    >
                      <FontAwesomeIcon icon={star.type} className="font-size-20 cursor-pointer" />
                    </div>
                  );
                })
              }
            </div>
          </div>
        </ListGroup.Item>
      );
    });
  }

  return (
    <Modal
      show={props.show}
      onHide={props.onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          <Translate className="text-uppercase" value="better_me.feedback_request_of" /> <strong style={{ color: '#007bff' }}>{userFeedbackObj.userName}</strong>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h4>{I18n.t('better_me.requested_compitencies')}</h4>
        <ListGroup>
          {userFeedbacks}
        </ListGroup>
        <h4>{I18n.t('better_me.more_competencies_to_give_feedback')}</h4>
        <ListGroup>
          {notUserFeedbacks}
        </ListGroup>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={handleSave}>
          {I18n.t('better_me.save')}
        </Button>
        <Button onClick={props.onHide}>
          {I18n.t('better_me.close')}
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default FeedbackRequestModal;
