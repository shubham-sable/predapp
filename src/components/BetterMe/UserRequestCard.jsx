import React from 'react';

import { Card, Image, ListGroup } from 'react-bootstrap';
import { Translate } from 'react-redux-i18n';

const UserRequestCard = ({ data, getFeedbackReqested }) => {
  return (
    <Card className="shadow-sm border-0">
      <Card.Header className="border-0 shadow-sm d-flex">
        <div className="mr-2">
          <Image src={data.sender.profile} height={32} width={32} roundedCircle />
        </div>
        <div className="align-self-center font-weight-bold font-size-14">{data.sender.name}</div>
      </Card.Header>
      <Card.Body>
        <div>
          <ListGroup>
            {
              data.competencies.map((feedback, index) => {
                return (
                  <ListGroup.Item className="feeback-list" variant='secondary' key={`feedback-${index}`}>{feedback}</ListGroup.Item>
                );
              })
            }
          </ListGroup>
        </div>
      </Card.Body>
      <Card.Footer
        className="border-0 shadow-sm pt-3 pb-3 answer_request cursor-pointer align-items-center user-select-none"
        onClick={() => getFeedbackReqested(data.competencies, data.sender.id, data.id, data.sender.name)}
      >
        <Translate className="text-uppercase" value="better_me.answer_request" />
      </Card.Footer>
    </Card>
  )
}

export default UserRequestCard;
