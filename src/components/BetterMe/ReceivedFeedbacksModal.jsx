import React from 'react';
import { Modal, Row, Button } from 'react-bootstrap';
import styled from "styled-components";

import { I18n } from 'react-redux-i18n';

import './BetterMeModal.css';

const CompetencyText = styled.div`
  padding: 0.5rem .75rem;
  border: 1px solid rgba(0, 0, 0, 0.125);
  border-radius: 25px;
`;

const ReceivedFeedbacksModal = (props) => {
  const { show, onHide, receivedfeedback } = props;

  let starFeedbacks = [];
  let capFeedbacks = [];
  let medalFeedbacks = [];

  starFeedbacks = receivedfeedback.feedbacks ? receivedfeedback.feedbacks.filter(x => x.feedback.indexOf('star') !== -1) : [];
  capFeedbacks = receivedfeedback.feedbacks ? receivedfeedback.feedbacks.filter(x => x.feedback.indexOf('cap') !== -1) : [];
  medalFeedbacks = receivedfeedback.feedbacks ? receivedfeedback.feedbacks.filter(x => x.feedback.indexOf('medal') !== -1) : [];

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header>
        <Modal.Title className="text-uppercase" id="contained-modal-title-vcenter">
          {I18n.t('better_me.received_feedbacks')}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {
          starFeedbacks.length > 0 &&
          <div className="mb-4">
            <div className="feedback-row" style={{ borderLeft: '10px solid #53C7E6', color: '#53C7E6', padding: '5px', fontWeight: '900', fontSize: 'large' }}>
              You Rock
            </div>
            <Row className="feedback-row col-md-12 p-0">
              {starFeedbacks.map((feedback) => {
                return (
                  <CompetencyText className="m-1 shadow-sm" key={feedback.Competencies}>
                    {feedback.Competencies}
                  </CompetencyText>
                );
              })}
            </Row>
          </div>
        }
        {
          capFeedbacks.length > 0 &&
          <div className="mb-4">
            <div className="feedback-row" style={{ borderLeft: '10px solid #EEDB1E', color: '#EEDB1E', padding: '5px', fontWeight: '900', fontSize: 'large' }}>
              Let's work on this
            </div>
            <Row className="feedback-row col-md-12 p-0">
              {capFeedbacks.map((feedback) => {
                return (
                  <CompetencyText className="m-1 shadow-sm" key={feedback.Competencies}>
                    {feedback.Competencies}
                  </CompetencyText>
                );
              })}
            </Row>
          </div>
        }
        {
          medalFeedbacks.length > 0 &&
          <div className="mb-4">
            <div className="feedback-row" style={{ borderLeft: '10px solid #F3ACD1', color: '#F3ACD1', padding: '5px', fontWeight: '900', fontSize: 'large' }}>
              Cool
            </div>
            <Row className="feedback-row col-md-12 p-0">
              {medalFeedbacks.map((feedback) => {
                return (
                  <CompetencyText className="m-1 shadow-sm" key={feedback.Competencies}>
                    {feedback.Competencies}
                  </CompetencyText>
                );
              })}
            </Row>
          </div>
        }
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>
          {I18n.t('better_me.close')}
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default ReceivedFeedbacksModal;
