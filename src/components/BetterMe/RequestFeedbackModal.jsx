import React, { useState } from 'react';

import { Modal, Button, Col, FormLabel, Image, Row } from 'react-bootstrap';
import Select, { components } from 'react-select';
import { Formik, Form, Field, ErrorMessage } from 'formik';

import { I18n } from 'react-redux-i18n';

const RequestFeedbackModal = (props) => {
  const { show, employees, totalFeedbacks, onHide } = props;
  const [submitted, setSubmitted] = useState(false);
  const [employee, setEmployee] = useState('');
  let employeesOption = [];
  let feedbackOption = [];

  employees.forEach((employee) => {
    employeesOption.push({
      value: employee.id,
      label: employee.name,
      imageUrl: '/assets/imgs/bradpitt.jpeg',
      position: employee.position
    });
  });

  totalFeedbacks.forEach((feedback) => {
    feedbackOption.push({ id: feedback.id, value: feedback.id, label: feedback.name, icon: '' });
  });

  const handleFormSubmit = (values) => {
    console.log('values', values);
    setEmployee(values.employee.label);
    setSubmitted(true);
    props.handleSubmit(values)
  };

  if (submitted) {
    setTimeout(() => {
      onHide();
    }, 3000);
  }

  const singleOptionType = (props) => (
    <components.Option {...props} className="d-flex">
      <Image src={props.data.imageUrl} height={35} />
      <div className="pl-3 font-size-14">
        <div className="font-weight-bold pb-2">{props.data.label}</div>
        <div>{props.data.position}</div>
      </div>
    </components.Option>
  );

  const singleValueType = (singleValueTypeProps) => (
    <components.SingleValue {...singleValueTypeProps} className="d-flex align-items-center">
      <Image src={singleValueTypeProps.data.imageUrl} height={20} />
      <div className="pl-3 font-weight-bold font-size-14">{singleValueTypeProps.data.label}</div>
    </components.SingleValue>
  );

  const singleFeedbackOptionType = (singlefeedbackOptionTypeProps) => (
    <components.Option
      {...singlefeedbackOptionTypeProps}
      key={`feedback-${singlefeedbackOptionTypeProps.data.value}`}
      className="d-flex flex-row justify-content-between"
    >
      <div className="feedback-name">{singlefeedbackOptionTypeProps.data.label}</div>
    </components.Option>
  );

  const singleFeedbackValueType = (singleFeedbackValueTypeProps) => (
    <components.SingleValue
      {...singleFeedbackValueTypeProps}
      key={`feedback-${singleFeedbackValueTypeProps.data.value}`}
      className="d-flex justify-content-between"
    >
      <div className="feedback-name">{singleFeedbackValueTypeProps.data.label}</div>
    </components.SingleValue>
  );

  return (
    <Modal
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      show={show}
      className={`quiz-modal create-take-part`}
    >
      <Modal.Header className="text-center" closeButton>
        <Modal.Title
          id="contained-modal-title-vcenter"
          className="w-100 font-size-20 modal-title font-weight-bold text-uppercase"
        >
          {I18n.t('better_me.request_feedbacks')}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="pt-4 pb-4 pl-3 pr-3" style={{ maxHeight: '85vh', height: 'auto' }}>
        {!submitted ? (
          <Formik
            enableReinitialize
            initialValues={{
              employee: '',
              feedbacks: []
            }}
            onSubmit={(values) => handleFormSubmit(values)}
          >
            {(renderProps) => {
              return (
                <Form className="take-part-form" onSubmit={renderProps.handleSubmit}>
                  <Row className="mb-4">
                    <Col sm={12}>
                      <FormLabel className="font-size-15 font-weight-bold">
                        {I18n.t('better_me.select_employee')}
                      </FormLabel>
                      <Select
                        isMulti={false}
                        closeMenuOnSelect={true}
                        value={renderProps.values.employee}
                        onChange={(selectedOption) => {
                          renderProps.setFieldValue('employee', selectedOption);
                        }}
                        options={employeesOption}
                        menuContainerStyle={{ zIndex: 999 }}
                        styles={{
                          menuPortal: (base) => {
                            const { zIndex, ...rest } = base;
                            return { ...rest, zIndex: 9999 };
                          }
                        }}
                        menuPortalTarget={document.body}
                        isSearchable={false}
                        name="employee"
                        components={{
                          Option: singleOptionType,
                          SingleValue: singleValueType
                        }}
                      />
                      <ErrorMessage
                        className="help-block text-danger"
                        name="employee"
                        component="div"
                      />
                    </Col>
                  </Row>
                  <Row className="mb-4">
                    <Col sm={12}>
                      <FormLabel className="font-size-15 font-weight-bold d-flex justify-content-between">
                        {I18n.t('better_me.feedbacks')}
                      </FormLabel>
                      <Select
                        isMulti={true}
                        closeMenuOnSelect={false}
                        value={renderProps.values.feedbacks}
                        onChange={(selectedOption) => {
                          renderProps.setFieldValue(
                            'feedbacks',
                            selectedOption ? selectedOption : []
                          );
                        }}
                        options={feedbackOption}
                        menuContainerStyle={{ zIndex: 999 }}
                        styles={{
                          menuPortal: (base) => {
                            const { zIndex, ...rest } = base;
                            return { ...rest, zIndex: 9999 };
                          }
                        }}
                        menuPortalTarget={document.body}
                        isSearchable={true}
                        name="feedbacks"
                        components={{
                          Option: singleFeedbackOptionType,
                          SingleValue: singleFeedbackValueType
                        }}
                      />
                      <ErrorMessage
                        className="help-block text-danger"
                        name="members"
                        component="div"
                      />
                    </Col>
                  </Row>
                  <Row className="mb-4">
                    <Col sm={12}>
                      <FormLabel className="font-size-15 font-weight-bold">
                        {I18n.t('better_me.comment')}
                      </FormLabel>
                      <Field
                        type="text"
                        className="form-control shadow-none"
                        name="comment"
                        placeholder="Add comment for selected competencies"
                        component="textarea"
                        rows="3"
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={12}>
                      <Button
                        className="font-weight-bold"
                        type="submit"
                        variant="primary"
                        size="lg"
                        block
                      >
                        {I18n.t('better_me.request_feedback')}
                      </Button>
                    </Col>
                  </Row>
                </Form>
              );
            }}
          </Formik>
        ) : (
            <Modal.Body className="p-0" style={{ maxHeight: '85vh', height: 'auto' }}>
              <div
                className="d-flex flex-column align-items-center justify-content-center"
                style={{ height: 300 }}
              >
                <div className="text-center pb-3 font-size-25">
                  {I18n.t('better_me.feedback_requested_from')}
                </div>
                <Image
                  src="/assets/imgs/thanks.jpeg"
                  roundedCircle
                  style={{ height: 150, width: 150, objectFit: 'cover' }}
                />
                <div className="text-center pt-3 font-size-32 font-weight-bold">
                  {employee}
                </div>
              </div>
            </Modal.Body>
          )}
      </Modal.Body> 
    </Modal>
  );
};

export default RequestFeedbackModal;
