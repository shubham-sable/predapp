import React from 'react';
import { connect } from 'react-redux';

import {
  Layout,
  Menu,
  Row,
  Col,
  Typography,
  Input,
 } from 'antd';

import { Translate } from 'react-redux-i18n';
import { AppstoreOutlined } from '@ant-design/icons';

import {
  NotificationIcon,
  ChatsIcon,
  DashboardIcon,
  LanguageIcon,
} from './../../svgs';

const { Item } = Menu;

// <Translate value="header.apps" />
// <Translate value="header.notification" />
// <Translate value="header.chat" />
// <Translate value="header.dashboard" />
// <Translate value="header.language" />

const HeaderBar = (props) =>{
  return(
    <Layout.Header className="site-layout-background" style={{ position: 'fixed', zIndex: 1, padding: 0, width: '100%' }}>
      <Row >
        <Col span={3} className="mt-3 ml-2 pt-1">
          <Typography.Title level={3}><Translate value={props.title} /></Typography.Title>
        </Col>
        <Col span={6} className="mt-3 pt-1">
          <Input.Search placeholder="Search.." allowClear enterButton />
        </Col>
        <Col offset={6}>
          <Menu mode="horizontal">
            <Item
              key="apps"
              icon={<AppstoreOutlined style={{fontSize:'30px'}} />}
            >
            </Item>
            <Item
              key="notification"
              icon={<NotificationIcon height={30} width={30} fill="#051c2c" />}
            >
            </Item>
            <Item
              key="chats"
              icon={<ChatsIcon height={30} width={30} fill="#051c2c" />}
            >
            </Item>
            <Item
              key="dashboard"
              icon={<DashboardIcon height={30} width={30} fill="#051c2c" />}
            >
            </Item>
            <Item
              key="logout"
              icon={<img src="/minion.jpeg" className="rounded-circle" alt="logout" height={35} />}
            >
            </Item>
            <Item
              key="language"
              icon={<LanguageIcon height={30} width={30} fill="#051c2c" />}
            >
            </Item>
          </Menu>
        </Col>
      </Row>
    </Layout.Header>
  );
}

const mapStateToProps = (state) => {
  return {
    ...state.common.titleSetting,
  };
}

export default connect(mapStateToProps)(HeaderBar);
