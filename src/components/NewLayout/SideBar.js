import React, { useState } from 'react';
import { connect } from 'react-redux';

import { Translate } from 'react-redux-i18n';
import { Link } from 'react-router-dom';
import {
  Layout,
  Menu,
  Divider,
} from 'antd';

import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
} from '@ant-design/icons';

import {
  NewsFeedIcon,
  EventIcon,
  GroupIcon,
  StarMeUpIcon,
  TakePartIcon,
  BetterMeIcon,
  PeopleDirectoryIcon,
  FollowCoworkersIcon,
  AddCoworkerIcon,
  JobsIcon,
} from './../../svgs';


const SideBar = (props) =>{

  const [isCollapsed, setIsCollapsed] = useState(true);

  const menuItems = [
    { path: '/', key: 'sidebar.news_feed', icon: NewsFeedIcon, notification: true },
    { path: '/events', key: 'sidebar.events', icon: EventIcon, notification: true },
    { path: '/groups', key: 'sidebar.groups', icon: GroupIcon, notification: false },
    { path: '/jobs', key: 'sidebar.jobs', icon: JobsIcon, notification: false },
    { path: '/star-me-up', key: 'sidebar.star_me_up', icon: StarMeUpIcon, notification: true },
    { path: '/take-part', key: 'sidebar.take_part', icon: TakePartIcon, notification: true },
    { path: '/better-me', key: 'sidebar.better_me', icon: BetterMeIcon, notification: true },
    { path: '/people-directory', key: 'sidebar.people_directory', icon: PeopleDirectoryIcon, notification: false },
    { path: '/follow-coworkers', key: 'sidebar.follow_coworkers', label: 'Follow Coworkers', icon: FollowCoworkersIcon, notification: false },
    // { path: '/', key: 'saved', label: 'Saved', imgUrl: 'subMenuListItem.png', bgPosition: '-45px -535px', isImage: false },
    // { path: '/', key: 'notes', label: 'Notes', imgUrl: 'subMenuListItem.png', bgPosition: '0 -712px', isImage: false },
    // { path: '/', key: 'org_chart', label: 'Org Chart', imgUrl: 'subMenuListItem.png', bgPosition: '-33px -712px', isImage: false },
    // { path: '/', key: 'interations', label: 'Interations', imgUrl: 'helperIcons.png', bgPosition: '0 -292px', isImage: false },
  ];



  return(
    <Layout.Sider width={250} position='fixed' trigger={null} collapsible collapsed={isCollapsed}>
      <Menu theme="dark" mode="inline" defaultSelectedKeys={props.title}>
        {isCollapsed ?
          <MenuUnfoldOutlined className="trigger mt-2 pl-4 pt-1 mb-3" onClick={() => setIsCollapsed(!isCollapsed)} style={{fontSize:'35px'}}/>
          :
          <MenuFoldOutlined className="trigger mt-2 pl-4 pt-1 mb-3" onClick={() => setIsCollapsed(!isCollapsed)} style={{fontSize:'35px'}}/>
        }
        {menuItems.map(({path, key, icon: Icon}) =>{
          {console.log(key===props.title)}
          return (
            <Menu.Item
              className="pl-4"
              style={{height:"50px"}}
              key={key}
              icon={<Icon width="35" height="35" />}
            >
              <Link to={path}><Translate value={key} /></Link>
            </Menu.Item>
          );
        })}
          <Menu.Item className="pl-4" style={{height:"50px"}} key={'invite_colleague'} icon={<AddCoworkerIcon width="40" height="40" />}>
            <Translate value={'sidebar.follow_coworker'} />
          </Menu.Item>
      </Menu>
    </Layout.Sider>
  );
}

const mapStateToProps = (state) => {
  return {
    ...state.common.titleSetting,
  };
}

export default connect(mapStateToProps)(SideBar);
