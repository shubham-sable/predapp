importScripts("https://www.gstatic.com/firebasejs/7.14.5/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/7.14.5/firebase-messaging.js");

let config = {
  apiKey: "AIzaSyDgGIzSGLAigFERmSaSIdNrdpH0BpZnUlw",
  projectId: 'prenigma-social',
  appId: "1:207847553885:web:dd300eac6b1530ffe286b4",
  messagingSenderId: "207847553885",
};

firebase.initializeApp(config);

const messaging = firebase.messaging();

if ('serviceWorker' in navigator) {
  window.addEventListener('load', async () => {
    // const registration = await navigator.serviceWorker.register('./firebase-messaging-sw.js', {
    //   updateViaCache: 'none'
    // });
    // console.log('registration', registration);
    // messaging.useServiceWorker(registration);

    messaging.onMessage((payload) => {
      console.log('payload', payload);
      const title = payload.notification.title;
      const options = {
        body: payload.notification.body,
        icon: payload.notification.icon,
        actions: [
          {
            action: payload.fcmOptions.link,
            title: 'Book Appointment'
          }
        ]
      };
      registration.showNotification(title, options);
    });
  });
}

messaging.setBackgroundMessageHandler(payload => {
  console.log('payload', payload);
  const notificationTitle = payload.data.title;
  const notificationOptions = {
    body: payload.data.body,
    icon: payload.data.icon
  };
  return self.registration.showNotification(notificationTitle, notificationOptions);
});

self.addEventListener('notificationclick', (event) => {
  if (event.action) {
    clients.openWindow(event.action);
  }
  event.notification.close();
});

// self.addEventListener("notificationclick", function (event) {
//   const clickedNotification = event.notification;
//   clickedNotification.close();
//   const promiseChain = clients
//     .matchAll({
//       type: "window",
//       includeUncontrolled: true
//     })
//     .then(windowClients => {
//       let matchingClient = null;
//       for (let i = 0; i < windowClients.length; i++) {
//         const windowClient = windowClients[i];
//         if (windowClient.url === feClickAction) {
//           matchingClient = windowClient;
//           break;
//         }
//       }
//       if (matchingClient) {
//         return matchingClient.focus();
//       } else {
//         return clients.openWindow(feClickAction);
//       }
//     });
//   event.waitUntil(promiseChain);
// });